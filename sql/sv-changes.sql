-- --------------------------------------------------------
--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) NOT NULL,
  `reg_code` varchar(255) NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `emerald_users`
--

CREATE TABLE IF NOT EXISTS `emerald_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `AccountID` int(11) NOT NULL,
  `Domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ServiceType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExternalRef` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Forward` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Active` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `emerald_users_history`
--

CREATE TABLE IF NOT EXISTS `emerald_users_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `AccountID` int(11) NOT NULL,
  `Domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ServiceType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExternalRef` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Forward` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Active` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ChDomain` int(11) NOT NULL,
  `ChLogin` int(11) NOT NULL,
  `ChPassword` int(11) NOT NULL,
  `ChFirstName` int(11) NOT NULL,
  `ChLastName` int(11) NOT NULL,
  `ChServiceType` int(11) NOT NULL,
  `ChExternalRef` int(11) NOT NULL,
  `ChAlias` int(11) NOT NULL,
  `ChForward` int(11) NOT NULL,
  `ChActive` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- Table structure for table `epg`
--

CREATE TABLE IF NOT EXISTS `epg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `channelId` int(11) NOT NULL,
  `channelNumber` int(11) NOT NULL,
  `majorChannelNumber` int(11) NOT NULL,
  `minorChannelNumber` int(11) NOT NULL,
  `callsign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `broadcastType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

-- --------------------------------------------------------

--
-- Table structure for table `epg_history`
--

CREATE TABLE IF NOT EXISTS `epg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `channelId` int(11) NOT NULL,
  `channelNumber` int(11) NOT NULL,
  `majorChannelNumber` int(11) NOT NULL,
  `minorChannelNumber` int(11) NOT NULL,
  `callsign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `broadcastType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=208 ;