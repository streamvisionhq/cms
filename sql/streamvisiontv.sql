/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : streamvisiontv

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2016-03-24 16:18:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_client
-- ----------------------------
DROP TABLE IF EXISTS `api_client`;
CREATE TABLE `api_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `domain_url` varchar(255) DEFAULT NULL,
  `domain_ip` varchar(45) DEFAULT NULL,
  `consumer_key` varchar(100) DEFAULT NULL,
  `consumer_secret` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  KEY `api_fname_inx` (`first_name`),
  KEY `api_lname_inx` (`last_name`),
  KEY `api_email_inx` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of api_client
-- ----------------------------
INSERT INTO `api_client` VALUES ('40', 'Sethu', 'Ramalingam', 'sethuphp@gmail.com', 'http://test.com', '23.91.14.101', '55156b6ae5b4972838', '7555156b6ae5bac', '1', '2015-03-27 14:38:34', null, null, null);
INSERT INTO `api_client` VALUES ('41', 'Sethu', 'Ramalingam', 'kumar@claysol.com', 'http://test.com', '23.91.14.101', '5534cad800afe62053', '575534cad800b61', '1', '2015-04-20 09:46:00', null, null, null);
INSERT INTO `api_client` VALUES ('35', 'ponsugumar', 'lingam', 'ponsukumar@colanonline.com', 'http://www.fakemailgenerator.com/#/jourrapide.com/rsauty1933/', '23.239.29.22', '550abea18a1b724588', '39550abea18a20f', '1', '2015-03-19 12:08:43', '2015-03-19 12:18:41', '35', '35');
INSERT INTO `api_client` VALUES ('36', 'ponsukumar', 'lingam', 'sudhan@colanonline.com', 'http://transload.cipldev.com/transload/settings/fields', '74.220.219.129', '550ac3e4ce4ef54598', '25550ac3e4ce547', '1', '2015-03-19 12:25:54', '2015-03-19 12:41:08', '36', '36');
INSERT INTO `api_client` VALUES ('37', 'Brock', 'lesner', 'anoop@colanonline.com', 'https://trello.com/b/I0d4GHmz/fusion-app', '192.230.65.4', '550ac2de924ad51233', '13550ac2de92509', '1', '2015-03-19 12:36:46', null, null, null);

-- ----------------------------
-- Table structure for channels
-- ----------------------------
DROP TABLE IF EXISTS `channels`;
CREATE TABLE `channels` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `short_description` text,
  `long_description` text,
  `channel_type` int(11) DEFAULT NULL,
  `livestream_url` varchar(255) DEFAULT NULL,
  `server_url` varchar(255) DEFAULT NULL,
  `channel_logo` varchar(255) DEFAULT NULL,
  `pre_roll` tinyint(4) DEFAULT NULL,
  `channel_created` datetime DEFAULT NULL,
  `channel_modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `channel_created_gmt` datetime DEFAULT NULL,
  `channel_modified_gmt` datetime DEFAULT NULL,
  `titan_channel_id` varchar(255) NOT NULL,
  `emerald_subscription_type` varchar(255) NOT NULL,
  PRIMARY KEY (`channel_id`),
  FULLTEXT KEY `channeltitle_inx` (`title`),
  FULLTEXT KEY `channelsdesc_inx` (`short_description`),
  FULLTEXT KEY `channelldesc_inx` (`long_description`)
) ENGINE=MyISAM AUTO_INCREMENT=194 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channels
-- ----------------------------
INSERT INTO `channels` VALUES ('188', 'KASW-61.2 (Decades)', '0', 'KASW-61.2 (Decades)', 'KASW-61.2 (Decades)', '1', 'http://199.66.168.201:80/hls/442A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream9-config&output=dvb_stream9-config-442KASW', 'Decades_1453076090.jpg', '1', '2016-01-18 00:14:50', '2016-01-28 17:56:46', '1', '1', '40', '1', '2016-01-18 00:14:50', '2016-01-28 17:56:46', '2010677', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('98', 'KPAZ-21.2 CHURCH', '0', 'PBS 8.1', 'The Public Broadcasting Service is an American public broadcaster and television program distributor.', '1', 'http://199.66.168.238:8081/KPAZ/CHURCH21.2/playlist.m3u8', 'http://199.66.168.238:8081/KPAZ/CHURCH21.2/playlist.m3u8', 'church-channel_1453069522.jpg', '1', '2015-09-30 15:30:27', '2016-01-28 17:56:46', '1', '1', '17', '1', '2015-09-30 15:30:27', '2016-01-28 17:56:46', '14065', 'StreamVisionBronze');
INSERT INTO `channels` VALUES ('147', 'KSAZ-DT1 (10.1) FOX', '0', 'FOX-T-SF-N', 'FOX-T-SF-N', '1', 'http://199.66.169.193:8081/hls/fox/playlist.m3u8', 'http://199.66.169.193:8081/hls/fox/playlist.m3u8', 'Fox3_1453069148.jpg', '1', '2015-12-08 22:56:49', '2016-01-28 17:56:46', '1', '1', '7', '1', '2015-12-08 22:56:49', '2016-01-28 17:56:46', '13870', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('189', 'KUTP-45.1 (My45)', '0', 'KUTP-45.1 (My45)', 'KUTP-45.1 (My45)', '1', 'http://199.66.168.201:80/hls/451A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream8-config&output=dvb_stream8-config-451KUTP', 'My45_1453081320.jpg', '1', '2016-01-18 01:42:00', '2016-01-28 17:56:46', '1', null, '31', '1', '2016-01-18 01:42:00', '2016-01-28 17:56:46', '13455', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('190', 'KUTP-45.2 (Movies)', '0', 'KUTP-45.2 (Movies)', 'KUTP-45.2 (Movies)', '1', 'http://199.66.168.201:80/hls/452A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream8-config&output=dvb_stream8-config-452KUTP', 'FoxMovies_1453081700.jpg', '1', '2016-01-18 01:48:20', '2016-01-28 17:56:46', '1', null, '32', '1', '2016-01-18 01:48:20', '2016-01-28 17:56:46', '13456', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('191', 'KUTP-45.3 (BUZZR)', '0', 'KUTP-45.3 (BUZZR)', 'KUTP-45.3 (BUZZR)', '1', 'http://199.66.168.201:80/hls/453A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream8-config&output=dvb_stream8-config-453KUTP', 'BUZZR_1453082089.jpg', '1', '2016-01-18 01:54:49', '2016-01-28 17:56:46', '1', null, '33', '1', '2016-01-18 01:54:49', '2016-01-28 17:56:46', '2004012', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('192', 'KUTP-45.4 (Movies)', '0', 'KUTP-45.4 (Movies)', 'KUTP-45.4 (Movies)', '1', 'http://199.66.168.201:80/hls/454A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream8-config&output=dvb_stream8-config-454KUTP', 'FoxMovies_1453082508.jpg', '1', '2016-01-18 02:01:48', '2016-01-28 17:56:46', '1', null, '34', '1', '2016-01-18 02:01:48', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('193', 'KPNX-12.1-720 TEST', '0', 'KPNX-12.1-720 TEST', 'KPNX-12.1-720 TEST', '1', 'http://199.66.169.193:8081/hls/test_02/playlist.m3u8', 'http://199.66.169.193:8081/hls/test_02/playlist.m3u8', 'NBC-Test_1453421115.jpg', '1', '2016-01-22 00:05:15', '2016-01-28 17:56:46', '1', '1', '10', '1', '2016-01-22 00:05:15', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('187', 'KASW-61.1 (CW6)', '0', 'KASW-61.1 (CW6)', 'KASW-61.1 (CW6)', '1', 'http://199.66.168.201:80/hls/441A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream9-config&output=dvb_stream9-config-441KASW', 'CW6_1453075697.jpg', '1', '2016-01-18 00:08:17', '2016-01-28 17:56:46', '1', '1', '39', '1', '2016-01-18 00:08:17', '2016-01-28 17:56:46', '14789', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('144', 'KPNX-12.1', '0', 'NBC-T-SF-NGX', 'NBC-T-SF-NGX', '1', 'http://199.66.169.193:8081/hls/nbc/playlist.m3u8', 'http://199.66.169.193:8081/hls/nbc/playlist.m3u8', 'nbc_1453069174.jpg', '1', '2015-12-02 21:30:39', '2016-01-28 17:56:46', '1', '1', '9', '1', '2015-12-02 21:30:39', '2016-01-28 17:56:46', '14093', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('152', 'KAET-PBS-8.1', '0', 'PBS-T-SF-N', 'PBS-T-SF-N', '1', 'http://199.66.169.193:8081/hls/pbs/playlist.m3u8', 'http://199.66.169.193:8081/hls/pbs/playlist.m3u8', 'PBS2_1453068859.jpg', '1', '2015-12-19 23:55:54', '2016-01-28 17:56:46', '1', '1', '3', '1', '2015-12-19 23:55:54', '2016-01-28 17:56:46', '14744', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('153', 'KNXV-15.2 ATV', '0', 'KNXV-15.2 ATV', 'KNXV-15.2 ATV', '1', 'http://199.66.169.193:8081/hls/ABC15.2/playlist.m3u8', 'http://199.66.169.193:8081/hls/ABC15.2/playlist.m3u8', 'AntennaTV_1453069409.jpg', '1', '2015-12-20 00:04:47', '2016-01-28 17:56:46', '1', '1', '14', '1', '2015-12-20 00:04:47', '2016-01-28 17:56:46', '13972', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('154', 'KNXV-15.3 LAFF', '0', 'KNXV-15.3 LAFF', 'KNXV-15.3 LAFF', '1', 'http://199.66.169.193:8081/hls/LAFF15.3/playlist.m3u8', 'http://199.66.169.193:8081/hls/LAFF15.3/playlist.m3u8', 'laff_1453069473.jpg', '1', '2015-12-20 00:24:06', '2016-01-28 17:56:46', '1', '1', '15', '1', '2015-12-20 00:24:06', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('155', 'KPHO-5.1 CBS', '0', 'TBN-T-SF-N', 'TBN-T-SF-N', '1', 'http://199.66.169.193:8081/hls/CBS5.1/playlist.m3u8', 'http://199.66.169.193:8081/hls/CBS5.1/playlist.m3u8', 'cbs-logo_1453068406.jpg', '1', '2015-12-20 00:25:56', '2016-01-28 17:56:46', '1', '1', '1', '1', '2015-12-20 00:25:56', '2016-01-28 17:56:46', '14076', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('156', 'KPHO-5.2 COZI', '0', 'KPHO-5.2 COZI', 'KPHO-5.2 COZI', '1', 'http://199.66.169.193:8081/hls/COZI5.2/playlist.m3u8', 'http://199.66.169.193:8081/hls/COZI5.2/playlist.m3u8', 'cozi_1453068423.jpg', '1', '2015-12-20 00:27:50', '2016-01-28 17:56:46', '1', '1', '2', '1', '2015-12-20 00:27:50', '2016-01-28 17:56:46', '2000052', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('157', 'KPAZ-21.1 TBN', '0', 'KPAZ-21.1 TBN', 'KPAZ-21.1 TBN', '1', 'http://199.66.168.238:8081/KPAZ/TBN21.1/playlist.m3u8', 'http://199.66.168.238:8081/KPAZ/TBN21.1/playlist.m3u8', 'TBNCrestRegular_1453069505.jpg', '1', '2015-12-20 00:29:13', '2016-01-28 17:56:46', '1', '1', '16', '1', '2015-12-20 00:29:13', '2016-01-28 17:56:46', '14065', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('174', 'KPPX-51.2 QUBO', '0', 'KPPX-51.2 QUBO', 'KPPX-51.2 QUBO', '1', 'http://199.66.168.238:8081/KPPX/QUBO51.2/playlist.m3u8', 'http://199.66.168.238:8081/KPPX/QUBO51.2/playlist.m3u8', 'qubo_1453069762.jpg', '1', '2016-01-15 05:31:57', '2016-01-28 17:56:46', '1', '1', '36', '1', '2016-01-15 05:31:57', '2016-01-28 17:56:46', '13725', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('167', 'KPAZ-21.3 JUCE', '0', 'KPAZ-21.3 JUCE', 'KPAZ-21.3 JUCE', '1', 'http://199.66.168.238:8081/KPAZ/JUCE21.3/playlist.m3u8', 'http://199.66.168.238:8081/KPAZ/JUCE21.3/playlist.m3u8', 'juce_1453069543.jpg', '1', '2016-01-15 04:13:17', '2016-01-28 17:56:46', '1', '1', '18', '1', '2016-01-15 04:13:17', '2016-01-28 17:56:46', '14066', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('163', 'KAET-LIFE (8.2)', '0', 'PBS 8.2 LIFE TFNX', 'PBS 8.2 LIFE TFNX', '1', 'http://199.66.169.193:8081/hls/PBS8.2/playlist.m3u8', 'http://199.66.169.193:8081/hls/PBS8.2/playlist.m3u8', 'Life_1453068839.jpg', '1', '2016-01-13 02:02:44', '2016-01-28 17:56:46', '1', '1', '4', '1', '2016-01-13 02:02:44', '2016-01-28 17:56:46', '14745', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('164', 'KAET-WORLD 8.3', '0', 'KAET-WORLD 8.3', 'KAET-WORLD 8.3', '1', 'http://199.66.169.193:8081/hls/PBS8.3/playlist.m3u8', 'http://199.66.169.193:8081/hls/PBS8.3/playlist.m3u8', 'world_1453068919.jpg', '1', '2016-01-13 14:28:09', '2016-01-28 17:56:46', '1', '1', '5', '1', '2016-01-13 14:28:09', '2016-01-28 17:56:46', '14746', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('165', 'KAET-8.4 MUSIC', '0', 'KAET-8.4 MUSIC', 'KAET-8.4 MUSIC', '1', 'http://199.66.169.193:8081/hls/PBS8.4/playlist.m3u8', 'http://199.66.169.193:8081/hls/PBS8.4/playlist.m3u8', 'KAET_1453068966.jpg', '1', '2016-01-13 14:31:54', '2016-01-28 17:56:46', '1', '1', '6', '1', '2016-01-13 14:31:54', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('166', 'KNXV-15.1 ABC', '0', 'KNXV-15.1 ABC', 'KNXV-15.1 ABC', '1', 'http://199.66.169.193:8081/hls/ABC15.1/playlist.m3u8', 'http://199.66.169.193:8081/hls/ABC15.1/playlist.m3u8', 'ABC2_1453069331.jpg', '1', '2016-01-13 14:46:31', '2016-01-28 17:56:46', '1', '1', '13', '1', '2016-01-13 14:46:31', '2016-01-28 17:56:46', '13971', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('171', 'KPNX-12.3 JUSTICE', '0', 'KPNX-12.3 JUSTICE', 'KPNX-12.3 JUSTICE', '1', 'http://199.66.168.238:8081/KPNX/JUSTICE12.3/playlist.m3u8', 'http://199.66.168.238:8081/KPNX/JUSTICE12.3/playlist.m3u8', 'Justice_1453069252.jpg', '1', '2016-01-15 04:45:18', '2016-01-28 17:56:46', '1', '1', '12', '1', '2016-01-15 04:45:18', '2016-01-28 17:56:46', '2008441', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('173', 'KPPX-51.1 ION', '0', 'KPPX-51.1 ION', 'KPPX-51.1 ION', '1', 'http://199.66.168.238:8081/KPPX/ION51.1/playlist.m3u8', 'http://199.66.168.238:8081/KPPX/ION51.1/playlist.m3u8', 'ion_1453069744.jpg', '1', '2016-01-15 05:18:55', '2016-01-28 17:56:46', '1', '1', '35', '1', '2016-01-15 05:18:55', '2016-01-28 17:56:46', '13724', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('168', 'KPAZ-21.4 TBNENL', '0', 'KPAZ-21.4 TBNENL', 'KPAZ-21.4 TBNENL', '1', 'http://199.66.168.238:8081/KPAZ/TBNENL21.4/playlist.m3u8', 'http://199.66.168.238:8081/KPAZ/TBNENL21.4/playlist.m3u8', 'Enlace_1453069560.jpg', '1', '2016-01-15 04:21:13', '2016-01-28 17:56:46', '1', '1', '19', '1', '2016-01-15 04:21:13', '2016-01-28 17:56:46', '14067', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('169', 'KPAZ-21.5 SMILE', '0', 'KPAZ-21.5 SMILE', 'KPAZ-21.5 SMILE', '1', 'http://199.66.168.238:8081/KPAZ/SMILE21.5/playlist.m3u8', 'http://199.66.168.238:8081/KPAZ/SMILE21.5/playlist.m3u8', 'SmileOfChild_1453069578.jpg', '1', '2016-01-15 04:26:02', '2016-01-28 17:56:46', '1', '1', '20', '1', '2016-01-15 04:26:02', '2016-01-28 17:56:46', '14068', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('170', 'KPNX-12.2 WEATHER', '0', 'KPNX-12.2 WEATHER', 'KPNX-12.2 WEATHER', '1', 'http://199.66.168.238:8081/KPNX/WX12.2/playlist.m3u8', 'http://199.66.168.238:8081/KPNX/WX12.2/playlist.m3u8', '12News_1453069236.jpg', '1', '2016-01-15 04:30:29', '2016-01-28 17:56:46', '1', '1', '11', '1', '2016-01-15 04:30:29', '2016-01-28 17:56:46', '14094', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('172', 'KSAZ-10.2 FOX2', '0', 'KSAZ-10.2 FOX2', 'KSAZ-10.2 FOX2', '1', 'http://199.66.168.238:8081/KSAZ/FOX10.2/playlist.m3u8', 'http://199.66.168.238:8081/KSAZ/FOX10.2/playlist.m3u8', 'Fox2_1453069025.jpg', '1', '2016-01-15 04:52:57', '2016-01-28 17:56:46', '1', '1', '8', '1', '2016-01-15 04:52:57', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('175', 'KPPX-51.3 ION LIFE', '0', 'KPPX-51.3 ION LIFE', 'KPPX-51.3 ION LIFE', '1', 'http://199.66.168.238:8081/KPPX/IONLIFE51.3/playlist.m3u8', 'http://199.66.168.238:8081/KPPX/IONLIFE51.3/playlist.m3u8', 'IONLife_1453072787.jpg', '1', '2016-01-15 05:35:25', '2016-01-28 17:56:46', '1', '1', '37', '1', '2016-01-15 05:35:25', '2016-01-28 17:56:46', '13726', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('176', 'KPPX-51.4 SHOP', '0', 'KPPX-51.4 SHOP', 'KPPX-51.4 SHOP', '1', 'http://199.66.168.238:8081/KPPX/SHOP51.4/playlist.m3u8', 'http://199.66.168.238:8081/KPPX/SHOP51.4/playlist.m3u8', 'shop_1453072803.jpg', '1', '2016-01-15 05:49:54', '2016-01-28 17:56:46', '1', '1', '38', '1', '2016-01-15 05:49:54', '2016-01-28 17:56:46', '2003207', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('177', 'KTVW-33.1 UNIVISION', '0', 'KTVW-33.1 UNIVISION', 'KTVW-33.1 UNIVISION', '1', 'http://199.66.168.238:8081/KTVW/UNIVISION33.1/playlist.m3u8', 'http://199.66.168.238:8081/KTVW/UNIVISION33.1/playlist.m3u8', 'univision2_1453069645.jpg', '1', '2016-01-15 05:57:42', '2016-01-28 17:56:46', '1', '1', '27', '1', '2016-01-15 05:57:42', '2016-01-28 17:56:46', '13369', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('178', 'KTVW-33.2 UNIMAS', '0', 'KTVW-33.2 UNIMAS', 'KTVW-33.2 UNIMAS', '1', 'http://199.66.168.238:8081/KTVW/UNIMAS33.2/playlist.m3u8', 'http://199.66.168.238:8081/KTVW/UNIMAS33.2/playlist.m3u8', 'unimas_1453069663.jpg', '1', '2016-01-15 06:05:26', '2016-01-28 17:56:46', '1', '1', '28', '1', '2016-01-15 06:05:26', '2016-01-28 17:56:46', '13370', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('179', 'KTVW-33.3 GRIT', '0', 'KTVW-33.3 GRIT', 'KTVW-33.3 GRIT', '1', 'http://199.66.168.238:8081/KTVW/GRIT33.3/playlist.m3u8', 'http://199.66.168.238:8081/KTVW/GRIT33.3/playlist.m3u8', 'Grit_1453069678.jpg', '1', '2016-01-15 06:06:58', '2016-01-28 17:56:46', '1', '1', '29', '1', '2016-01-15 06:06:58', '2016-01-28 17:56:46', '2007038', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('180', 'KTVW-33.4 BOUNCE', '0', 'KTVW-33.4 BOUNCE', 'KTVW-33.4 BOUNCE', '1', 'http://199.66.168.238:8081/KTVW/BOUNCE33.4/playlist.m3u8', 'http://199.66.168.238:8081/KTVW/BOUNCE33.4/playlist.m3u8', 'Bounce_1453069726.jpg', '1', '2016-01-15 06:09:31', '2016-01-28 17:56:46', '1', '1', '30', '1', '2016-01-15 06:09:31', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('181', 'KTVP-22.1 (3ABN)', '0', 'KTVP-22.1 (3ABN)', 'KTVP-22.1 (3ABN)', '1', 'http://199.66.168.201:80/hls/221A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-221KTVP', 'GoodNewsTV_1453069960.jpg', '1', '2016-01-17 22:32:40', '2016-01-28 17:56:46', '1', '1', '21', '1', '2016-01-17 22:32:40', '2016-01-28 17:56:46', '2002938', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('182', 'KTVP-22.2 (LLBN)', '0', 'KTVP-22.2 (LLBN)', 'KTVP-22.2 (LLBN)', '1', 'http://199.66.168.201:80/hls/222A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-222KTVP', 'LLBN_1453070159.jpg', '1', '2016-01-17 22:35:59', '2016-01-28 17:56:46', '1', '1', '22', '1', '2016-01-17 22:35:59', '2016-01-28 17:56:46', '2002940', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('183', 'KTVP-22.3 (SBN)', '0', 'KTVP-22.3 (SBN)', 'KTVP-22.3 (SBN)', '1', 'http://199.66.168.201:80/hls/223A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-223KTVP', 'SBN_1453070398.jpg', '1', '2016-01-17 22:39:58', '2016-01-28 17:56:46', '1', '1', '23', '1', '2016-01-17 22:39:58', '2016-01-28 17:56:46', '2009174', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('184', 'KTVP-22.4 (LC)', '0', 'KTVP-22.4 (LC)', 'KTVP-22.4 (LC)', '1', 'http://199.66.168.201:80/hls/224A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-224KTVP', 'LC3_1453071065.jpg', '1', '2016-01-17 22:46:29', '2016-01-28 17:56:46', '1', '1', '24', '1', '2016-01-17 22:46:29', '2016-01-28 17:56:46', '', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('185', 'KTVP-22.5 (AlmaVision)', '0', 'KTVP-22.5 (AlmaVision)', 'KTVP-22.5 (AlmaVision)', '1', 'http://199.66.168.201:80/hls/225A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-225KTVP', 'Almavision_1453072179.jpg', '1', '2016-01-17 23:09:39', '2016-01-28 17:56:46', '1', '1', '25', '1', '2016-01-17 23:09:39', '2016-01-28 17:56:46', '2009175', 'StreamVisionGold');
INSERT INTO `channels` VALUES ('186', 'KTVP-22.6 (JTV)', '0', 'KTVP-22.6 (JewelryTV)', 'KTVP-22.6 (JewelryTV)', '1', 'http://199.66.168.201:80/hls/226A.m3u8', 'https://199.66.168.201/stream_setup.php?view=output&input=dvb_stream10-config&output=dvb_stream10-config-226KTVP', 'JTV_1453072406.jpg', '1', '2016-01-17 23:13:26', '2016-01-28 17:56:46', '1', '1', '26', '1', '2016-01-17 23:13:26', '2016-01-28 17:56:46', '2009177', 'StreamVisionGold');

-- ----------------------------
-- Table structure for channel_type
-- ----------------------------
DROP TABLE IF EXISTS `channel_type`;
CREATE TABLE `channel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_inx` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of channel_type
-- ----------------------------
INSERT INTO `channel_type` VALUES ('1', 'Livestream');
INSERT INTO `channel_type` VALUES ('2', 'Video on Demand');
INSERT INTO `channel_type` VALUES ('3', 'Category');

-- ----------------------------
-- Table structure for collections
-- ----------------------------
DROP TABLE IF EXISTS `collections`;
CREATE TABLE `collections` (
  `collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_name` varchar(255) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `created_date_gmt` datetime NOT NULL,
  `modified_date_gmt` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`collection_id`),
  KEY `collectionname_inx` (`collection_name`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of collections
-- ----------------------------
INSERT INTO `collections` VALUES ('1', 'Uncategorized', '3', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1');
INSERT INTO `collections` VALUES ('15', 'Streamvision Help Videos', '6', '1', '0', '2015-03-25 23:14:18', '0000-00-00 00:00:00', '2015-03-25 23:14:18', '0000-00-00 00:00:00', '1');
INSERT INTO `collections` VALUES ('16', 'Streamvision Tutorials', '1', '1', '0', '2015-03-25 23:14:37', '0000-00-00 00:00:00', '2015-03-25 23:14:37', '0000-00-00 00:00:00', '1');
INSERT INTO `collections` VALUES ('14', 'Commercial Spots', '5', '1', '0', '2015-03-25 23:11:45', '0000-00-00 00:00:00', '2015-03-25 23:11:45', '0000-00-00 00:00:00', '1');
INSERT INTO `collections` VALUES ('13', 'Hospital Promo Videos', '2', '1', '0', '2015-03-25 23:11:32', '0000-00-00 00:00:00', '2015-03-25 23:11:32', '0000-00-00 00:00:00', '1');
INSERT INTO `collections` VALUES ('12', 'Casa Grande Community', '4', '1', '0', '2015-03-25 23:11:20', '0000-00-00 00:00:00', '2015-03-25 23:11:20', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`CustomerID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES ('1', '27521', 'Asim', 'Bawany_Test_72315', '2016-01-12 03:30:00');

-- ----------------------------
-- Table structure for customer_services
-- ----------------------------
DROP TABLE IF EXISTS `customer_services`;
CREATE TABLE `customer_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `ServiceID` int(11) NOT NULL,
  `ServiceModule` varchar(255) DEFAULT NULL,
  `ServiceName` varchar(255) DEFAULT NULL,
  `Login` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Values` text,
  `Active` int(1) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ServiceID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customer_services
-- ----------------------------
INSERT INTO `customer_services` VALUES ('1', '27521', '40670', 'Roku', 'StreamVisionGold', '27521AS001Bawany', 'test', '{\"ExternalRef\":\"Roku\"}', '1', '2016-01-12 03:30:00', '2016-01-12 03:32:05');
INSERT INTO `customer_services` VALUES ('2', '27521', '40671', 'Roku', 'AdditionalRoku', '27521AS001Bawany', 'test', '{\"ExternalRef\":\"Roku\"}', '1', '2016-01-12 03:30:00', '2016-01-12 03:32:05');

-- ----------------------------
-- Table structure for emerald_users
-- ----------------------------
DROP TABLE IF EXISTS `emerald_users`;
CREATE TABLE `emerald_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `AccountID` int(11) NOT NULL,
  `Domain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `FirstName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `LastName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ServiceType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ExternalRef` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Identifier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DeviceID` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Active` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of emerald_users
-- ----------------------------
INSERT INTO `emerald_users` VALUES ('4', '26685', '34096', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Living Room', '1GN386030678', '1', '2015-06-03 14:54:48', '2015-06-03 14:54:48', null);
INSERT INTO `emerald_users` VALUES ('5', '26773', '34223', 'localhost', '26773AS001Streamvision2', 'test', 'Living room', 'Streamvision2', 'StreamVisionBronze', 'NovaAdminRoku', 'LivingRoom', '1234567890', '0', '2015-06-03 14:54:49', '2016-02-07 04:46:37', null);
INSERT INTO `emerald_users` VALUES ('6', '26773', '34757', 'localhost', '26773AS003Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', '', '', '1', '2015-06-15 12:45:48', '2015-11-15 08:54:48', '2015-11-15 08:54:48');
INSERT INTO `emerald_users` VALUES ('7', '15761', '27243', 'localhost', '15761AS001Friedman', 'brn1944', 'Gregory-PBX1', 'Friedman', 'StreamVisionBronze', 'Roku', 'LivingRoom', '1GN386030678', '1', '2015-06-16 01:06:49', '2015-06-16 01:06:49', null);
INSERT INTO `emerald_users` VALUES ('8', '26773', '34224', 'localhost', '26773AS002Streamvision2', 'test', '12 Year', 'Old Girl\'s Room', 'StreamVisionGold', 'Roku', '12 Year Old Girl\'s Room', '1GN38T127221', '1', '2015-06-16 01:06:51', '2016-02-07 04:44:32', null);
INSERT INTO `emerald_users` VALUES ('9', '26685', '34225', 'localhost', '26685AS002Streamvision1', 'test', 'Test', 'Streamvision1', 'StreamVisionGold', 'Roku', '', '1GS3DX041644', '1', '2015-06-16 01:06:52', '2015-06-16 01:06:52', null);
INSERT INTO `emerald_users` VALUES ('11', '26773', '35149', 'localhost', '26773AS004Streamvision2', 'test', 'Steve', 'Schulte', 'StreamVisionGold', 'Roku', 'Steve Schulte', '2L645W088752', '1', '2015-07-02 11:47:48', '2016-02-07 04:44:32', null);
INSERT INTO `emerald_users` VALUES ('12', '26773', '35296', 'localhost', '26773AS005Streamvision2', 'test', 'Steve', 'Car', 'StreamVisionGold', 'Roku', 'Steves_Car', '1GS3D5007590', '1', '2015-07-08 13:14:07', '2016-02-07 04:44:32', null);
INSERT INTO `emerald_users` VALUES ('13', '26773', '35486', 'localhost', '26773AS006Streamvision2', 'test', 'GAF', 'House', 'StreamVisionGold', 'Roku', 'Gregs_House', '1GH359099178', '1', '2015-07-21 00:15:13', '2016-02-07 04:42:27', null);
INSERT INTO `emerald_users` VALUES ('14', '26685', '35596', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Living Room', '1GU44M002307', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `emerald_users` VALUES ('15', '26773', '59778', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Living Room', '1RE3C9074256', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `emerald_users` VALUES ('16', '26685', '49585', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Living Room', '1GN39A175233', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `emerald_users` VALUES ('17', '26773', '48673', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Living Room', '4E6520038919', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `emerald_users` VALUES ('18', '26773', '48870', 'localhost', '26685AS001Streamvision1', 'Test', 'Test', 'Streamvision1', 'StreamVisionBronze', 'NovaAdminRoku', 'Claysol Office', '\r\n1GN39A175233', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', null);
INSERT INTO `emerald_users` VALUES ('19', '26773', '36583', 'localhost', '26773AS007Streamvision2', 'test', 'Jason V', 'Streamvision2', 'StreamVisionGold', 'Roku', 'JasonV', '2LA4C3258884', '1', '2015-09-30 05:05:09', '2016-02-07 04:42:27', null);
INSERT INTO `emerald_users` VALUES ('20', '26773', '36739', 'localhost', '26773AS008Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', '', '', '1', '2015-10-06 06:44:36', '2015-10-06 06:46:41', '2015-10-06 06:46:41');
INSERT INTO `emerald_users` VALUES ('21', '26773', '36757', 'localhost', '26773AS008Streamvision2', 'test', 'Chevy', 'Streamvision2', 'StreamVisionGold', 'Roku', 'Chevy', '2LA54X125793', '1', '2015-10-07 02:35:29', '2016-02-07 04:42:27', null);
INSERT INTO `emerald_users` VALUES ('22', '26773', '36772', 'localhost', '26773AS009Streamvision2', 'test', 'DVEO', 'Streamvision2', 'StreamVisionGold', 'Roku', 'DVEO', '13A19M128362', '1', '2015-10-07 12:42:12', '2016-02-07 04:42:27', null);
INSERT INTO `emerald_users` VALUES ('23', '26773', '36806', 'localhost', '26773AS010Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', 'Chuck Hogg', '4E7573003658', '1', '2015-10-09 09:04:57', '2016-02-07 04:40:22', '2016-02-07 04:40:22');
INSERT INTO `emerald_users` VALUES ('24', '26773', '36819', 'localhost', '26773AS011Streamvision2', 'test', 'Jerry', 'New 10/8/15', 'StreamVisionGold', 'Roku', 'Jerry (New 10/8)', '4E756N101306', '1', '2015-10-09 15:29:42', '2016-02-07 04:40:22', null);
INSERT INTO `emerald_users` VALUES ('25', '26773', '37071', 'localhost', '26773AS012Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', '', '', '1', '2015-10-21 03:37:00', '2015-10-21 04:04:05', '2015-10-21 04:04:05');
INSERT INTO `emerald_users` VALUES ('26', '26773', '37256', 'localhost', '26773AS012Streamvision2', 'test', 'Steve', '2nd Roku', 'StreamVisionGold', 'Roku', 'Steve 2nd Roku', '2L6495111518', '1', '2015-10-28 08:45:23', '2016-02-07 04:40:22', null);
INSERT INTO `emerald_users` VALUES ('27', '28563', '37285', 'localhost', '28563AS01Streamvision', 'test', 'Tim', 'Miles', 'StreamVisionGold', 'Roku', 'Tim Miles 1', '2ld56y150571', '1', '2015-10-29 07:51:06', '2015-10-30 10:12:40', null);
INSERT INTO `emerald_users` VALUES ('28', '28578', '37309', 'localhost', '28578AS013Streamvision', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', 'Greg\'s Desk', '1GN386030677', '0', '0000-00-00 00:00:00', '2016-01-03 19:00:10', null);
INSERT INTO `emerald_users` VALUES ('29', '26773', '37311', 'localhost', '26773AS013Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', '', '', '0', '2015-10-29 12:37:13', '2015-11-15 08:54:48', '2015-11-15 08:54:48');
INSERT INTO `emerald_users` VALUES ('30', '26773', '37344', 'localhost', '26773AS014Streamvision2', 'test', 'BRandon', 'Staggs', 'StreamVisionGold', 'Roku', 'Brandon', '1GS41H003639', '1', '2015-10-30 11:29:33', '2016-02-07 04:40:22', null);
INSERT INTO `emerald_users` VALUES ('31', '26773', '37392', 'localhost', '26773AS015Streamvision2', 'test', 'GAF', 'Roku 4 Desk', 'StreamVisionGold', 'Roku', 'GregsRouk4Desk', 'YY00C9705336', '1', '2015-11-01 09:57:31', '2016-02-07 04:40:22', null);
INSERT INTO `emerald_users` VALUES ('32', '26773', '37402', 'localhost', '26773AS016Streamvision2', 'test', 'Steve', 'Roku 4', 'StreamVisionGold', 'Roku', 'Steve\'s Roku 4', 'YY006T003842', '1', '2015-11-02 03:26:36', '2016-02-07 04:38:17', null);
INSERT INTO `emerald_users` VALUES ('33', '28664', '37423', 'localhost', '28664AS01Streamvision', 'test', 'James', 'Sexton', 'StreamVisionGold', 'Roku', 'JamesSexton', '2LA53Y056743', '1', '2015-11-03 06:09:36', '2015-11-03 06:09:36', null);
INSERT INTO `emerald_users` VALUES ('34', '26773', '37631', 'localhost', '26773AS017Streamvision2', 'test', 'GAF', 'Roku Stick-2', 'StreamVisionGold', 'Roku', 'GAF Roku Stick-2', '2LD572048330', '1', '2015-11-08 09:33:45', '2016-02-07 04:38:17', null);
INSERT INTO `emerald_users` VALUES ('35', '28963', '37877', 'localhost', '28963AS001StreamVision3', 'test', 'GAF', 'Roku3-Desk - 0677', 'StreamVisionGold', 'Roku', 'Greg\'s Roku 3 on Desk', '1GN386030677', '1', '2015-11-15 05:00:35', '2016-02-07 07:14:25', null);
INSERT INTO `emerald_users` VALUES ('36', '28963', '37904', 'localhost', '28963AS002StreamVision3', 'test', 'GAF', 'Roku Stick-2 - 5787', 'StreamVisionGold', 'Roku', 'Greg Stick 2', '2LD55N005787', '1', '2015-11-16 09:42:10', '2016-02-07 07:14:25', null);
INSERT INTO `emerald_users` VALUES ('37', '28435', '38022', 'localhost', '28435AS001StreamVision', 'test', 'Patrick', 'Lawson', 'StreamVisionGold', 'Roku', 'Patrick_Lawson_Roku3-1', '4E654K148280', '1', '2015-11-19 09:49:16', '2015-11-19 09:51:21', null);
INSERT INTO `emerald_users` VALUES ('38', '28963', '38081', 'localhost', '28963AS003StreamVision3', 'test', 'GAF', 'Roku 3-5-Desk - 4173', 'StreamVisionGold', 'Roku', 'Roku 3-5-Desk', '632591004173', '1', '2015-11-21 10:43:11', '2016-02-07 07:14:25', null);
INSERT INTO `emerald_users` VALUES ('39', '29216', '38218', 'localhost', '29216AS01Streamvision', 'test', 'Kenneth', 'Garnett', 'StreamVisionGold', 'Roku', 'Nathan\'s Roku', '4E755H049104', '1', '2015-12-08 06:23:45', '2015-12-08 06:23:45', null);
INSERT INTO `emerald_users` VALUES ('40', '28506', '40491', 'localhost', '28506AS001Streamvision', 'Test', 'Shane', 'Mitchell', 'StreamVisionGold', 'Roku', 'Rock Solid Roku', '1GN386076415', '1', '2016-01-07 09:41:20', '2016-01-07 09:43:24', null);
INSERT INTO `emerald_users` VALUES ('41', '27521', '40670', 'localhost', '27521AS001Bawany', 'test', 'Asim', 'Bawany_Test_72315', 'StreamVisionGold', 'Roku', 'BawanyBrosLab', '6325AD186083', '1', '2016-01-12 03:30:00', '2016-01-12 03:32:05', null);
INSERT INTO `emerald_users` VALUES ('42', '28506', '40671', 'localhost', '28506AS002Streamvision', 'test', 'Shane', 'Mitchell', 'StreamVisionGold', 'Roku', 'ROck Solid Office', '4114AF081834', '1', '2016-01-12 03:57:07', '2016-01-15 07:44:08', null);
INSERT INTO `emerald_users` VALUES ('43', '27989', '40729', 'localhost', '27989AS001Streamvision', 'test', 'Anthony', 'Will', 'StreamVisionGold', 'Roku', 'Broadband - Will', '6325AP198398', '1', '2016-01-13 06:26:24', '2016-01-13 06:26:24', null);
INSERT INTO `emerald_users` VALUES ('44', '26773', '41548', 'localhost', '26773AS018Streamvision2', 'test', 'GAF', 'TCL RokuTV', 'StreamVisionGold', 'Roku', 'GAF TCL RokuTV', '2N00C0552477', '1', '2016-02-02 16:31:34', '2016-02-07 07:10:15', '2016-02-07 07:10:15');
INSERT INTO `emerald_users` VALUES ('45', '26773', '41643', 'localhost', '26773AS019Streamvision2', 'test', 'Dmitry', 'Kashin', 'StreamVisionGold', 'Roku', 'Dmitry Kashin', '6325AM186022', '1', '2016-02-04 12:24:00', '2016-02-07 04:36:16', null);
INSERT INTO `emerald_users` VALUES ('46', '28963', '41728', 'localhost', '28963AS004StreamVision3', 'test', 'GAF', 'TCL RokuTV - 2477', 'StreamVisionGold', 'Roku', 'TCL Roku TV', '2N00C0552477', '1', '2016-02-07 07:18:35', '2016-02-07 07:20:39', null);
INSERT INTO `emerald_users` VALUES ('53', '32089', '41790', 'localhost', '32089Test1a', 'test', 'StreamVisionDeployment', 'Test1a', 'StreamVisionGold', 'Roku', 'Test1a', '6325AD186083', '1', '2016-02-10 03:11:13', '2016-02-10 03:21:38', null);
INSERT INTO `emerald_users` VALUES ('54', '32090', '41791', 'localhost', '32090Test1b', 'Test', 'StreamVisionDeployment', 'Test1b', 'StreamVisionGold', 'Roku', 'Test1b', '6325AD186083', '1', '2016-02-10 03:25:48', '2016-02-10 03:25:48', null);
INSERT INTO `emerald_users` VALUES ('55', '32091', '41792', 'localhost', '32091Test2a', 'test', 'StreamVisionDeployment', 'Test2a', 'StreamVisionGold', 'Roku', 'Test2a', '6325AD186083', '1', '2016-02-28 04:33:10', '2016-02-28 04:33:10', null);
INSERT INTO `emerald_users` VALUES ('56', '32092', '41793', 'localhost', '32092Test2b', 'test', 'StreamVisionDeployment', 'Test2b', 'StreamVisionGold', 'Roku', 'Test2B', '6325AD186083', '1', '2016-02-28 04:33:10', '2016-02-28 04:33:10', null);
INSERT INTO `emerald_users` VALUES ('57', '32093', '41794', 'localhost', '32093Test3a', 'test', 'StreamVisionDeployment', 'Test3a', 'StreamVisionGold', 'Roku', 'test3a', '6325AD186083', '1', '2016-02-28 04:33:10', '2016-02-28 04:33:10', null);
INSERT INTO `emerald_users` VALUES ('58', '32094', '41795', 'localhost', '32094Test3b', 'test', 'StreamVisionDeployment', 'Test3b', 'StreamVisionGold', 'Roku', 'Test3b', '6325AD186083', '1', '2016-02-28 04:33:10', '2016-02-28 04:33:10', null);
INSERT INTO `emerald_users` VALUES ('59', '32592', '42446', 'localhost', '32592AS001StreamVision', 'test', 'Bobby', 'Norwood', 'StreamVisionGold', 'Roku', 'Norwood Test', '4E7550023787', '1', '2016-02-28 04:33:10', '2016-02-28 04:33:10', null);
INSERT INTO `emerald_users` VALUES ('60', '28963', '42740', 'localhost', '28963AS005StreamVision3', 'test', 'GAF', 'Roku2 - 0823', 'StreamVisionGold', 'Roku', 'GAF Roku 2', '1RH4D8030823', '1', '2016-03-06 03:56:31', '2016-03-06 05:36:00', null);
INSERT INTO `emerald_users` VALUES ('61', '26773', '43018', 'localhost', '26773AS020Streamvision2', 'test', 'Show', 'TCLTV', 'StreamVisionGold', 'Roku', 'ShowTCL-RokuTV', '2n00aa270902', '1', '2016-03-13 04:26:21', '2016-03-13 04:26:21', null);
INSERT INTO `emerald_users` VALUES ('62', '26773', '43090', 'localhost', '26773AS021Streamvision2', 'test', 'Test', 'Streamvision2', 'StreamVisionGold', 'Roku', 'TCL TV Show', '2N00HM516001', '1', '2016-03-16 06:40:16', '2016-03-16 06:42:21', null);

-- ----------------------------
-- Table structure for epg
-- ----------------------------
DROP TABLE IF EXISTS `epg`;
CREATE TABLE `epg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of epg
-- ----------------------------

-- ----------------------------
-- Table structure for epg_history
-- ----------------------------
DROP TABLE IF EXISTS `epg_history`;
CREATE TABLE `epg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `channelId` int(11) NOT NULL,
  `channelNumber` int(11) NOT NULL,
  `majorChannelNumber` int(11) NOT NULL,
  `minorChannelNumber` int(11) NOT NULL,
  `callsign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `broadcastType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of epg_history
-- ----------------------------

-- ----------------------------
-- Table structure for job_status
-- ----------------------------
DROP TABLE IF EXISTS `job_status`;
CREATE TABLE `job_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_desc` varchar(45) DEFAULT NULL,
  `status_color` char(7) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `jstatusdesc_inx` (`status_desc`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of job_status
-- ----------------------------
INSERT INTO `job_status` VALUES ('1', 'pending', '#1aa8b4');
INSERT INTO `job_status` VALUES ('2', 'waiting', '#1aa8b4');
INSERT INTO `job_status` VALUES ('3', 'processing', '#e46807');
INSERT INTO `job_status` VALUES ('4', 'success', '#7bc168');
INSERT INTO `job_status` VALUES ('5', 'fail', '#e25855');
INSERT INTO `job_status` VALUES ('6', 'cancelled', '#ad5ba5');

-- ----------------------------
-- Table structure for logs
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `Data` text,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logs
-- ----------------------------

-- ----------------------------
-- Table structure for media_library
-- ----------------------------
DROP TABLE IF EXISTS `media_library`;
CREATE TABLE `media_library` (
  `media_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `meta_tag` text,
  `meta_description` text,
  `poster_id` int(11) DEFAULT NULL,
  `media_name` varchar(255) DEFAULT NULL,
  `descriptions` text NOT NULL,
  `frame_width` int(11) NOT NULL,
  `frame_height` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  `content` text NOT NULL,
  `genre` text NOT NULL,
  `rating` float(5,1) NOT NULL,
  `rights` varchar(255) NOT NULL,
  `release_date` date NOT NULL,
  `show_description` text NOT NULL,
  `closed_captions` text NOT NULL,
  `generic_tags` text NOT NULL,
  `similar_content` text NOT NULL,
  `job_id` varchar(32) NOT NULL,
  `job_status_id` int(11) NOT NULL,
  `job_duration` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `created_date_gmt` datetime DEFAULT NULL,
  `modified_date_gmt` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_id`),
  KEY `medianame_inx` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of media_library
-- ----------------------------
INSERT INTO `media_library` VALUES ('28', 'test video', 'test', 'test', '73', 'multipleajith-mankatha_1431446102.mp4', '', '320', '240', '6319687', '', '', '0.0', '', '0000-00-00', '', '', '', '', '64933296ff31fef51e02f9bad538558e', '4', '65017', '1', null, '2015-05-12 15:55:03', null, '2015-05-12 15:55:03', null, '1');
INSERT INTO `media_library` VALUES ('29', 'test upvid 1', 'fd', 'fd', '74', 'HukusBukusTelewanChukusICICIAdvt_1431446716.mp4', '', '320', '240', '7127631', '', '', '1.0', '', '0000-00-00', '', '', '', '', 'b2b8fa543f8a39ac4d7bc00dc70737f3', '4', '79970', '1', '1', '2015-05-12 16:05:16', '2015-05-12 16:56:57', '2015-05-12 16:05:16', '2015-05-12 16:56:57', '1');
INSERT INTO `media_library` VALUES ('30', 'test upvid 2', null, null, '75', 'YennaiArindhaalOfficialTrailerAjithTrishaAnushka_1431446716.mp4', '', '320', '240', '9251209', '', '', '0.0', '', '0000-00-00', '', '', '', '', 'eca10b4b00c28f67398f1ab0e2e2f84f', '4', '94054', '1', null, '2015-05-12 16:05:16', null, '2015-05-12 16:05:16', null, '1');
INSERT INTO `media_library` VALUES ('31', 'test upvid 3', null, null, '76', 'UttamaVillainOfficialTrailer_1431446716.mp4', '', '320', '240', '9060772', '', '', '0.0', '', '0000-00-00', '', '', '', '', '3fd12b2126320cb6c9bdac925ca799cb', '4', '93153', '1', null, '2015-05-12 16:05:16', null, '2015-05-12 16:05:16', null, '1');

-- ----------------------------
-- Table structure for media_library_to_posters
-- ----------------------------
DROP TABLE IF EXISTS `media_library_to_posters`;
CREATE TABLE `media_library_to_posters` (
  `poster_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `poster_type` varchar(255) NOT NULL,
  PRIMARY KEY (`poster_id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of media_library_to_posters
-- ----------------------------
INSERT INTO `media_library_to_posters` VALUES ('73', '28', 'multipleajith-mankatha_1431446102_1.jpg', '');
INSERT INTO `media_library_to_posters` VALUES ('74', '29', 'HukusBukusTelewanChukusICICIAdvt_1431446716_1.jpg', '');
INSERT INTO `media_library_to_posters` VALUES ('75', '30', 'YennaiArindhaalOfficialTrailerAjithTrishaAnushka_1431446716_1.jpg', '');
INSERT INTO `media_library_to_posters` VALUES ('76', '31', 'UttamaVillainOfficialTrailer_1431446716_1.jpg', '');
INSERT INTO `media_library_to_posters` VALUES ('82', '29', 'Flat1_1431449817.jpg', 'square');
INSERT INTO `media_library_to_posters` VALUES ('81', '29', 'no_image.jpg', 'portrait');
INSERT INTO `media_library_to_posters` VALUES ('80', '29', 'no_image.jpg', '4X3');

-- ----------------------------
-- Table structure for media_to_channels
-- ----------------------------
DROP TABLE IF EXISTS `media_to_channels`;
CREATE TABLE `media_to_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of media_to_channels
-- ----------------------------
INSERT INTO `media_to_channels` VALUES ('86', '28', '49');
INSERT INTO `media_to_channels` VALUES ('85', '28', '48');

-- ----------------------------
-- Table structure for media_to_collections
-- ----------------------------
DROP TABLE IF EXISTS `media_to_collections`;
CREATE TABLE `media_to_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- ----------------------------
-- Records of media_to_collections
-- ----------------------------
INSERT INTO `media_to_collections` VALUES ('115', '66', '16');
INSERT INTO `media_to_collections` VALUES ('113', '66', '14');
INSERT INTO `media_to_collections` VALUES ('114', '66', '16');
INSERT INTO `media_to_collections` VALUES ('116', '28', '1');
INSERT INTO `media_to_collections` VALUES ('117', '29', '16');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL,
  `Message` text,
  `Description` text,
  `Type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`ErrorCode`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', '2005', 'Roku Service Unavilable', 'Radius authentication failed', 'error');
INSERT INTO `messages` VALUES ('2', '2002', 'Invalid Password', 'Service password is incorrect', 'error');
INSERT INTO `messages` VALUES ('3', '2001', 'Invalid Customer ID', 'Customer ID is invalid', 'error');
INSERT INTO `messages` VALUES ('4', '2004', 'Roku Service Unavilable', 'Service not found in database', 'error');
INSERT INTO `messages` VALUES ('5', '2003', 'Invalid Roku Device', 'Roku device is not available in list', 'error');
INSERT INTO `messages` VALUES ('6', '2006', 'You don\'t have permission for this channel, please upgrade your service.', 'Channel Permission Denied', 'error');
INSERT INTO `messages` VALUES ('7', '2007', 'An unknown error occurred while authenticating the request. Please contact administrator.', 'PHP fatol error', 'error');
INSERT INTO `messages` VALUES ('8', '2009', 'Roku device is already running with different account', 'Roku device is already running with different account', 'error');
INSERT INTO `messages` VALUES ('9', '2010', 'Error in deleting roku device, please contact your service provider', 'Some database error', 'error');
INSERT INTO `messages` VALUES ('10', '2011', 'This roku device is not linked with your account', 'This roku device is not linked with your account', 'error');
INSERT INTO `messages` VALUES ('11', '2012', 'No channels found', 'No channels found', 'error');
INSERT INTO `messages` VALUES ('12', '2013', 'No current shows found', 'No current shows found', 'error');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2015_05_25_105928_create_epg_table', '1');
INSERT INTO `migrations` VALUES ('2015_05_25_175754_create_epg_history_table', '1');
INSERT INTO `migrations` VALUES ('2015_05_26_181542_create_emerald_customer_table', '1');
INSERT INTO `migrations` VALUES ('2015_05_26_184047_create_emerald_customer_history_table', '1');
INSERT INTO `migrations` VALUES ('2015_06_04_091111_add_api_columns_to_emerald_history', '1');
INSERT INTO `migrations` VALUES ('2015_06_04_092925_modify_emerald_users_active_type_integer', '1');
INSERT INTO `migrations` VALUES ('2015_06_17_114729_add_titan_channel_subscription_type_columns_to_channels', '2');
INSERT INTO `migrations` VALUES ('2015_05_25_105928_create_epg_table', '1');
INSERT INTO `migrations` VALUES ('2015_05_25_175754_create_epg_history_table', '1');
INSERT INTO `migrations` VALUES ('2015_05_26_181542_create_emerald_customer_table', '2');
INSERT INTO `migrations` VALUES ('2015_05_26_184047_create_emerald_customer_history_table', '2');
INSERT INTO `migrations` VALUES ('2015_06_04_091111_add_api_columns_to_emerald_history', '2');
INSERT INTO `migrations` VALUES ('2015_06_04_092925_modify_emerald_users_active_type_integer', '2');
INSERT INTO `migrations` VALUES ('2015_06_17_114729_add_titan_channel_subscription_type_columns_to_channels', '3');

-- ----------------------------
-- Table structure for roku_devices
-- ----------------------------
DROP TABLE IF EXISTS `roku_devices`;
CREATE TABLE `roku_devices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `DeviceID` varchar(255) NOT NULL,
  `Identifier` varchar(255) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roku_devices
-- ----------------------------
INSERT INTO `roku_devices` VALUES ('3', '27521', '1234', 'Codup', '2016-03-22 17:27:58');
INSERT INTO `roku_devices` VALUES ('4', '27521', '874564', 'Codup', '2016-03-22 17:27:58');
INSERT INTO `roku_devices` VALUES ('5', '27521', '56888', 'Codup', '2016-03-22 17:27:58');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Stream', 'Vision TV', 'info@prolivestream.com', 'admin', '$2y$10$5yZSLxPACSY.5RB3ajUd1.X4qHUZqEFI8W32UYfGuTvWUvXHarjhq', 'XRV94ybDo3DXi7hHDwmMAWThhbKDs7XHk72bL0FUaZBf7zCLef3pqA0VSxZ6', '1');
