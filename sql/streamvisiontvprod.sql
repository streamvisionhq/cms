-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 25, 2017 at 02:55 AM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.6.13-1+deb.sury.org~trusty+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `streamvisiontv`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_client`
--

CREATE TABLE IF NOT EXISTS `api_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `domain_url` varchar(255) DEFAULT NULL,
  `domain_ip` varchar(45) DEFAULT NULL,
  `consumer_key` varchar(100) DEFAULT NULL,
  `consumer_secret` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  KEY `api_fname_inx` (`first_name`),
  KEY `api_lname_inx` (`last_name`),
  KEY `api_email_inx` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=43 ;

--
-- Dumping data for table `api_client`
--

INSERT INTO `api_client` (`client_id`, `first_name`, `last_name`, `email`, `domain_url`, `domain_ip`, `consumer_key`, `consumer_secret`, `status`, `date_created`, `date_modified`, `created_by`, `modified_by`) VALUES
(40, 'Sethu', 'Ramalingam', 'sethuphp@gmail.com', 'http://test.com', '23.91.14.101', '55156b6ae5b4972838', '7555156b6ae5bac', 1, '2015-03-27 14:38:34', NULL, NULL, NULL),
(41, 'Sethu', 'Ramalingam', 'kumar@claysol.com', 'http://test.com', '23.91.14.101', '5534cad800afe62053', '575534cad800b61', 1, '2015-04-20 09:46:00', NULL, NULL, NULL),
(35, 'ponsugumar', 'lingam', 'ponsukumar@colanonline.com', 'http://www.fakemailgenerator.com/#/jourrapide.com/rsauty1933/', '23.239.29.22', '550abea18a1b724588', '39550abea18a20f', 1, '2015-03-19 12:08:43', '2015-03-19 12:18:41', 35, 35),
(36, 'ponsukumar', 'lingam', 'sudhan@colanonline.com', 'http://transload.cipldev.com/transload/settings/fields', '74.220.219.129', '550ac3e4ce4ef54598', '25550ac3e4ce547', 1, '2015-03-19 12:25:54', '2015-03-19 12:41:08', 36, 36),
(37, 'Brock', 'lesner', 'anoop@colanonline.com', 'https://trello.com/b/I0d4GHmz/fusion-app', '192.230.65.4', '550ac2de924ad51233', '13550ac2de92509', 1, '2015-03-19 12:36:46', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE IF NOT EXISTS `channels` (
  `channel_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `short_description` text,
  `long_description` text,
  `channel_type` int(11) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `server_url` varchar(255) DEFAULT NULL,
  `callsign` varchar(50) DEFAULT NULL,
  `stream_name` varchar(255) DEFAULT NULL,
  `channel_logo` varchar(255) DEFAULT NULL,
  `pre_roll` tinyint(4) DEFAULT NULL,
  `channel_created` datetime DEFAULT NULL,
  `channel_modified` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `status` int(11) DEFAULT NULL,
  `channel_created_gmt` datetime DEFAULT NULL,
  `channel_modified_gmt` datetime DEFAULT NULL,
  `titan_channel_id` varchar(255) NOT NULL,
  `emerald_subscription_type` varchar(255) NOT NULL,
  PRIMARY KEY (`channel_id`),
  FULLTEXT KEY `channeltitle_inx` (`title`),
  FULLTEXT KEY `channelsdesc_inx` (`short_description`),
  FULLTEXT KEY `channelldesc_inx` (`long_description`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=207 ;

--
-- Dumping data for table `channels`
--

-- --------------------------------------------------------

--
-- Table structure for table `channel_type`
--

CREATE TABLE IF NOT EXISTS `channel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name_inx` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=4 ;

--
-- Dumping data for table `channel_type`
--

INSERT INTO `channel_type` (`id`, `name`) VALUES
(1, 'Livestream'),
(2, 'Video on Demand'),
(3, 'Category');

-- --------------------------------------------------------

--
-- Table structure for table `collections`
--

CREATE TABLE IF NOT EXISTS `collections` (
  `collection_id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_name` varchar(255) DEFAULT NULL,
  `sort_order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `created_date_gmt` datetime NOT NULL,
  `modified_date_gmt` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`collection_id`),
  KEY `collectionname_inx` (`collection_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=17 ;

--
-- Dumping data for table `collections`
--

INSERT INTO `collections` (`collection_id`, `collection_name`, `sort_order`, `created_by`, `modified_by`, `created_date`, `modified_date`, `created_date_gmt`, `modified_date_gmt`, `status`) VALUES
(1, 'Uncategorized', 3, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1),
(15, 'Streamvision Help Videos', 6, 1, 0, '2015-03-25 23:14:18', '0000-00-00 00:00:00', '2015-03-25 23:14:18', '0000-00-00 00:00:00', 1),
(16, 'Streamvision Tutorials', 1, 1, 1, '2015-03-25 23:14:37', '2016-03-17 11:18:49', '2015-03-25 23:14:37', '2016-03-17 11:18:49', 1),
(14, 'Commercial Spots', 5, 1, 0, '2015-03-25 23:11:45', '0000-00-00 00:00:00', '2015-03-25 23:11:45', '0000-00-00 00:00:00', 1),
(13, 'Hospital Promo Videos', 2, 1, 0, '2015-03-25 23:11:32', '0000-00-00 00:00:00', '2015-03-25 23:11:32', '0000-00-00 00:00:00', 1),
(12, 'Casa Grande Community', 4, 1, 0, '2015-03-25 23:11:20', '0000-00-00 00:00:00', '2015-03-25 23:11:20', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) NOT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Address1` text,
  `Address2` text,
  `City` varchar(70) DEFAULT NULL,
  `State` varchar(70) DEFAULT NULL,
  `Zip` varchar(70) DEFAULT NULL,
  `Phone` varchar(30) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Custom1` varchar(255) DEFAULT NULL,
  `Custom2` varchar(255) DEFAULT NULL,
  `Date` datetime DEFAULT NULL,
  `Status` int(1) DEFAULT NULL,
  PRIMARY KEY (`ID`,`CustomerID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `customers`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer_services`
--

CREATE TABLE IF NOT EXISTS `customer_services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `ServiceID` int(11) NOT NULL,
  `ServiceModule` varchar(255) DEFAULT NULL,
  `ServiceName` varchar(255) DEFAULT NULL,
  `Login` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Values` text,
  `Active` int(1) DEFAULT NULL,
  `LastBillDate` datetime DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`,`ServiceID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `customer_services`
--

-- --------------------------------------------------------

--
-- Table structure for table `dvr_storages`
--

CREATE TABLE IF NOT EXISTS `dvr_storages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerServiceId` int(11) NOT NULL,
  `pemdingDeleteDate` datetime DEFAULT NULL,
  `hours` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `customerServiceId` (`customerServiceId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=197 ;

--
-- Dumping data for table `dvr_storages`
--

-- --------------------------------------------------------

--
-- Table structure for table `epg`
--

CREATE TABLE IF NOT EXISTS `epg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `epg_history`
--

CREATE TABLE IF NOT EXISTS `epg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
  `channelId` int(11) NOT NULL,
  `channelNumber` int(11) NOT NULL,
  `majorChannelNumber` int(11) NOT NULL,
  `minorChannelNumber` int(11) NOT NULL,
  `callsign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `broadcastType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE IF NOT EXISTS `job_status` (
  `status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_desc` varchar(45) DEFAULT NULL,
  `status_color` char(7) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `jstatusdesc_inx` (`status_desc`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=7 ;

--
-- Dumping data for table `job_status`
--

INSERT INTO `job_status` (`status_id`, `status_desc`, `status_color`) VALUES
(1, 'pending', '#1aa8b4'),
(2, 'waiting', '#1aa8b4'),
(3, 'processing', '#e46807'),
(4, 'success', '#7bc168'),
(5, 'fail', '#e25855'),
(6, 'cancelled', '#ad5ba5');

-- --------------------------------------------------------

--
-- Table structure for table `log4php_log`
--

CREATE TABLE IF NOT EXISTS `log4php_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` datetime DEFAULT NULL,
  `logger` varchar(256) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `message` varchar(4000) DEFAULT NULL,
  `thread` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `line` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=337 ;

--
-- Dumping data for table `log4php_log`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `Data` text,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `logs`
--

-- --------------------------------------------------------

--
-- Table structure for table `media_library`
--

CREATE TABLE IF NOT EXISTS `media_library` (
  `media_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `meta_tag` text,
  `meta_description` text,
  `poster_id` int(11) DEFAULT NULL,
  `media_name` varchar(255) DEFAULT NULL,
  `descriptions` text NOT NULL,
  `frame_width` int(11) NOT NULL,
  `frame_height` int(11) NOT NULL,
  `file_size` int(11) NOT NULL,
  `content` text NOT NULL,
  `genre` text NOT NULL,
  `rating` float(5,1) NOT NULL,
  `rights` varchar(255) NOT NULL,
  `release_date` date NOT NULL,
  `show_description` text NOT NULL,
  `closed_captions` text NOT NULL,
  `generic_tags` text NOT NULL,
  `similar_content` text NOT NULL,
  `job_id` varchar(32) NOT NULL,
  `job_status_id` int(11) NOT NULL,
  `job_duration` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_date` datetime DEFAULT NULL,
  `created_date_gmt` datetime DEFAULT NULL,
  `modified_date_gmt` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`media_id`),
  KEY `medianame_inx` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC AUTO_INCREMENT=32 ;

--
-- Dumping data for table `media_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `media_library_to_posters`
--

CREATE TABLE IF NOT EXISTS `media_library_to_posters` (
  `poster_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `poster_type` varchar(255) NOT NULL,
  PRIMARY KEY (`poster_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `media_library_to_posters`
--

-- --------------------------------------------------------

--
-- Table structure for table `media_to_channels`
--

CREATE TABLE IF NOT EXISTS `media_to_channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=87 ;

--
-- Dumping data for table `media_to_channels`
--
-- --------------------------------------------------------

--
-- Table structure for table `media_to_collections`
--

CREATE TABLE IF NOT EXISTS `media_to_collections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED AUTO_INCREMENT=118 ;

--
-- Dumping data for table `media_to_collections`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL,
  `Message` text,
  `Description` text,
  `Type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`,`ErrorCode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`ID`, `ErrorCode`, `Message`, `Description`, `Type`) VALUES
(1, 2006, 'Authentication Failed', 'This error will occur Roku Radius authentication failed', 'error'),
(2, 2003, 'Invalid Customer ID or Password', 'This error will occur on entering Invalid Customer ID & Password', 'error'),
(3, 2002, 'Invalid Customer ID', 'This error will occur on Invalid Customer IDs', 'error'),
(4, 2005, 'Authentication Failed ', 'This error will occur Roku Service not found in database', 'error'),
(5, 2004, 'Invalid Device ID', 'This error will occur on Roku Device ID is not available in database during channel authorization', 'error'),
(6, 2007, 'This channel is not included in your subscribed package', 'This error will occur when user tried to access those channels which are not included on subscribed Package.', 'error'),
(7, 2008, 'Unknown error', 'This error will occur on PHP fatol error or request error strike', 'error'),
(8, 2009, 'Authentication Failed', 'This error will occur when some customer tried to use that roku device which is already registered with some other customer.', 'error'),
(9, 2010, 'Error in deleting roku device', 'Database Error on deleting Roku device', 'error'),
(11, 2011, 'No channel found', 'This error will occur when no channal  is  added on CMS', 'error'),
(12, 2012, 'No Shows Found', 'This error will occur when  Channals program data not found on EPG penal  \r\n', 'error'),
(13, 2013, 'Authentication Failed', 'This error will occur when DVR Radius authentication failed', 'error'),
(14, 2014, 'Authentication Failed ', 'This error will occur when DVR service not found in database', 'error'),
(15, 2015, 'DVR Access Denied', 'This error will occur when DVR Whole Home Service is not available and stream request from non default device', 'error'),
(16, 2016, 'Roku Device Inactive', 'This error will occur when Roku Device is inactive', 'error'),
(17, 2017, 'Server Error', 'This error will occur when No recorded event was found on DVR', 'error'),
(18, 2018, 'Server Error', 'This error will occur when radius seetings are missing on CMS', 'error'),
(19, 2001, 'Server Error', 'This error will occur when some connection problem', 'error'),
(20, 2019, 'Server Error', 'This error will occur when channel is inactive on CMS', 'error'),
(21, 2020, 'Server Error', 'This error will occur when nimble mode setting is missing', 'error'),
(22, 2021, 'Server Error', 'This error will occur when live nimble streaming or dvr host setting is missing', 'error'),
(23, 2022, 'Server Error', 'This error will occur when backup nimble streaming or dvr host setting is missing', 'error'),
(24, 2023, 'Server Error', 'This error will occur when customer management setting is missing', 'error'),
(25, 2024, 'DVR Error', 'This error will occur when Show Recording could not be deleted successfully from DVR.', 'error'),
(26, 2025, 'DVR Error', 'This error will occur when DVR could not add or update DVR request.', 'error'),
(27, 2026, 'DVR Error', 'This error will occur due to DVR Storage data could not be fetched.', 'error'),
(28, 2027, 'EPG Error', 'This error will occur when EPG Data could not be fetched from the EPG Guide.', 'error'),
(29, 2028, 'EPG Error', 'This error will occur when EPG Guide not found.', 'error'),
(30, 2029, 'EPG Error', 'This error will occur when EPG Guide File could not be parsed.', 'error'),
(31, 2030, 'Program Not Found', 'Currently, there is no show configured for this channel.', 'error');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_05_25_105928_create_epg_table', 1),
('2015_05_25_175754_create_epg_history_table', 1),
('2015_05_26_181542_create_emerald_customer_table', 1),
('2015_05_26_184047_create_emerald_customer_history_table', 1),
('2015_06_04_091111_add_api_columns_to_emerald_history', 1),
('2015_06_04_092925_modify_emerald_users_active_type_integer', 1),
('2015_06_17_114729_add_titan_channel_subscription_type_columns_to_channels', 2),
('2015_05_25_105928_create_epg_table', 1),
('2015_05_25_175754_create_epg_history_table', 1),
('2015_05_26_181542_create_emerald_customer_table', 2),
('2015_05_26_184047_create_emerald_customer_history_table', 2),
('2015_06_04_091111_add_api_columns_to_emerald_history', 2),
('2015_06_04_092925_modify_emerald_users_active_type_integer', 2),
('2015_06_17_114729_add_titan_channel_subscription_type_columns_to_channels', 3);

-- --------------------------------------------------------

--
-- Table structure for table `roku_devices`
--

CREATE TABLE IF NOT EXISTS `roku_devices` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CustomerID` int(11) DEFAULT NULL,
  `DeviceID` varchar(255) NOT NULL,
  `Identifier` varchar(255) DEFAULT NULL,
  `Default` int(1) DEFAULT NULL,
  `Status` int(1) NOT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `roku_devices`
--

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `InternalName` varchar(255) DEFAULT NULL,
  `ExternalName` varchar(255) DEFAULT NULL,
  `InternalRef` varchar(255) DEFAULT NULL,
  `ExternalRef` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`ID`, `Title`, `InternalName`, `ExternalName`, `InternalRef`, `ExternalRef`) VALUES
(1, 'StreamVision Basic', 'StreamVisionBasic', 'StreamVisionBasic', 'Roku', 'Staging 6'),
(2, 'StreamVision Bronze', 'StreamVisionBronze', 'StreamVisionBronze', 'Roku', 'Staging 6'),
(3, 'StreamVision Silver', 'StreamVisionSilver', 'StreamVisionSilver', 'Roku', 'Staging 6'),
(4, 'StreamVision Gold', 'StreamVisionGold', 'StreamVisionGold', 'Roku', 'Staging 6'),
(5, 'StreamVision Platinum', 'StreamVisionPlatinum', 'StreamVisionPlatinum', 'Roku', 'Staging 6'),
(6, 'Additional Roku', 'AdditionalRoku', 'Additional Roku', 'Roku', 'Staging 6'),
(7, 'DVR Basic', 'DVRBasic', 'DVR Basic', 'DVR', 'Staging 6'),
(8, 'DVR Whole Home', 'DVRWholeHome', 'DVR Whole Home', 'DVR', 'Staging 6'),
(9, 'DVR Additional', 'DVRAdditional', 'DVR Storage - 100 hrs Adt''l', 'DVR', 'Staging 6');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `settings_key_index` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=105 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`) VALUES
(1, 'dvr-endpoint', ''),
(2, 'dvr-retention', '0'),
(3, 'dvr-startup-storage', '360000'),
(4, 'dvr-additional-storage', '360000'),
(5, 'dvr-grace-period', ''),
(6, 'admin-email', 'info@streamvisiontv.com'),
(7, 'admin-name', 'Stream Vision'),
(8, 'version', '1.1.10.3'),
(9, 'service-provider-id', ''),
(10, 'root-path', '/var/www/html/streamvision/code'),
(11, 'radius-server-ip', ''),
(12, 'radius-server-host', ''),
(13, 'radius-shared-secret', ''),
(95, 'cm_mode', ''),
(96, 'customer-custom-field-1-label', ''),
(97, 'customer-custom-field-2-label', ''),
(98, 'nimble_stream', ''),
(99, 'nimble_dvr', ''),
(100, 'nimble_stream_backup', ''),
(101, 'nimble_dvr_backup', ''),
(102, 'nimble_port', ''),
(103, 'nimble_mode', 'live'),
(104, 'api_key', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `username`, `password`, `remember_token`, `status`) VALUES
(1, 'Stream', 'Vision TV', 'test@test.com', 'admin', '$2y$10$gXsUNF0zxyVJT46OdvCSGe.v70OaZdY1om6UOp8XotP.EIqu7OOb2', 'rjij0Hmlybarjj2Z3qxA1nkk91UWuxOn7I4ClEJgFaYoSWNTx2a2RXLT6OGw', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
