var deleteRokuDevice = function (deviceID) {
    var url = baseURL + "/roku-devices?action=delete&deviceID=" + deviceID + "";

    var r = confirm("Do you want to delete this device");

    if (r == true) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            beforeSend: function () {
                showOverlay();
            },
            success: function (data) {
                if (data['success']) {
                    if(data['default_device']){
                        $('.device_' + data['default_device'] + ' ul li .set_default_button').remove();
                        $('.device_' + data['default_device'] + ' ul li .status-link').remove();
                    }
                    hideOverlay();
                    $("#dialog-message").attr("title", "Successfully Deleted!");
                    $("#dialog-message").html("<p>" + data['message'] + "</p>");
                    $('.device_' + deviceID).remove();
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                } else {

                }
            }
        });
    }
}

var statusRokuDevice = function (deviceID, status) {
    var url = baseURL + "/roku-devices?action=changeStatus&status=" + status + "&deviceID=" + deviceID + "";

    var r = confirm("Do you want to change status for this device");

    if (r == true) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            beforeSend: function () {
                showOverlay();
            },
            success: function (data) {
                if (data['success']) {
                    hideOverlay();
                    $("#dialog-message").attr("title", "Status Changed!");
                    $("#dialog-message").html("<p>" + data['message'] + "</p>");
                    if (status == 1) {
                        $('.device_' + deviceID).find('.status').text("Active");
                        $('.device_' + deviceID).find('.status-link').attr("onclick", "statusRokuDevice('" + deviceID + "', 0);");
                        $('.device_' + deviceID).find('.status-icon').attr("title", "Inactive Device");
                        $('.device_' + deviceID).find('.status-icon').attr("src", baseURL + "/assets/images/active.png");
                    } else {
                        $('.device_' + deviceID).find('.status-link').attr("onclick", "statusRokuDevice('" + deviceID + "', 1);");
                        $('.device_' + deviceID).find('.status').text("Inactive");
                        $('.device_' + deviceID).find('.status-icon').attr("title", "Active Device");
                        $('.device_' + deviceID).find('.status-icon').attr("src", baseURL + "/assets/images/inactive.png");
                    }
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                } else {

                }
            }
        });
    }
}

var markDefault = function (deviceID, customerID) {
    var url = baseURL + "/roku-devices?action=markDefault&customerID=" + customerID + "&deviceID=" + deviceID + "";

    var r = confirm("Do you want set this device as default");

    if (r == true) {
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            beforeSend: function () {
                showOverlay();
            },
            success: function (data) {
                if (data['success']) {
                    hideOverlay();
                    $("#dialog-message").attr("title", "Successfully Set as Default");
                    $("#dialog-message").html("<p>" + data['message'] + "</p>");
                    $('.customer_' + customerID).find('.default').text("");
                    $('.device_' + deviceID).find('.default').text("Default");
                    $('.device_' + deviceID).find('.set_default_button').remove();
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                } else {

                }
            }
        });
    }
}

$(function () {
    $('#select_perpage').bind('change', function () {
        var url = $(this).val(); // get selected value
        if (url) { // require a URL
            window.location = url; // redirect
        }
        return false;
    });
    
    $('select[name=cm_mode]').change(function(){
        showCustomFields();
    });
    
    showCustomFields();

    //js for customer service toggle active/inactive status
    $('.button-wrap').on("click", function () {
        var buttonWrap = $(this);
        var serviceID = buttonWrap.parent().parent().parent().attr("data-id");
        var url = baseURL + "/customer/service/change-status/" + serviceID;
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (data) {
                    $(".alert-danger").remove();
                    $(".alert-success").remove();
                    $(".form-horizontal").before('<div class="alert alert-danger"><h3>' + data + '</h3></div>');
                } else {
                    $(".alert-danger").remove();
                    $(".alert-success").remove();
                    $(".form-horizontal").before('<div class="alert alert-success"><h3>Service updated successfully</h3></div>');
                    buttonWrap.toggleClass('button-active');
                }
            }
        });
    });

    $('.unassign-service').on("click", function () {
        var unAssignService = $(this);
        var serviceID = unAssignService.parent().parent().attr("data-id");
        var url = baseURL + "/customer/service/remove/" + serviceID;
        $.ajax({
            type: 'GET',
            url: url,
            success: function (data) {
                if (data) {
                    $(".alert-danger").remove();
                    $(".alert-success").remove();
                    $(".form-horizontal").before('<div class="alert alert-danger"><h3>' + data + '</h3></div>');
                } else {
                    $(".alert-danger").remove();
                    $(".alert-success").remove();
                    location.reload();
                }
            }
        });
    });

});

var showCustomFields = function(){
    if($('select[name=cm_mode] option:selected').val() == "cms"){
        $('input[name=customer-custom-field-1-label]').parent().parent().show();
        $('input[name=customer-custom-field-2-label]').parent().parent().show();
    }
}