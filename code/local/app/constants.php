<?php
/*
  |--------------------------------------------------------------------------
  | Register The Laravel Constants
  |--------------------------------------------------------------------------
 */

// Customer Constants

define("DEFAULT_CUSTOM_FIELD_LABEL1", "Custom Field 1");
define("DEFAULT_CUSTOM_FIELD_LABEL2", "Custom Field 2");

define("PASSWORD_EMPTY", "Password field must not be empty");
define("FIRSTNAME_EMPTY", "Firstname field must not be empty");
define("LASTNAME_EMPTY", "Lastname field must not be empty");
define("CITY_EMPTY", "City field must not be empty");
define("STATE_EMPTY", "State field must not be empty");
define("ZIP_EMPTY", "Zip field must not be empty");
define("EMAIL_EMPTY", "Email field must not be empty");


define("CUSTOMER_ADDED", "Customer Added Successfully.");
define("CUSTOMER_EDITED", "Customer Edited Successfully.");

//services
define("BASIC", "StreamVisionBasic");
define("BRONZE", "StreamVisionBronze");
define("SILVER", "StreamVisionSilver");
define("GOLD", "StreamVisionGold");
define("PLATINUM", "StreamVisionPlatinum");
define("ADDITIONAL_ROKU", "AdditionalRoku");
define("DVR_BASIC", "DVRBasic");
define("DVR_WHOLE_HOME", "DVRWholeHome");
define("ADDITIONAL_DVR", "DVRAdditional");

//emails
define("BILLING_API_EMAIL", "steve@streamvisiontv.com"); //used in billing api, whenever push data get failed

define("CENTRAL_LOGGING_SERVER_URL", "http://metrics.streamvisiontv.com:9000/LogReciever"); //used in billing api, whenever push data get failed
define("CENTRAL_LOGGING_SERVER_PORT", "9000"); //used in billing api, whenever push data get failed