<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmeraldCustomerHistoryTable extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Create Emerald Users History Table
            Schema::create("emerald_users_history", function(Blueprint $table) {
                  $table->increments("id");
                  $table->integer("CustomerID");
                  $table->integer("AccountID");
                  $table->string("Domain");
                  $table->string("Login");
                  $table->string("Password");
                  $table->string("FirstName");
                  $table->string("LastName");
                  $table->string("ServiceType");
                  $table->string("ExternalRef");
                  $table->string("Alias");
                  $table->string("Forward");
                  $table->string("Active");
                  $table->string("Action");
                  $table->integer("ChDomain");
                  $table->integer("ChLogin");
                  $table->integer("ChPassword");
                  $table->integer("ChFirstName");
                  $table->integer("ChLastName");
                  $table->integer("ChServiceType");
                  $table->integer("ChExternalRef");
                  $table->integer("ChAlias");
                  $table->integer("ChForward");
                  $table->integer("ChActive");
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            // Drop Emerald Users History Table
            Schema::dropIfExists("emerald_users_history");
      }

}
