<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpgTable extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Create EPG Table
            Schema::create('epg', function(Blueprint $table) {
                  $table->increments('id');
                  $table->longText('epgdata');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            //
      }

}
