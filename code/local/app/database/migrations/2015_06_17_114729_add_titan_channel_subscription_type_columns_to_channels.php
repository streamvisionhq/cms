<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitanChannelSubscriptionTypeColumnsToChannels extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Add the columns on migration
            Schema::table("channels", function(Blueprint $table) {
                  $table->string("titan_channel_id");
                  $table->string("emerald_subscription_type");
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            // Remove the columns on Rollback
            Schema::table("channels", function(Blueprint $table) {
                  $table->dropColumn(array('titan_channel_id', 'emerald_subscription_type'));
            });
      }

}
