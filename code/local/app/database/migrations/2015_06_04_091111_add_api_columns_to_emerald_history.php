<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApiColumnsToEmeraldHistory extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Add the columns on migration
            Schema::table("emerald_users_history", function(Blueprint $table) {
                  $table->string("APILogin");
                  $table->string("APIPassword");
                  $table->string("APIRef");
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            // Remove the columns on Rollback
            Schema::table("emerald_users_history", function(Blueprint $table) {
                  $table->dropColumn(array('APILogin', 'APIPassword', 'APIRef'));
            });
      }

}
