<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyEmeraldUsersActiveTypeInteger extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            DB::statement('ALTER TABLE `emerald_users` MODIFY `Active` INTEGER NULL;');
            DB::statement('ALTER TABLE `emerald_users_history` MODIFY `Active` INTEGER NULL;');
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            DB::statement('ALTER TABLE `emerald_users` MODIFY `Active` VARCHAR(255);');
            DB::statement('ALTER TABLE `emerald_users_history` MODIFY `Active` VARCHAR(255);');
      }

}
