<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmeraldCustomerTable extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Create Emerald Users Table
            Schema::create("emerald_users", function(Blueprint $table) {
                  $table->increments("id");
                  $table->integer("CustomerID");
                  $table->integer("AccountID");
                  $table->string("Domain");
                  $table->string("Login");
                  $table->string("Password");
                  $table->string("FirstName");
                  $table->string("LastName");
                  $table->string("ServiceType");
                  $table->string("ExternalRef");
                  $table->string("Identifier");
                  $table->string("DeviceID");
                  $table->string("Active");
                  $table->timestamps();
                  $table->softDeletes();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            // Drop Emerald Users Table
            Schema::dropIfExists("emerald_users");
      }

}
