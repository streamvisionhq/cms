<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpgHistoryTable extends Migration {

      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up() {
            // Create EPG History Table
            Schema::create('epg_history', function(Blueprint $table) {
                  $table->increments('id');
                  $table->longText('epgdata');
                  $table->integer('channelId');
                  $table->integer('channelNumber');
                  $table->integer('majorChannelNumber');
                  $table->integer('minorChannelNumber');
                  $table->string('callsign');
                  $table->string('network');
                  $table->string('broadcastType');
                  $table->string('description');
                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down() {
            // Drop EPG History Table
            Schema::dropIfExists('epg_history');
      }

}
