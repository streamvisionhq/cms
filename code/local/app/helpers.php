<?php

function getAwsPath() {
      return "https://s3.amazonaws.com/streamvisionhq/";
}

function getCdnPath() {
      return "https://streamvisionhq.a.cdnify.io/";
}

function getAwsBucket() {
      return "streamvisionhq";
}
