<?php

namespace Dvrapi;

use Dvrapi\libs\RequestHandler as RequestHandler;

/**
 * DVR Machine API Library
 */
class Dvrapi {
    
    /**
     * Construct
     * @return boolean
     */
    public function __construct() {
        $getEndPoint=DB::table('settings')->select('value')->where('key', 'dvr-endpoint')->first();
        if(!$getEndPoint->value){
            return false;
        }
    }
    
    /**
     * Manage DVR Storage API
     * @param type $action
     * @param type $client_id
     * @param type $rokutv_id
     * @param type $hours
     * @param type $retention
     * @return boolean
     */
    public static function DVRStorage($action, $clientID, $storage) {
        $return = array('status' => false, 'message' => 'Invalid Storage API');
        if ($action && $clientID && $storage) {
            $parameters = array('client_id' => $clientID, 'storage' => $storage);
            switch ($action) {
                case 'set':
                    $response = Dvrapi::process('set_client_dvr_storage', $parameters);
                    $response = str_replace('"', '', $response);
                    if (strpos($response, 'Storage created') !== false) {
                        $return = array('status' => true, 'message' => $response, 'client_id' => $clientID);
                    } else {
                        $return = array('status' => false, 'message' => $response);
                    }
                    break;
                case 'add':
                    $response = Dvrapi::process('add_client_dvr_storage', $parameters);
                    $response = str_replace('"', '', $response);
                    if (strpos($response, 'Client storage was added') !== false) {
                        $return = array('status' => true, 'message' => $response, 'client_id' => $clientID);
                    } else {
                        $return = array('status' => false, 'message' => $response);
                    }
                    break;
                case 'update':
                    $response = Dvrapi::process('update_client_dvr_storage', $parameters);
                    $response = str_replace('"', '', $response);
                    if (strpos($response, 'DVR request was updated') !== false) {
                        $return = array('status' => true, 'message' => $response, 'client_id' => $clientID);
                    } else {
                        $return = array('status' => false, 'message' => $response);
                    }
                    break;
            }
        }
        return $return;
    }

    /**
     * Get client current storage
     * @param type $clientID
     */
    public static function getStorage($clientID) {
        $parameters = array('client_id' => $clientID);
        $response = Dvrapi::process('get_client_dvr_storage', $parameters);
        if (strpos($response, 'No storage available for client id') === false) {
            $return = self::convertToArray($response);
        } else {
            $return = (object)array('status' => false, 'message' => $response);
        }
        return $return;
    }

    /**
     * Get EPG Channels
     * @return Object
     */
    public static function getEPG($date, $timeZone) {
        $parameters = array('date' => $date, 'timezone' => $timeZone);
        return self::convertToArray(Dvrapi::process('get_epg', $parameters));
    }

    /**
     * Manage Recording
     * @param type $action
     * @param type $epgxID
     * @param type $showID
     * @param type $clientID
     * @param type $clientTimezone
     * @param type $recordType
     * @param type $retention
     * @return boolean
     */
    public static function recordDVR($action, $epgID, $epgxID, $showID, $clientID, $clientTimezone, $recordType) {
        $return = array('status' => false, 'message' => 'Invalid Recording API');
        $parameters = array(
            'epg_id' => $epgID,
            'epgx_id' => $epgxID,
            'show_id' => $showID,
            'show_name' => "empty_name",            
            'client_id' => $clientID,            
            'client_timezone' => $clientTimezone,
            'start_datetime' => "0000-00-00 00:00:00",
            'max_duration' => 0,
            'r_rr' => $recordType,
            'retention' => 1,
            'play_url' => '',
            'virtuals' => 0,
            'virtual_dvrx_id' => 0,
            'type' => 'show',
            'catchuptv_id' => 0,
            'status' => 'scheduled',
            'setting_id' => 0            
        );
        switch ($action) {
            case 'add':
                $response = Dvrapi::process('add_dvrx_request', $parameters);
                $response = str_replace('"', '', $response);
                if (strpos($response, 'Show Recording Scheduled') !== false) {
                    $return = array('status' => true, 'message' => $response);
                } else {
                    $return = array('status' => false, 'message' => $response);
                }
                break;
            case 'update':
                $response = Dvrapi::process('update_dvrx_request', $parameters);
                $response = str_replace('"', '', $response);
                if (strpos($response, 'DVR request was updated') !== false) {
                    $return = array('status' => true, 'message' => $response);
                } else {
                    $return = array('status' => false, 'message' => $response);
                }
                break;
        }

        return $return;
    }

    /**
     * Get customer’s finished recording data along with EPG data
     * @param int $clientID
     * @return object
     */
    public static function getRecordShow($clientID) {
        $parameters = array('client_id' => $clientID);
        return self::convertToArray(Dvrapi::process('get_epg_recorded', $parameters));
    }
    
    /**
     * Get customer’s begun recording data along with EPG data
     * @param int $clientID
     * @return object
     */
    public static function getScheduledShow($clientID) {
        $parameters = array('client_id' => $clientID);
        return self::convertToArray(Dvrapi::process('get_dvrx_requests', $parameters));
    }

    /**
     * Delete customer’s scheduled and recorded recording data
     * @param type $clientID
     * @param type $showID
     * @param boolean $deleteRR delete all future RR recordings of it as well
     * @return type
     */
    public static function deleteRecordRequest($clientID, $showID, $deleteRR = false) {
        $parameters = array('client_id' => $clientID, 'show_id' => $showID);
        $apiToHit = $deleteRR ? 'delete_client_rr_records' : 'delete_dvrx_request';
        $response = Dvrapi::process($apiToHit, $parameters);
        $response = str_replace('"', '', $response);

        if (strpos($response, 'DVR request deleted') !== false || strpos($response, 'RR series records where deleted') !== false) {
            $return = array('status' => true, 'message' => $response);
        } else {
            $return = array('status' => false, 'message' => $response);
        }

        return $return;
    }

    /**
     * Get client record requests
     * @return type
     */
    public static function getRecordRequest($clientID) {
        $parameters = array('client_id' => $clientID);
        return self::convertToArray(Dvrapi::process('get_dvrx_requests', $parameters));
    }

    /**
     * Process Request
     * @param type $action
     * @param type $fields
     * @return JSON
     */
    public static function process($action, $fields = NULL) {
        $requestInstance = new RequestHandler;
        $response = $requestInstance->sendRequest($action, $fields);
        if ($response) {
            return $response;
        }
    }

    public static function convertToArray($data, $single=false) {
        if($single === true){
            return json_decode($data);
        }else{
            $first = json_decode($data);
            $secoond = json_decode($first);
            if($secoond === null){
                return $first;
            }else{
                return $secoond;
            }
        }
    }
    
    /**
     * Add Channel in DVR
     * @param type $channelID
     * @param type $streamName
     * @param type $streamURL
     * @return Array
     */
    public static function addChannel($channelID, $streamName, $streamURL){
        $parameters = array('epg_channel_id' => $channelID, 'stream_name' => $streamName, 'stream_url' => $streamURL);
        $response = Dvrapi::process('add_epgx_channel', $parameters);
        $response = str_replace('"', '', $response);

        if (strpos($response, 'Channel was added') !== false) {
            $return = array('status' => true, 'message' => $response);
        } else {
            $return = array('status' => false, 'message' => $response);
        }

        return $return;
    }
    
    /**
     * Update Channel in DVR
     * @param type $channelID
     * @param type $streamName
     * @param type $streamURL
     * @return Array
     */
    public static function updateChannel($channelID, $streamName, $streamURL){
        $parameters = array('epg_channel_id' => $channelID, 'stream_name' => $streamName, 'stream_url' => $streamURL);
        $response = Dvrapi::process('update_epgx_channel', $parameters);
        $response = str_replace('"', '', $response);

        if (strpos($response, 'DVR epgx channel was updated') !== false) {
            $return = array('status' => true, 'message' => $response);
        } else {
            $return = array('status' => false, 'message' => $response);
        }

        return $return;
    }
    
    /**
     * Delete channel from DVR
     * @param type $channelID
     * @return Array
     */
    public static function deleteChannel($channelID){
        $parameters = array('epg_channel_id' => $channelID);
        $response = Dvrapi::process('delete_epgx_channel', $parameters);
        $response = str_replace('"', '', $response);

        if (strpos($response, 'DVR epgx channel was deleted') !== false) {
            $return = array('status' => true, 'message' => $response);
        } else {
            $return = array('status' => false, 'message' => $response);
        }

        return $return;
    }
    
    /**
     * Get DVR Channels
     * @return Array
     */
    public static function getChannels() {        
        return self::convertToArray(Dvrapi::process('get_epgx_channels'), true);        
    }
}
