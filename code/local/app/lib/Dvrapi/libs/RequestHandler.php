<?php

namespace Dvrapi\libs;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB as DB;

class RequestHandler extends Facade {    
    public function sendRequest($action, $fields=NULL) {            
        
        $fields_string=NULL;
        
        if($fields){
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            rtrim($fields_string, '&');
        }
        
        $getEndPoint=DB::table('settings')->select('value')->where('key', 'dvr-endpoint')->first();
        
        $url = $getEndPoint->value.$action."/";        
        
        $ch = curl_init();        
        curl_setopt($ch,CURLOPT_URL, $url);        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if($fields){            
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        }
        $output = curl_exec($ch);        
        curl_close($ch);        
        
        return $output;
    }        
}
