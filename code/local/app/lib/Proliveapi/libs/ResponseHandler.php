<?php
namespace Proliveapi\libs;

use SoapBox\Formatter\Formatter;

class ResponseHandler {
	
	public function convertToResponse($content, $params = array())
	{
		$formatter = Formatter::make($content, Formatter::ARR);
		
		if(isset($params['format']) && $params['format'] == 'json') {
			header("content-type:application/json");
			echo $formatter->toJson();
			exit;
		} 
		
		header("Content-Type: application/xml");
		echo $formatter->toXml();
		exit;
		
	}
}