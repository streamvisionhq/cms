<?php
namespace Proliveapi\libs;

use Illuminate\Support\Facades\request as Request;

class RequestHandler  {
		
	public function getHttpHeaders()
	{
		return getallheaders();
	}
	
	public function getHttpMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}
	
	public function getRequestParams()
	{
		return Request::all();
	}
}