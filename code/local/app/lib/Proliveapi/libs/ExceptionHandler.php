<?php
namespace Proliveapi\libs;

class ExceptionHandler  {
	
	public function invalidCall()
	{
		return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Api request."));
	}
}