<?php
namespace Proliveapi;

use Proliveapi\libs\RequestHandler as RequestHandler;
use Proliveapi\libs\ResponseHandler as ResponseHandler;
use Proliveapi\service\Base as Base;

class Proliveapi {

	private $httpRequestHeaders;
	private $httpRequestMethod;
	private $httpRequestParams;
	private $apiService;
	private $allowedMethod;
	private $uriSegments;
	private $responseContent;
	
	public function __construct($service, $method, $segments)
	{
		$this->apiService     =  $service;
		$this->allowedMethod  =  $method;
		$this->uriSegments    =  $segments;
		$this->process();
	}
	
	public function process()
	{
		$requestInstance = new RequestHandler;	
		
		$this->httpRequestHeaders  =  $requestInstance->getHttpHeaders();
		$this->httpMethod          =  $requestInstance->getHttpMethod();
		$this->httpRequestParams   =  $requestInstance->getRequestParams();
				
		$baseInstance = new Base;
		$this->responseContent = $baseInstance->processApi($this->apiService, $this->allowedMethod, $this->uriSegments, $this->httpRequestHeaders, $this->httpMethod, $this->httpRequestParams);
		
		$this->response();
	}
	
	public function response()
	{
		$responseInstance = new ResponseHandler;	
		$responseInstance->convertToResponse($this->responseContent, $this->httpRequestParams);
	}
}