<?php
namespace Proliveapi\service;

use Proliveapi\service\Auth as Auth;
use Proliveapi\service\Validate as Validate;
use Proliveapi\service\Client as Client;
use Proliveapi\service\Channel as Channel;
use Proliveapi\service\Collection as Collection;
use Proliveapi\libs\ExceptionHandler as ExceptionHandler;

class Base  {

	private $clientId = 0;
	private $clientStatus;
	private $status;
	
	public function processApi($apiService, $allowedMethod, $uriSegments, $requestHeaders, $requestHttpMethod, $requestParams)
	{
		$validateInstance = new Validate;
		$isValidHttpRequestMethod = call_user_func_array(array ($validateInstance, 'validateRequestMethod'), array ($allowedMethod, $requestHttpMethod));
		
		if($isValidHttpRequestMethod == false) return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid HTTP REQUEST"));
		
		if($apiService['authenticate'] == true) {
			$authInstance = new Auth;
			$result = call_user_func_array(array ($authInstance, 'authClient'), array ($requestHeaders));
			
			if(is_null($result)) {
				return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Application Key or Application Secret."));
			} else {
				$this->clientId = $result->client_id;
				$this->clientStatus = $result->status; 
			}
		}
		
		$exceptionInstance = new ExceptionHandler;
		
		switch($apiService['service']) {
		    case 'client':
					$objInstance = new Client;
					$this->status = 0;
				break;
		
		    case 'channel':
					$objInstance = new Channel;
					$this->status = 1;
				break;
				
			case 'collection':
					$objInstance = new Collection;
					$this->status = 1;
				break;
				
			default:
					return $exceptionInstance->invalidCall();
				break;
		}
		
		if(method_exists($objInstance , $apiService['method'])) {
			if($this->status == 1) {
				if($this->clientStatus != 1) return array("response" => array("status" => "success", "data" => "null", "message" => "Please activate your account to access this api request."));
			}
			return call_user_func_array(array ($objInstance, $apiService['method']), array ($requestParams, $uriSegments, $this->clientId));
		} 
			
		return $exceptionInstance->invalidCall();
	}
}