<?php
namespace Proliveapi\service;

use Illuminate\Support\Facades\URL as URL;
use Proliveapi\service\Util as Util;
use Proliveapi\models\Collection as Collectionmodel;

class Collection  {
	
	private $collectionData = array();
	private $videoData = array();
	private $posterData = array();
	private $setVideos;
	private $collectionId;
	private $videoId;
	private $mediaPath;
	
	public function __construct() 
	{
		$this->mediaPath = Util::getCdnPath();
	}
	
	public function listVideos($params, $segments)
	{
		$this->videoId = 0;
		
		$results = Collectionmodel::getVideos($this->videoId, $params, $this->mediaPath);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Videos found."));
		} else {
			foreach($results as $key => $value) {
				$this->videoData['videos'][$key] = array_merge((array) $value, array("poster" => $this->getPosterByMedia($value->id)));
			}
			
			return array("response" => array("status" => "success", "data" => $this->videoData, "message" => "null"));
		}
	}
	
	private function getPosterByMedia($mediaId)
	{
		$results = Collectionmodel::getPosters($mediaId, $this->mediaPath);
		
		foreach($results as $key => $value) {
			$this->posterData[$key] = (array) $value;
		}
		
		return $this->posterData;
	}
	
	public function showVideo($params, $segments)
	{
		$this->videoId = $segments[4];
		
		$result = Collectionmodel::getVideos($this->videoId, $params, $this->mediaPath);
				
		if(is_null($result)) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Video found."));
		} else {
			$this->videoData['videos'][0] = array_merge((array) $result, array("poster" => $this->getPosterByMedia($result->id)));
						
			return array("response" => array("status" => "success", "data" => $this->videoData, "message" => "null"));
		}
	}
	
	public function listCollection($params, $segments)
	{
		$this->setVideos = (isset($params['videos'])) ? $params['videos'] : true;
		$this->collectionId = 0;
		$results = Collectionmodel::getCollections($this->collectionId, $params);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Collections found."));
		} else {
			foreach($results as $key => $value) {
				if($this->setVideos == true) {
					$playlists = Collectionmodel::getVideoByCollection($value->id, $this->mediaPath);
					
					if(sizeof($playlists) > 0) {
						foreach($playlists  as $pkey => $pvalue) {
							$this->playlistData['videos'][$pkey] = array_merge((array) $pvalue, array("poster" => $this->getPosterByMedia($pvalue->id)));
							//$this->playlistData['videos'][$pkey] = $pvalue;
						}
					} else {
						$this->playlistData['videos'] = array();
					}
					
					$this->collectionData[$key] = array_merge((array)$value, $this->playlistData);
				} else {
					$this->collectionData[$key] = (array)$value;
				}
			}
			return array("response" => array("status" => "success", "data" => $this->collectionData, "message" => "null"));
		}
	}
	
	public function showCollection($params, $segments)
	{
		$this->setVideos = (isset($params['videos'])) ? $params['videos'] : true;
		$this->collectionId = $segments[4];
		$result = Collectionmodel::getCollections($this->collectionId, $params);
		
		if(is_null($result)) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No collection found."));
		} else {
			if($this->setVideos == true) {
				$playlists = Collectionmodel::getVideoByCollection($result->id, $this->mediaPath);
				
				if(sizeof($playlists) > 0) {
					foreach($playlists  as $pkey => $pvalue) {
						$this->playlistData['videos'][$pkey] = array_merge((array) $pvalue, array("poster" => $this->getPosterByMedia($pvalue->id)));
						//$this->playlistData['videos'][$pkey] = $pvalue;
					}
				} else {
					$this->playlistData['videos'] = array();
				}
				
				$this->collectionData[0] = array_merge((array)$result, $this->playlistData);
			} else {
				$this->collectionData[0] = (array)$result;
			}
			
			return array("response" => array("status" => "success", "data" => $this->collectionData, "message" => "null"));
		}
	}
	
	public function listCollectionVideos($params, $segments)
	{
		$this->collectionId = $segments[4]; 
		$results = Collectionmodel::getVideoByCollectionId($this->collectionId, $params, $this->mediaPath);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Videos found."));
		} else {
			foreach($results as $key => $value) {
				$this->playlistData['videos'][$key] = array_merge((array) $value, array("poster" => $this->getPosterByMedia($value->id)));
				//$this->playlistData['videos'][$key] = $value;
			}
			return  array("response" => array("status" => "success", "data" => $this->playlistData, "message" => "null"));
		}
	}
	
}