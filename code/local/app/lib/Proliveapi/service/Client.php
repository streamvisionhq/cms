<?php
namespace Proliveapi\service;

use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Support\Facades\Mail as Mail;
use Proliveapi\service\Util as Util;
use Proliveapi\models\Client as Clientmodel;

class Client {
	private $clientData;
	private $uriSegments;
	private $resultData;
	
	public function create($params, $segments, $clientId)
	{
		$this->clientData = $params;
		$this->uriSegments = $segments;
		
		$validate = $this->validate($this->clientData, 0);
		
		if($validate == false) {
			return $this->response;
		} else {
			$this->clientData['consumer_key']     =  Util::getApiKey();
			$this->clientData['consumer_secret']  =  Util::getApiSecret();
			$this->clientData['domain_ip']        =  Util::getIpAddress($this->clientData['domain_url']);
			
			$result = Clientmodel::insertClient($this->clientData);
			
			if($result < 1) {
				$this->response = array("response" => array("status" => "error", "data" => "", "message" => "Failed to create client. Please try again."));
			}
			
			$this->mailClient($this->clientData, 'emails.clientsignup', 'Pro Livestream - Your API credentials!');
			return array("response" => array("status" => "success", "data" => "", "message" => "Client crerated successfully. Please check your inbox for api keys."));
		}
	}
	
	public function myAccount($params, $segments, $clientId)
	{
		$results = Clientmodel::getClientById($clientId);
		foreach($results as $key => $value) {
			$this->resultData[$key] = $value;
		}
		
		return array("response" => array("status" => "success", "data" => $this->resultData, "message" => "null"));
	}
	
	public function modifyClient($params, $segments, $clientId)
	{
		$requestId  =  $segments[3];
		$this->clientData = $params;
		$this->uriSegments = $segments;
		
		if($requestId != $clientId) 
			return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Client Id."));
			
		$validate = $this->validate($this->clientData, $requestId);
		
		if($validate == false) {
			return $this->response;
		} else {
			$this->clientData['domain_ip']        =  Util::getIpAddress($this->clientData['domain_url']);
			
			$result = Clientmodel::updateClient($this->clientData, $requestId);
			
			if($result < 1) {
				$this->response = array("response" => array("status" => "error", "data" => "", "message" => "Failed to create client. Please try again."));
			}
			
			$this->mailClient($this->clientData, 'emails.clientmodify', 'Pro Livestream - Account Modified!');
			return array("response" => array("status" => "success", "data" => "", "message" => "Client details modified successfully."));
		}
	}
	
	public function modifyClientStatus($params, $segments, $clientId)
	{
		$requestId  =  $segments[4];
		$this->clientData = $params;
		$this->uriSegments = $segments;
		
		if($requestId != $clientId) 
			return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Client Id."));
			
		$result = Clientmodel::updateClientStatus($requestId, $params['status']);
		$message = ($params['status'] == true) ? "Client account activated successfully." : "Client account deactivated successfully.";
		
		return array("response" => array("status" => "success", "data" => "null", "message" => $message));
	}
	
	public function regenerateClientApi($params, $segments, $clientId)
	{
		$requestId  =  $segments[3];
		$this->clientData = $params;
		$this->uriSegments = $segments;
		
		if($requestId != $clientId) 
			return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Client Id."));
			
		$consumer_key     =  Util::getApiKey();
		$consumer_secret  =  Util::getApiSecret();
		
		Clientmodel::updateClientApi($consumer_key, $consumer_secret, $requestId);
			
		$results = Clientmodel::getClientDetailsById($requestId);
		foreach($results as $key => $value) {
			$this->resultData[$key] = $value;
		}
		$this->mailClient($this->resultData, 'emails.clientapidetails', 'Pro Livestream - Your API credentials!');	
		
		return array("response" => array("status" => "success", "data" => "null", "message" => "App key generated successfully. Please check your inbox for api keys."));
	}
	
	public function removeClient($params, $segments, $clientId)
	{
		$requestId  =  $segments[3];
		$this->clientData = $params;
		$this->uriSegments = $segments;
		
		if($requestId != $clientId) 
			return array("response" => array("status" => "error", "data" => "null", "message" => "Invalid Client Id."));
			
		Clientmodel::deleteClient($requestId);
			
		return array("response" => array("status" => "success", "data" => "null", "message" => "Client account removed successfully."));
	}
	
	
	private function mailClient($clientData, $emailTemplate, $emailSubject)
	{
		$auser = array(
			'email' => $clientData['email'],
			'name'  => $clientData['first_name'].' '.$clientData['last_name'],
			'subject' => $emailSubject
		);
		
		Mail::send($emailTemplate, $clientData, function($message) use ($auser)
		{
			$message->to($auser['email'], $auser['name'])
					->subject($auser['subject']);
		});
	}
	
	private function validate($clientData, $id)
	{
		$messages = array( // setting up custom error messages for the field validation
			'first_name.required' => 'Please enter First Name',
			'last_name.required' => 'Please enter Last Name',
			'email.required' => 'Please enter Email',
			'domain_url.required' => 'Please enter Domain URL'
	    );
		
		$rules = array(
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email',
			'domain_url' => 'required|url',
		);
		
		$validator = Validator::make($clientData, $rules, $messages);
		
		if ($validator->fails()) { 
			foreach ($validator->messages()->getMessages() as $field_name => $messages) {
				$this->errors[$field_name] = $messages[0];
			}
			
			$this->response = array("response" => array("status" => "error", "data" => "null", "message" => $this->errors));
			return false;
		} else {
			//check email exists
			$result = Clientmodel::getClientByEmail($clientData['email'], $id);
			
			if(is_null($result)) {
				return true;
			} else {
				$this->response = array("response" => array("status" => "error", "data" => "null", "message" => array("email" => "Email already exists")));
				return false;
			}
		}
	}
}