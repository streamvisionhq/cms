<?php
namespace Proliveapi\service;

use Proliveapi\models\Client as Clientmodel;

class Auth  {

	private $basicAuthorization;
	private $apiConsumerKey = '';
	private $apiConsumerSecret = '';
	
	public function authClient($headerData)
	{
		$this->basicAuthorization       = (isset($headerData['Authorization'])) ? $headerData['Authorization'] : '';
		
		if($this->basicAuthorization != '') {
			$expBasicauth = explode(" ", $this->basicAuthorization);
			$decodeAuth = base64_decode($expBasicauth[1]);
			$expDecodeauth = explode(":", $decodeAuth);
			$this->apiConsumerKey     =  $expDecodeauth[0];
			$this->apiConsumerSecret  =  $expDecodeauth[1];  
		}
		
		
		return Clientmodel::getClientByAppCredential($this->apiConsumerKey, $this->apiConsumerSecret);
	}
}