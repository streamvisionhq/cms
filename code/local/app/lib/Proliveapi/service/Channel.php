<?php
namespace Proliveapi\service;

use Illuminate\Support\Facades\URL as URL;
use Proliveapi\models\Channel as Channelmodel;

class Channel  {
	private $channelData;
	private $channelId;
	private $playlistData = array();
	private $setVideos;
	private $setSubChannels;
	private $mediaPath;
	private $sub_channels = array();
	private $index        = array();
	
	public function __construct() 
	{
		$this->mediaPath = Util::getCdnPath();
	}
	
	public function listLiveStream($params, $segments)
	{
		$this->channelId = 0;
		$results = Channelmodel::getLivestreams($this->channelId, $params);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Livestreams found."));
		} else {
			foreach($results as $key => $value) {
				$this->channelData[$key] = $value;
			}
			return array("response" => array("status" => "success", "data" => $this->channelData, "message" => "null"));
		}
	}
	
	public function showLiveStream($params, $segments)
	{
		$this->channelId = $segments[4];
		$results = Channelmodel::getLivestreams($this->channelId, $params);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Livestreams found."));
		} else {
			foreach($results as $key => $value) {
				$this->channelData[$key] = $value;
			}
			return array("response" => array("status" => "success", "data" => $this->channelData, "message" => "null"));
		}
	}
	
	public function listVideosOnDemand($params, $segments)
	{
		$this->setVideos = (isset($params['playlist'])) ? $params['playlist'] : true;
		$this->setSubChannels = (isset($params['subchannel'])) ? $params['subchannel'] : true;
		$this->channelId = 0;
		$results = Channelmodel::getVideosondemand($this->channelId, $params);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Videos On Demand found."));
		} else {
			foreach($results as $key => $value) {
				
					if($this->setVideos == true) {
						if($value->channel_type == 2) {
							$playlists = Channelmodel::getPlaylistByDemand($value->id, $this->mediaPath);
							
							if(sizeof($playlists) > 0) {
								foreach($playlists  as $pkey => $pvalue) {
									$this->playlistData['playlist'][$pkey] = array_merge((array) $pvalue, array("poster" => $this->getPosterByMedia($pvalue->id)));
								}
							} else {
								$this->playlistData['playlist'] = array();
							}
							
							$this->channelData[$key] = array_merge((array)$value, $this->playlistData);
						} else {
							$this->channelData[$key] = (array)$value;
						}
					} else {
						$this->channelData[$key] = (array)$value;
					}
							
					if($this->setSubChannels == true) {
						if($value->channel_type == 3) {
							$this->sub_channels = array();
							$this->channelData[$key]['subchannel'] = $this->getChannelByCategory($value->id);
						}
					}
				
			}
			
			return array("response" => array("status" => "success", "data" => $this->channelData, "message" => "null"));
		}
	}
	
	public function getChannelByCategory($id)
	{
		$results = Channelmodel::getChannelByCategory($id);
		
		foreach($results as $result) {
			$this->sub_channels[$result->id][] = $result;
			if($result->channel_type == 2) {
				$playlists = Channelmodel::getPlaylistByDemand($result->id, $this->mediaPath);
							
				if(sizeof($playlists) > 0) {
					foreach($playlists  as $pkey => $pvalue) {
						$this->sub_channels[$result->id]['playlist'][$pkey] = array_merge((array) $pvalue, array("poster" => $this->getPosterByMedia($pvalue->id)));
						//$this->sub_channels[$result->id]['playlist'][] = 
					}
				} else {
					$this->sub_channels[$result->id]['playlist'] = array();
				}
				
				
			}
			$this->getChannelBycategory($result->id);
		}
		return $this->sub_channels;
	}
	
	
	public function showVideosOnDemand($params, $segments)
	{
		$this->setVideos = (isset($params['playlist'])) ? $params['playlist'] : true;
		$this->setSubChannels = (isset($params['subchannel'])) ? $params['subchannel'] : true;
		$this->channelId = $segments[4];
		$result = Channelmodel::getVideosondemand($this->channelId, $params);
		
		if(is_null($result)) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Videos On Demand found."));
		} else {
			if($this->setVideos == true) {
				if($result->channel_type == 2) {
					$playlists = Channelmodel::getPlaylistByDemand($result->id, $this->mediaPath);
					
					if(sizeof($playlists) > 0) {
						foreach($playlists  as $pkey => $pvalue) {
							$this->playlistData['playlist'][$pkey] = array_merge((array) $pvalue, array("poster" => $this->getPosterByMedia($pvalue->id)));
						}
					} else {
						$this->playlistData['playlist'] = array();
					}
					
					$this->channelData[0] = array_merge((array)$result, $this->playlistData);
				} else {
					$this->channelData[0] = (array)$result;
				}
			} else {
				$this->channelData[0] = (array)$result;
			}
			
			if($this->setSubChannels == true) {
				if($result->channel_type == 3) {
					$this->sub_channels = array();
					$this->channelData[0]['subchannel'] = $this->getChannelByCategory($result->id);
				}
			}
			
			return array("response" => array("status" => "success", "data" => $this->channelData, "message" => "null"));
		}
	}
	
	public function playlists($params, $segments)
	{
		$this->channelId = $segments[4];
		$results = Channelmodel::getPlaylistByChannelId($this->channelId, $params, $this->mediaPath);
		
		if(sizeof($results) < 1) {
			return array("response" => array("status" => "success", "data" => "null", "message" => "No Video Playlists found."));
		} else {
			foreach($results as $key => $value) {
				$this->playlistData[$key] = array_merge((array) $value, array("poster" => $this->getPosterByMedia($value->id)));
			}
			return  array("response" => array("status" => "success", "data" => $this->playlistData, "message" => "null"));
		}
	}
	
	private function getPosterByMedia($mediaId)
	{
		
		$results = Channelmodel::getPosters($mediaId, $this->mediaPath);
		
		foreach($results as $key => $value) {
			$this->posterData[$key] = (array) $value;
		}
		
		return $this->posterData;
	}
	
}