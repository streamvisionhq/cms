<?php
namespace Proliveapi\service;

use Illuminate\Support\Facades\Facade;

class Util extends Facade {
	
	public static function getApiKey()
	{
		return uniqid().rand(10000, 99999);
	}
	
	public static function getApiSecret()
	{
		return uniqid(rand(10, 99));
	}
	
	public static function getIpAddress($url)
	{
		$address = parse_url($url);
		return gethostbyname($address['host']);
	}
	
	public static function getCdnPath()
	{
		return "https://streamvisionhq.a.cdnify.io/";
	}
	
}