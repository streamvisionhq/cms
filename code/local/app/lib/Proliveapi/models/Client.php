<?php
namespace Proliveapi\models;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB as DB;

class Client extends Facade {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'api_client';
	
	public static function insertClient($clientData)
	{
		$id = DB::table('api_client')->insertGetId(array("first_name" => trim($clientData['first_name']), "last_name" => trim($clientData['last_name']), "email" => $clientData['email'], "domain_url" => $clientData['domain_url'], "domain_ip" => $clientData['domain_ip'], "consumer_key" => $clientData['consumer_key'], "consumer_secret" => $clientData['consumer_secret'], "status" => 1, "date_created" => date("Y-m-d H:i:s")));
		
		return $id;
	}
	
	public static function updateClient($clientData, $client_id)
	{
		DB::table('api_client')->where('client_id', $client_id)
							   ->update(array("first_name" => trim($clientData['first_name']), "last_name" => trim($clientData['last_name']), "email" => $clientData['email'], "domain_url" => $clientData['domain_url'], "domain_ip" => $clientData['domain_ip'], "date_modified" => date("Y-m-d H:i:s"), "created_by" => $client_id, "modified_by" => $client_id));
							   
		return $client_id;
	}
	
	public static function deleteClient($client_id)
	{
		DB::table('api_client')->where('client_id', '=', $client_id)->delete();
		
		return true;
	}
	
	public static function updateClientStatus($client_id, $status)
	{
		$clientStatus = ($status == true) ? 1 : 2;
		
		DB::table('api_client')->where('client_id', $client_id)
							   ->update(array("status" => $clientStatus, "date_modified" => date("Y-m-d H:i:s"), "created_by" => $client_id, "modified_by" => $client_id));
							   
		return $client_id;
	}
	
	public static function updateClientApi($app_key, $app_secret, $client_id)
	{
		DB::table('api_client')->where('client_id', $client_id)
							   ->update(array("consumer_key" => $app_key, "consumer_secret" => $app_secret, "date_modified" => date("Y-m-d H:i:s"), "created_by" => $client_id, "modified_by" => $client_id));
							   
		return $client_id;
	}
		
	public static function getClientById($client_id)
	{
		$query = DB::table('api_client')->select(DB::raw('client_id as id, first_name, last_name, email, domain_url, consumer_key as appkey, consumer_secret as appsecret, date_format(date_created, \'%b %d, %Y\') as created_date, date_format(date_modified, \'%b %d, %Y\') as modified_date, CASE WHEN status = 1 THEN \'Active\' ELSE \'Inactive\' END as status '))
										->where('client_id', $client_id)
									    ->first();
		return $query;
	}
	
	public static function getClientDetailsById($client_id)
	{
		$query = DB::table('api_client')->select(DB::raw('client_id as id, first_name, last_name, email, consumer_key, consumer_secret'))
										->where('client_id', $client_id)
									    ->first();
		return $query;
	}
	
	public static function getClientByEmail($email, $client_id)
	{
		$query = DB::table('api_client')->select('*')
										->where('email', $email);
		if($client_id > 0)  $query->where('client_id', '!=', $client_id);
		$result = $query->first();
		return $result;
	}
	
	public static function getClientByAppCredential($app_key, $app_secret)
	{
		$query = DB::table('api_client')->select('*')
										->where('consumer_key', $app_key)
										->where('consumer_secret', $app_secret)
									    ->first();
		return $query;
	}
}