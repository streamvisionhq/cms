<?php
namespace Proliveapi\models;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB as DB;
use Illuminate\Support\Facades\URL as URL;

class Channel extends Facade {
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'channels';
		
	public static function getLivestreams($id, $data)
	{	
		$img_path       =  URL::to('assets/data/image');
		$page           =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage    =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy         =  'sort_order';
		$order          =  'desc';
		$take           =   $itemPerPage;
		$skip           =   0;
		
		$query = DB::table('channels')->select(DB::raw('channel_id AS id, title, short_description, long_description, livestream_url AS stream_url, server_url, CONCAT("'.$img_path.'", "/", channel_logo) AS channel_logo, CASE WHEN pre_roll=1 THEN "Yes" ELSE "No" END AS pre_roll, sort_order AS sort_order, date_format(channel_created, \'%b %d, %Y\') AS created_date, date_format(channel_modified, \'%b %d, %Y\') AS modified_date'));
		
		//Where
		$query->where('status', 1);
		$query->where('channel_type', 1);
		
		if(isset($id) && $id > 0)  $query->where('channel_id', $id); 
		if(isset($data['q'])) $query->where('title', 'LIKE', '%'.trim($data['q']).'%'); 
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id < 1) $query->take($take)->skip($skip);
				
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date' || $data['sort'] == 'sort_order') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		if(isset($id) && $id < 1) $query->orderBy($sortBy, $order);
		
		if(isset($id) && $id > 0)
			return $query->first();
		else
			return $query->get();
	}
	
	public static function getVideosondemand($id, $data)
	{
		$img_path       =  URL::to('assets/data/image');
		$page           =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage    =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy         =  'created_date';
		$order          =  'desc';
		$take           =   $itemPerPage;
		$skip           =   0;
		
		$query = DB::table('channels')->select(DB::raw('channel_id AS id, parent_id, title, short_description, long_description, channel_type, CONCAT("'.$img_path.'", "/", channel_logo) AS channel_logo, CASE WHEN pre_roll=1 THEN "Yes" ELSE "No" END AS pre_roll, date_format(channel_created, \'%b %d, %Y\') AS created_date, date_format(channel_modified, \'%b %d, %Y\') AS modified_date'));
		
		//Where
		$query->where('status', 1);
		$query->where('channel_type', '>', 1);
		
		$query->where('parent_id', 0);
		
		if(isset($id) && $id > 0)  $query->where('channel_id', $id); 
		if(isset($data['q'])) $query->where('title', 'LIKE', '%'.trim($data['q']).'%'); 
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id < 1) $query->take($take)->skip($skip);
		
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		if(isset($id) && $id < 1) $query->orderBy($sortBy, $order);
		//echo $query->toSql(); exit;
		if(isset($id) && $id > 0)
			return $query->first();
		else
			return $query->get();
	}
	
	public static function getPlaylistByDemand($id, $mediaPath) 
	{	
		$query = DB::table('media_to_channels')->select(DB::raw('media_library.media_id AS id, media_library.name AS title, media_library.meta_tag, media_library.meta_description, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS media_logo, CONCAT("'.$mediaPath.'", "/", media_name) AS video_link, media_name AS video_name'));
		
		//Joins
		//$query->join('media_to_channels', 'channels.channel_id', '=', 'media_to_channels.channel_id');
		$query->join('media_library', 'media_to_channels.media_id', '=', 'media_library.media_id');
		$query->join('media_library_to_posters', 'media_library_to_posters.poster_id', '=', 'media_library.poster_id');
		
		//where
		$query->where('media_to_channels.channel_id', '=', $id);
		$query->where('media_library.status', '=', 1);
		$query->where('media_library.job_status_id', '=', 4);
		
		//order by
		$query->orderBy('media_library.media_id', 'asc');
		
		return $query->get();
	}
	
	public static function getChannelByCategory($id) 
	{
		$img_path       =  URL::to('assets/data/image');
		
		$query = DB::table('channels')->select(DB::raw('channel_id AS id, parent_id, title, short_description, long_description, channel_type, CONCAT("'.$img_path.'", "/", channel_logo) AS channel_logo, CASE WHEN pre_roll=1 THEN "Yes" ELSE "No" END AS pre_roll, date_format(channel_created, \'%b %d, %Y\') AS created_date, date_format(channel_modified, \'%b %d, %Y\') AS modified_date'));
		
		//Where
		$query->where('status', 1);
		$query->where('channel_type', '>', 1);
		$query->where('parent_id', $id);
		
		$query->orderBy('sort_order', 'ASC');
		
		return $query->get();
	}
	
	public static function getPlaylistByChannelId($id, $data, $mediaPath) 
	{
		$page         =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage  =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy       =  'created_date';
		$order        =  'desc';
		$take         =   $itemPerPage;
		$skip         =   0;
		
		$query = DB::table('media_to_channels')->select(DB::raw('media_library.media_id AS id, media_library.name AS title, media_library.meta_tag, media_library.meta_description, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS media_logo, CONCAT("'.$mediaPath.'", "/", media_name) AS video_link, media_name AS video_name, date_format(created_date, \'%b %d, %Y\') AS created_date, date_format(modified_date, \'%b %d, %Y\') AS modified_date'));
		
		//Joins
		$query->join('media_library', 'media_to_channels.media_id', '=', 'media_library.media_id');
		$query->join('media_library_to_posters', 'media_library_to_posters.poster_id', '=', 'media_library.poster_id');
		
		//Where
		$query->where('media_to_channels.channel_id', '=', $id);
		$query->where('media_library.status', '=', 1);
		$query->where('media_library.job_status_id', '=', 4);
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id > 0) $query->take($take)->skip($skip);
		
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		$query->orderBy($sortBy, $order);
		
		return $query->get();
	}
	
	public static function getPosters($id, $mediaPath)
	{
		$query = DB::table('media_library')->select(DB::raw('media_library_to_posters.poster_id, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS poster_image, media_library_to_posters.poster_type, media_library_to_posters.media_id'));
		//Joins
		$query->join('media_library_to_posters', 'media_library_to_posters.media_id', '=', 'media_library.media_id');
		//Where
		$query->where('media_library_to_posters.media_id', $id);
				
		return $query->get();
	}
}