<?php
namespace Proliveapi\models;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\DB as DB;
use Illuminate\Support\Facades\URL as URL;

class Collection extends Facade {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'media_library';
		
	public static function getVideos($id, $data, $mediaPath)
	{
		$page           =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage    =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy         =  'media_library.created_date';
		$order          =  'desc';
		$take           =  $itemPerPage;
		$skip           =  0;
		
		$query = DB::table('media_library')->select(DB::raw('media_library.media_id AS id, media_library.name AS title, media_library.meta_tag, media_library.meta_description, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS media_logo, CONCAT("'.$mediaPath.'", media_library.media_name) AS video_link, media_library.media_name AS video_name, date_format(media_library.created_date, \'%b %d, %Y\') AS created_date, date_format(media_library.modified_date, \'%b %d, %Y\') AS modified_date, media_library.descriptions, CONCAT(media_library.frame_width, " X ", media_library.frame_height) as frame_size, media_library.file_size, "bytes" AS file_size_desc, media_library.job_duration as duration, "ms" AS duration_sdesc, "milliseconds" AS duration_ldesc, media_library.content,  media_library.genre, media_library.rating, "5" AS rating_out_of, media_library.rights, date_format(media_library.release_date, \'%b %d, %Y\') AS release_date, media_library.show_description, media_library.closed_captions, media_library.generic_tags, media_library.similar_content'));
		
		//Joins
		$query->join('media_library_to_posters', 'media_library_to_posters.poster_id', '=', 'media_library.poster_id');
		
		//Where
		$query->where('media_library.status', 1);
		$query->where('media_library.job_status_id', 4);
		
		if(isset($id) && $id > 0)  $query->where('media_library.media_id', $id); 
		if(isset($data['q'])) $query->where('media_library.name', 'LIKE', '%'.trim($data['q']).'%'); 
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id < 1) $query->take($take)->skip($skip);
		
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		if(isset($id) && $id < 1) $query->orderBy($sortBy, $order);
		
		if(isset($id) && $id > 0)
			return $query->first();
		else
			return $query->get();
	}
	
	public static function getPosters($id, $mediaPath)
	{
		$query = DB::table('media_library')->select(DB::raw('media_library_to_posters.poster_id, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS poster_image, media_library_to_posters.poster_type, media_library_to_posters.media_id'));
		//Joins
		$query->join('media_library_to_posters', 'media_library_to_posters.media_id', '=', 'media_library.media_id');
		//Where
		$query->where('media_library_to_posters.media_id', $id);
				
		return $query->get();
	}
	
	public static function getCollections($id, $data)
	{
		$img_path       =  URL::to('assets/data/image');
		$page           =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage    =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy         =  'sort_order';
		$order          =  'desc';
		$take           =   $itemPerPage;
		$skip           =   0;
		
		$query = DB::table('collections')->select(DB::raw('collection_id AS id, collection_name AS title, sort_order AS sort_order,  date_format(created_date, \'%b %d, %Y\') AS created_date, date_format(modified_date, \'%b %d, %Y\') AS modified_date'));
		
		//Where
		$query->where('status', 1);
				
		if(isset($id) && $id > 0)  $query->where('collection_id', $id); 
		if(isset($data['q'])) $query->where('collection_name', 'LIKE', '%'.trim($data['q']).'%'); 
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id < 1) $query->take($take)->skip($skip);
		
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		if(isset($id) && $id < 1) $query->orderBy($sortBy, $order);
		
		if(isset($id) && $id > 0)
			return $query->first();
		else
			return $query->get();
	}
	
	public static function getVideoByCollection($id, $mediaPath) {
		$query = DB::table('collections')->select(DB::raw('media_library.media_id AS id, media_library.name AS title, media_library.meta_tag, media_library.meta_description, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS media_logo, CONCAT("'.$mediaPath.'", media_library.media_name) AS video_link, media_library.media_name AS video_name, media_library.descriptions, CONCAT(media_library.frame_width, " X ", media_library.frame_height) as frame_size, media_library.file_size, "bytes" AS file_size_desc, media_library.job_duration as duration, "ms" AS duration_sdesc, "milliseconds" AS duration_ldesc, media_library.content,  media_library.genre, media_library.rating, "5" AS rating_out_of, media_library.rights, date_format(media_library.release_date, \'%b %d, %Y\') AS release_date, media_library.show_description, media_library.closed_captions, media_library.generic_tags, media_library.similar_content'));
		
		//Joins
		$query->join('media_to_collections', 'collections.collection_id', '=', 'media_to_collections.collection_id');
		$query->join('media_library', 'media_to_collections.media_id', '=', 'media_library.media_id');
		$query->join('media_library_to_posters', 'media_library_to_posters.poster_id', '=', 'media_library.poster_id');
		
		//where
		$query->where('media_to_collections.collection_id', '=', $id);
		$query->where('media_library.status', '=', 1);
		$query->where('media_library.job_status_id', '=', 4);
		
		//order by
		$query->orderBy('media_to_collections.id', 'DESC');
		
		return $query->get();
	}
	
	public static function getVideoByCollectionId($id, $data, $mediaPath) {
		$page         =  isset($data['page']) ? $data['page'] : 1;
		$itemPerPage  =  isset($data['itemPerPage']) ? $data['itemPerPage'] : 10;
		$sortBy       =  'created_date';
		$order        =  'desc';
		$take         =   $itemPerPage;
		$skip         =   0;
		
		$query = DB::table('collections')->select(DB::raw('media_library.media_id AS id, media_library.name AS title, media_library.meta_tag, media_library.meta_description, CONCAT("'.$mediaPath.'", media_library_to_posters.image_name) AS media_logo, CONCAT("'.$mediaPath.'", media_library.media_name) AS video_link, media_library.media_name AS video_name, media_library.descriptions, CONCAT(media_library.frame_width, " X ", media_library.frame_height) as frame_size, media_library.file_size, "bytes" AS file_size_desc, media_library.job_duration as duration, "ms" AS duration_sdesc, "milliseconds" AS duration_ldesc, media_library.content,  media_library.genre, media_library.rating, "5" AS rating_out_of, media_library.rights, date_format(media_library.release_date, \'%b %d, %Y\') AS release_date, media_library.show_description, media_library.closed_captions, media_library.generic_tags, media_library.similar_content'));
		
		//Joins
		$query->join('media_to_collections', 'collections.collection_id', '=', 'media_to_collections.collection_id');
		$query->join('media_library', 'media_to_collections.media_id', '=', 'media_library.media_id');
		$query->join('media_library_to_posters', 'media_library_to_posters.poster_id', '=', 'media_library.poster_id');
		
		//Where
		$query->where('media_to_collections.collection_id', '=', $id);
		$query->where('media_library.status', '=', 1);
		$query->where('media_library.job_status_id', '=', 4);
		
		if(($page > 1)) $skip = ($page * $itemPerPage) - $itemPerPage;
		if(isset($id) && $id < 1) $query->take($take)->skip($skip);
		
		//Order
		if(isset($data['sort'])) { if($data['sort'] == 'title' || $data['sort'] == 'created_date') $sortBy = $data['sort']; }
		if(isset($data['order'])) { if($data['order'] == 'asc' || $data['order'] == 'desc') $order = $data['order']; }
		
		$query->orderBy($sortBy, $order);
		
		return $query->get();
	}
}