<?php

class Update extends BaseModel {

      /**
       * Check Update
       * @return type
       */
      public static function checkUpdate() {
            set_time_limit(0);
            $return = false;

            $updateResponse = json_decode(file_get_contents(Config::get('streamvision.service_provider_url') . "update/cms/" . Setting::get('service-provider-id') . "?key=K72kWVncihXmijxn2EAOlpabKu6-NGee&currentVersion=" . Setting::get('version')));
            if ( $updateResponse->success == true ) {
                  if ( $updateResponse->update == 1 ) {
                        $return = $updateResponse;
                  }
            }

            return $return;
      }

      /**
       * Do Update Action
       * @return boolean
       */
      public static function doUpdate() {
            set_time_limit(0);
            $return = false;

            if ( $updateResponse = self::checkUpdate() ) {
                  self::downloadFile($updateResponse->update_version . ".zip", $updateResponse->update_path);
                  if ( self::extractFile($updateResponse->update_version . ".zip") ) {
                        DatabaseMigration::updateQuery();
                        Setting::set('version', $updateResponse->update_version);
                        self::updateLog($updateResponse->update_version);
                        $return = true;
                  }
            }

            return $return;
      }

      /**
       * Update Log in Control
       */
      public static function updateLog($updateVersion) {
            file_get_contents(Config::get('streamvision.service_provider_url') . "update/cmslog/" . Setting::get('service-provider-id') . "?key=K72kWVncihXmijxn2EAOlpabKu6-NGee&updatedVersion=" . $updateVersion);
      }

      /**
       * Extract downloaded file
       * @param type $fileName
       * @return boolean
       */
      public static function extractFile($fileName) {
            $zip = new ZipArchive;
            if ( $zip->open($fileName) === TRUE ) {
                  $zip->extractTo(Setting::get('root-path'));
                  $zip->close();
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Donwload file from Control
       * @param type $fileName
       * @param type $path
       */
      public static function downloadFile($fileName, $path) {
            file_put_contents($fileName, fopen($path, 'r'));
      }

}
