<?php

class Customers {

      public static function generateCustomerID() {
            $count = DB::table('customers')->max('CustomerID');
            if ( $count <= "10000" ) {
                  $count = "10000";
            }
            return $count + 1;
      }

      /**
       * Check Customer is valid or invalid
       * @param type $id
       * @return boolean
       */
      public static function checkCustomer($id, $password = null) {
            if ( !is_numeric($id) ) {
                  return false;
            }

            $query = DB::table('customers');
            $query->select(DB::raw('count(*) as customer'));
            $query->where('CustomerID', '=', $id);
            $query->where('Status', '=', "1");

            $result = $query->get();

            if ( $result[0]->customer > 0 ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Check customer is active or not
       * @return boolean
       */
      public static function isCustomerActive($id) {
            if ( !is_numeric($id) ) {
                  return false;
            }

            $query = DB::table('customers');
            $query->select(DB::raw('count(*) as customer'));
            $query->where('CustomerID', '=', $id);
            $query->where('Status', '=', "1");
            $result = $query->get();

            if ( $result[0]->customer > 0 ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Insert Keep Alive data
       * @param type $customerID
       * @param type $deviceID
       * @param type $channelID
       */
      public static function addKeepAlive($customerID, $deviceID, $channelID) {
            $keepAliveData = array(
                  'CustomerID' => $customerID,
                  'DeviceID' => $deviceID,
                  'ChannelID' => $channelID,
                  'Date' => date('Y-m-d H:i:s'),
            );

            DB::table('keep_alive')->insert($keepAliveData);
      }

      /**
       * Insert Customer
       * @param type $data
       */
      public static function addCustomer($postData, $action = null) {

            switch ( $action ) {
                  case 'cms':
                        $customerData = array(
                              'CustomerID' => Customers::generateCustomerID(),
                              'FirstName' => $postData['firstName'],
                              'LastName' => $postData['lastName'],
                              'Password' => $postData['password'],
                              'Address1' => $postData['address1'],
                              'Address2' => $postData['address2'],
                              'City' => $postData['city'],
                              'State' => $postData['state'],
                              'Zip' => $postData['zip'],
                              'Phone' => $postData['phone'],
                              'Email' => $postData['email'],
                              'Custom1' => $postData['custom1'],
                              'Custom2' => $postData['custom2'],
                              'Date' => date('Y-m-d H:i:s'),
                              'Status' => $postData['status']
                        );
                        break;
                  default:
                        $customerData = array(
                              'CustomerID' => $postData['CustomerID'],
                              'FirstName' => $postData['FirstName'],
                              'LastName' => $postData['LastName'],
                              'Date' => date('Y-m-d H:i:s'),
                              'Status' => 1
                        );
                        break;
            }

            DB::table('customers')->insert($customerData);
      }

      /**
       * Edit Custom CMS Customer
       * @param type $data
       */
      public static function editCustomer($id, $postData) {
            $customerData = array(
                  'FirstName' => $postData['firstName'],
                  'LastName' => $postData['lastName'],
                  'Password' => $postData['password'],
                  'Address1' => $postData['address1'],
                  'Address2' => $postData['address2'],
                  'City' => $postData['city'],
                  'State' => $postData['state'],
                  'Zip' => $postData['zip'],
                  'Phone' => $postData['phone'],
                  'Email' => $postData['email'],
                  'Custom1' => $postData['custom1'],
                  'Custom2' => $postData['custom2'],
                  'Date' => date('Y-m-d H:i:s'),
                  'Status' => $postData['status']
            );
            DB::table('customers')->where('CustomerID', $id)->update($customerData);
      }

      public static function validateCustomerFormData($postData) {
            $messages = array(
                  'password.required' => PASSWORD_EMPTY,
                  'firstName.required' => FIRSTNAME_EMPTY,
                  'lastName.required' => LASTNAME_EMPTY,
                  'city.required' => CITY_EMPTY,
                  'state.required' => STATE_EMPTY,
                  'zip.required' => ZIP_EMPTY,
                  'email.required' => EMAIL_EMPTY,
            );

            $rules = array(
                  'password' => 'required',
                  'firstName' => 'required',
                  'lastName' => 'required',
                  'city' => 'required',
                  'state' => 'required',
                  'zip' => 'required',
                  'email' => 'required',
            );

            return Validator::make($postData, $rules, $messages);
      }

      /**
       * Get all customers
       * @return object
       */
      public static function getCustomers($filters = NULL, $paginate = NULL) {
            $result = DB::table('customers')->select('*');
            if ( $filters ) {
                  if ( isset($filters['customerID']) ) {
                        $result = $result->where('CustomerID', '=', $filters['customerID']);
                  }
                  if ( isset($filters['firstName']) ) {
                        $result = $result->where('FirstName', 'like', '%' . $filters['firstName'] . '%');
                  }
                  if ( isset($filters['lastName']) ) {
                        $result = $result->where('lastName', 'like', '%' . $filters['lastName'] . '%');
                  }
                  if ( isset($filters['email']) ) {
                        $result = $result->where('Email', 'like', '%' . $filters['email'] . '%');
                  }
            }

            $result->orderBy('CustomerID', 'desc');

            if ( $paginate ) {
                  return $result->paginate($paginate);
            }
            else {
                  return $result->get();
            }
      }

      /**
       * Get customer detail
       * @param type $customerID
       * @return object
       */
      public static function getCustomer($customerID) {
            return DB::table('customers')->select('*')->where('CustomerID', '=', $customerID)->first();
      }

      /**
       * Check whether this customer has DVR enabled.
       * This sends a radius authentification service request.
       * @return boolean
       */
      public static function hasDvrEnabled($customerId) {
            return self::hasDvrBasicService($customerId) === true;
      }

      /**
       * Check whether a specified user has the basic DVR service required for DVR
       * to function. Does a RADIUS request.
       * @param int $customerId
       * @param string $password (optional) doesn't check the password if not provided
       * @return string|boolean true if the user has basic DVR, string error code on failure 
       */
      public static function hasDvrBasicService($customerId, $password = null) {
            $services = CustomerServices::getServices($customerId, 'DVR', 1, Config::get('streamvision.dvr_services.basic'), null, $password);
            if ( $services === false ) {
                  return '2014';
            }

            if ( !CustomerServices::radiusAuthentication($services[0]->Login, $services[0]->Password) ) {
                  return '2013';
            }

            return true;
      }

      /**
       * Check whether a specified user has the whole home DVR service required for DVR
       * to function. Does a RADIUS request.
       * @param int $customerId
       * @param string $password (optional) doesn't check the password if not provided
       * @return string|boolean true if the user has basic DVR, string error code on failure 
       */
      public static function hasDvrWholeHomeService($customerId) {
            $services = CustomerServices::getServices($customerId, 'DVR', 1, Config::get('streamvision.dvr_services.wholeHome'));
            if ( $services === false ) {
                  return false;
            }

            if ( !CustomerServices::radiusAuthentication($services[0]->Login, $services[0]->Password) ) {
                  return false;
            }

            return true;
      }

      /**
       * Create Customer If not exist
       * @param type $data
       */
      public static function ifCreateCustomer($data) {
            if ( !$customer = Customers::checkCustomer($data['CustomerID']) ) {
                  $customerData = array(
                        'CustomerID' => $data['CustomerID'],
                        'FirstName' => $data['FirstName'],
                        'LastName' => $data['LastName'],
                  );
                  self::addCustomer($customerData);
            }
      }

}
