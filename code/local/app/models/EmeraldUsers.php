<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class EmeraldUsers implements UserInterface, RemindableInterface {

      use UserTrait,
          RemindableTrait;

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'emerald_users';

      /**
       * The attributes excluded from the model's JSON form.
       *
       * @var array
       */
      public static function getEmeraldUserData($data) {
            $result = DB::table('emerald_users')->select('*')
                    ->where('1');

            return $result;
      }

}
