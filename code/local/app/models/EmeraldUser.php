<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class EmeraldUser extends Eloquent {

      use SoftDeletingTrait;

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'emerald_users';

      /**
       * Soft delete timestamp
       * @var [type]
       */
      protected $dates = ['deleted_at'];

      /**
       * Fillable fields
       * @var Array
       */
      protected $fillable = ['CustomerID',
            'AccountID',
            'Domain',
            'Login',
            'Password',
            'FirstName',
            'LastName',
            'ServiceType',
            'ExternalRef',
            'Identifier',
            'DeviceID',
            'Active'];

      /**
       * Custom query scope for Customer
       * @param  [type] $query Current query instance
       * @param  [type] $id    Customer ID
       * @return [type]        Current query with the customer scope applied
       */
      public function scopeCustomer($query, $id, $password) {
            return $query->where('CustomerID', $id)->where('Password', $password);
      }

      /**
       * Custom query scope for Account
       * @param  [type] $query Current query instance
       * @param  [type] $id    Account ID
       * @return [type]        Current query with the account scope applied
       */
      public function scopeAccount($query, $id) {
            return $query->where('AccountID', $id);
      }

      /**
       * Custom query scope for Device
       * @param  [type] $query Current query instance
       * @param  [type] $id    Device ID
       * @return [type]        Current query with the device scope applied
       */
      public function scopeDevice($query, $id) {
            return $query->where('DeviceID', $id);
      }

}
