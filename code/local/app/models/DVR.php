<?php

use Dvrapi\Dvrapi;

class DVR extends BaseModel {

      /**
       * Update Message
       * @param type $data
       * @return type
       */
      public static function updateSettings($data) {
            Setting::set('dvr-endpoint', $data['dvr-endpoint']);
            Setting::set('dvr-grace-period', $data['dvr-grace-period']);
      }

      /**
       * Add DVR Storage
       * @param type $customerID
       * @param type $serviceID
       * @return boolean
       */
      public static function addDVRStorage($customerID, $serviceID, $API = NULL, $dbStorage = NULL) {
            if ( $API == "create" ) {
                  $DVRStorage = Dvrapi::DVRStorage('set', $customerID, Setting::get('dvr-startup-storage'));
                  if ( $DVRStorage['status'] == false ) {
                        Dvrapi::DVRStorage('update', $customerID, Setting::get('dvr-startup-storage'));
                  }
            }
            elseif ( $API == "increase_stroage" ) {
                  Dvrapi::DVRStorage('add', $customerID, Setting::get('dvr-additional-storage'));
            }

            if ( $dbStorage ) {
                  if ( $dbStorage == "startup" ) {
                        $storage = Setting::get('dvr-startup-storage');
                  }
                  elseif ( $dbStorage == "additional" ) {
                        $storage = Setting::get('dvr-additional-storage');
                  }

                  if ( self::checkStorage($serviceID) ) {
                        self::clearPendingDate($serviceID);
                  }
                  else {
                        DB::table('dvr_storages')->insert(
                                ['customerServiceId' => $serviceID, 'pemdingDeleteDate' => NULL, 'hours' => $storage]
                        );
                  }
            }
      }

      /**
       * Deactivate DVR Storage
       * @param type $serviceID
       * @return boolean
       */
      public static function deactivateDVRStorage($serviceID) {
            $gracePeriod = Setting::get('dvr-grace-period');
            if ( $gracePeriod > 0 ) {
                  $pemdingDeleteDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+" . $gracePeriod . " days"));
            }
            else {
                  $pemdingDeleteDate = date('Y-m-d H:i:s');
            }
            return DB::table('dvr_storages')->where('customerServiceId', $serviceID)->update(['pemdingDeleteDate' => $pemdingDeleteDate]);
      }

      /**
       * Clear Delete Pending Date
       * @param type $serviceID
       * @return boolean
       */
      public static function clearPendingDate($serviceID) {
            return DB::table('dvr_storages')->where('customerServiceId', $serviceID)->update(['pemdingDeleteDate' => NULL]);
      }

      /**
       * Update Hours
       * @param type $serviceID
       * @return boolean
       */
      public static function updateHours($serviceID, $hours) {
            return DB::table('dvr_storages')->where('customerServiceId', $serviceID)->update(['hours' => $hours]);
      }

      /**
       * Get Hours
       * @param type $serviceID
       * @return type
       */
      public static function getHours($serviceID) {
            $result = DB::table('dvr_storages')->select('hours')->where('customerServiceId', $serviceID)->first();
            return $result->hours;
      }

      /**
       * Get Customer Total DVR Storage
       * @param type $customerID
       * @return String
       */
      public static function getTotalStorage($customerID) {
            $result = DB::select(DB::raw("SELECT Sum(dvr_storages.hours) AS totalHours FROM dvr_storages INNER JOIN customer_services ON customer_services.ServiceID = dvr_storages.customerServiceId WHERE customer_services.CustomerID = " . $customerID));
            return $result[0]->totalHours;
      }

      /**
       * Delete DVR Storage
       * @param type $serviceID
       * @return boolean
       */
      public static function deleteDVRStorage($serviceID, $customerID) {

            //$serviceStorageTime = self::getHours($serviceID);
            //$totalStorage = self::getTotalStorage($customerID);
            //$updateStorage = $totalStorage - $serviceStorageTime;
            //Dvrapi::DVRStorage('update', $customerID, $updateStorage);

            return DB::table('dvr_storages')->where('customerServiceId', '=', $serviceID)->delete();
      }

      /**
       * Check client storage is available
       * @param type $serviceID
       * @return boolean
       */
      public static function checkStorage($serviceID) {
            $result = DB::table('dvr_storages')->select(DB::raw('count(*) as storage'))->where('customerServiceId', '=', $serviceID)->get();
            if ( $result[0]->storage > 0 ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Manage DVR Storage
       * @param type $data
       */
      public static function manageDVRStorage($data) {
            switch ( strtolower($data['Action']) ) {
                  case 'add':
                        self::actionAdd($data);
                        break;
                  case 'update':
                        self::actionUpdate($data);
                        break;
                  case 'delete':
                        self::actionDelete($data);
                        break;
            }
      }

      /**
       * Re-Activate DVR Storage
       * @param type $customerID
       * @param type $serviceID
       * @param type $serviceType
       */
      public static function reActivateDVRStorage($customerID, $serviceID, $serviceType) {
            if ( self::checkStorage($serviceID) ) {
                  self::clearPendingDate($serviceID);
            }
            else {
                  if ( $serviceType == "DVRBasic" ) {

                        $storage = Setting::get('dvr-startup-storage'); //Get Storage for basic
                        //DVR API for Storage
                        $dvrStorage = Dvrapi::DVRStorage('set', $customerID, Setting::get('dvr-startup-storage'));
                        if ( $dvrStorage['status'] == false ) {
                              Dvrapi::DVRStorage('update', $customerID, Setting::get('dvr-startup-storage'));
                        }
                  }
                  elseif ( $serviceType == "DVRAdditional" ) {
                        $storage = Setting::get('dvr-additional-storage'); //Get Storage for additional
                        Dvrapi::DVRStorage('add', $customerID, $storage); //DVR API to add additional space
                  }

                  if ( $serviceType == "DVRBasic" || $serviceType == "DVRAdditional" ) {
                        DB::table('dvr_storages')->insert(
                                ['customerServiceId' => $serviceID, 'pemdingDeleteDate' => NULL, 'hours' => $storage]
                        );
                  }
            }
      }

      /**
       * Get Delete Pending Storage customer services 
       * @return type
       */
      public static function getDeletePendingStorage() {
            return DB::table('dvr_storages')->join('customer_services', 'customer_services.ServiceID', '=', 'dvr_storages.customerServiceId')->select('customer_services.CustomerID', 'dvr_storages.id', 'dvr_storages.customerServiceId', 'dvr_storages.pemdingDeleteDate', 'dvr_storages.hours')->where('pemdingDeleteDate', '<=', date('Y-m-d H:i:s'))->get();
      }

      /**
       * DVR Services on add action
       * @param type $data
       */
      private static function actionAdd($data) {
            $API = NULL;
            $dbStorage = NULL;
            switch ( $data['ServiceType'] ) {
                  case 'DVRBasic':
                        $API = "create";
                        $dbStorage = "startup";
                        break;
                  case 'DVRAdditional':
                        $API = "increase_stroage";
                        $dbStorage = "additional";

                        $services = CustomerServices::getServices($data['CustomerID'], 'DVR', 1, Config::get('streamvision.dvr_services.basic'));
                        if ( $services === false ) {
                              self::sendEmail('emails.dvr.additional', array('customerID' => $data['CustomerID'], 'accountID' => $data['AccountID']), 'Notification: Additional DVR but Basic DVR in not available');
                        }
                        break;
            }
            if ( $data['ServiceType'] == "DVRBasic" || $data['ServiceType'] == "DVRAdditional" ) {
                  self::addDVRStorage($data['CustomerID'], $data['AccountID'], $API, $dbStorage);
            }
      }

      /**
       * DVR Services on update action
       * @param type $data
       */
      private static function actionUpdate($data) {
            if ( $data['Active'] == 0 ) {
                  if ( $data['ServiceType'] == "DVRBasic" ) {

                        //Check Whole Home Service & Additional DVR Service
                        $serviceWholeHome = CustomerServices::getServices($data['CustomerID'], 'DVR', 1, Config::get('streamvision.dvr_services.wholeHome'));
                        $serviceAdditional = CustomerServices::getServices($data['CustomerID'], 'DVR', 1, Config::get('streamvision.dvr_services.additional'));

                        if ( $serviceWholeHome || $serviceAdditional ) {
                              self::sendEmail('emails.dvr.deactivate-basic', array('customerID' => $data['CustomerID'], 'accountID' => $data['AccountID']), 'Notification: Additional DVR but Basic DVR in not available');
                        }
                  }
                  self::deactivateDVRStorage($data['AccountID']);
            }
            else {
                  self::reActivateDVRStorage($data['CustomerID'], $data['AccountID'], $data['ServiceType']);
            }
      }

      /**
       * DVR Services on delete action
       * @param type $data
       */
      private static function actionDelete($data) {
            self::deleteDVRStorage($data['AccountID'], $data['CustomerID']);
      }

      public static function getEvent($customerId, $eventID) {
            $recordedShow = Dvrapi::getRecordShow($customerId);
            foreach ( (array) $recordedShow as $show ) {
                  if ( $show->eventid == $eventID ) {
                        return $show;
                  }
            }
            return false;
      }

      /**
       * Get customer's total storage
       * @param type $customerID
       * @return int
       */
      public static function totalStorage($customerID) {
            if ( $services = CustomerServices::getServices($customerID, "DVR", 1, array(Config::get('streamvision.dvr_services.basic'), Config::get('streamvision.dvr_services.additional'))) ) {
                  $totalServices = count($services);
                  return $totalServices * Setting::get('dvr-startup-storage');
            }
            else {
                  return 0;
            }
      }

}
