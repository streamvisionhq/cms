<?php

/**
 * Database Migration Class
 */
class DatabaseMigration {

    public static function updateQuery() {

        /**
         * Table structure for api_client
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `api_client` (
            `client_id` int(11) NOT NULL AUTO_INCREMENT,
            `first_name` varchar(100) DEFAULT NULL,
            `last_name` varchar(100) DEFAULT NULL,
            `email` varchar(100) DEFAULT NULL,
            `domain_url` varchar(255) DEFAULT NULL,
            `domain_ip` varchar(45) DEFAULT NULL,
            `consumer_key` varchar(100) DEFAULT NULL,
            `consumer_secret` varchar(100) DEFAULT NULL,
            `status` int(11) DEFAULT NULL,
            `date_created` datetime DEFAULT NULL,
            `date_modified` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `modified_by` int(11) DEFAULT NULL,
            PRIMARY KEY (`client_id`),
            KEY `api_fname_inx` (`first_name`),
            KEY `api_lname_inx` (`last_name`),
            KEY `api_email_inx` (`email`)
        ) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC");

        /**
         * Table structure for channels
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `channels` (
            `channel_id` int(11) NOT NULL AUTO_INCREMENT,
            `title` varchar(255) DEFAULT NULL,
            `parent_id` int(11) NOT NULL DEFAULT '0',
            `short_description` text,
            `long_description` text,
            `channel_type` int(11) DEFAULT NULL,
            `path` varchar(255) DEFAULT NULL,
            `callsign` varchar(50) DEFAULT NULL,
            `stream_name` varchar(255) DEFAULT NULL,
            `channel_logo` varchar(255) DEFAULT NULL,
            `pre_roll` tinyint(4) DEFAULT NULL,
            `channel_created` datetime DEFAULT NULL,
            `channel_modified` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `modified_by` int(11) DEFAULT NULL,
            `sort_order` int(11) DEFAULT '0',
            `status` int(11) DEFAULT NULL,
            `channel_created_gmt` datetime DEFAULT NULL,
            `channel_modified_gmt` datetime DEFAULT NULL,
            `titan_channel_id` varchar(255) NOT NULL,
            `emerald_subscription_type` varchar(255) NOT NULL,
            PRIMARY KEY (`channel_id`),
            FULLTEXT KEY `channeltitle_inx` (`title`),
            FULLTEXT KEY `channelsdesc_inx` (`short_description`),
            FULLTEXT KEY `channelldesc_inx` (`long_description`)
        ) ENGINE=MyISAM AUTO_INCREMENT=194 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC");
        
        if ( !Schema::hasColumn('channels', 'free_channel') && !Schema::hasColumn('channels', 'channel_number') ) {
            //Add Columns for custom customers management system scope
            DB::statement("ALTER TABLE customers
                ADD COLUMN `free_channel` tinyint(4) DEFAULT 0 AFTER `emerald_subscription_type`,
                ADD COLUMN `channel_number` varchar(50) DEFAULT NULL AFTER `free_channel`");
        }

        /**
         * Table structure for channel_type
         */
        DB::statement('CREATE TABLE IF NOT EXISTS `channel_type` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(100) DEFAULT NULL,
            PRIMARY KEY (`id`),
            KEY `name_inx` (`name`)
        ) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC');

        DB::insert("INSERT IGNORE INTO `channel_type` VALUES ('1', 'Livestream');");
        DB::insert("INSERT IGNORE INTO `channel_type` VALUES ('2', 'Video on Demand');");
        DB::insert("INSERT IGNORE INTO `channel_type` VALUES ('3', 'Category');");

        /**
         * Table structure for collections
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `collections` (
            `collection_id` int(11) NOT NULL AUTO_INCREMENT,
            `collection_name` varchar(255) DEFAULT NULL,
            `sort_order` int(11) NOT NULL,
            `created_by` int(11) NOT NULL,
            `modified_by` int(11) NOT NULL,
            `created_date` datetime NOT NULL,
            `modified_date` datetime NOT NULL,
            `created_date_gmt` datetime NOT NULL,
            `modified_date_gmt` datetime NOT NULL,
            `status` int(11) DEFAULT NULL,
            PRIMARY KEY (`collection_id`),
            KEY `collectionname_inx` (`collection_name`)
        ) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC");

        DB::insert("INSERT IGNORE INTO `collections` VALUES ('1', 'Uncategorized', '3', '0', '0', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '1')");
        DB::insert("INSERT IGNORE INTO `collections` VALUES ('15', 'Streamvision Help Videos', '6', '1', '0', '2015-03-25 23:14:18', '0000-00-00 00:00:00', '2015-03-25 23:14:18', '0000-00-00 00:00:00', '1')");
        DB::insert("INSERT IGNORE INTO `collections` VALUES ('16', 'Streamvision Tutorials', '1', '1', '0', '2015-03-25 23:14:37', '0000-00-00 00:00:00', '2015-03-25 23:14:37', '0000-00-00 00:00:00', '1')");
        DB::insert("INSERT IGNORE INTO `collections` VALUES ('14', 'Commercial Spots', '5', '1', '0', '2015-03-25 23:11:45', '0000-00-00 00:00:00', '2015-03-25 23:11:45', '0000-00-00 00:00:00', '1')");
        DB::insert("INSERT IGNORE INTO `collections` VALUES ('13', 'Hospital Promo Videos', '2', '1', '0', '2015-03-25 23:11:32', '0000-00-00 00:00:00', '2015-03-25 23:11:32', '0000-00-00 00:00:00', '1')");
        DB::insert("INSERT IGNORE INTO `collections` VALUES ('12', 'Casa Grande Community', '4', '1', '0', '2015-03-25 23:11:20', '0000-00-00 00:00:00', '2015-03-25 23:11:20', '0000-00-00 00:00:00', '1')");

        /**
         * Table structure for customers
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `customers` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `CustomerID` int(11) NOT NULL,
            `FirstName` varchar(255) DEFAULT NULL,
            `LastName` varchar(255) DEFAULT NULL,
            `Date` datetime DEFAULT NULL,
            PRIMARY KEY (`ID`,`CustomerID`)
        ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1");

        if ( !Schema::hasColumn('customers', 'Password') && !Schema::hasColumn('customers', 'Address1') && !Schema::hasColumn('customers', 'Address2') && !Schema::hasColumn('customers', 'City') && !Schema::hasColumn('customers', 'State') && !Schema::hasColumn('customers', 'Zip') && !Schema::hasColumn('customers', 'Phone') && !Schema::hasColumn('customers', 'Email') && !Schema::hasColumn('customers', 'Custom1') && !Schema::hasColumn('customers', 'Custom2') && !Schema::hasColumn('customers', 'Status') ) {
            //Add Columns for custom customers management system scope
            DB::statement("ALTER TABLE customers
                ADD COLUMN `Password` varchar(255) AFTER `LastName`,
                ADD COLUMN `Address1` text AFTER `Password`,
                ADD COLUMN `Address2` text AFTER `Address1`,
                ADD COLUMN `City` varchar(70) AFTER `Address2`,
                ADD COLUMN `State` varchar(70) AFTER `City`,
                ADD COLUMN `Zip` varchar(70) AFTER `State`,
                ADD COLUMN `Phone` varchar(30) AFTER `Zip`,
                ADD COLUMN `Email` varchar(255) AFTER `Phone`,
                ADD COLUMN `Custom1` varchar(255) AFTER `Email`,
                ADD COLUMN `Custom2` varchar(255) AFTER `Custom1`,
                ADD COLUMN `Status` INT(1) DEFAULT '1' AFTER `Date`");
        }

        /**
         * Table structure for customer_services
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `customer_services` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `CustomerID` int(11) DEFAULT NULL,
            `ServiceID` int(11) NOT NULL,
            `ServiceModule` varchar(255) DEFAULT NULL,
            `ServiceName` varchar(255) DEFAULT NULL,
            `Login` varchar(255) DEFAULT NULL,
            `Password` varchar(255) DEFAULT NULL,
            `Values` text,
            `Active` int(1) DEFAULT NULL,
            `CreatedDate` datetime DEFAULT NULL,
            `UpdatedDate` datetime DEFAULT NULL,
            PRIMARY KEY (`ID`,`ServiceID`)
        ) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1");

        if ( !Schema::hasColumn('customer_services', 'LastBillDate') ) {
            //Add Columns for last bill date
            Schema::table('customer_services', function($table) {
                $table->dateTime('LastBillDate')->after('Active')->nullable();
            });
            DB::statement('UPDATE customer_services SET LastBillDate = "' . date("Y-m-d H:i:s") . '" WHERE ID != ""');
        }

        /**
         * Table structure for dvr_storages
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `dvr_storages` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `customerServiceId` int(11) NOT NULL,
            `pemdingDeleteDate` datetime DEFAULT NULL,
            `hours` int(11) NOT NULL DEFAULT '0',
            PRIMARY KEY (`id`),
            KEY `customerServiceId` (`customerServiceId`)
        ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8");

        /**
         * Table structure for epg
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `epg` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

        /**
         * Table structure for epg_history
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `epg_history` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `epgdata` longtext COLLATE utf8_unicode_ci NOT NULL,
            `channelId` int(11) NOT NULL,
            `channelNumber` int(11) NOT NULL,
            `majorChannelNumber` int(11) NOT NULL,
            `minorChannelNumber` int(11) NOT NULL,
            `callsign` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `network` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `broadcastType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

        /**
         * Table structure for job_status
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `job_status` (
            `status_id` int(11) NOT NULL AUTO_INCREMENT,
            `status_desc` varchar(45) DEFAULT NULL,
            `status_color` char(7) NOT NULL,
            PRIMARY KEY (`status_id`),
            KEY `jstatusdesc_inx` (`status_desc`)
        ) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC");

        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('1', 'pending', '#1aa8b4')");
        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('2', 'waiting', '#1aa8b4')");
        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('3', 'processing', '#e46807')");
        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('4', 'success', '#7bc168')");
        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('5', 'fail', '#e25855')");
        DB::insert("INSERT IGNORE INTO `job_status` VALUES ('6', 'cancelled', '#ad5ba5')");

        /**
         * Table structure for logs
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `logs` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `CustomerID` int(11) DEFAULT NULL,
            `Data` text,
            `Date` datetime DEFAULT NULL,
            PRIMARY KEY (`ID`)
        ) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1");

        /**
         * Table structure for media_library
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `media_library` (
            `media_id` int(11) NOT NULL AUTO_INCREMENT,
            `name` varchar(255) DEFAULT NULL,
            `meta_tag` text,
            `meta_description` text,
            `poster_id` int(11) DEFAULT NULL,
            `media_name` varchar(255) DEFAULT NULL,
            `descriptions` text NOT NULL,
            `frame_width` int(11) NOT NULL,
            `frame_height` int(11) NOT NULL,
            `file_size` int(11) NOT NULL,
            `content` text NOT NULL,
            `genre` text NOT NULL,
            `rating` float(5,1) NOT NULL,
            `rights` varchar(255) NOT NULL,
            `release_date` date NOT NULL,
            `show_description` text NOT NULL,
            `closed_captions` text NOT NULL,
            `generic_tags` text NOT NULL,
            `similar_content` text NOT NULL,
            `job_id` varchar(32) NOT NULL,
            `job_status_id` int(11) NOT NULL,
            `job_duration` int(11) NOT NULL,
            `created_by` int(11) DEFAULT NULL,
            `modified_by` int(11) DEFAULT NULL,
            `created_date` datetime DEFAULT NULL,
            `modified_date` datetime DEFAULT NULL,
            `created_date_gmt` datetime DEFAULT NULL,
            `modified_date_gmt` datetime DEFAULT NULL,
            `status` int(11) DEFAULT NULL,
            PRIMARY KEY (`media_id`),
            KEY `medianame_inx` (`name`)
        ) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC");

        /**
         * Table structure for media_library_to_posters
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `media_library_to_posters` (
            `poster_id` int(11) NOT NULL AUTO_INCREMENT,
            `media_id` int(11) NOT NULL,
            `image_name` varchar(255) DEFAULT NULL,
            `poster_type` varchar(255) NOT NULL,
            PRIMARY KEY (`poster_id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8");

        /**
         * Table structure for media_to_channels
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `media_to_channels` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `media_id` int(11) DEFAULT NULL,
            `channel_id` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED");

        /**
         * Table structure for media_to_collections
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `media_to_collections` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `media_id` int(11) DEFAULT NULL,
            `collection_id` int(11) DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED");

        /**
         * Table structure for messages
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `messages` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `ErrorCode` int(11) NOT NULL,
            `Message` text,
            `Description` text,
            `Type` varchar(20) DEFAULT NULL,
            PRIMARY KEY (`ID`,`ErrorCode`)
        ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1");

        DB::insert("INSERT IGNORE INTO `messages` VALUES ('1', '2006', 'Authentication Failed', 'This error will occur Roku Radius authentication failed', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('2', '2003', 'Invalid Customer ID or Password', 'This error will occur on entering Invalid Customer ID & Password', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('3', '2002', 'Invalid Customer ID', 'This error will occur on Invalid Customer IDs', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('4', '2005', 'Authentication Failed ', 'This error will occur Roku Service not found in database', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('5', '2004', 'Invalid Device ID', 'This error will occur on Roku Device ID is not available in database during channel authorization', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('6', '2007', 'Channel Access Denied ', 'This error will occur when user tried to access those channels which are not included on subscribed Package.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('7', '2008', 'Unknown error', 'This error will occur on PHP fatol error or request error strike', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('8', '2009', 'Authentication Failed', 'This error will occur when some customer tried to use that roku device which is already registered with some other customer.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('9', '2010', 'Error in deleting roku device', 'Database Error on deleting Roku device', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('11', '2011', 'No channel found', 'This error will occur when no channal  is  added on CMS', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('12', '2012', 'No Shows Found', 'This error will occur when  Channals program data not found on EPG penal  \r\n', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('13', '2013', 'Authentication Failed', 'This error will occur when DVR Radius authentication failed', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('14', '2014', 'Authentication Failed ', 'This error will occur when DVR service not found in database', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('15', '2015', 'DVR Access Denied', 'This error will occur when DVR Whole Home Service is not available and stream request from non default device', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('15', '2015', 'DVR Access Denied', 'This error will occur when DVR Whole Home Service is not available and stream request from non default device', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('16', '2016', 'Roku Device Inactive', 'This error will occur when Roku Device is inactive', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('17', '2017', 'Server Error', 'This error will occur when No recorded event was found on DVR', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('18', '2018', 'Server Error', 'This error will occur when radius seetings are missing on CMS', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('19', '2001', 'Server Error', 'This error will occur when some connection problem', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('20', '2019', 'Server Error', 'This error will occur when channel is inactive on CMS', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('21', '2020', 'Server Error', 'This error will occur when nimble mode setting is missing', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('22', '2021', 'Server Error', 'This error will occur when live nimble streaming or dvr host setting is missing', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('23', '2022', 'Server Error', 'This error will occur when backup nimble streaming or dvr host setting is missing', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('24', '2023', 'Server Error', 'This error will occur when customer management setting is missing', 'error')");

        DB::insert("INSERT IGNORE INTO `messages` VALUES ('25', '2024', 'DVR Error', 'This error will occur when Show Recording could not be deleted successfully from DVR.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('26', '2025', 'DVR Error', 'This error will occur when DVR could not add or update DVR request.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('27', '2026', 'DVR Error', 'This error will occur due to DVR Storage data could not be fetched.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('28', '2027', 'EPG Error', 'This error will occur when EPG Data could not be fetched from the EPG Guide.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('29', '2028', 'EPG Error', 'This error will occur when EPG Guide not found.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('30', '2029', 'EPG Error', 'This error will occur when EPG Guide File could not be parsed.', 'error')");
        DB::insert("INSERT IGNORE INTO `messages` VALUES ('31', '2030', 'Program Not Found', 'Currently, there is no show configured for this channel.', 'error')");

        /**
         * Table structure for roku_devices
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `roku_devices` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `CustomerID` int(11) DEFAULT NULL,
            `DeviceID` varchar(255) NOT NULL,
            `Identifier` varchar(255) DEFAULT NULL,
            `Default` int(1) DEFAULT NULL,
            `Status` int(1) DEFAULT NULL,
            `Date` datetime DEFAULT NULL,
            PRIMARY KEY (`ID`)
        ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1");

        /**
         * Table structure for services
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `services` (
            `ID` int(11) NOT NULL AUTO_INCREMENT,
            `Title` varchar(255) DEFAULT NULL,
            `InternalName` varchar(255) DEFAULT NULL,
            `ExternalName` varchar(255) DEFAULT NULL,
            `InternalRef` varchar(255) DEFAULT NULL,
            `ExternalRef` varchar(255) DEFAULT NULL,
            PRIMARY KEY (`ID`)
        ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1");

        DB::insert("INSERT IGNORE INTO `services` VALUES ('1', 'StreamVisionSelect', 'StreamVisionSelect', 'StreamVisionSelect', 'Roku', 'cms02', '1')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('2', 'StreamVisionHispanic', 'StreamVisionHispanic', 'StreamVisionHispanic', 'Roku', 'cms02', '1')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('3', 'StreamVisionChoice', 'StreamVisionChoice', 'StreamVisionChoice', 'Roku', 'cms02', '1')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('4', 'StreamVisionChoicePlus', 'StreamVisionChoicePlus', 'StreamVisionChoicePlus', 'Roku', 'cms02', '1')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('5', 'StreamVisionFreeTV', 'StreamVisionFreeTV', 'StreamVisionFreeTV', 'Roku', 'cms02', '1')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('6', 'Additional Roku', 'AdditionalRoku', 'Additional Roku', 'Roku', 'cms02', '0')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('7', 'DVR Basic', 'DVRBasic', 'DVR Basic', 'Roku', 'cms02', '0')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('8', 'DVR Whole Home', 'DVRWholeHome', 'DVR Whole Home', 'DVR', 'cms02', '0')");
        DB::insert("INSERT IGNORE INTO `services` VALUES ('9', 'DVR Additional', 'DVRAdditional', 'DVR Storage - 100 hrs Adt\'l', 'DVR', 'cms02', '0')");

        /**
         * Table structure for settings
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `settings` (
            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
            `value` text COLLATE utf8_unicode_ci NOT NULL,
            PRIMARY KEY (`id`),
            KEY `settings_key_index` (`key`)
        ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");

        self::checkUpdateKeyValue("cm_mode", "");
        self::checkUpdateKeyValue("nimble_stream_backup", "");
        self::checkUpdateKeyValue("nimble_dvr_backup", "");
        self::checkUpdateKeyValue("nimble_stream", "");
        self::checkUpdateKeyValue("nimble_dvr", "");
        self::checkUpdateKeyValue("nimble_port", "8081");
        self::checkUpdateKeyValue("dvr-endpoint", "");
        self::checkUpdateKeyValue("dvr-retention", "0");
        self::checkUpdateKeyValue("dvr-startup-storage", "360000");
        self::checkUpdateKeyValue("dvr-additional-storage", "360000");
        self::checkUpdateKeyValue("dvr-grace-period", "3");
        self::checkUpdateKeyValue("admin-email", "info@streamvisiontv.com");
        self::checkUpdateKeyValue("admin-name", "Stream Vision");
        self::checkUpdateKeyValue("version", "1.1");
        self::checkUpdateKeyValue("service-provider-id", "");
        self::checkUpdateKeyValue("root-path", "");
        self::checkUpdateKeyValue("radius-server-ip", "");
        self::checkUpdateKeyValue("radius-server-host", "");
        self::checkUpdateKeyValue("radius-shared-secret", "");

        /**
         * Table structure for users
         */
        DB::statement("CREATE TABLE IF NOT EXISTS `users` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `firstname` varchar(255) NOT NULL,
            `lastname` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `username` varchar(100) NOT NULL,
            `password` varchar(100) NOT NULL,
            `remember_token` varchar(100) NOT NULL,
            `status` int(11) NOT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8");

        DB::insert("INSERT IGNORE INTO `users` VALUES ('1', 'Stream', 'Vision TV', 'info@prolivestream.com', 'admin', '$2y$10$5yZSLxPACSY.5RB3ajUd1.X4qHUZqEFI8W32UYfGuTvWUvXHarjhq', 'uUni8nfPXsIMY4yuVRSirazhgL7RoaC7OT4Gf3URRcqajhQ1HReUoHYEQPVo', '1')");

        DB::statement("CREATE TABLE IF NOT EXISTS `log4php_log` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `timestamp` datetime DEFAULT NULL,
            `logger` varchar(256) DEFAULT NULL,
            `level` varchar(32) DEFAULT NULL,
            `message` varchar(4000) DEFAULT NULL,
            `thread` int(11) DEFAULT NULL,
            `file` varchar(255) DEFAULT NULL,
            `line` varchar(10) DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        DB::statement("CREATE TABLE IF NOT EXISTS `entity_updates` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `entity` varchar(255) DEFAULT NULL,
            `last_modified` datetime DEFAULT NULL,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    }

    public static function checkUpdateKeyValue($key, $value) {
        $result = DB::table("settings")->select('*')->where("key", $key);
        if ( $result->count() > 0 ) {
            return true;
        }
        else {
            DB::table("settings")->insert(["key" => $key, "value" => $value]);
        }
    }

}
