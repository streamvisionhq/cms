<?php

class RokuDevices {

      /**
       * Get Roku Devices By Customer ID
       * @param [int] $customerID
       * @return object
       */
      public static function getDevicesByCustomerID($customerID, $paginate = NULL, $status = NULL, $except = NULL) {
            $result = DB::table('roku_devices')->select('DeviceID', 'Identifier', 'Date', 'Default')->where('CustomerID', $customerID);
            if ( $status ) {
                  $result = $result->where('Status', 1);
            }
            if ( $except ) {
                  $result = $result->where('DeviceID', '<>', $except);
            }
            if ( $paginate ) {
                  return $result->paginate($paginate);
            }
            else {
                  return $result->get();
            }
      }

      /**
       * Get Device Details By Device ID
       * @param [string] $deviceID
       * @return object
       */
      public static function getDevice($deviceID, $default = NULL) {
            return DB::table('roku_devices')->select('*')->where('DeviceID', $deviceID)->first();
      }

      /**
       * Get device detail of customer
       * @param [string] $deviceID
       * @param [int] $customerID
       * @return boolean
       */
      public static function customerDevice($deviceID, $customerID) {
            $result = DB::table('roku_devices')->select('CustomerID', 'Identifier', 'Status')->where('DeviceID', '=', $deviceID)->where('CustomerID', '=', $customerID);
            if ( $result->count() > 0 ) {
                  return $result->first();
            }
            else {
                  return false;
            }
      }

      /**
       * Check roku device is available or not
       * @param [string] $deviceID
       * @param [int] $customerID
       * @return boolean
       */
      public static function checkDevice($deviceID, $status = NULL) {
            $result = DB::table('roku_devices')->select(DB::raw('count(*) as device'))->where('DeviceID', '=', $deviceID);
            if ( $status ) {
                  $result = $result->where('Status', '=', $status);
            }
            $result = $result->get();
            if ( $result[0]->device > 0 ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Get total number of roku devices of customer
       * @param type $customerID
       * @return [int]
       */
      public static function countRokuDevices($customerID, $status = NULL) {
            $result = DB::table('roku_devices')->select(DB::raw('count(*) as devices'))->where('CustomerID', '=', $customerID);
            if ( $status ) {
                  $result = $result->where('Status', '=', $status);
            }
            $result = $result->get();
            return $result[0]->devices;
      }

      /**
       * Add roku device
       * @param type $customerID
       * @param type $deviceID
       * @param type $identifier
       */
      public static function addRokuDevice($customerID, $deviceID, $identifier) {
            $identifier = str_replace("_", " ", $identifier);
            $default = NULL;

            if ( self::countRokuDevices($customerID) == 0 ) {
                  $default = 1;
            }

            DB::table('roku_devices')->insert(
                    ['CustomerID' => $customerID, 'DeviceID' => $deviceID, 'Identifier' => $identifier, 'Default' => $default, 'Status' => 1, 'Date' => date('Y-m-d H:i:s')]
            );
      }

      /**
       * Check total number of roku devices is less then or equal to total number of roku services of customer
       * @param type $customerID
       * @return boolean
       */
      public static function isRokuDeviceService($customerID) {
            //Get total customer roku services                    
            $totalRokuServices = CustomerServices::countRokuServices($customerID, "Roku");
            //Get total customer roku devices
            $totalRokuDevices = RokuDevices::countRokuDevices($customerID, 1);
            if ( $totalRokuDevices < $totalRokuServices ) {
                  return array('status' => true, 'addRoku' => 1, 'totalServices' => "$totalRokuServices", 'totalDevices' => "$totalRokuDevices");
            }
            elseif ( $totalRokuDevices == $totalRokuServices ) {
                  return array('status' => true, 'addRoku' => 0, 'totalServices' => "$totalRokuServices", 'totalDevices' => "$totalRokuDevices");
            }
            else {
                  return array('status' => false, 'addRoku' => 0, 'totalServices' => "$totalRokuServices", 'totalDevices' => "$totalRokuDevices");
            }
      }

      /**
       * Delete roku device
       * @param type $deviceID
       * @return boolean
       */
      public static function deleteDevice($deviceID) {
            if ( $device = self::getDevice($deviceID) ) {
                  $delete = DB::table('roku_devices')->where('DeviceID', '=', $deviceID)->delete();
                  if ( $device->Default == 1 ) {
                        $updateDevice = DB::table('roku_devices')->select('DeviceID', 'CustomerID')->orderBy('Date', 'desc')->take(1)->first();
                        if ( !empty($updateDevice) ) {
                              self::markDefault($updateDevice->DeviceID, $updateDevice->CustomerID);
                              $delete = $updateDevice->DeviceID;
                        }
                  }
                  return $delete;
            }
      }

      /**
       * Mark Default Device
       * @param type $deviceID
       * @param type $customerID
       */
      public static function markDefault($deviceID, $customerID) {
            DB::table('roku_devices')->where('CustomerID', $customerID)->update(['Default' => NULL]);
            return DB::table('roku_devices')->where('DeviceID', $deviceID)->update(['Default' => 1]);
      }

      /**
       * Change Roku Device Status
       * @param type $deviceID
       * @param type $status
       * @return type
       */
      public static function changeStatus($deviceID, $status) {
            return DB::table('roku_devices')->where('DeviceID', $deviceID)->update(['Status' => $status]);
      }

      /**
       * Get Devices
       * @param type $paginate
       * @param type $filters
       * @return object
       */
      public static function getDevices($paginate = NULL, $filters = NULL) {
            $result = DB::table('roku_devices')->select('*');
            if ( $filters ) {
                  if ( isset($filters['deviceID']) ) {
                        $result = $result->where('DeviceID', '=', $filters['deviceID']);
                  }
                  if ( isset($filters['identifier']) ) {
                        $result = $result->where('Identifier', 'like', '%' . $filters['identifier'] . '%');
                  }
                  if ( isset($filters['customerID']) ) {
                        $result = $result->where('CustomerID', '=', $filters['customerID']);
                  }
            }
            if ( $paginate ) {
                  return $result->paginate($paginate);
            }
            else {
                  return $result->get();
            }
      }

      /**
       * Get Default Device
       * @param type $customerID
       * @return Object
       */
      public static function getDefaultDevice($customerID) {
            return DB::table('roku_devices')->select('*')->where('CustomerID', $customerID)->where('Default', 1)->first();
      }

      /**
       * Delete all roku devices of customer
       * @param type $customerID
       * @return Boolean
       */
      public static function deleteDevices($customerID) {
            return DB::table('roku_devices')->where('CustomerID', '=', $customerID)->delete();
      }

}
