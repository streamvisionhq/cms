<?php

class BaseModel {

    /**
     * Send Email
     * @param type $path
     * @param type $data
     * @param type $subject
     */
    public static function sendEmail($path, $data, $subject) {

        $sendingDetail = array(
              'email' => Setting::get('admin-email'),
              'name' => Setting::get('admin-name'),
              'subject' => $subject
        );

        return Mail::send($path, $data, function($message) use ($sendingDetail) {
                    $message->to($sendingDetail['email'], $sendingDetail['name'])->subject($sendingDetail['subject']);
                });
    }

    public static function getCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function postCurl($url, $fields) {
        $fields_string = NULL;
        $fields = $fields["Billing"];
        if ( $fields ) {
            foreach ( $fields as $key => $value ) {
                foreach ( $value as $k => $val ) {
                    $fields_string .= "Billing[" . $key . "][" . $k . "]=" . $val . '&';
                }
            }
            rtrim($fields_string, '&');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ( $fields ) {
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function update_last_modified($entity, $last_modified) {

        $last_modified = date("Y-m-d H:i:s", strtotime($last_modified));

        // Check first if entity exists
        $result = DB::table('entity_updates')->select('*')->where('entity', $entity)->first();

        if ( !$result ) {
            DB::table('entity_updates')->insert(array('entity' => $entity, 'last_modified' => $last_modified));
        }
        else {
            DB::table('entity_updates')->where('entity', $entity)->update(array('last_modified' => $last_modified));
        }
    }

    public static function get_last_modified($entity) {
        $result = DB::table('entity_updates')->select('*')->where('entity', $entity)->first();
        if ( $result ) {
            return $result->last_modified;
        }
        else {
            $last_modified = date("Y-m-d H:i:s");
            DB::table('entity_updates')->insert(array('entity' => $entity, 'last_modified' => $last_modified));
            return $last_modified;
        }
    }

}
