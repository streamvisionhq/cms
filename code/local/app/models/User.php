<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User implements UserInterface, RemindableInterface {

      use UserTrait,
          RemindableTrait;

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'users';

      /**
       * The attributes excluded from the model's JSON form.
       *
       * @var array
       */
      protected $hidden = array('password', 'remember_token');

      public static function validateUserData($udata, $type) {
            switch ( $type ) {
                  case 'byemail':
                        $result = DB::table('users')->select('id')
                                ->where('email', $udata['email'])
                                ->first();
                        break;
            }

            return $result;
      }

      public static function getUserData($uid) {
            $result = DB::table('users')->select('*')
                    ->where('id', $uid)
                    ->first();
            return $result;
      }

      public static function updateUserData($postData, $id) {
            DB::table('users')->where('id', $id)
                    ->update(array('firstname' => $postData['firstname'],
                          'lastname' => $postData['lastname'],
                          'email' => $postData['email'],
                          'username' => $postData['username'],
            ));
            if ( isset($postData['serviceProviderID']) ) {
                  Setting::set('service-provider-id', $postData['serviceProviderID']);
            }
            Setting::set('root-path', $postData['rootPath']);
            Setting::set('root-url', $postData['rootURL']);
            Setting::set('cms-name', $postData['cmsName']);
            return true;
      }

      public static function updateUserPassword($password, $id) {
            DB::table('users')->where('id', $id)
                    ->update(array('password' => $password));
            return true;
      }

}
