<?php

class Messages {

      /**
       * Get message from error code
       * @param type $errorCode
       * @return string
       */
      public static function getMessage($errorCode) {
            $result = DB::table('messages')->select('Message')->where('ErrorCode', $errorCode);
            if ( $result->count() > 0 ) {
                  $result = $result->first();
                  return $result->Message;
            }
      }

      /**
       * Get all messages
       * @return object
       */
      public static function getMessages() {
            return $result = DB::table('messages')->select('*')->orderBy('ErrorCode', 'asc')->get();
      }

      /**
       * Get message detail from error code
       * @param type $errorCode
       * @return object
       */
      public static function getMessageDetail($errorCode) {
            return DB::table('messages')->select('*')->where('ErrorCode', $errorCode)->first();
      }

      /**
       * Update Message
       * @param type $data
       * @return type
       */
      public static function updateMessage($data) {
            return DB::table('messages')->where('ErrorCode', $data['errorCode'])->update(['Message' => $data['message'], 'Description' => $data['description'], 'Type' => $data['type']]);
      }

}
