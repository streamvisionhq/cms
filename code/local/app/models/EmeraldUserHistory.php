<?php

class EmeraldUserHistory extends Eloquent {

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'emerald_users_history';

}
