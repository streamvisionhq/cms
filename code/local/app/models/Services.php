<?php

class Services {

    /**
     * Get service detail
     * @param type $id
     * @return object
     */
    public static function getService($ID) {
        $result = DB::table('services')->select('*')->where('ID', $ID);
        if ($result->count() > 0) {
            return $result->first();
        }
    }

    /**
     * Get all services
     * @return object
     */
    public static function getServices() {
        return $result = DB::table('services')->select('*')->orderBy('InternalRef', 'asc')->get();
    }

    /**
     * Update Service
     * @param type $data
     * @return boolean
     */
    public static function updateService($ID, $data) {
        return DB::table('services')->where('ID', $ID)->update(['Title' => $data['title'], 'ExternalName' => $data['externalName'], 'ExternalRef' => $data['externalRef']]);
    }

    /**
     * Add Service
     * @param type $data
     * @return boolean
     */
    public static function addService($post) {
        $data = [
            'Title' => $post['title'],
            'InternalName' => $post['internalName'],
            'ExternalName' => $post['externalName'],
            'InternalRef' => $post['internalRef'],
            'ExternalRef' => $post['externalRef'],
        ];
        return DB::table('services')->insert($data);
    }

    /**
     * 
     * @param type $externalName
     * @param type $externalRef
     * @return type
     */
    public static function getInternalData($externalName, $externalRef) {
        return DB::table('services')->select('*')->where('ExternalName', $externalName)->where('ExternalRef', $externalRef)->first();
    }

    /**
     * Get all basic tier like Basic,Bronze,Silver,Gold & Platinum
     * @return object
     */
    public static function getallServices($reference) {
        $result = DB::table('services')->select('*');        
        switch ($reference) {
            case 'Roku':
                $result = $result->where('ChannelService', '=', 1);
                break;
            case 'DVRBasic':
                $result = $result->where('InternalName', '=', DVR_BASIC)->orWhere('InternalName', '=', ADDITIONAL_ROKU);
                break;
            case 'DVRWholeHome':
                $result = $result->where('InternalName', '=', ADDITIONAL_ROKU)->orWhere('InternalName', '=', ADDITIONAL_DVR)->orWhere('InternalName', '=', DVR_WHOLE_HOME);
                break;
            case 'Others':
                $result = $result->where('InternalName', '=', ADDITIONAL_ROKU)->orWhere('InternalName', '=', ADDITIONAL_DVR);
                break;
            default:
                break;
        }

        return $result->get();
    }
    
    public static function getChannelServices() {
        $services = [];
        $result = DB::table('services')->select('*')->where('ChannelService', '=', 1)->get();
        if($result){
            foreach ($result as $key => $value) {
                array_push($services, $value->InternalName);
            }
        }
        return $services;
    }

}
