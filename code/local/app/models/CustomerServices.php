<?php

class CustomerServices {

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'customer_services';

      /**
       * Get Services
       * @param [int] $customerID
       * @param [string] $serviceModule
       * @param [int] $Active
       * @param [string] $serviceName
       * @return object
       */
      public static function getServices($customerID, $serviceModule, $Active = 1, $serviceName = "all", $userName = NULL, $password = NULL) {

            if ( Customers::isCustomerActive($customerID) ) {

                  $result = DB::table('customer_services')->select('*')->where('CustomerID', $customerID)->where('ServiceModule', $serviceModule)->where('Active', $Active);

                  if ( $serviceName != "all" ) {
                        if ( is_array($serviceName) ) {
                              $result = $result->whereIn('ServiceName', $serviceName);
                        }
                        else {
                              $result = $result->where('ServiceName', $serviceName);
                        }
                  }

                  if ( $userName ) {
                        $result = $result->where('Login', $userName);
                  }

                  if ( $password ) {
                        $result = $result->where('Password', $password);
                  }
                  if ( $result->count() > 0 ) {
                        return $result->get();
                  }
                  else {
                        return false;
                  }
            }
            else {
                  return false;
            }
      }

      /**
       * Get total number of roku services of customer
       * @param type $customerID
       * @param type $serviceModule
       * @return Int
       */
      public static function countRokuServices($customerID, $serviceModule) {
            $result = DB::table('customer_services')->select(DB::raw('count(*) as services'))->where('ServiceModule', '=', $serviceModule)->where('CustomerID', '=', $customerID)->where('Active', '=', 1)->get();
            return $result[0]->services;
      }

      /**
       * Radius Service Authentication with Emerald
       * @param type $login
       * @param type $password
       * @return boolean
       */
      public static function radiusAuthentication($login, $password) {
            if ( Setting::get('cm_mode') == "cms" ) {
                  return true;
            }
            $radius = new Radius(Setting::get('radius-server-ip'), Setting::get('radius-shared-secret'));
            $result = $radius->AccessRequest($login, $password);
            if ( $result ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Add Customer Service
       * @param type $data
       * @return boolean
       */
      public static function addService($data) {
            return DB::table('customer_services')->insert(
                            ['CustomerID' => $data['CustomerID'], 'ServiceID' => $data['AccountID'], 'ServiceModule' => $data['ExternalRef'], 'ServiceName' => $data['ServiceType'], 'Login' => $data['Login'], 'Password' => $data['Password'], 'Values' => json_encode($data), 'Active' => $data['Active'], 'LastBillDate' => NULL, 'CreatedDate' => date('Y-m-d H:i:s'), 'UpdatedDate' => date('Y-m-d H:i:s')]
            );
      }

      /**
       * Update Customer Service
       * @param type $data
       * @return boolean
       */
      public static function updateService($data) {
            return DB::table('customer_services')
                            ->where('CustomerID', $data['CustomerID'])
                            ->where('ServiceID', $data['AccountID'])
                            ->update(['ServiceID' => $data['AccountID'], 'ServiceModule' => $data['ExternalRef'], 'ServiceName' => $data['ServiceType'], 'Login' => $data['Login'], 'Password' => $data['Password'], 'Values' => json_encode($data), 'Active' => $data['Active'], 'UpdatedDate' => date('Y-m-d H:i:s')]);
      }

      public static function updateServiceStatus($id, $status) {
            DB::table('customer_services')->where('ServiceID', $id)->update(['Active' => $status]);
      }

      /**
       * Delete Customer Service
       * @param type $customerID
       * @param type $serviceID
       * @return boolean
       */
      public static function deleteService($customerID, $serviceID) {
            return DB::table('customer_services')->where('CustomerID', '=', $customerID)->where('ServiceID', '=', $serviceID)->delete();
      }

      /**
       * Check customer service is available or not
       * @param type $serviceID
       * @param type $customerID
       * @return boolean
       */
      public static function checkService($serviceID, $customerID) {
            $result = DB::table('customer_services')->select(DB::raw('count(*) as service'))->where('ServiceID', '=', $serviceID)->where('CustomerID', '=', $customerID)->get();
            if ( $result[0]->service > 0 ) {
                  return true;
            }
            else {
                  return false;
            }
      }

      /**
       * Get Customer Modules
       * @param type $customerID
       * @return object
       */
      public static function getCustomerModules($customerID) {

            $dvrEnabled = Customers::hasDvrBasicService($customerID);
            if ( $dvrEnabled !== true ) {
                  $dvrEnabled = false;
            }

            return array(
                  'dvr' => $dvrEnabled,
            );
      }

      /**
       * Count All Active Services
       * @param type $customerID
       * @return Integer
       */
      public static function countActiveServices($customerID) {
            $result = DB::table('customer_services')->select(DB::raw('count(*) as services'))->where('CustomerID', '=', $customerID)->where('Active', '=', 1)->get();
            return $result[0]->services;
      }

      /**
       * Get service detail by customer id
       * @param string $customerID
       * @return object
       */
      public static function getCustomerService($customerID) {
            return DB::table('customer_services')->select('*')->where('CustomerID', '=', $customerID)->get();
      }

      /**
       * Get service detail by service id
       * @param string $customerID
       * @return object
       */
      public static function getService($serviceID) {
            return DB::table('customer_services')->select('*')->where('ServiceID', '=', $serviceID)->first();
      }

      /**
       * Get service detail by service name
       * @param string $name
       * @return object
       */
      public static function getServiceByName($name) {
            return DB::table('customer_services')->select('*')->where('ServiceName', '=', $name)->first();
      }

      /**
       * check if Customer have service
       * @param string $customerID
       * @return boolean
       */
      public static function checkCustomerServices($customerID, $reference = false, $except = false) {
            $rows = DB::table('customer_services')->select(DB::raw('count(*) as total'))
                    ->where('CustomerID', '=', $customerID);
            
            switch ($reference) {
                case "Roku":
                    $channelServices = Services::getChannelServices();
                    $rows = $rows->whereIn('ServiceName', $channelServices);
                    break;
                case "DVR":
                    $rows = $rows->where('ServiceName', '=', DVR_BASIC);
                    break;
                case "DVRWholeHome":
                    $rows = $rows->where('ServiceName', '=', DVR_WHOLE_HOME);
                    break;
                case "DVRAdditional":
                    $rows = $rows->where('ServiceName', '=', ADDITIONAL_DVR);
                    break;
                case "DVRBasic":
                    $rows = $rows->where('ServiceName', '=', DVR_BASIC);
                    break;
                default:
                    break;
            }

            if ( $except ) {
                  $rows = $rows->where('ServiceID', '!=', $except);
            }

            $rows = $rows->where('Active', '=', "1")->first();

            if ( $rows->total > 0 ) {
                  return $rows->total;
            }
            else {
                  return false;
            }
      }

      public static function checkGlobalServiceDependency($service) {
            $error = false;
            if ( $service->Active == "1" ) {
                  switch ( $service->ServiceModule ) {
                        case "Roku":
                              if ( $service->ServiceName == BASIC || $service->ServiceName == BRONZE || $service->ServiceName == SILVER || $service->ServiceName == GOLD || $service->ServiceName == PLATINUM ) {
                                    $total = CustomerServices::checkCustomerServices($service->CustomerID, false, $service->ServiceID);
                                    if ( $total > 0 ) {
                                          $error = "You can not inactive tier level service directly first inactive all other services.";
                                    }
                              }
                              break;
                        case "DVR":
                              if ( $service->ServiceName == DVR_BASIC ) {
                                    if ( CustomerServices::checkCustomerServices($service->CustomerID, DVR_WHOLE_HOME) ) {
                                          $error = "You can not inactive DVR Basic service directly first inactive DVR whole home or dvr additional services.";
                                    }
                                    elseif ( CustomerServices::checkCustomerServices($service->CustomerID, ADDITIONAL_DVR) ) {
                                          $error = "You can not inactive DVR Basic service directly first inactive DVR whole home or dvr additional services.";
                                    }
                              }
                              break;
                        default:
                  }
            }
            else {
                  switch ( $service->ServiceModule ) {
                        case "Roku":
                              if ( $service->ServiceName == BASIC || $service->ServiceName == BRONZE || $service->ServiceName == SILVER || $service->ServiceName == GOLD || $service->ServiceName == PLATINUM ) {
                                    $total = CustomerServices::checkCustomerServices($service->CustomerID, "Roku", $service->ServiceID);
                                    if ( $total > 0 ) {
                                          $error = "You can only assign one tier at a time.";
                                    }
                              }
                              elseif ( $service->ServiceName == ADDITIONAL_ROKU ) {
                                    if ( !CustomerServices::checkCustomerServices($service->CustomerID, "Roku") ) {
                                          $error = "You can not activate this service without any tier level service.";
                                    }
                              }
                              break;
                        case "DVR":
                              if ( $service->ServiceName == DVR_BASIC ) {
                                    if ( !CustomerServices::checkCustomerServices($service->CustomerID, "Roku") ) {
                                          $error = "You can not activate this service without any tier level service.";
                                    }
                                    elseif ( CustomerServices::checkCustomerServices($service->CustomerID, "DVR") ) {
                                          $error = "You can only have one " . DVR_BASIC . " Service at a time.";
                                    }
                              }
                              elseif ( $service->ServiceName == DVR_WHOLE_HOME ) {
                                    if ( !CustomerServices::checkCustomerServices($service->CustomerID, DVR_BASIC) ) {
                                          $error = "You can not activate this service without DVR Basic service.";
                                    }
                              }
                              elseif ( $service->ServiceName == ADDITIONAL_DVR ) {
                                    if ( !CustomerServices::checkCustomerServices($service->CustomerID, DVR_BASIC) ) {
                                          $error = "You can not activate this service without DVR Basic service.";
                                    }
                              }
                              break;
                        default:
                  }
            }
            if ( $error ) {
                  return $error;
            }
            else {
                  return false;
            }
      }

      public static function getAllBillableServices() {
            $services = DB::table('customer_services')->select('*')
                    ->where("Active", "=", "1")
                    ->whereRaw('(DATE(`LastBillDate`) IS NULL OR DATE(NOW()) > DATE_ADD(LastBillDate, INTERVAL 30 DAY))')
                    ->get();

            $api_key = Setting::get('api_key');
            $service_provider_id = Setting::get('service-provider-id');
            $full_name = Setting::get('admin-name');
            $url = Config::get('streamvision.service_provider_url') . "billing/add/" . $service_provider_id . "?key=" . $api_key;

            foreach ( $services as $key => $service ) {
                  $service_id = $service_name = $customer_id = $name = null;

                  if ( !empty($service->CustomerID) ) {
                        $customer = Customers::getCustomer($service->CustomerID);
                        $customer_id = $customer->CustomerID;
                        $name = $customer->FirstName . " " . $customer->LastName;
                  }

                  if ( !empty($service->ServiceID) ) {
                        $service_id = $service->ServiceID;
                  }
                  if ( !empty($service->ServiceName) ) {
                        $service_name = $service->ServiceName;
                  }

                  $fields["Billing"][$key]["customer_id"] = $customer_id;
                  $fields["Billing"][$key]["customer_full_name"] = $name;
                  $fields["Billing"][$key]["service_id"] = $service_id;
                  $fields["Billing"][$key]["service_name"] = $service_name;
                  $fields["Billing"][$key]["subscription_date_time"] = $service->CreatedDate;
                  $fields["Billing"][$key]["time_zone"] = Config::get('app.timezone');
            }

            if ( !empty($fields) ) {
                  $response = json_decode(BaseModel::postCurl($url, $fields));

                  if ( !empty($response->savedServiceIds) ) {
                        foreach ( $response->savedServiceIds as $saved_service ) {
                              DB::table('customer_services')->where("ServiceID", "=", $saved_service)->update(["LastBillDate" => date("Y-m-d H:i:s")]);
                        }
                  }

                  if ( !empty($response->errors) ) {
                        $response->service_provider_id = $service_provider_id;
                        $response->service_provider_name = $full_name;
                        $response->service_failed_count = array_count_values((array) $response->errors);

                        Mail::send("emails.billingresponsefailed", ["data" => $response], function($message) {
                              $message->from(Setting::get('admin-email'), 'ALERT: Some billing data could not be pushed to billing DB');
                              $message->to(BILLING_API_EMAIL)->cc(['gaf@streamvisiontv.com', 'jerry@streamvisiontv.com']);
                              $message->subject("Alert!");
                        });
                  }
            }
      }

}
