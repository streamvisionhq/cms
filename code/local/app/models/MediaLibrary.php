<?php

class MediaLibrary {

      /**
       * The database table used by the model.
       *
       * @var string
       */
      protected $table = 'media_library';

      public static function allCollections() {
            $results = DB::table('collections')->where('collection_id', '!=', 1)
                    ->select('*')
                    ->orderBy('sort_order', 'asc')
                    ->get();
            return $results;
      }

      public static function getAllCollections() {
            $results = DB::table('collections')->select('*')
                    ->orderBy('sort_order', 'asc')
                    ->get();
            return $results;
      }

      public static function getTopCollection() {
            $results = DB::table('collections')->where('collection_id', '!=', 1)
                    ->select('collection_id', 'status')
                    ->orderBy('sort_order', 'asc')
                    ->first();
            return $results;
      }

      public static function getCollectionById($collection_id) {
            $result = DB::table('collections')->select('*')
                    ->where('collection_id', $collection_id)
                    ->first();
            return $result;
      }

      public static function chkCollectionExists($postdata, $id = 0) {
            if ( $id > 0 ) {
                  $result = DB::table('collections')->where('collection_name', '=', trim($postdata['collection_name']))
                          ->where('collection_id', '!=', $id)
                          ->select('*')
                          ->first();
            }
            else {
                  $result = DB::table('collections')->where('collection_name', '=', trim($postdata['collection_name']))
                          ->select('*')
                          ->first();
            }

            return $result;
      }

      public static function addCollection($postdata) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $created_date = date("Y-m-d H:i:s");

            $id = DB::table('collections')->insertGetId(array('collection_name' => trim($postdata['collection_name']), 'created_date' => $created_date, 'created_by' => $postdata['created_by'], 'created_date_gmt' => $gmt_date, 'status' => 1));

            return true;
      }

      public static function modifyCollection($postdata) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $modified_date = date("Y-m-d H:i:s");

            $id = DB::table('collections')->where('collection_id', $postdata['collection_id'])
                    ->update(array('collection_name' => trim($postdata['collection_name']), 'modified_date' => $modified_date, 'modified_by' => $postdata['modified_by'], 'modified_date_gmt' => $gmt_date, 'status' => 1));

            return true;
      }

      public static function removeCollection($collection_id) {
            DB::table('collections')->where('collection_id', '=', $collection_id)->delete();
            DB::table('media_to_collections')->where('collection_id', '=', $collection_id)->delete();
      }

      public static function modifyCollectionStatus($collection_id, $status) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $modified_date = date("Y-m-d H:i:s");
            DB::table('collections')->where('collection_id', $collection_id)
                    ->update(array('status' => $status, 'modified_date' => $modified_date, 'modified_date_gmt' => $gmt_date));
      }

      public static function modifyCollectionOrder($collections) {
            DB::table('collections')->where('collection_id', '!=', 1)
                    ->update(array('sort_order' => 0));
            $i = 1;
            foreach ( $collections as $collection ) {
                  DB::table('collections')->where('collection_id', $collection)
                          ->update(array('sort_order' => $i));
                  $i++;
            }
      }

      public static function addVideo($postData, $job_id, $convFile, $convThumb) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $created_date = date("Y-m-d H:i:s");
            $arr_media = array();
            $arr_meta = array();

            $arr_media = array('name' => trim($postData['name']), 'media_name' => $convFile, 'job_id' => $job_id, 'job_status_id' => 1, 'created_date' => $created_date, 'created_by' => $postData['created_by'], 'status' => 2, 'created_date_gmt' => $gmt_date);

            if ( isset($postData['meta_tag']) ) {
                  $arr_meta = array('meta_tag' => trim($postData['meta_tag']), 'meta_description' => trim($postData['meta_description']));
            }

            $media_data = array_merge($arr_media, $arr_meta);

            $media_id = DB::table('media_library')->insertGetId($media_data);

            $poster_id = DB::table('media_library_to_posters')->insertGetId(
                    array('media_id' => $media_id, 'image_name' => $convThumb)
            );

            DB::table('media_library')->where('media_id', '=', $media_id)
                    ->update(array('poster_id' => $poster_id));

            return true;
      }

      public static function modifyVideo($postData, $job_id, $poster_id, $convFile, $convThumb, $poster_image, $poster_type) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $modified_date = date("Y-m-d H:i:s");
            $array_sec = array();
            $array_jobdata = array();

            $array_pri = array('name' => trim($postData['name']), 'meta_tag' => trim($postData['meta_tag']), 'meta_description' => trim($postData['meta_description']), 'job_id' => $job_id, 'modified_date' => $modified_date, 'modified_by' => $postData['modified_by'], 'modified_date_gmt' => $gmt_date, 'descriptions' => trim($postData['descriptions']), 'content' => trim($postData['contents']), 'genre' => trim($postData['genre']), 'rating' => $postData['rating'], 'rights' => trim($postData['rights']), 'release_date' => $postData['release_date'], 'show_description' => trim($postData['show_description']), 'closed_captions' => trim($postData['closed_captions']), 'generic_tags' => trim($postData['generic_tags']), 'similar_content' => trim($postData['similar_content']));

            if ( $convFile != '' ) {
                  $array_sec = array('media_name' => $convFile, 'job_status_id' => 1, 'job_duration' => 0, 'status' => 2, 'frame_width' => 0, 'frame_height' => 0, 'file_size' => 0);
            }
            else {
                  $array_sec = array('frame_width' => $postData['frame_width'], 'frame_height' => $postData['frame_height'], 'file_size' => $postData['file_size'], 'job_duration' => $postData['duration']);
            }

            $array_combine = array_merge($array_pri, $array_sec, $array_jobdata);

            DB::table('media_library')->where('media_id', $postData['media_id'])
                    ->update($array_combine);

            if ( $convFile != '' ) {
                  DB::table('media_library_to_posters')->where('poster_id', $poster_id)
                          ->update(array('image_name' => $convThumb));
                  DB::table('media_to_collections')->where('media_id', '=', $postData['media_id'])->delete();
                  DB::table('media_to_channels')->where('media_id', '=', $postData['media_id'])->delete();
            }

            DB::table('media_library_to_posters')->where('media_id', '=', $postData['media_id'])->where('poster_id', '!=', $poster_id)->delete();


            foreach ( $poster_image as $key => $val ) {
                  DB::table('media_library_to_posters')->insert(
                          array('media_id' => $postData['media_id'], 'image_name' => $val, 'poster_type' => $poster_type[$key]));
            }

            return true;
      }

      public static function getVideoById($media_id) {
            $result = DB::table('media_library')->select(DB::raw('*, CASE WHEN job_status_id = 4 THEN ( SELECT image_name FROM media_library_to_posters WHERE poster_id = media_library.poster_id AND media_id = media_library.media_id ) ELSE \'no_image.jpg\' END AS media_image'))
                    ->where('media_id', $media_id)
                    ->first();
            return $result;
      }

      public static function getPostersByid($media_id) {
            $results = DB::table('media_library_to_posters')->select('*')
                    ->where('media_id', $media_id)
                    ->where('poster_type', '!=', '')
                    ->get();
            return $results;
      }

      public static function searchVideo($term = '') {
            $query = DB::table('media_library')->select(DB::raw('media_id, name, media_name, job_id, job_duration, CASE WHEN job_status_id = 4 THEN ( SELECT image_name FROM media_library_to_posters WHERE poster_id = media_library.poster_id AND media_id = media_library.media_id ) ELSE \'no_image.jpg\' END AS media_image, status, CASE WHEN job_status_id > 0 THEN  ( SELECT status_desc FROM job_status WHERE status_id = job_status_id ) ELSE \'null\' END as job_status_desc, job_status_id, CASE WHEN job_status_id > 0 THEN  ( SELECT status_color FROM job_status WHERE status_id = job_status_id ) ELSE \'#1aa8b4\' END as job_status_color'));

            if ( $term != '' )
                  $query->where('name', 'LIKE', '%' . trim($term) . '%');

            $query->where('job_status_id', '=', 4);
            $query->orderBy('created_date', 'desc');

            $results = $query->paginate(10);

            return $results;
      }

      public static function allVideos() {
            $results = DB::table('media_library')->select(DB::raw('media_id, name, media_name, job_id, job_duration, CASE WHEN job_status_id = 4 THEN ( SELECT image_name FROM media_library_to_posters WHERE poster_id = media_library.poster_id AND media_id = media_library.media_id ) ELSE \'no_image.jpg\' END AS media_image, status, CASE WHEN job_status_id > 0 THEN  ( SELECT status_desc FROM job_status WHERE status_id = job_status_id ) ELSE \'null\' END as job_status_desc, job_status_id, CASE WHEN job_status_id > 0 THEN  ( SELECT status_color FROM job_status WHERE status_id = job_status_id ) ELSE \'#1aa8b4\' END as job_status_color'))
                    ->orderBy('created_date', 'desc')
                    ->paginate(16);
            return $results;
      }

      public static function modifyVideoJobStatus($media_id, $job_status_id, $job_duration, $job_data) {
            if ( $job_status_id == 4 )
                  $status = 1;
            else
                  $status = 2;

            $array_jobdata = array();
            if ( sizeof($job_data) > 0 ) {
                  $array_jobdata = $job_data;
            }

            $array_pri = array('job_status_id' => $job_status_id, 'job_duration' => $job_duration, 'status' => $status);
            $array_combine = array_merge($array_pri, $array_jobdata);
            DB::table('media_library')->where('media_id', $media_id)
                    ->update($array_combine);

            return true;
      }

      public static function getJobStatusByDesc($job_status_desc) {
            $result = DB::table('job_status')->select('status_id')
                    ->where('status_desc', trim($job_status_desc))
                    ->first();
            return $result;
      }

      public static function removeVideo($media_id) {
            DB::table('media_library')->where('media_id', '=', $media_id)->delete();
            DB::table('media_to_collections')->where('media_id', '=', $media_id)->delete();
            DB::table('media_to_channels')->where('media_id', '=', $media_id)->delete();
      }

      public static function modifyVideoStatus($media_id, $status) {
            $gmt_date = gmdate("Y-m-d H:i:s");
            $modified_date = date("Y-m-d H:i:s");
            DB::table('media_library')->where('media_id', $media_id)
                    ->update(array('status' => $status, 'modified_date' => $modified_date, 'modified_date_gmt' => $gmt_date));
      }

      public static function getMediaByCollection($collection_id) {
            $results = DB::table('media_library')->where('media_to_collections.collection_id', '=', $collection_id)
                    ->join('media_to_collections', 'media_library.media_id', '=', 'media_to_collections.media_id')
                    ->join('media_library_to_posters', 'media_library.poster_id', '=', 'media_library_to_posters.poster_id')
                    ->orderBy('media_to_collections.id', 'desc')
                    ->paginate(10);
            return $results;
      }

      public static function addMediaToCollection($media_id, $collection_id) {
            DB::table('media_to_collections')->insert(
                    array('media_id' => $media_id, 'collection_id' => $collection_id)
            );

            return true;
      }

      public static function removeMediaFromCollection($id) {
            DB::table('media_to_collections')->where('id', $id)
                    ->delete();

            return true;
      }

}
