<?php

use Dvrapi\Dvrapi;

class Channel {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'channels';

    public static function getChannelDataById($channel_id) {
        $result = DB::table('channels')->select('*')
                ->where('channel_id', $channel_id)
                ->first();
        return $result;
    }

    public static function getAllChannelData($per_page, $sort_by, $sort_order, $start) {
        DB::statement('SET @rownum:="' . $start . '"');

        $results = DB::table('channels')->select('channel_id', 'title', 'channel_logo', 'path', 'callsign', 'stream_name', 'titan_channel_id', DB::Raw('date_format(channel_created, \'%b %d, %Y\') as channel_created, CASE WHEN channel_type > 0 THEN  ( SELECT name FROM channel_type WHERE id = channel_type ) ELSE \'n/a\' END as channel_typedesc, @rownum:=@rownum+1 AS s_no, CASE WHEN channel_type = 1 THEN \'lstream\' WHEN channel_type = 2 THEN \'vod\' ELSE \'category\' END AS ch_class '), 'channel_type', 'sort_order', 'status', 'parent_id')->where('parent_id', '=', 0)->orderBy($sort_by, $sort_order)->paginate($per_page);
        ;

        return $results;
    }

    public static function getAllChannel() {
        DB::statement('SET @rownum:="0"');

        $results = DB::table('channels')->select('channel_id', 'title', 'channel_logo', 'path', 'callsign', 'stream_name', 'titan_channel_id', DB::Raw('date_format(channel_created, \'%b %d, %Y\') as channel_created, CASE WHEN channel_type > 0 THEN  ( SELECT name FROM channel_type WHERE id = channel_type ) ELSE \'n/a\' END as channel_typedesc, @rownum:=@rownum+1 AS s_no, CASE WHEN channel_type = 1 THEN \'lstream\' WHEN channel_type = 2 THEN \'vod\' ELSE \'category\' END AS ch_class '), 'channel_type', 'sort_order', 'status', 'parent_id')->where('parent_id', '=', 0)->orderBy('sort_order', 'ASC')->get();

        return $results;
    }

    public static function getChannelByCategory($parent_id) {
        DB::statement('SET @rownum:=0');
        $results = DB::table('channels')->select('channel_id', 'title', 'channel_logo', 'path', DB::Raw('date_format(channel_created, \'%b %d, %Y\') as channel_created, CASE WHEN channel_type > 0 THEN  ( SELECT name FROM channel_type WHERE id = channel_type ) ELSE \'n/a\' END as channel_typedesc, @rownum:=@rownum+1 AS s_no, CASE WHEN channel_type = 1 THEN \'lstream\' WHEN channel_type = 2 THEN \'vod\' ELSE \'category\' END AS ch_class'), 'channel_type', 'sort_order', 'status', 'parent_id')->where('parent_id', '=', $parent_id)->orderBy('sort_order', 'ASC')->get();

        return $results;
    }

    public static function getAllFreeChannels() {
        $results = DB::table('channels')->select('*')->where('free_channel', '=', 1)->get();
        return $results;
    }

    public static function getAllLiveStream() {
        $results = DB::table('channels')->select('channel_id', 'title', 'channel_logo', 'channel_type', 'sort_order')->where('channel_type', '=', 1)->orderBy('sort_order', 'ASC')->get();

        return $results;
    }

    public static function addLiveStream($postData, $channel_logo) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $created_date = date("Y-m-d H:i:s");

        DB::table('channels')->insert(
                array(
                    'title' => trim($postData['title']),
                    'short_description' => trim($postData['short_description']),
                    'long_description' => trim($postData['long_description']),
                    'channel_type' => $postData['channel_type'],
                    'path' => $postData['path'],
                    'callsign' => $postData['callsign'],
                    'stream_name' => $postData['stream_name'],
                    'titan_channel_id' => $postData['titan_channel_id'],
                    'emerald_subscription_type' => $postData['service_category'],
                    'channel_logo' => $channel_logo,
                    'pre_roll' => $postData['pre_roll'],
                    'channel_created' => $created_date,
                    'created_by' => $postData['created_by'],
                    'status' => 1,
                    'channel_created_gmt' => $gmt_date,
                    'free_channel' => $postData['free_channel'],
                    'channel_number' => $postData['channel_number'] ? $postData['channel_number'] : NULL
                )
        );

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyLiveStream($postData, $channel_logo) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");

        DB::table('channels')->where('channel_id', $postData['channel_id'])
                ->update(
                        array(
                            'title' => trim($postData['title']),
                            'short_description' => trim($postData['short_description']),
                            'long_description' => trim($postData['long_description']),
                            'channel_type' => $postData['channel_type'],
                            'path' => $postData['path'],
                            'callsign' => $postData['callsign'],
                            'stream_name' => $postData['stream_name'],
                            'titan_channel_id' => $postData['titan_channel_id'],
                            'emerald_subscription_type' => $postData['service_category'],
                            'channel_logo' => $channel_logo,
                            'pre_roll' => $postData['pre_roll'],
                            'channel_number' => $postData['channel_number'] ? $postData['channel_number'] : NULL,
                            'channel_modified' => $modified_date,
                            'modified_by' => $postData['modified_by'],
                            'channel_modified_gmt' => $gmt_date
                        )
        );

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyLiveStreamUrl($postData) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");


        DB::table('channels')->where('channel_id', $postData['channel_id'])
                ->update(array('path' => $postData['path'], 'channel_modified' => $modified_date, 'channel_modified_gmt' => $gmt_date));

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyServerUrl($postData) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");

        DB::table('channels')->where('channel_id', $postData['channel_id'])
                ->update(array('channel_modified' => $modified_date, 'channel_modified_gmt' => $gmt_date));

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyStreamOrder($channels) {
        DB::table('channels')->where('channel_type', '=', 1)
                ->update(array('sort_order' => 0));
        $i = 1;
        foreach ($channels as $channel) {
            DB::table('channels')->where('channel_id', $channel)
                    ->update(array('sort_order' => $i));
            $i++;
        }

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyChannelOrder($channels, $sorder) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");

        foreach ($channels as $c_parent => $c_childs) {
            $i = 1;
            $parent_id = $c_parent;
            foreach ($c_childs as $channel) {
                DB::table('channels')->where('channel_id', $channel)
                        ->update(array('parent_id' => $parent_id, 'sort_order' => $i, 'channel_modified' => $modified_date, 'channel_modified_gmt' => $gmt_date));

                $i++;
            }
        }

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function getAllCollections() {
        $results = DB::table('collections')->select('*')->where('status', '=', 1)->get();
        return $results;
    }

    public static function getMediaByCollection($collection_id) {
        $results = DB::table('media_library')->select(DB::raw('*, CASE WHEN job_status_id = 4 THEN ( SELECT image_name FROM media_library_to_posters WHERE poster_id = media_library.poster_id AND media_id = media_library.media_id ) ELSE \'no_image.jpg\' END AS media_image'))->where('media_to_collections.collection_id', '=', $collection_id)
                        ->where('media_library.status', '=', 1)
                        ->join('media_to_collections', 'media_library.media_id', '=', 'media_to_collections.media_id')->paginate(10);
        return $results;
    }

    public static function addVideoOnDemand($postData, $channel_logo) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $created_date = date("Y-m-d H:i:s");

        $id = DB::table('channels')->insertGetId(array('title' => trim($postData['title']), 'short_description' => trim($postData['short_description']), 'long_description' => trim($postData['long_description']), 'parent_id' => $postData['parent_id'], 'channel_type' => $postData['channel_type'], 'channel_logo' => $channel_logo, 'pre_roll' => $postData['pre_roll'], 'channel_created' => $created_date, 'created_by' => $postData['created_by'], 'status' => 1, 'channel_created_gmt' => $gmt_date)
        );

        $exp_trailer = explode(',', $postData['trailers']);

        foreach ($exp_trailer as $trailer) {
            DB::table('media_to_channels')->insert(
                    array('media_id' => $trailer, 'channel_id' => $id)
            );
        }

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyVideoOnDemand($postData, $channel_logo) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");

        DB::table('channels')->where('channel_id', $postData['channel_id'])
                ->update(array('title' => trim($postData['title']), 'short_description' => trim($postData['short_description']), 'long_description' => trim($postData['long_description']), 'titan_channel_id' => $postData['titan_channel_id'], 'emerald_subscription_type' => $postData['service_category'], 'parent_id' => $postData['parent_id'], 'channel_type' => $postData['channel_type'], 'channel_logo' => $channel_logo, 'pre_roll' => $postData['pre_roll'], 'channel_modified' => $modified_date, 'modified_by' => $postData['modified_by'], 'channel_modified_gmt' => $gmt_date));

        DB::table('media_to_channels')->where('channel_id', '=', $postData['channel_id'])->delete();

        $exp_trailer = explode(',', $postData['trailers']);

        foreach ($exp_trailer as $trailer) {
            DB::table('media_to_channels')->insert(
                    array('media_id' => $trailer, 'channel_id' => $postData['channel_id'])
            );
        }

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function getMediaByChannel($channel_id) {
        $results = DB::table('media_library')->select(DB::raw('*, CASE WHEN job_status_id = 4 THEN ( SELECT image_name FROM media_library_to_posters WHERE poster_id = media_library.poster_id AND media_id = media_library.media_id ) ELSE \'no_image.jpg\' END AS media_image'))->where('media_to_channels.channel_id', '=', $channel_id)
                        ->join('media_to_channels', 'media_library.media_id', '=', 'media_to_channels.media_id')->orderBy('media_to_channels.id', 'asc')->get();
        return $results;
    }

    public static function removeChannel($channel_id, $channel_type) {
        DB::table('channels')->where('channel_id', '=', $channel_id)->delete();

        if ($channel_type == 2) {
            DB::table('media_to_channels')->where('channel_id', '=', $channel_id)->delete();
        }

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyChannelStatus($channel_id, $status) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");
        DB::table('channels')->where('channel_id', $channel_id)
                ->update(array('status' => $status, 'channel_modified' => $modified_date, 'channel_modified_gmt' => $gmt_date));

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function getCategoryDataById($category_id) {
        $result = DB::table('channels')->select('*')
                ->where('channel_id', $category_id)
                ->first();
        return $result;
    }

    public static function addCategory($postData, $category_image) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $created_date = date("Y-m-d H:i:s");

        DB::table('channels')->insert(
                array('title' => trim($postData['category_name']), 'parent_id' => $postData['parent_id'], 'short_description' => trim($postData['short_description']), 'channel_logo' => $category_image, 'channel_created' => $created_date, 'created_by' => $postData['created_by'], 'channel_type' => $postData['channel_type'], 'status' => 1, 'channel_created_gmt' => $gmt_date)
        );

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function modifyCategory($postData, $category_image) {
        $gmt_date = gmdate("Y-m-d H:i:s");
        $modified_date = date("Y-m-d H:i:s");

        DB::table('channels')->where('channel_id', $postData['category_id'])
                ->update(array('title' => trim($postData['category_name']), 'parent_id' => $postData['parent_id'], 'short_description' => trim($postData['short_description']), 'channel_logo' => $category_image, 'channel_modified' => $modified_date, 'modified_by' => $postData['modified_by'], 'status' => 1, 'channel_modified_gmt' => $gmt_date)
        );

        BaseModel::update_last_modified("channels", date("Y-m-d H:i:s"));
    }

    public static function updateSettings($data) {
        set_time_limit(0);

        Setting::set('nimble_stream', $data['nimble_stream']);
        Setting::set('nimble_dvr', $data['nimble_dvr']);
        Setting::set('nimble_stream_backup', $data['nimble_stream_backup']);
        Setting::set('nimble_dvr_backup', $data['nimble_dvr_backup']);
        Setting::set('nimble_port', $data['nimble_port']);
        Setting::set('nimble_mode', $data['nimble_mode']);


        $svChannels = Channel::getAllChannel();
        foreach ($svChannels as $svc) {

            $path = $svc->path;
            $stream_url = Channel::getStreamURL($path, 'dvr');

            // Update stream URL of every channel on DVR according to current settings
            Dvrapi::updateChannel($svc->titan_channel_id, $svc->stream_name, $stream_url);
        }
    }

    public static function getStreamURL($path, $action = "all") {

        if ($mode = Setting::get('nimble_mode')) {

            if ($mode == "backup") {
                $nimble_stream = Setting::get('nimble_stream_backup');
                $nimble_dvr = Setting::get('nimble_dvr_backup');
            } else {
                $nimble_stream = Setting::get('nimble_stream');
                $nimble_dvr = Setting::get('nimble_dvr');
            }

            switch ($action) {
                case 'dvr':
                    $stream_url = "http://" . $nimble_dvr . ":" . Setting::get('nimble_port') . "/" . $path;
                    break;
                case 'streaming':
                    $stream_url = "http://" . $nimble_stream . ":" . Setting::get('nimble_port') . "/" . $path;

                    if (Channel::hasDifferentNimbleStreamURL($path)) {
                        $stream_url = $path;
                    }

                    break;
                default:
                    $stream_url['streaming'] = "http://" . $nimble_stream . "/" . $path;

                    if (Channel::hasDifferentNimbleStreamURL($path)) {
                        $stream_url['streaming'] = $path;
                    }

                    $stream_url['dvr'] = "http://" . $nimble_dvr . "/" . $path;
            }

            return $stream_url;
        }
    }

    /**
     * Checking here if channel has different nimble stream url set other then global nimble stream url
     * @param string $path
     * @return boolean
     */
    public static function hasDifferentNimbleStreamURL($path) {
        $url = parse_url($path);
        if (isset($url['scheme'])) {
            return true;
        }
        return false;
    }

    public static function titan_channel_validiation($value, $action) {
        ini_set('memory_limit', '512M');
        $filePath = realpath(base_path() . '/epgdata/GetSchedules.xml');
        $epgChannels = EPGController::readEpgXmlFile($filePath);
        $epgChannels = json_decode($epgChannels, true)['Channel'];
        $return = false;

        foreach ($epgChannels as $epgChannel) {
            if ($epgChannel['@attributes'][$action] == $value) {
                $return = true;
                break;
            }
        }

        return $return;
    }

    public static function get_major_minor($stream_name) {
        $stream_name_array = explode("-", $stream_name);
        $major_minor = end($stream_name_array);
        $major_minor = explode(".", $major_minor);
        return $major_minor;
    }

}
