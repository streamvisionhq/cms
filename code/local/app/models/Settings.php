<?php

class Settings {

      /**
       * Update Settings
       * @param type $data
       * @return type
       */
      public static function updateSettings($data) {
            if ( isset($data['customer-management-settings']) ) {
                  Setting::set('cm_mode', $data['cm_mode']);
                  Setting::set('customer-custom-field-1-label', $data['customer-custom-field-1-label']);
                  Setting::set('customer-custom-field-2-label', $data['customer-custom-field-2-label']);
            }

            if ( isset($data['edit-radius-settings']) ) {
                  Setting::set('radius-server-ip', $data['radius-server-ip']);
                  Setting::set('radius-server-host', $data['radius-server-host']);
                  Setting::set('radius-shared-secret', $data['radius-shared-secret']);
            }
      }

}
