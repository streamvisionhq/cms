<?php

class Logs {

    public static function addLog($customerID = NULL, $data) {
        DB::table('logs')->insert(
                ['CustomerID' => $customerID, 'Data' => json_encode($data), 'Date' => date('Y-m-d H:i:s')]
        );
    }

    public static function sendLog($fields) {
        $data = [
              'customer_id' => $fields["customer_id"],
              'action_type' => $fields["action_type"],
              'service_provider_id' => Setting::get('service-provider-id'),
              'service_provider_name' => Setting::get('service-provider-id'),
              'stream_type' => $fields["stream_type"],
              'titan_id' => $fields['titan_id'],
              'major_number' => $fields['major_number'],
              'minor_number' => $fields['minor_number'],
              'show_title' => $fields['show_title'],
              'show_episode_title' => $fields['show_episode_title'],
              'utc_time_stamp' => gmdate('Y-m-d H:i:s')
        ];

        $ch = curl_init();
        $data = http_build_query($data);
        $getUrl = CENTRAL_LOGGING_SERVER_URL . "?" . $data;
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_PORT, CENTRAL_LOGGING_SERVER_PORT);
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        $response = curl_exec($ch);
        $err = curl_error($ch);
        curl_close($ch);
    }

}
