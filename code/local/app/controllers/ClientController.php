<?php

class ClientController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      public function getIndex() {
            $filters = NULL;
            $data['perPage'] = $this->perPage;
            $data["cm_mode"] = Setting::get('cm_mode');

            if ( isset($_GET['per_page']) && $_GET['per_page'] > 0 ) {
                  $data['perPage'] = $_GET['per_page'];
            }

            switch ( Input::get('action') ) {
                  case 'filter':
                        if ( Input::get('customerID') ) {
                              $filters['customerID'] = Input::get('customerID');
                        }
                        if ( Input::get('firstName') ) {
                              $filters['firstName'] = Input::get('firstName');
                        }
                        if ( Input::get('lastName') ) {
                              $filters['lastName'] = Input::get('lastName');
                        }
                        if ( Input::get('email') ) {
                              $filters['email'] = Input::get('email');
                        }
                        if ( Input::get('deviceID') ) {
                              if ( $device = RokuDevices::getDevice(Input::get('deviceID')) ) {
                                    $filters['customerID'] = $device->CustomerID;
                              }
                              else {
                                    $filters['customerID'] = "no_customer";
                              }
                        }
                        break;
            }
            $data['customers'] = Customers::getCustomers($filters, $data['perPage']);
            return View::make('customers/customers')->with($data);
      }

      public function addNewClient() {
            if ( Input::get('save-customer') ) {
                  $postData = Input::all();

                  //validate all required fields are filled and return laravel validator object
                  $validator = Customers::validateCustomerFormData($postData);

                  // send back to the page with the input data and errors
                  if ( $validator->fails() ) {
                        return Redirect::to('add-new-customer/')->withInput()->withErrors($validator);
                  }
                  else {
                        // creates random id for customer and add new customer record in database
                        Customers::addCustomer($postData, 'cms');
                        Session::flash('flash_message', CUSTOMER_ADDED);
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('customers');
                  }
            }

            $data = BaseController::getSettingsFieldsLabels();
            return View::make('customers/add-edit-customer')->with($data);
      }

      public function editClient($CustomerID) {
            $data = BaseController::getSettingsFieldsLabels();
            $data["customer"] = Customers::getCustomer($CustomerID);
            if ( Input::get('save-customer') ) {
                  $postData = Input::all();

                  //validate all required fields are filled and return laravel validator object
                  $validator = Customers::validateCustomerFormData($postData);

                  // send back to the page with the input data and errors
                  if ( $validator->fails() ) {
                        return Redirect::to('edit-customer/' . $CustomerID)->withInput()->withErrors($validator);
                  }
                  else {
                        // creates random id for customer and add new customer record in database
                        Customers::editCustomer($CustomerID, $postData);
                        if ( $data["customer"]->Password != $postData["password"] ) {
                              DB::table('customer_services')->where('CustomerID', $CustomerID)->update(['Password' => $postData["password"]]);
                        }
                        Session::flash('flash_message', CUSTOMER_EDITED);
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('edit-customer/' . $CustomerID);
                  }
            }
            return View::make('customers/add-edit-customer')->with($data);
      }

      public function getRokuDevices($customerID) {
            $filters = NULL;
            $data['perPage'] = $this->perPage;

            if ( isset($_GET['per_page']) && $_GET['per_page'] > 0 ) {
                  $data['perPage'] = $_GET['per_page'];
            }

            switch ( Input::get('action') ) {
                  case 'delete':
                        $deviceID = Input::get('deviceID');
                        if ( $deviceID ) {
                              if ( RokuDevices::deleteDevice($deviceID) ) {
                                    return Response::json(array('success' => true, 'message' => 'Roku device (' . $deviceID . ') deleted!'));
                              }
                              else {
                                    return Response::json(array('success' => false, 'message' => 'Error in deleting roku device'));
                              }
                        }
                        else {
                              return Response::json(array('success' => false, 'message' => 'Device ID is missing'));
                        }
                        die();
                        break;
                  case 'filter':
                        if ( Input::get('deviceID') ) {
                              $filters['deviceID'] = Input::get('deviceID');
                        }
                        if ( Input::get('identifier') ) {
                              $filters['identifier'] = Input::get('identifier');
                        }
                        break;
            }

            $data['devices'] = RokuDevices::getDevicesByCustomerID($customerID, $data['perPage'], $filters);
            $data['customer'] = Customers::getCustomer($customerID);
            return View::make('customers/roku-devices')->with($data);
      }

}
