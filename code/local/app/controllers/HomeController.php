<?php

class HomeController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      public function getIndex() {
            return View::make('dashboard');
      }

}
