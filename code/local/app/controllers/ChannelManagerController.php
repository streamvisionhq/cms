<?php

use Dvrapi\Dvrapi;

class ChannelManagerController extends BaseController {

    private $uid = 0;
    private $sub_channels = array();
    private $index = array();

    public function __construct() {
        $this->uid = Auth::id();
    }

    public function showChannel() {
        $per_page = 25;
        $sort_by = 'sort_order';
        $sort_order = 'asc';
        $order_title = 'asc';
        $order_type = 'asc';
        $order_date = 'asc';
        $order_sort = 'asc';

        if (isset($_GET['per_page']) && $_GET['per_page'] > 0)
            $per_page = $_GET['per_page'];
        if (isset($_GET['sort_by']) && $_GET['sort_by'] != '')
            $sort_by = $_GET['sort_by'];
        if (isset($_GET['sort_order']) && $_GET['sort_order'] != '')
            $sort_order = $_GET['sort_order'];
        if (isset($_GET['channel_type']) && $_GET['channel_type'] > 0)
            $channel_type = $_GET['channel_type'];

        $page = (isset($_GET['page'])) ? $_GET['page'] : 1;
        $s_no = ($page == 1) ? 0 : ($per_page * ($page - 1));
        $start = $s_no;

        $results = Channel::getAllChannelData($per_page, $sort_by, $sort_order, $start);

        foreach ($results as $result) {
            $this->getChannelByCategory($result->channel_id);
        }

        switch ($sort_by) {
            case 'title':
                $order_title = ($sort_order == 'desc') ? 'asc' : 'desc';
                break;

            case 'channel_type':
                $order_type = ($sort_order == 'desc') ? 'asc' : 'desc';
                break;

            case 'channel_created':
                $order_date = ($sort_order == 'desc') ? 'asc' : 'desc';
                break;

            case 'sort_order':
                $order_sort = ($sort_order == 'desc') ? 'asc' : 'desc';
                break;
        }

        if (Setting::get('nimble_mode') == "live") {
            $ip = Setting::get('nimble_stream');
        } else {
            $ip = Setting::get('nimble_stream_backup');
        }

        $data = array(
            'channels' => $results,
            'subchannels' => $this->sub_channels,
            'index' => $this->index,
            'per_page' => $per_page,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'order_title' => $order_title,
            'order_type' => $order_type,
            'order_date' => $order_date,
            'order_sort' => $order_sort,
            's_no' => $s_no,
            'curPage' => $page,
            'ip' => $ip,
            'port' => Setting::get('nimble_port'),
        );

        return View::make('channelmanager')->with($data);
    }

    public function getChannelByCategory($channel_id) {

        $results = Channel::getChannelByCategory($channel_id);
        foreach ($results as $result) {
            $id = $result->channel_id;
            $parent_id = $result->parent_id;
            $this->sub_channels[$id] = (array) $result;
            $this->index[$parent_id][] = $id;
            $this->getChannelByCategory($result->channel_id);
        }
    }

    public function addCategory() {
        $parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : 0;
        $data = array('parent_id' => $parent_id);

        return View::make('addcategory')->with($data);
    }

    public function doAddCategory() {
        $postData = Input::all();
        $input = array('category_image' => Input::file('category_image'));

        $messages = array(// setting up custom error messages for the field validation
            'category_name.required' => 'Please enter Category Name',
            'short_description.required' => 'Please enter Short Description',
            'category_image.required' => 'Please upload Category Image',
        );

        $rules = array(
            'category_name' => 'required',
            'short_description' => 'required',
            'category_image' => 'required|mimes:jpeg',
        );

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('category_image')) {
                $file = Input::file('category_image');
                $fileName = $this->uploadCategoryImage($file, 108, 69);
            }

            Channel::addCategory($postData, $fileName);

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function modifyCategory() {
        $category_id = Request::segment(3);
        $per_page = $_GET['per_page'];
        $sort_by = $_GET['sort_by'];
        $sort_order = $_GET['sort_order'];
        $page = $_GET['page'];

        $result = Channel::getCategoryDataById($category_id);

        $data = array(
            'category_id' => $result->channel_id,
            'category_name' => $result->title,
            'short_description' => $result->short_description,
            'parent_id' => $result->parent_id,
            'category_image' => $result->channel_logo,
            'per_page' => $per_page,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'page' => $page,
        );

        return View::make('editcategory')->with($data);
    }

    public function doModifyCategory() {
        $postData = Input::all();
        $input = array('category_image' => Input::file('category_image'));
        $fileName = Input::get('hidcategory_image');

        $messages = array(// setting up custom error messages for the field validation
            'category_name.required' => 'Please enter Category Name',
            'short_description.required' => 'Please enter Short Description',
            'category_image.required' => 'Please upload Category Image',
        );

        $rules = array(
            'category_name' => 'required',
            'short_description' => 'required',
            'category_image' => 'mimes:jpeg',
        );

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('category_image')) {
                $file = Input::file('category_image');
                $fileName = $this->uploadCategoryImage($file, 108, 69);
            }

            Channel::modifyCategory($postData, $fileName);

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    private function uploadCategoryImage($file, $width, $height) {
        $tstamp = time();

        //get upload media file details
        $orgFilename = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $file->getClientOriginalName());
        $fileExtension = $file->getClientOriginalExtension();
        $fileType = $file->getClientMimeType();
        $fileSize = $file->getClientSize();

        //rename file
        $fileWithOutExt = basename($orgFilename, "." . $fileExtension);
        $fileName = $fileWithOutExt . "_" . $tstamp . "." . $fileExtension;

        //upload file to the server
        $destPath = 'assets/data/image';
        $file->move($destPath, $fileName);

        $image = Image::make(sprintf('assets/data/image/%s', $fileName))->resize(108, 69)->save();

        return $fileName;
    }

    public function addLiveStream() {

        $services = array();

        $channelServices = Services::getChannelServices();

        if ($channelServices) {
            foreach ($channelServices as $service) {
                $services[$service] = $service;
            }
        }

        $data = array(
            'is_backupstream_available' => Setting::get('is-backup-available'),
            'services' => $services
        );

        return View::make('addlivestream')->with($data);
    }

    public function modifyLiveStream() {
        $channel_id = Request::segment(3);
        $per_page = $_GET['per_page'];
        $sort_by = $_GET['sort_by'];
        $sort_order = $_GET['sort_order'];
        $page = $_GET['page'];

        $result = Channel::getChannelDataById($channel_id);

        $services = array();

        $channelServices = Services::getChannelServices();

        if ($channelServices) {
            foreach ($channelServices as $service) {
                $services[$service] = $service;
            }
        }

        $data = array(
            'channel_id' => $result->channel_id,
            'free_channel' => $result->free_channel,
            'title' => $result->title,
            'short_description' => $result->short_description,
            'long_description' => $result->long_description,
            'channel_logo' => $result->channel_logo,
            'pre_roll' => $result->pre_roll,
            'path' => $result->path,
            'callsign' => $result->callsign,
            'titan_channel_id' => $result->titan_channel_id,
            'service_category' => $result->emerald_subscription_type,
            'per_page' => $per_page,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'page' => $page,
            'is_backupstream_available' => Setting::get('is-backup-available'),
            'streamname' => $result->stream_name,
            'services' => $services,
            'channel_number' => $result->channel_number,
        );

        return View::make('editlivestream')->with($data);
    }

    public function doAddLiveStream() {
        $postData = Input::all();

        $input = array('channel_logo' => Input::file('channel_logo'));

        $messages = array(// setting up custom error messages for the field validation
            'title.required' => 'Please enter Title',
            'short_description.required' => 'Please enter Short Description',
            'long_description.required' => 'Please enter Long Description',
            'callsign.required' => 'Please enter Channel Call Sign',
            'callsign.valid_call_sign' => 'Invalid Channel Call Sign',
            'stream_name.required' => 'Please enter Stream Name',
            'channel_logo.required' => 'Please upload Channel Logo',
            'path.required' => 'Please enter Channel Path',
            'titan_channel_id.required' => 'Please enter Titan TV Channel Reference',
            'titan_channel_id.valid_titan' => 'Invalid Titan TV Channel Reference',
            'service_category.required' => 'Please enter Channel Service Category',
        );

        if (isset($postData['free_channel'])) {
            $postData['titan_channel_id'] = abs( crc32( uniqid() ) );
            $postData['stream_name'] = 'master/Free-DT3-3.3';
            $postData['service_category'] = 'StreamVisionFreeTV';
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
                'path' => 'required|unique:channels',
                'channel_logo' => 'required|mimes:jpeg',
            );
        } else {
            $postData['free_channel'] = 0;
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
                'path' => 'required|unique:channels',
                'callsign' => 'required|unique:channels|valid_call_sign',
                'stream_name' => 'required|unique:channels',
                'channel_logo' => 'required|mimes:jpeg',
                'service_category' => 'required',
                'titan_channel_id' => 'required|unique:channels|valid_titan',
            );

            Validator::extend('valid_titan', function ($attribute, $value, $parameters) {
                return Channel::titan_channel_validiation($value, 'channelId');
            });

            Validator::extend('valid_call_sign', function ($attribute, $value, $parameters) {
                return Channel::titan_channel_validiation($value, 'callsign');
            });
        }

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages


        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('channel_logo')) {
                $originalFileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('channel_logo')->getClientOriginalName());
                $extension = Input::file('channel_logo')->getClientOriginalExtension();
                $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                $fileName = $fileNameWithOutExt . "_" . time() . "." . $extension;

                $destinationPath = 'assets/data/image';
                Input::file('channel_logo')->move($destinationPath, $fileName);

                $image = Image::make(sprintf('assets/data/image/%s', $fileName))->resize(108, 69)->save();
            }
            // Check from global settings if running on Backup Stream URL
            $path = $postData['path'];

            $stream_url = Channel::getStreamURL($path, 'dvr');

            // Ad to DVR first, if success, then add the channel to the database
            $dvrResponse = Dvrapi::addChannel($postData['titan_channel_id'], $postData['stream_name'], $stream_url);

            Channel::addLiveStream($postData, $fileName);

            Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function doModifyLiveStream() {
        $postData = Input::all();
        $input = array('channel_logo' => Input::file('channel_logo'));
        $fileName = Input::get('hidchannel_logo');

        $messages = array(// setting up custom error messages for the field validation
            'title.required' => 'Please enter Title',
            'short_description.required' => 'Please enter Short Description',
            'long_description.required' => 'Please enter Long Description',
            'callsign.required' => 'Please enter Channel Call Sign',
            'callsign.valid_call_sign' => 'Invalid Channel Call Sign',
            'stream_name.required' => 'Please enter Stream Name',
            'path.required' => 'Please enter Channel Path',
            'titan_channel_id.required' => 'Please enter Titan TV Channel Reference',
            'service_category.required' => 'Please enter Channel Service Category',
            'titan_channel_id.valid_titan' => 'Invalid Titan TV Channel Reference',
        );

        if (isset($postData['free_channel'])) {
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
                'path' => 'required',
            );
        } else {
            $rules = array(
                'title' => 'required',
                'short_description' => 'required',
                'long_description' => 'required',
                'path' => 'required',
                'callsign' => 'required|valid_call_sign',
                'stream_name' => 'required',
                'service_category' => 'required',
                'titan_channel_id' => 'required|valid_titan',
            );

            Validator::extend('valid_titan', function ($attribute, $value, $parameters) {
                return Channel::titan_channel_validiation($value, 'channelId');
            });

            Validator::extend('valid_call_sign', function ($attribute, $value, $parameters) {
                return Channel::titan_channel_validiation($value, 'callsign');
            });
        }

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('channel_logo')) {
                $originalFileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('channel_logo')->getClientOriginalName());
                $extension = Input::file('channel_logo')->getClientOriginalExtension();
                $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                $fileName = $fileNameWithOutExt . "_" . time() . "." . $extension;

                $destinationPath = 'assets/data/image';
                Input::file('channel_logo')->move($destinationPath, $fileName);

                $image = Image::make(sprintf('assets/data/image/%s', $fileName))->resize(108, 69)->save();
            }

            // Check from global settings if running on Backup Stream URL
            $path = $postData['path'];

            $stream_url = Channel::getStreamURL($path, 'dvr');

            // Update on DVR first, if success, update in database
            $dvrResponse = Dvrapi::updateChannel($postData['titan_channel_id'], $postData['stream_name'], $stream_url);

            Channel::modifyLiveStream($postData, $fileName);

            Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function saveLiveStream() {
        $postData = Input::all();

        $messages = array(// setting up custom error messages for the field validation
            'path.required' => 'Please enter channel path',
        );

        $rules = array(
            'path' => 'required',
        );

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {

            Channel::modifyLiveStreamUrl($postData);

            $response_values = array(
                'success' => 1
            );
        }

        Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

        return Response::json($response_values);
    }

    public function saveServerUrl() {
        $postData = Input::all();

        $messages = array();

        $rules = array();

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {

            Channel::modifyServerUrl($postData);

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function sortChannel() {
        $results = Channel::getAllChannel();

        foreach ($results as $result) {
            $this->getChannelByCategory($result->channel_id);
        }
        $data = array(
            'channels' => $results,
            'subchannels' => $this->sub_channels,
            'index' => $this->index,
        );

        return View::make('sortablechannel')->with($data);
    }

    public function saveChannelOrder() {
        $postData = Input::all();

        Channel::modifyChannelOrder($postData['item'], 1);

        $response_values = array(
            'success' => 1
        );

        Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

        return Response::json($response_values);
    }

    public function sortLiveStream() {
        $results = Channel::getAllLiveStream();
        $data = array(
            'streams' => $results
        );

        return View::make('sortablelivestream')->with($data);
    }

    public function saveStreamorder() {
        $postData = Input::all();

        Channel::modifyStreamOrder($postData['channel']);

        $response_values = array(
            'success' => 1
        );

        Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

        return Response::json($response_values);
    }

    public function addVideoOnDemand() {
        $parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : 0;

        $collections = Channel::getAllCollections();
        $videos = Channel::getMediaByCollection(1);
        $data = array(
            'collections' => $collections,
            'videos' => $videos,
            'parent_id' => $parent_id
        );

        return View::make('addvideoondemand')->with($data);
    }

    public function doAddVideoOnDemand() {
        $postData = Input::all();
        $input = array('channel_logo' => Input::file('channel_logo'));

        $messages = array(// setting up custom error messages for the field validation
            'title.required' => 'Please enter Title',
            'short_description.required' => 'Please enter Short Description',
            'long_description.required' => 'Please enter Long Description',
            'trailers.required' => 'Please select Add Media',
            'channel_logo.required' => 'Please upload Channel Logo',
        );

        $rules = array(
            'title' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'trailers' => 'required',
            'channel_logo' => 'required|mimes:jpeg',
        );

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('channel_logo')) {
                $originalFileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('channel_logo')->getClientOriginalName());
                $extension = Input::file('channel_logo')->getClientOriginalExtension();
                $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                $fileName = $fileNameWithOutExt . "_" . time() . "." . $extension;

                $destinationPath = 'assets/data/image';
                Input::file('channel_logo')->move($destinationPath, $fileName);

                $image = Image::make(sprintf('assets/data/image/%s', $fileName))->resize(108, 69)->save();
            }

            Channel::addVideoOnDemand($postData, $fileName);

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function modifyVideoOnDemand() {
        $channel_id = Request::segment(3);
        $result = Channel::getChannelDataById($channel_id);
        $collections = Channel::getAllCollections();
        $videos = Channel::getMediaByCollection(1);
        $per_page = $_GET['per_page'];
        $sort_by = $_GET['sort_by'];
        $sort_order = $_GET['sort_order'];
        $page = $_GET['page'];
        $mediatrailers = Channel::getMediaByChannel($result->channel_id);

        $data = array(
            'channel_id' => $result->channel_id,
            'title' => $result->title,
            'short_description' => $result->short_description,
            'long_description' => $result->long_description,
            'channel_logo' => $result->channel_logo,
            'parent_id' => $result->parent_id,
            'pre_roll' => $result->pre_roll,
            'collections' => $collections,
            'videos' => $videos,
            'mediatrailers' => $mediatrailers,
            'per_page' => $per_page,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'page' => $page,
        );

        return View::make('editvideoondemand')->with($data);
    }

    public function doModifyVideoOnDemand() {
        $postData = Input::all();
        $input = array('channel_logo' => Input::file('channel_logo'));
        $fileName = Input::get('hidchannel_logo');

        $messages = array(// setting up custom error messages for the field validation
            'title.required' => 'Please enter Title',
            'short_description.required' => 'Please enter Short Description',
            'long_description.required' => 'Please enter Long Description',
            'trailers.required' => 'Please select Add Media',
            'channel_logo.required' => 'Please upload Channel Logo',
        );

        $rules = array(
            'title' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'trailers' => 'required',
            'channel_logo' => 'mimes:jpeg',
        );

        $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

        if ($validator->fails()) {
            $verrors = array();

            foreach ($validator->messages()->getMessages() as $field_name => $messages) {
                $verrors[$field_name] = $messages[0];
            }

            $response_values = array(
                'validation_failed' => 1,
                'errors' => $verrors
            );
        } else {
            if (Input::file('channel_logo')) {
                $originalFileName = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('channel_logo')->getClientOriginalName());
                $extension = Input::file('channel_logo')->getClientOriginalExtension();
                $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                $fileName = $fileNameWithOutExt . "_" . time() . "." . $extension;

                $destinationPath = 'assets/data/image';
                Input::file('channel_logo')->move($destinationPath, $fileName);

                $image = Image::make(sprintf('assets/data/image/%s', $fileName))->resize(108, 69)->save();
            }

            Channel::modifyVideoOnDemand($postData, $fileName);

            $response_values = array(
                'success' => 1
            );
        }

        return Response::json($response_values);
    }

    public function removeChannel() {
        $channel_id = Request::segment(3);
        $channel_type = Request::segment(4);

        $titan_channel_id = Channel::getChannelDataById($channel_id)->titan_channel_id;

        // Delete channel on DVR first, if success, delete from database
        $dvrResponse = Dvrapi::deleteChannel($titan_channel_id);

        if ($channel_type == 3) {
            $this->removeChild($channel_id, $channel_type);
        } else {
            Channel::removeChannel($channel_id, $channel_type);
        }

        Setting::set('channel_last_update_date', date("Y-m-d H:i:s"));

        $response_values = array(
            'success' => 1
        );

        return Response::json($response_values);
    }

    private function removeChild($cid, $ctype) {
        $results = Channel::getChannelByCategory($cid);

        foreach ($results as $result) {
            Channel::removeChannel($result->channel_id, $result->channel_type);
            $this->removeChild($result->channel_id, $result->channel_type);
        }

        Channel::removeChannel($cid, $ctype);
    }

    public function showCollection() {
        $collection_id = Request::segment(3);
        $page = (isset($_GET['page'])) ? $_GET['page'] : 2;
        $videos = Channel::getMediaByCollection($collection_id);

        $data = array(
            'collection_id' => $collection_id,
            'videos' => $videos,
            'page' => $page + 1
        );

        return View::make('videocollection')->with($data);
    }

    public function makeActive() {
        $channel_id = Request::segment(3);

        Channel::modifyChannelStatus($channel_id, 1);

        $response_values = array(
            'success' => 1
        );

        return Response::json($response_values);
    }

    public function makeInactive() {
        $channel_id = Request::segment(3);

        Channel::modifyChannelStatus($channel_id, 2);

        $response_values = array(
            'success' => 1
        );

        return Response::json($response_values);
    }

    public function settings() {

        if (Input::get('edit-channel-settings')) {
            $postData = Input::all();

            $messages = array(
                'nimble_stream' => 'API Endpoint field must not be empty',
                'nimble_dvr' => 'DVR Grace Period field must not be empty',
                'nimble_stream_backup' => 'DVR Grace Period field must not be empty',
                'nimble_dvr_backup' => 'DVR Grace Period field must not be empty'
            );

            $rules = array(
                'nimble_stream' => 'required',
                'nimble_dvr' => 'required',
                'nimble_stream_backup' => 'required',
                'nimble_dvr_backup' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);

            if ($validator->fails()) {  // send back to the page with the input data and errors
                return Redirect::to('settings/channel/')->withInput()->withErrors($validator);
            } else {
                Channel::updateSettings($postData);
                Session::flash('flash_message', 'Channel settings updated.');
                Session::flash('flash_type', 'alert-success');
            }
        }

        $data = array(
            'nimble_stream' => Setting::get('nimble_stream'),
            'nimble_dvr' => Setting::get('nimble_dvr'),
            'nimble_stream_backup' => Setting::get('nimble_stream_backup'),
            'nimble_dvr_backup' => Setting::get('nimble_dvr_backup'),
            'nimble_mode' => Setting::get('nimble_mode'),
            'nimble_port' => Setting::get('nimble_port'),
            'cm_mode' => Setting::get('cm_mode'),
        );

        return View::make('manage.channel')->with($data);
    }

}
