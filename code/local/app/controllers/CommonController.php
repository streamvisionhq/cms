<?php

class CommonController extends BaseController {

      public function __construct() {
            
      }

      public function showLogin() {
            return View::make('login'); // show the form
      }

      public function doLogin() {
            $postData = Input::all(); // getting all of the post data

            $messages = array(//setting up custom error messages for the field validation
                  'username.required' => 'Please enter Username',
                  'password.required' => 'Please enter Password',
            );

            $rules = array(
                  'username' => 'required',
                  'password' => 'required',
            );

            $validator = Validator::make($postData, $rules, $messages); // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) { // send back to the page with the input data and errors
                  return Redirect::to('login')->withInput()->withErrors($validator);
            }
            else {
                  $userData = array('username' => $postData['username'], 'password' => $postData['password']);

                  if ( Auth::attempt($userData) ) {
                        return Redirect::to('channelmanager');
                  }
                  else {
                        Session::flash('flash_message', 'Invalid username and password.');
                        Session::flash('flash_type', 'alert-danger');

                        return View::make('login');
                  }
            }
      }

      public function showForgot() {
            return View::make('forgot');   // show the form
      }

      public function doForgot() {
            $postData = Input::all();  // getting all of the post data

            $data = array('forgoterror' => '');

            // setting up custom error messages for the field validation
            $messages = array(
                  'email.required' => 'Please enter email address',
            );

            $rules = array(
                  'email' => 'required|email',
            );

            // doing the validation, passing post data, rules and the messages
            $validator = Validator::make($postData, $rules, $messages);

            if ( $validator->fails() ) { // send back to the page with the input data and errors
                  return Redirect::to('forgot')->withInput()->withErrors($validator);
            }
            else {
                  $result = User::validateUserData($postData, 'byemail');

                  if ( is_object($result) && $result->id > 0 ) {
                        $newPassword = str_random(10);
                        User::updateUserPassword(Hash::make($newPassword), $result->id);
                        $user = User::getUserData($result->id);

                        $data = array(
                              'firstname' => $user->firstname,
                              'lastname' => $user->lastname,
                              'username' => $user->username,
                              'password' => $newPassword,
                        );

                        $auser = array(
                              'email' => $user->email,
                              'name' => $user->firstname . ' ' . $user->lastname
                        );

                        Mail::send('emails.recoverpassword', $data, function($message) use ($auser) {
                              $message->to($auser['email'], $auser['name'])
                                      ->subject('Pro Livestream - Your login credentials!');
                        });

                        Session::flash('flash_message', 'The email has been sent. Please check your inbox.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('forgot');
                  }
                  else {
                        Session::flash('flash_message', 'The email address does n\'t exists.');
                        Session::flash('flash_type', 'alert-danger');
                        return View::make('forgot');
                  }
            }
      }

      public function doLogout() {
            Auth::logout();
            Session::flush();
            return Redirect::to('/');
      }

}
