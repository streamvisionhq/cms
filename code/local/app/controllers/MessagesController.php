<?php

class MessagesController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      /**
       * Manage Message
       * @return type
       */
      public function manageMessages() {
            $data['messages'] = Messages::getMessages();
            return View::make('manage/messages/messages')->with($data);
      }

      /**
       * Edit Screen
       * @param type $errorCode
       * @return type
       */
      public function editMessage($errorCode) {

            $data['message'] = Messages::getMessageDetail($errorCode);

            if ( Input::get('edit-message') ) {
                  $postData = Input::all(); // getting all of the post data

                  $messages = array(// setting up custom error messages for the field validation			
                        'message.required' => 'Message field must not be empty',
                        'description.required' => 'Description field must not be empty',
                        'type.required' => 'Select any one message type',
                  );

                  $rules = array(
                        'message' => 'required',
                        'description' => 'required',
                        'type' => 'required'
                  );

                  $validator = Validator::make($postData, $rules, $messages);

                  if ( $validator->fails() ) {  // send back to the page with the input data and errors
                        return Redirect::to('message/edit/' . $errorCode)->withInput()->withErrors($validator);
                  }
                  else {
                        $updateData = array('errorCode' => $errorCode, 'message' => Input::get('message'), 'description' => Input::get('description'), 'type' => Input::get('type'));
                        Messages::updateMessage($updateData);

                        Session::flash('flash_message', 'Error Message (' . $errorCode . ') updated.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('message/edit/' . $errorCode);
                  }
            }

            return View::make('manage/messages/edit-message')->with($data);
      }

}
