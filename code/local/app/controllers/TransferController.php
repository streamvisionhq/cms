<?php

class TransferController extends BaseController {

      public function customerTransfer() {
            if ( $emeraldUsers = Transfer::getEmeraldUsers() ) {
                  foreach ( $emeraldUsers as $emeraldUser ) {
                        $CustomerID = $emeraldUser->CustomerID;
                        if ( Customers::checkCustomer($CustomerID) ) {
                              Customers::addCustomer(array('CustomerID' => $CustomerID, 'FirstName' => $emeraldUser->FirstName, 'LastName' => $emeraldUser->LastName));
                              CustomerServices::addService(array('CustomerID' => $CustomerID, 'AccountID' => $emeraldUser->AccountID, 'ExternalRef' => $emeraldUser->ExternalRef, 'ServiceType' => $emeraldUser->ServiceType, 'Login' => $emeraldUser->Login, 'Password' => $emeraldUser->Password, 'Active' => $emeraldUser->Active, 'Domain' => $emeraldUser->Domain));
                        }
                  }
            }
      }

}
