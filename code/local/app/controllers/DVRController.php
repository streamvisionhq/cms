<?php

use Dvrapi\Dvrapi;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

require_once(Config::get('log4php.Logger'));
Logger::configure(Config::get('log4php.config'));

class DVRController extends BaseController {

    /**
     * log4php Object
     * @var object
     */
    public $logs;

    /**
     * Constructor of the class EmeraldController
     *
     */
    public function __construct() {
        $this->logs = Logger::getLogger('API');
    }

    /**
     * DVR Settings Screen
     * @return type
     */
    public function settings() {

        if ( Input::get('edit-dvr-settings') ) {
            $postData = Input::all();

            $messages = array(
                  'dvr-endpoint.required' => 'API Endpoint field must not be empty',
                  'dvr-grace-period' => 'DVR Grace Period field must not be empty'
            );

            $rules = array(
                  'dvr-endpoint' => 'required',
                  'dvr-grace-period' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);

            if ( $validator->fails() ) {  // send back to the page with the input data and errors
                return Redirect::to('settings/dvr/')->withInput()->withErrors($validator);
            }
            else {
                DVR::updateSettings($postData);
                Session::flash('flash_message', 'DVR settings updated.');
                Session::flash('flash_type', 'alert-success');

                return Redirect::to('settings/dvr/');
            }
        }

        $data = array(
              'endpoint' => Setting::get('dvr-endpoint'),
              'retention' => Setting::get('dvr-retention'),
              'startupStorage' => Setting::get('dvr-startup-storage'),
              'additionalStorage' => Setting::get('dvr-additional-storage'),
              'cm_mode' => Setting::get('cm_mode'),
              'gracePeriod' => Setting::get('dvr-grace-period')
        );
        return View::make('manage.dvr')->with($data);
    }

    /**
     * Handles a request to record a show from the mobile apps.
     * Expects the following post data: {
     *  epgId: (int) EPG channel id
     *  epgxId: (int) epgx_id id from the EPG data
     *  channelId: (int) StreamVision Channel manager channel ID, ignored for now
     *  titanShowId: (int) the TitanTV EPG ID for the show to record
     *  showName: (string) the name of the show, ignored for now
     *  customerTimezone: (string) the timezone the client is in
     *  startDatetime: (string) date and time when the show starts, in "2016-01-22 13:01:00" format, ignored for now
     *  maxDuration: (int) recording duration for the show, ignored for now
     *  series: (boolean) whether to record a the whole series, should be false, for single episode
     *  retention: (int) retention period for the show, ignored for now
     * }
     * @param int $customerId the id of the customer sending the request
     * @param string $password customers password, used in filter before this route
     * @return 
     */
    public function record($customerId, $password) {
        $params = array('CustomerID' => $customerId, 'Password' => $password);
        try {
            $this->logs->info('DVRController record Start.');
            return $this->sendDvrRecordRequest('add', $customerId);
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Sends a DVR recording request to the DVR system, either to add a recording or to update it
     * @param string $type 'add' to add a new recording, 'update' to update an existing one
     * @param int $customerId the customer the recording is for
     * @return array returns $this->handleSuccess(...) or $this->handleError(...) with approriate messages
     */
    private function sendDvrRecordRequest($type, $customerId) {
        try {
            $params = array('Type' => $type, 'CustomerID' => $customerId);
            $this->logs->info('DVRController sendDvrRecordRequest Start.');
            $postData = $this->prepareDvrData();

            $svChannel = EChannel::where('channel_id', $postData['channelId'])->first();
            
            // auhtorize channel
            if ( $rokuServices = CustomerServices::getServices($customerId, "Roku", 1, $svChannel->emerald_subscription_type) ) {
                if ( CustomerServices::radiusAuthentication($rokuServices[0]->Login, $rokuServices[0]->Password) ) {
                    if ( SubscriptionType::hasAccess($rokuServices[0]->ServiceName, $svChannel->emerald_subscription_type) ) {
                        $response = Dvrapi::recordDVR($type, $postData['epgId'], $postData['epgxId'], $postData['titanShowId'], $customerId, $postData['customerTimezone'], $postData['series'] == "true" ? 'rr' : 'r'
                        );

                        if ( $response['status'] ) {
                            $this->logs->trace(json_encode($response));
                            $this->logs->info('DVRController sendDvrRecordRequest Successfully Ends for CustomerID:' . $customerId . ', Type:' . $type);
                            $this->logs->info('DVRController record/update Ends for CustomerID:' . $customerId . ', Type:' . $type);
                            return $this->handleSuccess(array(), $response['message']);
                        }
                        else {
                            $this->logs->error(Messages::getMessage('2025') . ', Code: 2025, CustomerID:' . $customerId . ', Type:' . $type);
                            $this->logs->error('Response:' . json_encode($response));
                            return $this->handleError(Messages::getMessage('2025'), "2025");
                        }
                    }
                    else {
                        $this->logs->error(Messages::getMessage('2007') . ', Code: 2007, CustomerID:' . $customerId . ', Type:' . $type);
                        return $this->handleError(Messages::getMessage('2007'), "2007");
                    }
                }
                else {
                    $this->logs->error(Messages::getMessage('2006') . ', Code: 2006, CustomerID:' . $customerId . ', Type:' . $type);
                    return $this->handleError(Messages::getMessage('2006'), "2006");
                }
            }
            else {
                $this->logs->error(Messages::getMessage('2005') . ', Code: 2005, CustomerID:' . $customerId . ', Type:' . $type);
                $this->logs->error('Response:' . json_encode($rokuServices));
                return $this->handleError(Messages::getMessage('2005'), "2005");
            }
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Deletes a show recording.
     * Expects the following post data: {
     *  titanShowId: (int) the TitanTV EPG ID for the show to delete,
     *  deleteRR: (string) whether to delete RR recorded shows as well ("true" if yes)
     * }
     * @param int $customerId the id of the customer sending the request
     * @param string $password customers password, used in filter before this route
     * @return 
     */
    public function delete($customerId, $password) {
        $params = array('CustomerID' => $customerId, 'Password' => $password);
        try {
            $this->logs->info('DVRController delete Start.');
            $showId = Input::get('titanShowId');
            $deleteRR = Input::get('deleteRR') == 'true';
            $this->logs->trace(json_encode(array('CustomerID:' => $customerId, 'ShowID' => $showId, 'DeleteRR' => $deleteRR)));
            $response = Dvrapi::deleteRecordRequest($customerId, $showId, $deleteRR);

            if ( $response['status'] ) {
                $this->logs->trace(json_encode($response));
                $this->logs->info('DVRController delete Successfully Ends for CustomerID:' . $customerId);
                return $this->handleSuccess(array(), $response['message']);
            }
            else {
                $this->logs->trace(json_encode($response));
                $this->logs->error(Messages::getMessage('2024') . ', Code: 2024 ,CustomerId:' . $customerId . ', ShowID:' . $showId . ', deleteRR:' . $deleteRR);
                $this->logs->error('Request:' . json_encode($params) . ', Response:' . json_encode($response));
                return $this->handleError(Messages::getMessage('2024'), "2024");
            }
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Updates a show recording.
     * Expects the following post data: {
     *  epgId: (int) EPG channel id
     *  epgxId: (int) epgx_id id from the EPG data
     *  channelId: (int) StreamVision Channel manager channel ID
     *  titanShowId: (int) the TitanTV EPG ID for the show to update
     *  showName: (string) the name of the show, ignored for now
     *  customerTimezone: (string) the timezone the client is in
     *  startDatetime: (string) date and time when the show starts, in "2016-01-22 13:01:00" format, ignored for now
     *  maxDuration: (int) recording duration for the show, ignored for now
     *  series: (boolean) whether to record a the whole series, should be false, for single episode
     *  retention: (int) retention period for the show, ignored for now
     * }
     * @param int $customerId the id of the customer sending the request
     * @param string $password customers password, used in filter before this route
     * @return 
     */
    public function update($customerId, $password) {
        $params = array('CustomerID' => $customerId, 'Password' => $password);
        try {
            $this->logs->info('DVRController update Start.');
            return $this->sendDvrRecordRequest('update', $customerId);
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Gets the customer playlist and queue from the DVR API.
     * @param int $customerId the id of the customer sending the request
     * @param string $password customers password, used in filter before this route
     * @return 
     */
    public function playlist($customerId, $password) {
        try {
            $params = array('CustomerID' => $customerId, 'Password' => $password);
            $this->logs->info('DVRController playlist Start.');
            $this->logs->trace('CustomerID:' . $customerId);
            $recorded = Dvrapi::getRecordShow($customerId);
            $queued = Dvrapi::getScheduledShow($customerId);
            if ( $queued == null ) {
                $queued = array();
            }
            $this->remapQueuedShows($queued);
            $this->logs->trace(json_encode(array(
                  'recorded' => $recorded,
                  'queued' => $queued
                            )
                    )
            );
            $this->logs->info('DVRController playlist Successfully Ends for CustomerID:' . $customerId);

            return $this->handleSuccess(array(
                          'recorded' => $recorded,
                          'queued' => $queued
            ));
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Changes the keys in scheduled/queued shows to match the same key names as recorded shows.
     */
    private function remapQueuedShows(&$queued) {
        foreach ( $queued AS &$q ) {
            $q->title = $q->show_name;
            $q->duration = $q->max_duration;
            $q->starttimeutc = $q->start_datetime;
            $q->eventid = $q->show_id;
            $q->event_play_url = $q->play_url;

            $startDate = Carbon::parse($q->start_datetime, 'UTC');
            $startDate->hour = 0;
            $startDate->minute = 0;
            $startDate->second = 0;
            $q->startdateutc = $startDate->toDateTimeString();

            unset($q->show_name);
            unset($q->max_duration);
            unset($q->start_datetime);
            unset($q->show_id);
            unset($q->play_url);
        }
    }

    public function getEpgv2($page) {
        set_time_limit(0);
        try {
            $data = Input::all();
            $this->logs->info('DVRController getEPG Page Vise Start.');
            $dvrEpg = Dvrapi::getEPG(date('Y-m-d'), 'America/Phoenix');

            $request = Request::create('/api/epgv2/page/' . $page, 'GET');
            $epgData = json_decode(Route::dispatch($request)->getContent());
            //return json_encode($epgData);
            $result = array();
            foreach ( $epgData->data AS &$channel ) {
                if ( property_exists($channel, 'shows') ) {
                    if ( !isset($result[$channel->channelId]) ) {
                        $result[$channel->channelId] = array(
                              'channel_id' => $channel->channelId,
                              'shows' => array()
                        );
                        $newChannel = &$result[$channel->channelId];
                    }
                    else {
                        $newChannel = &$result[$channel->channelId];
                    }
                    $dvrShows = $this->getShowsForChannel($dvrEpg, $channel->channelId);

                    foreach ( $channel->shows AS &$show ) {
                        $newShow = array();
                        foreach ( $dvrShows AS &$dvrShow ) {
                            if ( $this->areShowsEqual($show, $dvrShow, $channel->channelId) ) {
                                $newShow['epgx_id'] = $dvrShow->epgx_id;
                                $newShow['eventid'] = $dvrShow->eventid;
                                $newShow['event_play_url'] = $dvrShow->event_play_url;
                                $newShow['id'] = $dvrShow->id;
                                $newShow['episodetitle'] = $dvrShow->episodetitle;
                            }
                        }

                        // convert keys to fit the app needs, easier to do it here than rework app
                        $newShow['title'] = $show->Title;
                        $newShow['description'] = $show->Description;
                        $epgStartDate = new DateTime($show->startTimeUtc);
                        $epgEndDate = new DateTime($show->endTimeUtc);
                        $newShow['starttimeutc'] = $epgStartDate->format('Y-m-d H:i:s');
                        $newShow['duration'] = $epgEndDate->format('U') - $epgStartDate->format('U');

                        $epgAdjStartDate = new DateTime($show->adjustedstarttime);
                        $epgAdjEndDate = new DateTime($show->adjustedendtime);
                        $newShow['adjustedstarttime'] = $epgAdjStartDate->format('Y-m-d H:i:s');
                        $newShow['adjustedendtime'] = $epgAdjEndDate->format('Y-m-d H:i:s');

                        // since EPG returns duplicates, only add if not added already
                        if ( !$this->isShowInArray($newChannel['shows'], $newShow) ) {
                            $newChannel['shows'][] = $newShow;
                        }
                    }
                }
            }
            $this->logs->trace(json_encode($result));
            $this->logs->info('DVRController getEPG Successfully Ends.');
            $last_modified_date = $this->getLastModifiedDate($page);
            return $this->handleSuccess(array(
                          'epg' => array_values($result) // get rid of channel ids as array indexes
                            ), null, $last_modified_date);
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($data));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Gtes the EPG wit necessary data to do DVR requests.
     * Expects customerDate (YYYY-MM-DD) and customerTimezone (string) as post params.
     * @return
     */
    public function getEpg() {
        set_time_limit(0);
        try {
            $data = Input::all();
            $this->logs->info('DVRController getEPG Start.');
            $postData = Input::only('customerDate', 'customerTimezone');
            $this->logs->trace(json_encode($postData));
            $date = Carbon::createFromFormat('Y-m-d H:i:s', $postData['customerDate'] . ' 00:00:00', $postData['customerTimezone']);
            $date->setTimezone('UTC');
            $dateString = $date->toDateString();
            $dvrEpg = Dvrapi::getEPG($dateString, $postData['customerTimezone']);

            $request = Request::create('/api/epg', 'GET', Input::all());
            $epgData = json_decode(Route::dispatch($request)->getContent());
            //return json_encode($epgData);
            $result = array();
            foreach ( $epgData->data AS &$channel ) {
                if ( property_exists($channel, 'shows') ) {
                    if ( !isset($result[$channel->channelId]) ) {
                        $result[$channel->channelId] = array(
                              'channel_id' => $channel->channelId,
                              'shows' => array()
                        );
                        $newChannel = &$result[$channel->channelId];
                    }
                    else {
                        $newChannel = &$result[$channel->channelId];
                    }
                    $dvrShows = $this->getShowsForChannel($dvrEpg, $channel->channelId);

                    foreach ( $channel->shows AS &$show ) {
                        $newShow = array();
                        foreach ( $dvrShows AS &$dvrShow ) {
                            if ( $this->areShowsEqual($show, $dvrShow, $channel->channelId) ) {
                                $newShow['epgx_id'] = $dvrShow->epgx_id;
                                $newShow['eventid'] = $dvrShow->eventid;
                                $newShow['event_play_url'] = $dvrShow->event_play_url;
                                $newShow['id'] = $dvrShow->id;
                                $newShow['episodetitle'] = $dvrShow->episodetitle;
                            }
                        }

                        // convert keys to fit the app needs, easier to do it here than rework app
                        $newShow['title'] = $show->Title;
                        $newShow['description'] = $show->Description;
                        $epgStartDate = new DateTime($show->startTimeUtc);
                        $epgEndDate = new DateTime($show->endTimeUtc);
                        $newShow['starttimeutc'] = $epgStartDate->format('Y-m-d H:i:s');
                        $newShow['duration'] = $epgEndDate->format('U') - $epgStartDate->format('U');

                        $epgAdjStartDate = new DateTime($show->adjustedstarttime);
                        $epgAdjEndDate = new DateTime($show->adjustedendtime);
                        $newShow['adjustedstarttime'] = $epgAdjStartDate->format('Y-m-d H:i:s');
                        $newShow['adjustedendtime'] = $epgAdjEndDate->format('Y-m-d H:i:s');

                        // since EPG returns duplicates, only add if not added already
                        if ( !$this->isShowInArray($newChannel['shows'], $newShow) ) {
                            $newChannel['shows'][] = $newShow;
                        }
                    }
                }
            }
            $this->logs->trace(json_encode($result));
            $this->logs->info('DVRController getEPG Successfully Ends.');
            return $this->handleSuccess(array(
                          'epg' => array_values($result) // get rid of channel ids as array indexes
            ));
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($data));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Parses EPG data from the DVR API to find shows in a specfici channel.
     * @param array $dvrEpg
     * @param int $channelId
     */
    private function getShowsForChannel($dvrEpg, $channelId) {
        $shows = array();
        foreach ( $dvrEpg AS &$show ) {
            if ( $show->channelid == $channelId ) {
                $shows[] = $show;
            }
        }

        return $shows;
    }

    /**
     * Checks whether a show has been added to an array yet.
     * @param array $array the array of shows
     * @param array $showToFind the show to look for
     * @return boolean
     */
    private function isShowInArray(&$array, &$showToFind) {
        foreach ( $array AS &$show ) {
            if ( $show['title'] == $showToFind['title'] //&& $show['channelid'] == $showToFind['channelid']
                    && $show['starttimeutc'] == $showToFind['starttimeutc'] ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Compares shows from EPG and DVR APIs to determine if they are the same.
     * Uses date, description and title to deterine equalty.
     * @param array $epgShow
     * @param array $dvrShow
     */
    private function areShowsEqual($epgShow, $dvrShow, $epgChannelId) {
        if ( $epgChannelId != $dvrShow->channelid ) {
            return false;
        }

        if ( $epgShow->eventId != $dvrShow->eventid ) {
            return false;
        }

        $epgDateString = substr($epgShow->startTimeUtc, 0, 10);
        $epgTimeString = substr($epgShow->startTimeUtc, 11, 8);
        $epgDate = new DateTime($epgDateString . ' ' . $epgTimeString, new DateTimeZone('UTC'));
        $dvrDate = new DateTime($dvrShow->starttimeutc, new DateTimeZone('UTC'));
        if ( $epgDate != $dvrDate ) {
            return false;
        }

        return true;
    }

    /**
     * Get the users remaining and total storage.
     * @param int $customerId the id of the customer sending the request
     * @param string $password customers password, used in filter before this route
     */
    public function storage($customerId, $password) {
        try {
            $params = array('CustomerID' => $customerId, 'Password' => $password);
            $this->logs->info('DVRController storage Start.');
            $response = Dvrapi::getStorage($customerId);
            $this->logs->trace(json_encode($response));
            if ( is_string($response) ) {
                $this->logs->error(Messages::getMessage('2026') . ', Code: 2026, CustomerID:' . $customerId);
                $this->logs->error('Request:' . json_encode($params) . ', Response:' . json_encode($response));
                return $this->handleError(Messages::getMessage('2026'), "2026");
            }
            else {
                $this->logs->trace(json_encode(array(
                      'remaining' => $response->available,
                      'total' => DVR::totalStorage($customerId),
                      'used' => $response->used
                                )
                        )
                );
                $this->logs->info('DVRController storage Successfully Ends CustomerID:' . $customerId);
                return $this->handleSuccess(array(
                              'remaining' => $response->available,
                              'total' => DVR::totalStorage($customerId),
                              'used' => $response->used
                ));
            }
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Takes only the required values from the POST request.
     * @return array 
     */
    protected function prepareDvrData() {
        return Input::only('epgId', 'epgxId', 'titanShowId', 'customerTimezone', 'series', 'channelId');
    }

    /**
     * Deactivate Pneding Delete DVR Storages (Cron)
     */
    public function deactivateDVR() {
        try {
            $this->logs->info('DVRController deactivateDVR Start.');
            $count = 0;
            if ( $storages = DVR::getDeletePendingStorage() ) {
                foreach ( $storages as $storage ) {
                    $count++;
                    DVR::deleteDVRStorage($storage->customerServiceId, $storage->CustomerID);
                }
            }
            $this->logs->info('DVRController deactivateDVR Successfully Ends.');
            echo $count . " Storage Deleted";
        } catch ( Exception $e ) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Authorize Recorded Stream From Roku
     * @param type $customerId
     * @param type $deviceID     
     */
    public function authorizeRecordedStream($customerId, $deviceID, $eventID) {
        try {
            $params = array('CustomerID' => $customerId, 'DeviceID' => $deviceID, 'EventID' => $eventID);
            $action = Input::get('action');
            $this->logs->info('DVRController authorizeRecordedStream Start.');
            $this->logs->trace('CustomerID:' . $customerId . ', deviceID:' . $deviceID . ', EventID:' . $eventID);
            $hasDvrBasic = Customers::hasDvrBasicService($customerId);

            if ( $hasDvrBasic !== true ) {
                $this->logs->error(Messages::getMessage($hasDvrBasic) . ', Code: ' . $hasDvrBasic . ', CustomerID:' . $customerId . ', deviceId:' . $deviceID . ', EventID:' . $eventID);
                $this->logs->error('Request:' . json_encode($params));
                return $this->handleError(Messages::getMessage($hasDvrBasic), $hasDvrBasic);
            }

            if ( !Customers::hasDvrWholeHomeService($customerId) ) {
                $default = RokuDevices::getDefaultDevice($customerId);
                if ( $default->DeviceID != $deviceID ) {
                    $this->logs->error(Messages::getMessage(2015) . ', Code: 2015, CustomerID:' . $customerId . ', deviceId:' . $deviceID . ', EventID:' . $eventID);
                    $this->logs->error('Request:' . json_encode($params));
                    return $this->handleError(Messages::getMessage(2015), 2015);
                }
            }

            if ( !$event = DVR::getEvent($customerId, $eventID) ) {
                $this->logs->error(Messages::getMessage(2017) . ', Code: 2017, CustomerID:' . $customerId . ', deviceId:' . $deviceID . ', EventID:' . $eventID);
                $this->logs->error('Request:' . json_encode($params));
                return $this->handleError(Messages::getMessage(2017), 2017);
            }
            $this->logs->trace(json_encode(array('event_play_url' => $event->event_play_url, 'title' => $event->title, 'episodetitle' => $event->episodetitle)));
            $this->logs->info('DVRController authorizeRecordedStream Successfully Ends on DVR.');

            $svChannel = EChannel::where('titan_channel_id', $event->channelid)->where('status', 1)->first();

            //Get major & minor number from stream name                                                
            $major_minor = Channel::get_major_minor($svChannel->stream_name);

            //Send log to central server
            $data = [
                  'customer_id' => $customerId,
                  'action_type' => $action,
                  'stream_type' => 'DVR',
                  'titan_id' => $event->channelid,
                  'major_number' => $major_minor[0],
                  'minor_number' => $major_minor[1],
                  'show_title' => $event->title,
                  'show_episode_title' => $event->episodetitle,
            ];
            $url = action('LogController@process', $data);
            $process = new Process('curl "' . $url . '" &');
            $process->start();

            return $this->handleSuccess(['event_play_url' => $event->event_play_url, 'title' => $event->title, 'episodetitle' => $event->episodetitle, 'titan_id' => $event->channelid, 'major_number' => $major_minor[0], 'minor_number' => $major_minor[1]]);
        } catch ( Exception $e ) {
            $this->logs->error($e);
            $this->logs->error('Request:' . json_encode($params));
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    public function refreshChannels() {
        $channelErrors = array();
        try {
            $this->logs->info('DVRController refreshChannels Start.');
            $dvrChannels = Dvrapi::getChannels();
            $svChannels = Channel::getAllChannel(); // Retrive all channels from StreamVision Database

            foreach ( $svChannels as $svc ) {
                $changes = false;
                $GLOBALS['cmsChannel'] = $svc;
                $channelObject = array_filter($dvrChannels, function ($e) {
                    global $cmsChannel;
                    return $e->epg_channel_id == $cmsChannel->titan_channel_id;
                });

                $path = $svc->path;
                $streamURL = Channel::getStreamURL($path, 'dvr');

                if ( count($channelObject) == 0 ) {
                    // Add Channels to DVR
                    $dvrResponse = Dvrapi::addChannel($svc->titan_channel_id, $svc->stream_name, $streamURL);
                    if ( $dvrResponse['status'] != true || $dvrResponse['status'] != 1 ) {
                        $channelErrors['error'][] = array('titan_channel_id' => $svc->titan_channel_id, 'sv_channel_id' => $svc->channel_id, 'stream_name' => $svc->stream_name, 'error_message' => $dvrResponse['message']);
                    }
                    else {
                        $channelErrors['success'][] = array('titan_channel_id' => $svc->titan_channel_id, 'sv_channel_id' => $svc->channel_id, 'stream_name' => $svc->stream_name);
                    }
                }
                else {
                    foreach ( $channelObject as $channelData ) {
                        if ( $channelData->stream_name != $svc->stream_name ) {
                            $changes = true;
                        }
                        if ( $channelData->stream_url != $streamURL ) {
                            $changes = true;
                        }

                        if ( $changes === true ) {
                            $dvrResponse = Dvrapi::updateChannel($svc->titan_channel_id, $svc->stream_name, $streamURL);

                            if ( $dvrResponse['status'] != true || $dvrResponse['status'] != 1 ) {
                                $channelErrors['error'][] = array('titan_channel_id' => $svc->titan_channel_id, 'sv_channel_id' => $svc->channel_id, 'stream_name' => $svc->stream_name, 'error_message' => $dvrResponse['message']);
                            }
                            else {
                                $channelErrors['success'][] = array('titan_channel_id' => $svc->titan_channel_id, 'sv_channel_id' => $svc->channel_id, 'stream_name' => $svc->stream_name);
                            }
                        }
                    }
                }
            }

            $this->logs->trace(json_encode($channelErrors));
            $this->logs->info('DVRController refreshChannels Successfully Ends on DVR.');
            return $this->handleSuccess(array(
                          'message' => "Channels Refreshed successfully on DVR",
                          'channels_report' => $channelErrors
            ));
        } catch ( Exception $e ) {
            $this->logs->error(json_encode($channelErrors));
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

}
