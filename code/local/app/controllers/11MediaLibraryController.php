<?php

class MediaLibraryControllerUnused extends BaseController {

      private $uid = 0;
      private $zencoder_apikey;
      public $op;

      public function __construct() {
            $this->uid = Auth::id();
            $this->zencoder_apikey = 'a5ae9e0b95c11364c40419366fa6520e';
      }

      public function showMediaLibrary() {
            $collections = MediaLibrary::allCollections();
            $result = MediaLibrary::getTopCollection();

            $data = array(
                  'collections' => $collections,
                  'collect_id' => $result->collection_id,
                  'collect_status' => $result->status,
            );

            return View::make('medialibrary')->with($data);
      }

      public function getSearchVideo() {
            $term = (isset($_GET['term'])) ? $_GET['term'] : '';
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

            $results = MediaLibrary::searchVideo($term);

            $data = array(
                  'lvideos' => $results,
                  'term' => $term,
                  'page' => $page
            );

            return View::make('listsearchvideo')->with($data);
      }

      public function addCollection() {
            return View::make('addcollection');
      }

      public function doAddCollection() {
            $response_values = array();
            $postData = Input::all();

            $messages = array(// setting up custom error messages for the field validation
                  'collection_name.required' => 'Please enter Collection Name'
            );

            $rules = array(
                  'collection_name' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  $verrors = array();

                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }

                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $result = MediaLibrary::chkCollectionExists($postData, 0);

                  if ( is_null($result) ) {
                        MediaLibrary::addCollection($postData);

                        $response_values = array(
                              'success' => 1
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['collection_name'] = 'Collection Name already exists';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function modifyCollection() {
            $collection_id = Request::segment(3);
            $result = MediaLibrary::getCollectionById($collection_id);

            $data = array(
                  'collection_id' => $result->collection_id,
                  'collection_name' => $result->collection_name,
            );

            return View::make('editcollection')->with($data);
      }

      public function doModifyCollection() {
            $response_values = array();
            $postData = Input::all();

            $messages = array(// setting up custom error messages for the field validation
                  'collection_name.required' => 'Please enter Collection Name'
            );

            $rules = array(
                  'collection_name' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  $verrors = array();

                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }

                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $result = MediaLibrary::chkCollectionExists($postData, $postData['collection_id']);

                  if ( is_null($result) ) {
                        MediaLibrary::modifyCollection($postData);

                        $response_values = array(
                              'success' => 1
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['collection_name'] = 'Collection Name already exists';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function removeCollection() {
            $collection_id = Request::segment(3);

            MediaLibrary::removeCollection($collection_id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeCollectionActive() {
            $collection_id = Request::segment(3);

            MediaLibrary::modifyCollectionStatus($collection_id, 1);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeCollectionInactive() {
            $collection_id = Request::segment(3);

            MediaLibrary::modifyCollectionStatus($collection_id, 2);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function getVideo() {
            $results = MediaLibrary::allVideos();
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

            $data = array(
                  'videos' => $results,
                  'curPage' => $page
            );

            return View::make('video')->with($data);
      }

      public function addVideo() {
            return View::make('addvideo');
      }

      public function doAddVideo() {
            ini_set('max_execution_time', 0);

            $postData = Input::all();
            $input = array('media_name' => Input::file('media_name'));
            $verrors = array();
            $error = false;

            $messages = array(// setting up custom error messages for the field validation
                  'name.required' => 'Please enter Media Name',
                  'meta_tag.required' => 'Please enter Meta Tag',
                  'meta_description.required' => 'Please enter Meta Description',
                  'media_name.required' => 'Please upload Media File.',
            );

            $rules = array(
                  'name' => 'required',
                  'meta_tag' => 'required',
                  'meta_description' => 'required',
                  'media_name' => 'required | max:900000000',
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }
                  $error = true;
            }
            else if ( Input::file('media_name')->getClientOriginalName() != '' && Input::file('media_name')->getMimeType() != 'video/mp4' ) {
                  $verrors['media_name'] = "Invalid file type, video must be of type mp4.";
                  $error = true;
            }

            if ( $error == true ) {
                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $tstamp = time();
                  $originalFileName = Input::file('media_name')->getClientOriginalName();
                  $extension = Input::file('media_name')->getClientOriginalExtension();
                  $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                  $fileName = $fileNameWithOutExt . "_" . $tstamp . "." . $extension;
                  $destinationPath = 'assets/uploads';
                  Input::file('media_name')->move($destinationPath, $fileName);

                  $convThumbname = $fileNameWithOutExt . "_" . $tstamp;
                  $convFile = $fileNameWithOutExt . "_" . $tstamp . ".mp4";
                  $convThumb = $fileNameWithOutExt . "_" . $tstamp . ".png";

                  //create jobs			
                  try {
                        // Initialize the Services_Zencoder class
                        $zencoder = new Services_Zencoder($this->zencoder_apikey);

                        // New Encoding Job
                        //
					$encoding_job = $zencoder->jobs->create(
                                array(
                                      "input" => URL::to('/') . "/assets/uploads/" . $fileName,
                                      "outputs" => array(
                                            array(
                                                  "label" => "web",
                                                  "thumbnails" => array(
                                                        "width" => 290,
                                                        "height" => 218,
                                                        "public" => true,
                                                        "filename" => $convThumbname,
                                                        "base_url" => "s3://provideodev/media/image/"
                                                  ),
                                                  "public" => true,
                                                  "filename" => $convFile,
                                                  "url" => "s3://provideodev/media/" . $convFile
                                            )
                                      )
                                )
                        );

                        $job_id = $encoding_job->id;

                        // Store Job/Output IDs to update their status when notified or to check their progress.

                        MediaLibrary::addVideo($postData, $job_id, $convFile, $convThumb);

                        $response_values = array(
                              'success' => 1
                        );
                  } catch ( Services_Zencoder_Exception $e ) {
                        $verrors = array();
                        $verrors['media_name'] = 'Failed to transcode media file.';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function editVideo() {
            $media_id = Request::segment(3);
            $page = $_GET['cur_page'];
            $result = MediaLibrary::getVideoById($media_id);

            $data = array(
                  'media_id' => $result->media_id,
                  'name' => $result->name,
                  'meta_tag' => $result->meta_tag,
                  'meta_description' => $result->meta_description,
                  'media_image' => $result->media_image,
                  'job_id' => $result->job_id,
                  'job_duration' => $result->job_duration,
                  'descriptions' => $result->descriptions,
                  'frame_width' => $result->frame_width,
                  'frame_height' => $result->frame_height,
                  'file_size' => $result->file_size,
                  'content' => $result->content,
                  'genre' => $result->genre,
                  'rating' => $result->rating,
                  'release_date' => $result->release_date,
                  'show_description' => $result->show_description,
                  'closed_captions' => $result->closed_captions,
                  'generic_tags' => $result->generic_tags,
                  'rights' => $result->rights,
                  'similar_content' => $result->similar_content,
                  'curPage' => $page
            );

            return View::make('editvideo')->with($data);
      }

      public function doEditVideo() {
            ini_set('max_execution_time', 0);

            $postData = Input::all();
            $input = array('media_name' => Input::file('media_name'));
            $job_id = $postData['job_id'];
            $convFile = '';
            $convThumb = '';
            $verrors = array();
            $error = false;

            $messages = array(// setting up custom error messages for the field validation
                  'name.required' => 'Please enter Media Name',
                  'meta_tag.required' => 'Please enter Meta Tag',
                  'meta_description.required' => 'Please enter Meta Description'
            );

            $rules = array(
                  'name' => 'required',
                  'meta_tag' => 'required',
                  'meta_description' => 'required',
                  'media_name' => 'max:900000000',
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }
                  $error = true;
            }
            else if ( Input::file('media_name') ) {
                  if ( Input::file('media_name')->getClientOriginalName() != '' && Input::file('media_name')->getMimeType() != 'video/mp4' ) {
                        $verrors['media_name'] = "Invalid file type, video must be of type mp4.";
                        $error = true;
                  }
            }

            if ( $error == true ) {
                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  if ( Input::file('media_name') ) {
                        $tstamp = time();
                        $originalFileName = Input::file('media_name')->getClientOriginalName();
                        $extension = Input::file('media_name')->getClientOriginalExtension();
                        $fileNameWithOutExt = basename($originalFileName, "." . $extension);
                        $fileName = $fileNameWithOutExt . "_" . $tstamp . "." . $extension;
                        $destinationPath = 'assets/uploads';
                        Input::file('media_name')->move($destinationPath, $fileName);

                        $convThumbname = $fileNameWithOutExt . "_" . $tstamp;
                        $convFile = $fileNameWithOutExt . "_" . $tstamp . ".mp4";
                        $convThumb = $fileNameWithOutExt . "_" . $tstamp . ".png";

                        try {
                              // Initialize the Services_Zencoder class
                              $zencoder = new Services_Zencoder($this->zencoder_apikey);

                              // New Encoding Job
                              $encoding_job = $zencoder->jobs->create(
                                      array(
                                            "input" => URL::to('/') . "/assets/uploads/" . $fileName,
                                            "outputs" => array(
                                                  array(
                                                        "label" => "web",
                                                        "thumbnails" => array(
                                                              "width" => 290,
                                                              "height" => 218,
                                                              "public" => true,
                                                              "filename" => $convThumbname,
                                                              "base_url" => "s3://provideodev/media/image/"
                                                        ),
                                                        "public" => true,
                                                        "filename" => $convFile,
                                                        "url" => "s3://provideodev/media/" . $convFile
                                                  )
                                            )
                                      )
                              );

                              $job_id = $encoding_job->id;
                        } catch ( Services_Zencoder_Exception $e ) {
                              $job_id = 0;
                        }
                  }

                  if ( $job_id > 0 ) {
                        MediaLibrary::modifyVideo($postData, $job_id, $convFile, $convThumb);
                        if ( $convFile == '' ) {
                              $suc_msg = 'Media details modified successfully.';
                        }
                        else {
                              $suc_msg = 'Transcoded Job created successfully for media file.';
                        }
                        $response_values = array(
                              'success' => 1,
                              'suc_msg' => $suc_msg
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['media_name'] = 'Failed to transcode media file.';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function processTranscodeJob() {
            $media_id = Request::segment(3);
            $job_id = Request::segment(4);
            $job_duration = 0;
            $job_data = array();
            try {
                  $zencoder = new Services_Zencoder($this->zencoder_apikey);
                  $process_job = $zencoder->jobs->details($job_id);

                  $result = MediaLibrary::getJobStatusByDesc($process_job->state);
                  $job_status_id = $result->status_id;
                  $job_duration = ($result->status_id == 4) ? $process_job->outputs['web']->duration_in_ms : 0;
                  $job_data['frame_width'] = ($result->status_id == 4) ? $process_job->outputs['web']->width : 0;
                  $job_data['frame_height'] = ($result->status_id == 4) ? $process_job->outputs['web']->height : 0;
                  $job_data['file_size'] = ($result->status_id == 4) ? $process_job->outputs['web']->file_size_bytes : 0;

                  MediaLibrary::modifyVideoJobStatus($media_id, $job_status_id, $job_duration, $job_data);

                  $response_values = array(
                        'success' => 1,
                        'suc_data' => "The trancoded job was " . $process_job->state
                  );
            } catch ( Services_Zencoder_Exception $e ) {
                  $response_values = array(
                        'error' => 1,
                        'err_data' => 'Failed to calling transcode jobs.'
                  );
            }

            return Response::json($response_values);
      }

      public function removeVideo() {
            $media_id = Request::segment(3);

            MediaLibrary::removeVideo($media_id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeMediaActive() {
            $media_id = Request::segment(3);

            MediaLibrary::modifyVideoStatus($media_id, 1);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeMediaInactive() {
            $media_id = Request::segment(3);

            MediaLibrary::modifyVideoStatus($media_id, 2);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function mediaByCollection() {
            $collection_id = Request::segment(3);
            $collection_status = Request::segment(4);

            $results = MediaLibrary::getMediaByCollection($collection_id);

            $data = array(
                  'medias' => $results,
                  'collection_id' => $collection_id,
                  'collection_status' => $collection_status
            );

            return View::make('viewmediacollection')->with($data);
      }

      public function showMedia() {
            $media_id = Request::segment(3);
            $result = MediaLibrary::getVideoById($media_id);

            $data = array(
                  'media_id' => $result->media_id,
                  'name' => $result->name,
                  'meta_tag' => $result->meta_tag,
                  'meta_description' => $result->meta_description,
                  'media_image' => $result->media_image,
                  'media_name' => $result->media_name,
                  'descriptions' => $result->descriptions,
                  'frame_width' => $result->frame_width,
                  'frame_height' => $result->frame_height,
                  'file_size' => $result->file_size,
                  'content' => $result->content,
                  'genre' => $result->genre,
                  'rating' => $result->rating,
                  'rights' => $result->rights,
                  'release_date' => $result->release_date,
                  'show_description' => $result->show_description,
                  'closed_captions' => $result->closed_captions,
                  'generic_tags' => $result->generic_tags,
                  'similar_content' => $result->similar_content,
            );

            return View::make('showvideo')->with($data);
      }

      public function addMediaToCollection() {
            $collection_id = Request::segment(3);
            $media_id = Request::segment(4);

            MediaLibrary::addMediaToCollection($media_id, $collection_id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function removeMediaFromCollection() {
            $id = Request::segment(3);


            MediaLibrary::removeMediaFromCollection($id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

}
