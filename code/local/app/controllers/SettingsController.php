<?php

class SettingsController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      public function radius() {

            if ( Input::get('edit-radius-settings') ) {
                  $postData = Input::all();

                  $messages = array(
                        'radius-server-ip.required' => 'Radius Server IP field must not be empty',
                        'radius-server-host.required' => 'Radius Server Host field must not be empty',
                        'radius-shared-secret' => 'Radius Shared Secret field must not be empty'
                  );

                  $rules = array(
                        'radius-server-ip' => 'required',
                        'radius-server-host' => 'required',
                        'radius-shared-secret' => 'required'
                  );

                  $validator = Validator::make($postData, $rules, $messages);

                  if ( $validator->fails() ) {  // send back to the page with the input data and errors
                        return Redirect::to('settings/radius/')->withInput()->withErrors($validator);
                  }
                  else {
                        Settings::updateSettings($postData);
                        Session::flash('flash_message', 'Radius settings updated.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('settings/radius/');
                  }
            }

            $data = array(
                  'radiusServerIP' => Setting::get('radius-server-ip'),
                  'radiusServerHost' => Setting::get('radius-server-host'),
                  'radiusSharedSecret' => Setting::get('radius-shared-secret'),
                  'cm_mode' => Setting::get('cm_mode'),
            );
            return View::make('manage.radius')->with($data);
      }

      public function refresh_epg() {

            $data = array(
                  'cm_mode' => Setting::get('cm_mode'),
            );

            if ( Input::get('action') == 'doUpdate' ) {
                  exec('php /var/www/html/streamvision/code/local/epgdata/script.php');
                  $data['updated'] = 1;
            }

            return View::make('manage.refresh_epg')->with($data);
      }

      public function cms() {
            $disabled = false;
            if ( Input::get('customer-management-settings') ) {
                  $postData = Input::all();

                  if ( Setting::get('cm_mode') == null ) {
                        $messages = array(
                              'cm_mode.required' => 'Select atleast one Custom Managemnt System',
                        );

                        $rules = array(
                              'cm_mode' => 'required',
                        );
                  }
                  else {
                        $messages = $rules = [];
                        $postData["cm_mode"] = Setting::get('cm_mode');
                  }

                  $validator = Validator::make($postData, $rules, $messages);

                  if ( $validator->fails() ) {  // send back to the page with the input data and errors
                        return Redirect::to('settings/cms/')->withInput()->withErrors($validator);
                  }
                  else {
                        Settings::updateSettings($postData);
                        Session::flash('flash_message', 'Customer Management System settings updated.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('settings/cms/');
                  }
            }

            $options = ['class' => 'form-control'];
            if ( Setting::get('cm_mode') != null ) {
                  $options["disabled"] = "disabled";
            }

            $data = array(
                  'cm_mode' => Setting::get('cm_mode'),
                  'disabled' => $disabled,
                  'customer_custom_field_1_label' => Setting::get('customer-custom-field-1-label'),
                  'customer_custom_field_2_label' => Setting::get('customer-custom-field-2-label'),
            );

            $data["options"] = $options;

            return View::make('manage.cms')->with($data);
      }

      public function authentication() {
            if ( Input::get('api-key-settings') ) {
                  $postData = Input::all();

                  if ( Setting::get('api-key') == null ) {
                        $messages = array(
                              'api-key.required' => 'Please enter API key',
                              'serviceProviderID.required' => 'Please enter Service Provider ID',
                        );

                        $rules = array(
                              'api-key' => 'required',
                              'serviceProviderID' => 'required',
                        );
                  }
                  else {
                        $messages = $rules = [];
                        $postData["api-key"] = Setting::get('api_key');
                        $postData["service-provider-id"] = Setting::get('service-provider-id');
                  }

                  $validator = Validator::make($postData, $rules, $messages);

                  if ( $validator->fails() ) {  // send back to the page with the input data and errors
                        return Redirect::to('settings/authentication/')->withInput()->withErrors($validator);
                  }
                  else {
                        $url = Config::get('streamvision.service_provider_url') . 'verify/' . $postData['serviceProviderID'] . '?key=' . $postData['api-key'];
                        $response = BaseModel::getCurl($url);
                        $response = json_decode($response);

                        if ( $response->success == "true" ) {
                              Setting::set('api_key', $postData['api-key']);
                              Setting::set('service-provider-id', $postData['serviceProviderID']);
                              Session::flash('flash_message', 'The user details has been modified successfully.');
                              Session::flash('flash_type', 'alert-success');
                        }
                        else {
                              Session::flash('flash_message', 'API Key or Service Provider ID is invalid.');
                              Session::flash('flash_type', 'alert-danger');
                        }

                        return Redirect::to('settings/authentication/');
                  }
            }

            $data = array(
                  'cm_mode' => Setting::get('cm_mode'),
                  'api_key' => Setting::get('api_key'),
                  'serviceProviderID' => Setting::get('service-provider-id'),
            );
            return View::make('manage.authentication')->with($data);
      }

}
