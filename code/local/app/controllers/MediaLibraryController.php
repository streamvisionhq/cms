<?php

use Xabbuh\PandaClient\Api;

class MediaLibraryController extends BaseController {

      private $uid = 0;
      private $panda_accesskey;
      private $panda_secretkey;
      private $panda_apihost;
      private $panda_cloudid;
      private $res_error = array();
      private $res_success = array();
      public $op;

      public function __construct() {
            $this->uid = Auth::id();
            $this->panda_accesskey = 'aa4ab4b59f22c584b646';
            $this->panda_secretkey = '1439b809d987a01c68d1';
            $this->panda_apihost = 'api.pandastream.com';
            $this->panda_cloudid = '3af170341f6afa62e819859f8e047d3c';
      }

      public function showMediaLibrary() {
            $collections = MediaLibrary::allCollections();
            $result = MediaLibrary::getTopCollection();

            if ( is_null($result) ) {
                  $data = array(
                        'collections' => $collections,
                        'collect_id' => 0,
                        'collect_status' => 1,
                  );
            }
            else {
                  $data = array(
                        'collections' => $collections,
                        'collect_id' => $result->collection_id,
                        'collect_status' => $result->status,
                  );
            }

            return View::make('medialibrary')->with($data);
      }

      public function getSearchVideo() {
            $term = (isset($_GET['term'])) ? $_GET['term'] : '';
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

            $results = MediaLibrary::searchVideo($term);

            $data = array(
                  'lvideos' => $results,
                  'term' => $term,
                  'page' => $page
            );

            return View::make('listsearchvideo')->with($data);
      }

      public function addCollection() {
            return View::make('addcollection');
      }

      public function doAddCollection() {
            $response_values = array();
            $postData = Input::all();

            $messages = array(// setting up custom error messages for the field validation
                  'collection_name.required' => 'Please enter Collection Name'
            );

            $rules = array(
                  'collection_name' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  $verrors = array();

                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }

                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $result = MediaLibrary::chkCollectionExists($postData, 0);

                  if ( is_null($result) ) {
                        MediaLibrary::addCollection($postData);

                        $response_values = array(
                              'success' => 1
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['collection_name'] = 'Collection Name already exists';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function modifyCollection() {
            $collection_id = Request::segment(3);
            $result = MediaLibrary::getCollectionById($collection_id);

            $data = array(
                  'collection_id' => $result->collection_id,
                  'collection_name' => $result->collection_name,
            );

            return View::make('editcollection')->with($data);
      }

      public function doModifyCollection() {
            $response_values = array();
            $postData = Input::all();

            $messages = array(// setting up custom error messages for the field validation
                  'collection_name.required' => 'Please enter Collection Name'
            );

            $rules = array(
                  'collection_name' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  $verrors = array();

                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }

                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $result = MediaLibrary::chkCollectionExists($postData, $postData['collection_id']);

                  if ( is_null($result) ) {
                        MediaLibrary::modifyCollection($postData);

                        $response_values = array(
                              'success' => 1
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['collection_name'] = 'Collection Name already exists';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      public function removeCollection() {
            $collection_id = Request::segment(3);

            MediaLibrary::removeCollection($collection_id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function sortCollection() {
            $results = MediaLibrary::getAllCollections();
            $data = array(
                  'collections' => $results
            );

            return View::make('sortcollections')->with($data);
      }

      public function saveCollectionorder() {
            $postData = Input::all();

            MediaLibrary::modifyCollectionOrder($postData['collection']);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeCollectionActive() {
            $collection_id = Request::segment(3);

            MediaLibrary::modifyCollectionStatus($collection_id, 1);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeCollectionInactive() {
            $collection_id = Request::segment(3);

            MediaLibrary::modifyCollectionStatus($collection_id, 2);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function getVideo() {
            $results = MediaLibrary::allVideos();
            $page = (isset($_GET['page'])) ? $_GET['page'] : 1;

            $data = array(
                  'videos' => $results,
                  'curPage' => $page
            );

            return View::make('video')->with($data);
      }

      public function addVideo() {
            return View::make('addvideo');
      }

      public function doAddVideo() {
            ini_set('max_execution_time', 0);

            $postData = Input::all();
            $input = array('media_name' => Input::file('media_name'));
            $verrors = array();
            $error = false;

            $messages = array(// setting up custom error messages for the field validation
                  'name.required' => 'Please enter Media Name',
                  'meta_tag.required' => 'Please enter Meta Tag',
                  'meta_description.required' => 'Please enter Meta Description',
                  'media_name.required' => 'Please upload Media File.',
            );

            $rules = array(
                  'name' => 'required',
                  'meta_tag' => 'required',
                  'meta_description' => 'required',
                  'media_name' => 'required | max:2147483648',
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }
                  $error = true;
            }
            else if ( Input::file('media_name')->getClientOriginalName() != '' && Input::file('media_name')->getMimeType() != 'video/mp4' ) {
                  $verrors['media_name'] = "Invalid file type, video must be of type mp4.";
                  $error = true;
            }

            if ( $error == true ) {
                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  $tstamp = time();

                  //get upload media file details
                  $orgFilename = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('media_name')->getClientOriginalName());
                  $fileExtension = Input::file('media_name')->getClientOriginalExtension();
                  $fileType = Input::file('media_name')->getClientMimeType();
                  $fileSize = Input::file('media_name')->getClientSize();

                  //rename file
                  $fileWithOutExt = basename($orgFilename, "." . $fileExtension);
                  $fileName = $fileWithOutExt . "_" . $tstamp . "." . $fileExtension;

                  //upload file to the server
                  $destPath = 'assets/uploads';
                  Input::file('media_name')->move($destPath, $fileName);

                  //converted file details for transcoding
                  $convThumbname = $fileWithOutExt . "_" . $tstamp;
                  $convFile = $fileWithOutExt . "_" . $tstamp . ".mp4";
                  $convThumb = $fileWithOutExt . "_" . $tstamp . "_1.jpg";

                  $filepath = URL::to('/') . "/assets/uploads/" . $fileName;

                  //create jobs
                  $job_id = $this->createTranscodeJob($filepath, $convThumbname);

                  // Store Job/Output IDs to update their status when notified or to check their progress.

                  MediaLibrary::addVideo($postData, $job_id, $convFile, $convThumb);

                  $response_values = array(
                        'success' => 1
                  );
            }

            return Response::json($response_values);
      }

      public function editVideo() {
            $media_id = Request::segment(3);
            $page = $_GET['cur_page'];
            $poster1_img = 'no_image.jpg';
            $poster2_img = 'no_image.jpg';
            $poster3_img = 'no_image.jpg';

            $result = MediaLibrary::getVideoById($media_id);
            $results = MediaLibrary::getPostersById($media_id);

            if ( sizeof($results) > 0 ) {
                  foreach ( $results as $res ) {
                        if ( $res->poster_type == '4X3' ) {
                              $poster1_img = $res->image_name;
                        }
                        if ( $res->poster_type == 'portrait' ) {
                              $poster2_img = $res->image_name;
                        }
                        if ( $res->poster_type == 'square' ) {
                              $poster3_img = $res->image_name;
                        }
                  }
            }

            $data = array(
                  'media_id' => $result->media_id,
                  'name' => $result->name,
                  'meta_tag' => $result->meta_tag,
                  'meta_description' => $result->meta_description,
                  'media_image' => $result->media_image,
                  'poster_id' => $result->poster_id,
                  'job_id' => $result->job_id,
                  'job_duration' => $result->job_duration,
                  'descriptions' => $result->descriptions,
                  'frame_width' => $result->frame_width,
                  'frame_height' => $result->frame_height,
                  'file_size' => $result->file_size,
                  'content' => $result->content,
                  'genre' => $result->genre,
                  'rating' => $result->rating,
                  'release_date' => $result->release_date,
                  'show_description' => $result->show_description,
                  'closed_captions' => $result->closed_captions,
                  'generic_tags' => $result->generic_tags,
                  'rights' => $result->rights,
                  'similar_content' => $result->similar_content,
                  'poster1_img' => $poster1_img,
                  'poster2_img' => $poster2_img,
                  'poster3_img' => $poster3_img,
                  'curPage' => $page
            );

            return View::make('editvideo')->with($data);
      }

      public function doEditVideo() {
            ini_set('max_execution_time', 0);

            $postData = Input::all();
            $input = array('media_name' => Input::file('media_name'));
            $job_id = $postData['job_id'];
            $poster_id = $postData['poster_id'];
            $poster1_img = $postData['poster1_img'];
            $poster2_img = $postData['poster2_img'];
            $poster3_img = $postData['poster3_img'];
            $convFile = '';
            $convThumb = '';
            $poster_image = array();
            $poster_type = array();
            $poster_path = $this->getPath();
            $verrors = array();
            $error = false;

            $messages = array(// setting up custom error messages for the field validation
                  'name.required' => 'Please enter Media Name',
                  'meta_tag.required' => 'Please enter Meta Tag',
                  'meta_description.required' => 'Please enter Meta Description'
            );

            $rules = array(
                  'name' => 'required',
                  'meta_tag' => 'required',
                  'meta_description' => 'required',
                  'media_name' => 'max:2147483648',
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {
                  foreach ( $validator->messages()->getMessages() as $field_name => $messages ) {
                        $verrors[$field_name] = $messages[0];
                  }
                  $error = true;
            }
            else if ( Input::file('media_name') ) {
                  if ( Input::file('media_name')->getClientOriginalName() != '' && Input::file('media_name')->getMimeType() != 'video/mp4' ) {
                        $verrors['media_name'] = "Invalid file type, video must be of type mp4.";
                        $error = true;
                  }
            }
            else {
                  //validate posters if any
                  if ( Input::file('add_poster1') ) {
                        if ( Input::file('add_poster1')->getClientOriginalName() != '' && Input::file('add_poster1')->getMimeType() != 'image/jpeg' ) {
                              $verrors['poster_1'] = "Invalid file type, video must be of type jpg.";
                              $error = true;
                        }
                        else {
                              list($width, $height) = getimagesize(Input::file('add_poster1'));

                              if ( $width <= $height ) {
                                    $verrors['poster_1'] = "Poster width should be greater than height of upload image.";
                                    $error = true;
                              }
                        }
                  }

                  if ( Input::file('add_poster2') ) {
                        if ( Input::file('add_poster2')->getClientOriginalName() != '' && Input::file('add_poster2')->getMimeType() != 'image/jpeg' ) {
                              $verrors['poster_2'] = "Invalid file type, video must be of type jpg.";
                              $error = true;
                        }
                        else {
                              list($width, $height) = getimagesize(Input::file('add_poster2'));

                              if ( $width >= $height ) {
                                    $verrors['poster_2'] = "Poster should be valid portrait image.";
                                    $error = true;
                              }
                        }
                  }

                  if ( Input::file('add_poster3') ) {
                        if ( Input::file('add_poster3')->getClientOriginalName() != '' && Input::file('add_poster3')->getMimeType() != 'image/jpeg' ) {
                              $verrors['poster_3'] = "Invalid file type, video must be of type jpg.";
                              $error = true;
                        }
                        else {
                              list($width, $height) = getimagesize(Input::file('add_poster3'));

                              if ( $width != $height ) {
                                    $verrors['poster_3'] = "Poster should be valid square image.";
                                    $error = true;
                              }
                        }
                  }
            }

            if ( $error == true ) {
                  $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $verrors
                  );
            }
            else {
                  //Add Posters
                  if ( Input::file('add_poster1') ) {
                        $poster1 = Input::file('add_poster1');

                        $poster_image[0] = $this->createPoster($poster1, 269, 152);
                        $poster_type[0] = '4X3';
                  }
                  else {
                        $poster_image[0] = $poster1_img;
                        $poster_type[0] = '4X3';
                  }

                  if ( Input::file('add_poster2') ) {
                        $poster2 = Input::file('add_poster2');

                        $poster_image[1] = $this->createPoster($poster2, 142, 202);
                        $poster_type[1] = 'portrait';
                  }
                  else {
                        $poster_image[1] = $poster2_img;
                        $poster_type[1] = 'portrait';
                  }

                  if ( Input::file('add_poster3') ) {
                        $poster3 = Input::file('add_poster3');

                        $poster_image[2] = $this->createPoster($poster3, 209, 209);
                        $poster_type[2] = 'square';
                  }
                  else {
                        $poster_image[2] = $poster3_img;
                        $poster_type[2] = 'square';
                  }


                  if ( Input::file('media_name') ) {
                        $tstamp = time();

                        //get upload media file details
                        $orgFilename = preg_replace('/[^a-zA-Z0-9-_\.]/', '', Input::file('media_name')->getClientOriginalName());
                        $fileExtension = Input::file('media_name')->getClientOriginalExtension();
                        $fileType = Input::file('media_name')->getClientMimeType();
                        $fileSize = Input::file('media_name')->getClientSize();

                        //rename file
                        $fileWithOutExt = basename($orgFilename, "." . $fileExtension);
                        $fileName = $fileWithOutExt . "_" . $tstamp . "." . $fileExtension;

                        //upload file to the server
                        $destPath = 'assets/uploads';
                        Input::file('media_name')->move($destPath, $fileName);

                        //converted file details for transcoding
                        $convThumbname = $fileWithOutExt . "_" . $tstamp;
                        $convFile = $fileWithOutExt . "_" . $tstamp . ".mp4";
                        $convThumb = $fileWithOutExt . "_" . $tstamp . "_1.jpg";

                        $filepath = URL::to('/') . "/assets/uploads/" . $fileName;

                        //create jobs
                        $job_id = $this->createTranscodeJob($filepath, $convThumbname);
                  }

                  if ( $job_id != '' ) {
                        MediaLibrary::modifyVideo($postData, $job_id, $poster_id, $convFile, $convThumb, $poster_image, $poster_type);

                        if ( $convFile == '' ) {
                              $suc_msg = 'Media details modified successfully.';
                        }
                        else {
                              $suc_msg = 'Transcoded Job created successfully for media file.';
                        }
                        $response_values = array(
                              'success' => 1,
                              'suc_msg' => $suc_msg
                        );
                  }
                  else {
                        $verrors = array();
                        $verrors['media_name'] = 'Failed to transcode media file.';

                        $response_values = array(
                              'validation_failed' => 1,
                              'errors' => $verrors
                        );
                  }
            }

            return Response::json($response_values);
      }

      private function createPoster($poster, $width, $height) {
            $tstamp = time();

            //get upload media file details
            $orgFilename = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $poster->getClientOriginalName());
            $fileExtension = $poster->getClientOriginalExtension();
            $fileType = $poster->getClientMimeType();
            $fileSize = $poster->getClientSize();

            //rename file
            $fileWithOutExt = basename($orgFilename, "." . $fileExtension);
            $fileName = $fileWithOutExt . "_" . $tstamp . "." . $fileExtension;

            //upload file to the server
            $destPath = 'assets/data/image';
            $poster->move($destPath, $fileName);

            //create poster thumnail
            Image::make(sprintf('assets/data/image/%s', $fileName))->resize($width, $height)->save();

            //move poster to s3 account
            $filepath = URL::to('/') . "/assets/data/image/" . $fileName;
            $this->moveAws($filepath, $fileName);

            return $fileName;
      }

      public function addVideos() {
            return View::make('addvideos');
      }

      public function doAddVideos() {
            ini_set('max_execution_time', 0);

            $videoData = array();
            $postData = Input::all();
            $title_arr = Input::get('name');
            $files = Input::file('media_name');
            $videoData['created_by'] = $postData['created_by'];

            $err = 0;
            $msg = '';
            $valid = true;
            foreach ( $title_arr as $key => $val ) {
                  if ( $val == '' ) {
                        $err = 1;
                        $msg = "Please enter title";
                  }

                  if ( method_exists($postData['media_name'][$key], 'getClientOriginalName') != 1 ) {
                        $msg .= ($err > 0) ? " and upload media file " : " Please upload media file ";
                        $err = 1;
                  }
                  else if ( $postData['media_name'][$key]->getClientMimeType() != 'video/mp4' ) {
                        $msg .= ($err > 0) ? " and upload valid file type for media file " : " Please upload valid file type for media file ";
                        $err = 1;
                  }

                  if ( $err == 1 ) {
                        $this->res_error['code'][$key] = 'failed';
                        $this->res_error['status'][$key] = $msg . '.';
                        $valid = false;
                  }

                  $err = 0;
                  $msg = '';
            }

            if ( $valid == true ) {
                  foreach ( $files as $kfile => $vfile ) {
                        $tstamp = time();
                        $videoData['name'] = $postData['name'][$kfile];

                        //get upload media file details
                        $orgFilename = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $vfile->getClientOriginalName());
                        $fileExtension = $vfile->getClientOriginalExtension();
                        $fileType = $vfile->getClientMimeType();
                        $fileSize = $vfile->getClientSize();

                        if ( $fileType != 'video/mp4' ) {
                              $this->res_error['code'][$kfile] = 'failed';
                              $this->res_error['status'][$kfile] = 'Invalid file type for media.';
                        }
                        else {
                              //rename file
                              $fileWithOutExt = basename($orgFilename, "." . $fileExtension);
                              $fileName = $fileWithOutExt . "_" . $tstamp . "." . $fileExtension;

                              //upload file to the server
                              $destPath = 'assets/uploads';
                              $vfile->move($destPath, $fileName);

                              //converted file details for transcoding
                              $convThumbname = $fileWithOutExt . "_" . $tstamp;
                              $convFile = $fileWithOutExt . "_" . $tstamp . ".mp4";
                              $convThumb = $fileWithOutExt . "_" . $tstamp . "_1.jpg";

                              $filepath = URL::to('/') . "/assets/uploads/" . $fileName;

                              //create jobs
                              $job_id = $this->createTranscodeJob($filepath, $convThumbname);

                              if ( $job_id != '' ) {
                                    MediaLibrary::addVideo($videoData, $job_id, $convFile, $convThumb);
                                    $this->res_error['code'][$kfile] = 'success';
                                    $this->res_error['status'][$kfile] = 'Transcode job for media created successfully.';
                              }
                              else {
                                    $this->res_error['code'][$kfile] = 'failed';
                                    $this->res_error['status'][$kfile] = 'Failed to transcode media file.';
                              }
                        }
                  }
            }

            return Response::json($this->res_error);
      }

      private function createTranscodeJob($file, $thumb) {
            $cloud = Api::getCloudInstance(
                            $this->panda_accesskey, $this->panda_secretkey, $this->panda_apihost, $this->panda_cloudid
            );
            $video = $cloud->encodeVideoByUrl($file, array(), $thumb, '');
            return $video->getId();
      }

      public function processTranscodeJob() {
            $media_id = Request::segment(3);
            $job_id = Request::segment(4);
            $job_duration = 0;
            $job_data = array();

            $cloud = Api::getCloudInstance(
                            $this->panda_accesskey, $this->panda_secretkey, $this->panda_apihost, $this->panda_cloudid
            );

            $process_job = $cloud->getVideo($job_id);

            $result = MediaLibrary::getJobStatusByDesc($process_job->getStatus());
            $job_status_id = $result->status_id;
            $job_duration = ($result->status_id == 4) ? $process_job->getDuration() : 0;
            $job_data['frame_width'] = ($result->status_id == 4) ? $process_job->getWidth() : 0;
            $job_data['frame_height'] = ($result->status_id == 4) ? $process_job->getHeight() : 0;
            $job_data['file_size'] = ($result->status_id == 4) ? $process_job->getFilesize() : 0;

            MediaLibrary::modifyVideoJobStatus($media_id, $job_status_id, $job_duration, $job_data);

            $response_values = array(
                  'success' => 1,
                  'suc_data' => "The trancoded job was " . $process_job->getStatus()
            );

            return Response::json($response_values);
      }

      public function removeVideo() {
            $media_id = Request::segment(3);
            $job_id = Request::segment(4);

            MediaLibrary::removeVideo($media_id);

            $cloud = Api::getCloudInstance(
                            $this->panda_accesskey, $this->panda_secretkey, $this->panda_apihost, $this->panda_cloudid
            );

            $videoObj = $cloud->getVideo($job_id);
            $result = $cloud->deleteVideo($videoObj);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeMediaActive() {
            $media_id = Request::segment(3);

            MediaLibrary::modifyVideoStatus($media_id, 1);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function makeMediaInactive() {
            $media_id = Request::segment(3);

            MediaLibrary::modifyVideoStatus($media_id, 2);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function mediaByCollection() {
            $collection_id = Request::segment(3);
            $collection_status = Request::segment(4);

            $results = MediaLibrary::getMediaByCollection($collection_id);

            $data = array(
                  'medias' => $results,
                  'collection_id' => $collection_id,
                  'collection_status' => $collection_status
            );

            return View::make('viewmediacollection')->with($data);
      }

      public function showMedia() {
            $media_id = Request::segment(3);
            $result = MediaLibrary::getVideoById($media_id);

            $data = array(
                  'media_id' => $result->media_id,
                  'name' => $result->name,
                  'meta_tag' => $result->meta_tag,
                  'meta_description' => $result->meta_description,
                  'media_image' => $result->media_image,
                  'media_name' => $result->media_name,
                  'descriptions' => $result->descriptions,
                  'frame_width' => $result->frame_width,
                  'frame_height' => $result->frame_height,
                  'file_size' => $result->file_size,
                  'content' => $result->content,
                  'genre' => $result->genre,
                  'rating' => $result->rating,
                  'rights' => $result->rights,
                  'release_date' => $result->release_date,
                  'show_description' => $result->show_description,
                  'closed_captions' => $result->closed_captions,
                  'generic_tags' => $result->generic_tags,
                  'similar_content' => $result->similar_content,
            );

            return View::make('showvideo')->with($data);
      }

      public function addMediaToCollection() {
            $collection_id = Request::segment(3);
            $media_id = Request::segment(4);

            MediaLibrary::addMediaToCollection($media_id, $collection_id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      public function removeMediaFromCollection() {
            $id = Request::segment(3);


            MediaLibrary::removeMediaFromCollection($id);

            $response_values = array(
                  'success' => 1
            );

            return Response::json($response_values);
      }

      private function getPath() {
            $origpath = str_replace("\\", "/", base_path());
            $exppath = explode("/", $origpath);
            array_pop($exppath);
            $impath = implode("/", $exppath);
            $newpath = str_replace("/", "\\", $impath) . '/assets/data/image/';
            return $newpath;
      }

      private function moveAws($filepath, $filename) {
            $s3 = AWS::get('s3');

            $s3->putObject(array(
                  'Bucket' => getAwsBucket(),
                  'Key' => $filename,
                  'Body' => file_get_contents($filepath),
                  'ACL' => 'public-read'
            ));
      }

      public function testMedia() {
            $s3 = AWS::get('s3');

            $path = str_replace("\\", "/", base_path());
            $exp_path = explode("/", $path);
            array_pop($exp_path);

            $impath = implode("/", $exp_path);

            echo $filepath = str_replace("/", "\\", $impath) . '/assets/img/progressbar.gif';
            //exit;
            //$filepath = app_path().'/loading.gif'; 
            //$filepath = app_path().'/sample4.jpg'; 
            //$filepath = 'http://localhost/prolivenew/assets/img/sample4.jpg';

            $s3->putObject(array(
                  'Bucket' => 'provideodev',
                  'Key' => 'progressbar.gif',
                  'SourceFile' => $filepath,
                  'ACL' => 'public-read'
            ));
            $res = $s3->waitUntilObjectExists(array(
                  'Bucket' => 'provideodev',
                  'Key' => 'progressbar.gif'
            ));
            echo '<pre>';
            print_r($res);
      }

}
