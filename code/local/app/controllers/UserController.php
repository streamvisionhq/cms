<?php

class UserController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      public function showProfile() {
            $data = array(
                  'firstname' => Auth::user()->firstname,
                  'lastname' => Auth::user()->lastname,
                  'email' => Auth::user()->email,
                  'username' => Auth::user()->username,
                  'serviceProviderID' => Setting::get('service-provider-id'),
                  'cm_mode' => Setting::get('cm_mode'),
                  'rootPath' => Setting::get('root-path'),
                  'rootURL' => Setting::get('root-url'),
                  'cmsName' => Setting::get('cms-name'),
            );

            return View::make('profile')->with($data);
      }

      public function saveProfile() {
            $postData = Input::all(); // getting all of the post data

            $messages = array(// setting up custom error messages for the field validation
                  'firstname.required' => 'Please enter First Name',
                  'lastname.required' => 'Please enter Last Name',
                  'email.required' => 'Please enter Email',
                  'username.required' => 'Please enter Username',
                  'rootPath' => 'Please enter Root Path',
                  'rootURL' => 'Please enter Root URL',
                  'cmsName' => 'Please enter CMS Name',
            );

            $rules = array(
                  'firstname' => 'required',
                  'lastname' => 'required',
                  'email' => 'required|email',
                  'username' => 'required',
                  'rootPath' => 'required',
                  'rootURL' => 'required',
                  'cmsName' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) {  // send back to the page with the input data and errors
                  return Redirect::to('settings')->withInput()->withErrors($validator);
            }
            else {
                  User::updateUserData($postData, $this->uid);

                  Session::flash('flash_message', 'The user details has been modified successfully.');
                  Session::flash('flash_type', 'alert-success');

                  return Redirect::to('settings');
            }
      }

      public function showChangePassword() {
            $data = array(
                  'cm_mode' => Setting::get('cm_mode'),
            );
            return View::make('changepassword')->with($data);
      }

      public function savePassword() {
            $postData = Input::all();  // getting all of the post data

            $messages = array(// setting up custom error messages for the field validation
                  'oldpassword.required' => 'Please enter Old Password.',
                  'password.required' => 'Please enter Password.',
                  'password_confirmation.required' => 'Please enter Password Confirmation.',
            );

            $rules = array(
                  'oldpassword' => 'required',
                  'password' => 'required|confirmed',
                  'password_confirmation' => 'required',
            );

            $validator = Validator::make($postData, $rules, $messages);  // doing the validation, passing post data, rules and the messages

            if ( $validator->fails() ) { // send back to the page with the input data and errors
                  return Redirect::to('changepassword')->withInput()->withErrors($validator);
            }
            else {

                  if ( Hash::check($postData['oldpassword'], Auth::user()->password) ) {
                        User::updateUserPassword(Hash::make($postData['password']), $this->uid);
                        Session::flash('flash_message', 'The password has been modified successfully.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('changepassword');
                  }
                  else {
                        Session::flash('flash_message', 'Invalid old password.');
                        Session::flash('flash_type', 'alert-danger');

                        return View::make('changepassword');
                  }
            }
      }

}
