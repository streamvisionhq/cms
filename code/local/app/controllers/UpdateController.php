<?php

class UpdateController extends BaseController {

      public function checkUpdate() {

            if ( Input::get('action') == 'doUpdate' ) {
                  $data['updated'] = 0;
                  if ( Update::doUpdate() ) {
                        $data['updated'] = 1;
                  }
            }
            else {
                  $data['update'] = 0;
                  if ( $updateResponse = Update::checkUpdate() ) {
                        $data['update'] = 1;
                        $data['updateVersion'] = $updateResponse->update_version;
                  }

                  $data['currentVersion'] = Setting::get('version');
            }
            return View::make('update')->with($data);
      }

}
