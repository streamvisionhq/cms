<?php

class BaseController extends Controller {

    public $perPage = 25;

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout() {
        if ( !is_null($this->layout) ) {
            $this->layout = View::make($this->layout);
        }
    }

    protected function handleError($message, $errorCode = null) {
        return self::getErrorReturnValue($message, $errorCode);
    }

    protected function handleSuccess($response, $message = null, $last_modified = false) {

        $data = array("success" => true, "data" => $response, "message" => $message);

        if ( $last_modified ) {
            $data["last_modified"] = $last_modified;
        }

        return Response::json($data, 200, array('Content-Type' => 'application/json;charset=utf8'), JSON_UNESCAPED_UNICODE);
    }

    /**
     * Processes a message and error code values and returns correct array for API
     * responses. Seperated into a static function so it can be used in a dvr_customer_auth filter.
     * @param string $message the error message to display
     * @param string $errorCode (optional) error code for the error
     * @return array standard API failure response
     */
    public static function getErrorReturnValue($message, $errorCode = null) {
        return array("success" => false, "data" => null, "error_code" => "$errorCode", "message" => $message);
    }

    public static function getSettingsFieldsLabels() {
        $custom1 = DEFAULT_CUSTOM_FIELD_LABEL1;
        $custom2 = DEFAULT_CUSTOM_FIELD_LABEL2;
        if ( Setting::get('customer-custom-field-1-label') != null ) {
            $custom1 = Setting::get('customer-custom-field-1-label');
        }
        if ( Setting::get('customer-custom-field-2-label') != null ) {
            $custom2 = Setting::get('customer-custom-field-2-label');
        }
        return ['custom1' => $custom1, 'custom2' => $custom2];
    }

    public function getDatePage($page) {
        if ( $page == "1" || $page == "2" ) {
            $date = date("Y-m-d");
        }
        elseif ( $page == "3" || $page == "4" ) {
            switch ( $page ) {
                case "3": $page = "1";
                    break;
                default: $page = "2";
                    break;
            }
            $date = date('Y-m-d', strtotime("+1 day"));
        }
        elseif ( $page == "5" || $page == "6" ) {
            switch ( $page ) {
                case "5": $page = "1";
                    break;
                default: $page = "2";
                    break;
            }
            $date = date('Y-m-d', strtotime("+2 day"));
        }
        else {
            return false;
        }
        return [
              "date" => $date,
              "page" => $page
        ];
    }

    public function getLastModifiedDate($page) {
        $filePath = realpath(base_path() . "/epgdata/");
        $file = $filePath . "/epg_pages_last_modified.txt";
        
        if ( file_exists($file) ) {
            $dates = file_get_contents($file);
            $dates = json_decode($dates, true);
            if ( !empty($dates) ) {

                $date_page = $this->getDatePage($page);
                if ( $date_page ) {
                    foreach ( $dates as $key => $value ) {
                        if ( strpos($key, $date_page["date"]) !== false ) {
                            return $value;
                        }
                    }
                }
            }
        }
        return false;
    }

}
