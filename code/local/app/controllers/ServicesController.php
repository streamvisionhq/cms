<?php

class ServicesController extends BaseController {

    private $uid = 0;

    public function __construct() {
        $this->uid = Auth::id();
    }

    /**
     * Manage Services
     * @return HTML
     */
    public function manageServices() {
        $data['services'] = Services::getServices();
        return View::make('manage/services/services-list')->with($data);
    }

    /**
     * Edit Screen
     * @param type $errorCode
     * @return type
     */
    public function editService($ID) {

        $data['service'] = Services::getService($ID);

        if (Input::get('edit-service')) {
            $postData = Input::all(); // getting all of the post data

            $messages = array(// setting up custom error messages for the field validation			
                'title.required' => 'Service title field must not be empty',
                'externalName.required' => 'External Name field must not be empty',
                'externalRef.required' => 'External Reference field must not be empty',
            );

            $rules = array(
                'title' => 'required',
                'externalName' => 'required',
                'externalRef' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);

            if ($validator->fails()) {  // send back to the page with the input data and errors
                return Redirect::to('service/edit/' . $ID)->withInput()->withErrors($validator);
            } else {
                Services::updateService($ID, $postData);

                Session::flash('flash_message', 'Service "' . $postData['title'] . '" updated.');
                Session::flash('flash_type', 'alert-success');

                return Redirect::to('service/edit/' . $ID);
            }
        }

        return View::make('manage/services/edit-service')->with($data);
    }

    public function addService() {

        if (Input::get('add-service')) {
            $postData = Input::all(); // getting all of the post data

            $messages = array(// setting up custom error messages for the field validation			
                'title.required' => 'Service title field must not be empty',
                'internalName.required' => 'Internal Name field must not be empty',
                'externalName.required' => 'External Name field must not be empty',
                'internalRef.required' => 'Internal Reference field must not be empty',
                'externalRef.required' => 'External Reference field must not be empty',
            );

            $rules = array(
                'title' => 'required',
                'internalName' => 'required',
                'externalName' => 'required',
                'internalRef' => 'required',
                'externalRef' => 'required'
            );

            $validator = Validator::make($postData, $rules, $messages);

            if ($validator->fails()) {  // send back to the page with the input data and errors
                return Redirect::to('service/add/')->withInput()->withErrors($validator);
            } else {
                Services::addService($postData);

                Session::flash('flash_message', 'Service "' . $postData['title'] . '" added.');
                Session::flash('flash_type', 'alert-success');

                return Redirect::to('service/add/');
            }
        }

        return View::make('manage/services/add-service');
    }

}
