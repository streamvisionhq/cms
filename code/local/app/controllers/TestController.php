<?php

class TestController extends BaseController {

    public function test() {
        $stream_name = 'master/KPAZ-DT-21.1';
        $stream_array = explode("-", $stream_name);
        $stream = end($stream_array);
        $mm_array = explode(".", $stream);
        $major = $mm_array[0];
        $minor = $mm_array[1];
        die();
        set_time_limit(0);
        echo "<pre>";
        //Update::databaseMirgration();

        /**
         * Get Client Current DVR Storage
         */
        //print_r(Dvrapi::getStorage(2773610));

        /**
         * Create Client Storage
         */
        //print_r(Dvrapi::DVRStorage('set', 1234567, 36000));

        /**
         * Add additional more storage (like: previos storage= 14400 sec., after add more 1000 sec total = 15400
         */
        //print_r(Dvrapi::DVRStorage('add', 2773610, 7200000));

        /**
         * Update Client Storage
         */
        //print_r(Dvrapi::DVRStorage('update', 2773610, 150));

        /**
         * Get EPG (show_id is referred as obj.eventid and epg_id is referred as obj.id)
         */
        print_r(Dvrapi::getEPG('2016-08-26', 'US/Pacific'));

        /**
         * Add recording
         */
        //print_r(Dvrapi::recordDVR('add', 436808, 10, 705467136, 2773610, 'Asia/Karachi', 'R'));

        /**
         * Update recording
         */
        //print_r(Dvrapi::recordDVR('update', 436808, 10, 705467136, 2773610, 'Asia/Karachi', 'RR'));

        /**
         * Get customer’s begun recording or finished recording data along with EPG data
         */
        //print_r(Dvrapi::getRecordShow(27521));        

        /**
         * Delete customer’s scheduled and recorded recording data
         */
        //echo Dvrapi::deleteRecordRequest(3340212, 705502816);  
        //print_r(Dvrapi::addChannel('14076', 'master/KTVK-DT-3.1', 'http://199.66.169.204:8081/hls/CBS5.1/playlist.m3u8'));
        //print_r(Dvrapi::updateChannel('14065', 'master/KPAZ-DT2-21.2', 'http://199.66.169.204:8081/hls/CHURCH21.2/playlist.m3u8'));
        //print_r(Dvrapi::deleteChannel('88888'));
        //print_r(Dvrapi::getChannels());   
        //print_r(Dvrapi::DVRStorage('add', 1, Setting::get('dvr-additional-storage')));                
    }

    public function epg_test() {
        die();
        $data = file_get_contents("GetSchedules.xml");
        $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);

        $array = array();

        $count_channel = 0;

        $pages = 2;
        $interval = ceil(24 / $pages);
        $step = 60 * 60 * $interval;

        $range = $this->hoursRange($lower = 0, $upper = 86400, $step, $format = '');

        foreach ( $xml->Channel as $data ) {
            $channel = (array) $data;

            if ( isset($channel['Show']) ) {

                foreach ( (array) $channel['Show'] as $key => $show_data ) {

                    $show = (array) $show_data;

                    $show_date = date("Y-m-d", strtotime($show['@attributes']['adjustedstartTime']));
                    $show_time = date("H:i", strtotime($show['@attributes']['adjustedstartTime']));

                    if ( strtotime($show_time) >= strtotime($range[0]) && strtotime($show_time) < strtotime($range[1]) ) {
                        $array[$show_date]['1']['Channel'][$channel['@attributes']['channelId']]["@attributes"] = $channel['@attributes'];
                        $array[$show_date]['1']['Channel'][$channel['@attributes']['channelId']]["Show"][] = $show;
                    }
                    elseif ( strtotime($show_time) > strtotime($range[1]) ) {
                        $array[$show_date]['2']['Channel'][$channel['@attributes']['channelId']]["@attributes"] = $channel['@attributes'];
                        $array[$show_date]['2']['Channel'][$channel['@attributes']['channelId']]["Show"][] = $show;
                    }
                }
            }

            $count_channel++;
        }

        foreach ( $array as $date => $channels ) {
            foreach ( $channels as $interval => $value ) {
                $filename = 'Schedule-' . $date . '-' . $interval . '.json';
                if ( !file_exists($filename) ) {
                    $fp = fopen($filename, 'w');
                    fwrite($fp, json_encode($value));
                    fclose($fp);
                    $this->update_last_modified_date($filename);
                }
            }
        }
    }

    public function update_last_modified_date($epg_file) {

        $file = 'epg_pages_last_modified.txt';
        
        if ( !file_exists($file) ) {
            $fh = fopen($file, 'w');
        }

        $dates = file_get_contents($file);
        $dates = json_decode($dates, true);

        $content = [];

        if ( !empty($dates) ) {

            $dates[$epg_file] = date("Y-m-d H:i:s");

            $fp = fopen($file, 'w');
            fwrite($fp, json_encode($dates));
            fclose($fp);

            $today = date('Y-m-d');
            $tomorrow = date('Y-m-d', strtotime("+1 day"));
            $after_tomorrow = date('Y-m-d', strtotime("+2 day"));

            foreach ( $dates as $key => $value ) {
                if ( strpos($key, $today) !== false ) {
                    $content[$key] = $value;
                }

                if ( strpos($key, $tomorrow) !== false ) {
                    $content[$key] = $value;
                }

                if ( strpos($key, $after_tomorrow) !== false ) {
                    $content[$key] = $value;
                }
            }
        }
        else {
            $content[$epg_file] = date("Y-m-d H:i:s");
        }

        $fp = fopen($file, 'w');
        fwrite($fp, json_encode($content));
        fclose($fp);
    }

    public function hoursRange($lower = 0, $upper = 86400, $step = 3600, $format = '') {
        $times = array();
        foreach ( range($lower, $upper, $step) as $increment ) {
            $increment = gmdate('H:i', $increment);
            if ( !in_array($increment, $times) ) {
                $times[] = (string) $increment;
            }
        }

        return $times;
    }

}
