<?php

class RokuDeviceController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      public function getIndex() {
            $filters = NULL;
            $data['perPage'] = $this->perPage;

            switch ( Input::get('action') ) {
                  case 'delete':
                        $deviceID = Input::get('deviceID');
                        if ( $deviceID ) {
                              if ( $res = RokuDevices::deleteDevice($deviceID) ) {
                                    return Response::json(array('success' => true, 'message' => 'Roku device (' . $deviceID . ') deleted!', 'default_device' => $res));
                              }
                              else {
                                    return Response::json(array('success' => false, 'message' => 'Error in deleting roku device'));
                              }
                        }
                        else {
                              return Response::json(array('success' => false, 'message' => 'Device ID is missing'));
                        }
                        die();
                        break;
                  case 'markDefault':
                        $deviceID = Input::get('deviceID');
                        $customerID = Input::get('customerID');
                        if ( $deviceID && $customerID ) {
                              if ( RokuDevices::markDefault($deviceID, $customerID) ) {
                                    return Response::json(array('success' => true, 'message' => 'Roku device (' . $deviceID . ') set as default!'));
                              }
                              else {
                                    return Response::json(array('success' => false, 'message' => 'Error in mark as default device'));
                              }
                        }
                        else {
                              return Response::json(array('success' => false, 'message' => 'Invalid Parameters'));
                        }
                        break;
                  case 'changeStatus':
                        $deviceID = Input::get('deviceID');
                        $status = Input::get('status');
                        if ( $deviceID ) {
                              if ( RokuDevices::changeStatus($deviceID, $status) ) {
                                    return Response::json(array('success' => true, 'message' => 'Roku device (' . $deviceID . ') status changed!'));
                              }
                              else {
                                    return Response::json(array('success' => false, 'message' => 'Error in changing status'));
                              }
                        }
                        else {
                              return Response::json(array('success' => false, 'message' => 'Invalid Parameters'));
                        }
                        break;
                  case 'filter':
                        if ( Input::get('deviceID') ) {
                              $filters['deviceID'] = Input::get('deviceID');
                        }
                        if ( Input::get('identifier') ) {
                              $filters['identifier'] = Input::get('identifier');
                        }
                        if ( Input::get('customerID') ) {
                              $data['customer'] = Customers::getCustomer(Input::get('customerID'));
                              $filters['customerID'] = Input::get('customerID');
                        }
                        break;
            }

            $data['devices'] = RokuDevices::getDevices($this->perPage, $filters);
            return View::make('roku-devices/roku-devices')->with($data);
      }

}
