<?php

use Proliveapi\Proliveapi;

class ApiCollectionController extends BaseController {

      private $allowedMethod;
      private $uriSegments;
      private $apiService = array();

      public function __construct() {
            $this->uriSegments = Request::segments();
      }

      public function processCollectionApi() {
            $method = '';
            $segment1 = $this->uriSegments[2];
            $segment2 = $this->uriSegments[3];

            $this->allowedMethod = "GET";

            switch ( $segment1 ) {
                  case 'vod':
                        if ( $segment2 == 'list' ) {
                              $method = 'listVideos';
                        }
                        if ( $segment2 == 'show' ) {
                              $method = 'showVideo';
                        }
                        break;

                  case 'collections':
                        if ( $segment2 == 'list' ) {
                              $method = 'listCollection';
                        }
                        if ( $segment2 == 'show' ) {
                              $method = 'showCollection';
                        }
                        if ( $segment2 == 'videolist' ) {
                              $method = 'listCollectionVideos';
                        }
                        break;
            }

            $this->apiService = array("service" => "collection", "method" => $method, "authenticate" => true);
            $apiInstance = new Proliveapi($this->apiService, $this->allowedMethod, $this->uriSegments);
      }

}
