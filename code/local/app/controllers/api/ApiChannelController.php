<?php

use Proliveapi\Proliveapi;

class ApiChannelController extends BaseController {

      private $allowedMethod;
      private $uriSegments;
      private $apiService = array();

      public function __construct() {
            $this->uriSegments = Request::segments();
      }

      public function processChannelApi() {
            $method = '';
            $segment1 = $this->uriSegments[2];
            $segment2 = $this->uriSegments[3];

            $this->allowedMethod = "GET";

            switch ( $segment1 ) {
                  case 'livestreams':
                        if ( $segment2 == 'list' ) {
                              $method = 'listLiveStream';
                        }
                        if ( $segment2 == 'show' ) {
                              $method = 'showLiveStream';
                        }
                        break;

                  case 'vod':
                        if ( $segment2 == 'list' ) {
                              $method = 'listVideosOnDemand';
                        }
                        if ( $segment2 == 'show' ) {
                              $method = 'showVideosOnDemand';
                        }
                        break;

                  case 'playlists':
                        if ( $segment2 == 'list' ) {
                              $method = 'playlists';
                        }
                        break;
            }

            $this->apiService = array("service" => "channel", "method" => $method, "authenticate" => true);
            $apiInstance = new Proliveapi($this->apiService, $this->allowedMethod, $this->uriSegments);
      }

}
