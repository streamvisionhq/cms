<?php
require_once(Config::get('log4php.Logger'));
Logger::configure(Config::get('log4php.config'));

class DeviceController extends \BaseController {

      /**
       * log4php Object
       * @var object
       */
      public $logs;

      /**
       * Constructor of the class DeviceController
       *
       */
      public function __construct() {
            $this->logs = Logger::getLogger('API');
      }

      /**
       * Auto discover Device ID and authentication of roku
       * @param type $customerID
       * @param type $Password
       * @param type $deviceID
       * @param type $Identifier
       * @return Object
       */
      public function deviceAuthentication($customerID, $Password, $deviceID, $Identifier) {
            $params = array('CustomerID' => $customerID, 'Password' => $Password, 'DeviceID' => $deviceID, 'Identifier' => $Identifier);
            try {
                  $this->logs->info('DeviceController deviceAuthentication Start.');
                  if ( Customers::checkCustomer($customerID) ) {
                        $channelServices = Services::getChannelServices();
                        if ( $rokuServices = CustomerServices::getServices($customerID, "Roku", 1, $channelServices, NULL, $Password) ) {
                              if ( CustomerServices::radiusAuthentication($rokuServices[0]->Login, $rokuServices[0]->Password) ) {
                                    $isRokuDeviceService = RokuDevices::isRokuDeviceService($customerID);
                                    if ( $isRokuDeviceService['status'] == true ) {
                                          if ( !RokuDevices::checkDevice($deviceID) ) {
                                                if ( $isRokuDeviceService['addRoku'] == 1 ) {
                                                      RokuDevices::addRokuDevice($customerID, $deviceID, $Identifier); //Add Roku Device for customer
                                                      $this->logs->trace(json_encode(array('CustomerID' => $customerID, 'Identifier' => $Identifier)));
                                                      $this->logs->info('DeviceController deviceAuthentication Successfully Ends for Device:' . $deviceID . ' & CustomerID:' . $customerID);
                                                      return $this->handleSuccess(array('CustomerID' => $customerID, 'Identifier' => $Identifier));
                                                }
                                                else {
                                                      $devices = RokuDevices::getDevicesByCustomerID($customerID, NULL, 1, $deviceID);
                                                      $output = array('delete' => true, 'total_services' => $isRokuDeviceService['totalServices'], 'total_devices' => $isRokuDeviceService['totalDevices'], 'devices' => $devices);
                                                      $this->logs->trace(json_encode($output));
                                                      $this->logs->info('DeviceController deviceAuthentication Successfully Ends for Device:' . $deviceID . ' & CustomerID:' . $customerID);
                                                      return $this->handleSuccess($output);
                                                }
                                          }
                                          else {
                                                if ( $device = RokuDevices::customerDevice($deviceID, $customerID) ) {
                                                      if ( $device->Status == 1 ) {
                                                            $modules = CustomerServices::getCustomerModules($customerID);
                                                            
                                                            $services = [];
                                                            
                                                            foreach ($rokuServices as $key => $value) {
                                                                $row = SubscriptionType::get_access_rights($value->ServiceName, true);
                                                                if(isset($row[0])){
                                                                    array_push($services, $row[0]);
                                                                }
                                                            }

                                                            $roku_service = $services;

                                                            $output = array('device' => $device, 'modules' => $modules, 'roku_service' => $roku_service);
                                                            $this->logs->trace(json_encode($output));
                                                            $this->logs->info('DeviceController deviceAuthentication Successfully Ends for Device:' . $deviceID . ' & CustomerID:' . $customerID);
                                                            return $this->handleSuccess($output);
                                                      }
                                                      else {
                                                            $this->logs->error(Messages::getMessage('2016') . ', Code: 2016, CustomerID:' . $customerID . ', DeviceID:' . $deviceID);
                                                            $this->logs->error('Request:' . json_encode($params));
                                                            return $this->handleError(Messages::getMessage('2016'), "2016");
                                                      }
                                                }
                                                else {
                                                      $this->logs->error(Messages::getMessage('2009') . ', Code: 2009, CustomerID:' . $customerID . ', DeviceID:' . $deviceID);
                                                      $this->logs->error('Request:' . json_encode($params));
                                                      return $this->handleError(Messages::getMessage('2009'), "2009");
                                                }
                                          }
                                    }
                                    else {
                                          $devices = RokuDevices::getDevicesByCustomerID($customerID, NULL, 1, $deviceID);
                                          $output = array('delete' => true, 'total_services' => $isRokuDeviceService['totalServices'], 'total_devices' => $isRokuDeviceService['totalDevices'], 'devices' => $devices);
                                          $this->logs->trace(json_encode($output));
                                          $this->logs->info('DeviceController deviceAuthentication Successfully Ends for Device:' . $deviceID . ' & CustomerID:' . $customerID);
                                          return $this->handleSuccess($output);
                                    }
                              }
                              else {
                                    $this->logs->error('Roku Radius authentication failed for Customer ID:' . $customerID . ', Code: 2006');
                                    $this->logs->error('Request:' . json_encode($params));
                                    return $this->handleError(Messages::getMessage('2006'), "2006");
                              }
                        }
                        else {
                              $this->logs->error('Invalid CustomerID:' . $customerID . ' or Password, Code:2003');
                              $this->logs->error('Request:' . json_encode($params));
                              return $this->handleError(Messages::getMessage('2003'), "2003");
                        }
                  }
                  else {
                        $this->logs->error('Customer ID not valid:' . $customerID . ', Code:2002');
                        $this->logs->error('Request:' . json_encode($params));
                        return $this->handleError(Messages::getMessage('2002'), "2002");
                  }
            } catch ( Exception $e ) {
                  $this->logs->error($e);
                  $this->logs->error('Request:' . json_encode($params));
                  return $this->handleError(Messages::getMessage('2008'), "2008");
            }
      }

      /**
       * Keep live
       * @param type $customerID
       * @param type $deviceID
       * @param type $channelID
       * @return Object
       */
      public function keepAlive($customerID, $deviceID, $channelID) {
            Customers::addKeepAlive($customerID, $deviceID, $channelID);
            return $this->handleSuccess($customerID);
      }

      /**
       * Delete customer roku device
       * @param type $customerID
       * @param type $Password
       * @param type $deviceID
       * @return Object
       */
      public function deleteDevice($customerID, $Password, $deviceID) {
            try {
                  $params = array('CustomerID' => $customerID, 'Password' => $Password, 'DeviceID' => $deviceID);
                  $this->logs->info('DeviceController deleteDevice Start.');
                  if ( Customers::checkCustomer($customerID) ) {
                        $channelServices = Services::getChannelServices();
                        if ( $rokuServices = CustomerServices::getServices($customerID, "Roku", 1, $channelServices, NULL, $Password) ) {
                              if ( RokuDevices::customerDevice($deviceID, $customerID) ) {
                                    if ( RokuDevices::deleteDevice($deviceID) ) {
                                          $this->logs->info('DeviceController deleteDevice Successfully Ends for Device:' . $deviceID . ' & CustomerID:' . $customerID);
                                          return $this->handleSuccess($deviceID);
                                    }
                                    else {
                                          $this->logs->error(Messages::getMessage('2010') . ', Code: 2010, CustomerID:' . $customerID . ', DeviceID:' . $deviceID);
                                          $this->logs->error('Request:' . json_encode($params));
                                          return $this->handleError(Messages::getMessage('2010'), "2010");
                                    }
                              }
                              else {
                                    $this->logs->error(Messages::getMessage('2009') . ', Code: 2009, CustomerID:' . $customerID . ', DeviceID:' . $deviceID);
                                    $this->logs->error('Request:' . json_encode($params));
                                    return $this->handleError(Messages::getMessage('2009'), "2009");
                              }
                        }
                        else {
                              $this->logs->error('Invalid CustomerID:' . $customerID . ' or Password, Code:2003');
                              $this->logs->error('Request:' . json_encode($params));
                              return $this->handleError(Messages::getMessage('2003'), "2003");
                        }
                  }
                  else {
                        $this->logs->error('Customer ID not valid:' . $customerID . ', Code:2002');
                        $this->logs->error('Request:' . json_encode($params));
                        return $this->handleError(Messages::getMessage('2002'), "2002");
                  }
            } catch ( Exception $e ) {
                  $this->logs->error($e);
                  $this->logs->error('Request:' . json_encode($params));
                  return $this->handleError(Messages::getMessage('2008'), "2008");
            }
      }

}
