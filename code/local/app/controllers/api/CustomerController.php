<?php

require_once(Config::get('log4php.Logger'));
Logger::configure(Config::get('log4php.config'));

use Guzzle\Http\Client;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class CustomerController extends \BaseController {

    /**
     * log4php Object
     * @var object
     */
    public $logs;

    /**
     * Constructor of the class CustomerController
     *
     */
    public function __construct() {
        $this->logs = Logger::getLogger('API');
    }

    /**
     * Gets Registered Devices for the given Customer ID.
     * @param  integer $customerID Customer ID
     * @return Array   Array of registered devices.
     */
    public function getRegisteredDevices($customerID, $Password) {
        try {
            $this->logs->info('CustomerController getRegisteredDevices Start.');
            if ($checkCustomers = Customers::checkCustomer($customerID)) {
                $channelServices = Services::getChannelServices();
                if ($services = CustomerServices::getServices($customerID, "Roku", 1, $channelServices, NULL, $Password)) {
                    if ($customerServices = CustomerServices::radiusAuthentication($services[0]->Login, $services[0]->Password)) {
                        $devices = RokuDevices::getDevicesByCustomerID($customerID, NULL, 1);

                        $dvrEnabled = Customers::hasDvrEnabled($customerID);
                        $this->logs->trace(json_encode(
                                        array(
                                            'ServiceType' => $services[0]->ServiceName,
                                            'AccountID' => $services[0]->ServiceID,
                                            'Devices' => $devices,
                                            'DvrEnabled' => $dvrEnabled,
                                            'WholeHome' => Customers::hasDvrWholeHomeService($customerID)
                                        )
                        ));
                        $this->logs->info('CustomerController getRegisteredDevices Successfully Ends for CustomerID:' . $customerID);
                        return $this->handleSuccess(array(
                                    'ServiceType' => $services[0]->ServiceName,
                                    'AccountID' => $services[0]->ServiceID,
                                    'Devices' => $devices,
                                    'DvrEnabled' => $dvrEnabled,
                                    'WholeHome' => Customers::hasDvrWholeHomeService($customerID)
                        ));
                    } else {
                        $this->logs->error(Messages::getMessage('2006') . ', Code: 2006, CustomerID:' . $customerID . ', Password:' . $Password);
                        $this->logs->error(json_encode($customerServices));
                        return $this->handleError(Messages::getMessage('2006'), "2006");
                    }
                } else {
                    $this->logs->error(Messages::getMessage('2003') . ', Code: 2003, CustomerID:' . $customerID . ', Password:' . $Password);
                    $this->logs->error(json_encode($services));
                    return $this->handleError(Messages::getMessage('2003'), "2003");
                }
            } else {
                $this->logs->error(Messages::getMessage('2002') . ', Code: 2002, CustomerID:' . $customerID . ', Password:' . $Password);
                $this->logs->error(json_encode($checkCustomers));
                return $this->handleError(Messages::getMessage('2002'), "2002");
            }
        } catch (Exception $e) {
            Log::error($e);
            $this->logs->error($e);
            $this->logs->error("Request CustomerID:" . $customerID . ', Password:' . $Password);
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    /**
     * Checks if the account is authorized to play the channel.
     * @param  integer $channelID Channel ID to be authorized
     * @return string            Returns the URL of the channel, if successfully authorized.
     */
    public function authorizeChannel($deviceID) {
        try {
            $this->logs->info('CustomerController authorizeChannel Start.');
            $channelID = Input::get('channelID');
            $action = Input::get('action');
            $this->logs->trace('DeviceID:' . $deviceID . ', ChannelID:' . $channelID);

            if ($svChannel = EChannel::where('channel_id', $channelID)->where('status', 1)->first()) {
                if ($deviceDetail = RokuDevices::getDevice($deviceID)) {
                    if ($rokuServices = CustomerServices::getServices($deviceDetail->CustomerID, "Roku", 1, $svChannel->emerald_subscription_type)) {
                        if ($customerServices = CustomerServices::radiusAuthentication($rokuServices[0]->Login, $rokuServices[0]->Password)) {
                            if ($subscriptionsType = SubscriptionType::hasAccess($rokuServices[0]->ServiceName, $svChannel->emerald_subscription_type)) {
                                $path = $svChannel->path;
                                $stream_url = Channel::getStreamURL($path, 'streaming');
                                $this->logs->trace(json_encode(array('url' => $channelID, 'path' => $stream_url)));
                                $this->logs->info('CustomerController authorizeChannel Successfully Ends for DeviceID:' . $deviceID . ', ChannelID:' . $channelID);

                                //Get major & minor number from stream name                                                
                                $major_minor = Channel::get_major_minor($svChannel->stream_name);

                                //Send log to central server
                                $data = [
                                    'customer_id' => $deviceDetail->CustomerID,
                                    'action_type' => $action,
                                    'stream_type' => 'LIVE',
                                    'titan_id' => $svChannel->titan_channel_id,
                                    'major_number' => $major_minor[0],
                                    'minor_number' => $major_minor[1],
                                    'show_title' => '',
                                    'show_episode_title' => '',
                                ];
                                $url = action('LogController@process', $data);
                                $process = new Process('curl "' . $url . '" &');
                                $process->start();

                                return $this->handleSuccess(['url' => $stream_url, 'titan_id' => $svChannel->titan_channel_id, 'major_number' => $major_minor[0], 'minor_number' => $major_minor[1]]);
                            } else {
                                $this->logs->error(Messages::getMessage('2007') . ', Code: 2007 DeviceID:' . $deviceID . ', ChannelID:' . $channelID);
                                $this->logs->error(json_encode($subscriptionsType));
                                return $this->handleError(Messages::getMessage('2007'), "2007");
                            }
                        } else {
                            $this->logs->error(Messages::getMessage('2006') . ', Code: 2006, DeviceID:' . $deviceID . ', ChannelID:' . $channelID);
                            $this->logs->error(json_encode($customerServices));
                            return $this->handleError(Messages::getMessage('2006'), "2006");
                        }
                    } else {
                        $this->logs->error(Messages::getMessage('2007') . ', Code: 2007 DeviceID:' . $deviceID . ', ChannelID:' . $channelID);
                        $this->logs->error(json_encode($rokuServices));
                        return $this->handleError(Messages::getMessage('2007'), "2007");
                    }
                } else {
                    $this->logs->error(Messages::getMessage('2004') . ', Code: 2004, DeviceID:' . $deviceID . ', ChannelID:' . $channelID);
                    $this->logs->error(json_encode($deviceDetail));
                    return $this->handleError(Messages::getMessage('2004'), "2004");
                }
            } else {
                $this->logs->error(Messages::getMessage('2019') . ', Code: 2019, DeviceID:' . $deviceID . ', ChannelID:' . $channelID);
                $this->logs->error(json_encode($svChannel));
                return $this->handleError(Messages::getMessage('2019'), "2019");
            }
        } catch (Exception $e) {
            Log::error($e);
            $this->logs->error($e);
            $this->logs->error('Request DeviceID:' . $deviceID . ', ChannelId:' . $channelID);
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

    public function refreshEPG() {
        try {
            $this->logs->info('CustomerController refreshEPG Start.');
            $listing = FTP::connection()->getDirListing(".", "-la");
            dd($listing);
            $this->logs->info('CustomerController refreshEPG Ends Successfully.');
        } catch (Exception $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2008'), "2008");
        }
    }

}
