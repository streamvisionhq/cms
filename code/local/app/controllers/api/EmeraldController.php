<?php
require_once(Config::get('log4php.Logger'));
Logger::configure(Config::get('log4php.config'));

class EmeraldController extends \BaseController {

      /**
       * log4php Object
       * @var object
       */
      public $logs;

      /**
       * Constructor of the class EmeraldController
       *
       */
      public function __construct() {
            $this->logs = Logger::getLogger('API');
      }

      public function update() {
            try {
                  $this->logs->info('EmeraldController Update Start.');
                  if ( Setting::get('cm_mode') != "cms" ) {
                        $data = Input::all();

                        // Push the data to the history table.
                        try {
                              $this->logs->info(json_encode($data));
                              Logs::addLog($data['CustomerID'], $data);
                        } catch ( Exception $e ) {
                              $this->logs->error($e);
                              $this->logs->error(json_encode($data));
                              Log::error($e);
                        }

                        if ( $data['ExternalRef'] ) {
                              if ( $internalData = Services::getInternalData($data['ServiceType'], $data['ExternalRef']) ) {
                                    //Replace External Values to Internal Values
                                    $data['ServiceType'] = $internalData->InternalName;
                                    $data['ExternalRef'] = $internalData->InternalRef;

                                    $allowedExternalRef = Config::get('streamvision.services_category');
                                    if ( in_array($data['ExternalRef'], $allowedExternalRef) ) {
                                          if ( $data['CustomerID'] && $data['AccountID'] && $data['ServiceType'] && $data['ExternalRef'] && $data['Login'] && $data['Password'] ) {

                                                Customers::ifCreateCustomer($data);

                                                if ( $data['Action'] == "delete" ) {
                                                      $data['Action'] = "update";
                                                }

                                                switch ( strtolower($data['Action']) ) {
                                                      case 'add':
                                                            if ( CustomerServices::checkService($data['AccountID'], $data['CustomerID']) ) {
                                                                  CustomerServices::updateService($data);
                                                            }
                                                            else {
                                                                  CustomerServices::addService($data);
                                                            }
                                                            break;
                                                      case 'update':
                                                            if ( CustomerServices::checkService($data['AccountID'], $data['CustomerID']) ) {
                                                                  CustomerServices::updateService($data);
                                                                  if ( CustomerServices::countActiveServices($data['CustomerID']) == 0 ) {
                                                                        RokuDevices::deleteDevices($data['CustomerID']);
                                                                  }
                                                            }
                                                            else {
                                                                  CustomerServices::addService($data);
                                                            }
                                                            break;
                                                      case 'delete':
                                                            CustomerServices::deleteService($data['CustomerID'], $data['AccountID']);
                                                            if ( CustomerServices::countActiveServices($data['CustomerID']) == 0 ) {
                                                                  RokuDevices::deleteDevices($data['CustomerID']);
                                                            }
                                                            break;
                                                      default:
                                                            break;
                                                }
                                                //Manage DVR Storage
                                                if ( $data['ExternalRef'] == "DVR" ) {
                                                      DVR::manageDVRStorage($data);
                                                }
                                          }
                                    }
                              }
                        }
                  }
                  $this->logs->info('EmeraldController Update End.');
                  return Response::xml([
                                'retcode' => 0,
                                'message' => 'Looks good'
                  ]);
            } catch ( Exception $e ) {
                  $this->logs->error($e);
            }
      }

}
