<?php

class LogController extends \BaseController {

    public function process() {
        //get current show of the channel
        $currentShow = Input::get('show_title');
        
        if ( Input::get('stream_type') == 'LIVE' ) {
            $epg = new EPGController();
            $currentShow = '';
            $channel = $epg->getChannel(Input::get('titan_id'));
            if ( isset($channel->Show) ) {
                $show = $epg->getCurrentShow($channel->Show);
                if ( $show ) {
                    $currentShowInfo = $epg->getShowInfo($show);
                    $currentShow = $currentShowInfo['Title'];
                }
            }
        }
        
        $data = [
              'customer_id' => Input::get('customer_id'),
              'action_type' => Input::get('action_type'),
              'stream_type' => Input::get('stream_type'),
              'titan_id' => Input::get('titan_id'),
              'major_number' => Input::get('major_number'),
              'minor_number' => Input::get('minor_number'),
              'show_title' => $currentShow,
              'show_episode_title' => Input::get('show_episode_title'),
        ];
        
        Logs::sendLog($data);
    }

}
