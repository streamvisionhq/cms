<?php

use Proliveapi\Proliveapi;

class ApiClientController extends BaseController {

      private $allowedMethod;
      private $uriSegments;
      private $apiService = array();

      public function __construct() {
            $this->uriSegments = Request::segments();
      }

      public function processChannelApi() {
            $method = '';
            $segment1 = $this->uriSegments[2];
            $authUser = true;

            switch ( $segment1 ) {
                  case 'register':
                        $method = 'create';
                        $authUser = false;
                        $this->allowedMethod = "POST";
                        break;

                  case 'account':
                        $this->allowedMethod = "GET";
                        $method = 'myAccount';
                        break;

                  case 'update':
                        $this->allowedMethod = "POST";
                        $method = 'modifyClient';
                        break;

                  case 'status':
                        $this->allowedMethod = "POST";
                        $method = 'modifyClientStatus';
                        break;

                  case 'regenerateapi':
                        $this->allowedMethod = "POST";
                        $method = 'regenerateClientApi';
                        break;

                  case 'remove':
                        $this->allowedMethod = "GET";
                        $method = 'removeClient';
                        break;
            }

            $this->apiService = array("service" => "client", "method" => $method, "authenticate" => $authUser);
            $apiInstance = new Proliveapi($this->apiService, $this->allowedMethod, $this->uriSegments);
      }

}
