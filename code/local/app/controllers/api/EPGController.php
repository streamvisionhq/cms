<?php

use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Collection;
use Underscore\Types\Arrays;

require_once(Config::get('log4php.Logger'));
Logger::configure(Config::get('log4php.config'));

class EPGController extends \BaseController {

    /**
     * log4php Object
     * @var object
     */
    public $logs;

    /**
     * Web Service Name
     * @var string
     */
    protected $name = 'titantvEPG';

    /**
     * WSDL of the EPG service
     * @var string
     */
    protected $wsdl = 'http://data.titantvguide.titantv.com/service.asmx?WSDL';

    /**
     * EPG File location
     * @var string
     */
    protected $epgFileLocation = "/epgdata/GetSchedules.xml";

    /**
     * Yesterdays EPG File location
     * @var string
     */
    protected $yesterdayEpgFileLocation = "/epgdata/YesterdaySchedules.xml";

    /**
     * Attributes key in the EPG file.
     * @var string
     */
    protected $attributes = '@attributes';

    /**
     * @var boolean
     */
    protected $trace = true;

    /**
     * Constructor of the class EPGController
     *
     */
    public function __construct() {
        $this->logs = Logger::getLogger('API');
    }

    /**
     * Get all the available functions
     *
     * @return mixed
     */
    public function functions() {
        /* // Add a new service to the wrapper
          SoapWrapper::add(function ($service) {
          $service
          ->name('titantvEPG')
          ->wsdl('http://data.titantvguide.titantv.com/service.asmx?WSDL')
          ->trace(true)
          ->cache(WSDL_CACHE_NONE);                                       // Optional: Set the WSDL cache
          });

          $data = [
          'registrationKey' => 'A47zs5vhy0KbMWqQQ%2b5CnQ%3d%3d'
          ];

          // Using the added service, JSON_PRETTY_PRINT)
          SoapWrapper::service('titantvEPG', function ($service) use ($data) {
          $result = $service->call('GetSchedules', [$data])->TitanTV;
          //$jsonResponse = json_encode($service->call('GetSchedules', [$data])->TitanTV);
          $show = $result->Channel[0]->Show[0]->Title->lang;
          $data = $this->utf8ize($show);
          $json = json_encode($data);
          $error = json_last_error();
          return $this->response->array([
          'result' => $show
          ]);
          }); */
        $channels = $this->getChannelsFromEPGData();
        $collections = new Collection($channels);
        return $collections->toJson();

        /* $epgs = EPG::all();
          return $epgs->toJson(); */
    }

    /**
     * Maps to /epg/channels. Returns of all the channels metadata.
     * @return Channels Custom channels object with only necessary data.
     */
    public function getChannels() {
        try {
            $this->logs->info('EPGController getChannels Start.');
            $channels = $this->getChannelsFromEPGData();
            if ($channels == 2028) {
                $this->logs->error('EPG Guide File not found.');
                return $this->handleError(Messages::getMessage('2028'), "2028");
            }
            if ($channels == 2029) {
                $this->logs->error('EPG Guide File not parsed.');
                return $this->handleError(Messages::getMessage('2029'), "2029");
            }
            $titanChannels = array();
            $channelsInfoToReturn = array_map(array($this, "getChannelInfo"), $channels);
            foreach ($channelsInfoToReturn as $ch) {
                $titanChannels[$ch['channelId']] = $ch;
            }

            $svChannels = $this->getOnlySVChannels($titanChannels);
            if ($svChannels) {
                $this->logs->info('EPGController getChannels Successfully Ends.');

                if (!$channel_last_update_date = Setting::get('channel_last_update_date')) {
                    $channel_last_update_date = date("Y-m-d H:i:s");
                    Setting::set('channel_last_update_date', $channel_last_update_date);
                }

                return $this->handleSuccess($svChannels);
            } else {
                $this->logs->error(Messages::getMessage('2011'), "2011");
                return $this->handleError(Messages::getMessage('2011'), "2011");
            }
        } catch (Exception $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /**
     * Maps to /epg/channels. Returns of all the channels metadata.
     * @return Channels Custom channels object with only necessary data.
     */
    public function getChannels_v2() {
        try {
            $this->logs->info('EPGController getChannels Start.');
            $channels = $this->getChannelsFromEPGData();
            if ($channels == 2028) {
                $this->logs->error('EPG Guide File not found.');
                return $this->handleError(Messages::getMessage('2028'), "2028");
            }
            if ($channels == 2029) {
                $this->logs->error('EPG Guide File not parsed.');
                return $this->handleError(Messages::getMessage('2029'), "2029");
            }
            $titanChannels = array();
            $channelsInfoToReturn = array_map(array($this, "getChannelInfo"), $channels);
            foreach ($channelsInfoToReturn as $ch) {
                $titanChannels[$ch['channelId']] = $ch;
            }

            $svChannels = $this->getOnlySVChannels($titanChannels);
            if ($svChannels) {
                $this->logs->info('EPGController getChannels Successfully Ends.');

                if (!$channel_last_update_date = Setting::get('channel_last_update_date')) {
                    $channel_last_update_date = date("Y-m-d H:i:s");
                    Setting::set('channel_last_update_date', $channel_last_update_date);
                }

                $last_modified = BaseModel::get_last_modified("channels");

                return $this->handleSuccess($svChannels, null, $last_modified);
            } else {
                $this->logs->error(Messages::getMessage('2011'), "2011");
                return $this->handleError(Messages::getMessage('2011'), "2011");
            }
        } catch (Exception $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /**
     * Maps to /epg. Returns all the channels along with the shows informations.
     * @return [type] [description]
     */
    public function getFullEPG() {
        try {
            $this->logs->info('EPGController getFullEPG Start.');
            $channels = $this->getChannelsFromEPGData();
            if ($channels == 2028) {
                $this->logs->error('EPG Guide File not found.');
                return $this->handleError(Messages::getMessage('2028'), "2028");
            }
            if ($channels == 2029) {
                $this->logs->error('EPG Guide File not parsed.');
                return $this->handleError(Messages::getMessage('2029'), "2029");
            }
            $this->logs->trace('EPG File Found.');
            $channelsWithShows = array_map(array($this, "getChannelInfoWithShow"), $channels);

            $titanChannels = array();
            foreach ($channelsWithShows as $ch) {
                $titanChannels[$ch['channelId']] = $ch;
            }
            $svChannelsEPG = $this->getOnlySVChannels($titanChannels);
            $this->logs->info('EPGController getFullEPG Successfully Ends.');
            return $this->handleSuccess($svChannelsEPG);
        } catch (Exception $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /**
     * Maps to /epg_v2. Returns all the channels along with the shows informations.
     * @return [type] [description]
     */
    public function getPageViseEPG($page) {
        try {
            $this->logs->info('EPGController getFullEPG Start.');
            $channels = $this->getChannelsFromEPGDataPageVise($page);
            if ($channels == 2028) {
                $this->logs->error('EPG Guide File not found.');
                return $this->handleError(Messages::getMessage('2028'), "2028");
            }
            if ($channels == 2029) {
                $this->logs->error('EPG Guide File not parsed.');
                return $this->handleError(Messages::getMessage('2029'), "2029");
            }
            $this->logs->trace('EPG File Found.');
            $channelsWithShows = array_map(array($this, "getChannelInfoWithShow"), $channels);

            $titanChannels = array();
            foreach ($channelsWithShows as $ch) {
                $titanChannels[$ch['channelId']] = $ch;
            }
            $svChannelsEPG = $this->getOnlySVChannels($titanChannels);
            $this->logs->info('EPGController getFullEPG Successfully Ends.');

            $last_modified_date = $this->getLastModifiedDate($page);
            return $this->handleSuccess($svChannelsEPG, null, $last_modified_date);
        } catch (Exception $e) {

            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /**
     * Returns current show in the specified channel.
     * @param  integer $channelId TitanTV Channel Id.
     * @return Show                Current running show on the specified channel.
     */
    public function getCurrentChannelShow($channelId) {
        try {
            $this->logs->info('EPGController getCurrentChannelShow Start.');
            $channel = $this->getChannel($channelId);
            if (!is_object($channel)) {
                if ($channel == 2028) {
                    $this->logs->error('EPG Guide File not found.');
                    return $this->handleError(Messages::getMessage('2028'), "2028");
                }
                if ($channel == 2029) {
                    $this->logs->error('EPG Guide File not parsed.');
                    return $this->handleError(Messages::getMessage('2029'), "2029");
                }
            }

            if (isset($channel->Show)) {
                $show = $this->getCurrentShow($channel->Show);
                if ($show) {
                    $currentShowInfo = $this->getShowInfo($show);
                    $this->logs->info('EPGController getCurrentChannelShow Successfully Ends.');
                    return $this->handleSuccess($currentShowInfo);
                }
            }
            $this->logs->error("Currently, there is no show configured for this channel.");
            return $this->handleError(Messages::getMessage('2030'), "2030");
            return $this->handleError("Currently, there is no show configured for this channel.");
        } catch (Exeption $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /**
     * Returns the current shows of all SV channels
     * @return [Array] Current Shows of all Channels
     */
    public function getCurrentShows() {
        try {
            $this->logs->info('EPGController getCurrentShows Start.');
            $channels = $this->getChannelsFromEPGData();
            if ($channels == 2028) {
                $this->logs->error('EPG Guide File not found.');
                return $this->handleError(Messages::getMessage('2028'), "2028");
            }
            if ($channels == 2029) {
                $this->logs->error('EPG Guide File not parsed.');
                return $this->handleError(Messages::getMessage('2029'), "2029");
            }
            $currentShows = [];
            $titanChannels = [];
            $attributes = '@attributes';
            foreach ($channels as $ch) {
                $channelAttributes = $ch->$attributes;
                $titanChannels[$channelAttributes->channelId] = $ch;
            }
            $svChannelsEPG = $this->getOnlySVChannelsWithoutAdditionalInfo($titanChannels);

            foreach ($svChannelsEPG as $key => $svChannel) {
                if (isset($svChannel->Show)) {
                    $show = $this->getCurrentShow($svChannel->Show);
                    if ($show) {
                        $currentShowInfo = $this->getShowInfo($show);
                        $chAttributes = $svChannel->$attributes;
                        $currentShowInfo['channelId'] = (property_exists($chAttributes, 'channelId') ? (int) $chAttributes->channelId : 0);
                        $currentShowInfo['channelNumber'] = (property_exists($chAttributes, 'channelNumber') ? (int) $chAttributes->channelNumber : 0);
                        $currentShowInfo['channelDescription'] = (property_exists($chAttributes, 'description') && is_string($chAttributes->description) ? $chAttributes->description : '');
                        $currentShows[] = $currentShowInfo;
                    }
                }
            }

            if ($currentShows) {
                $this->logs->info('EPGController getCurrentShows Successfully Ends.');
                return $this->handleSuccess($currentShows);
            } else {
                $this->logs->error(Messages::getMessage('2012'), "2012");
                return $this->handleError(Messages::getMessage('2012'), "2012");
            }
        } catch (Exception $e) {
            $this->logs->error($e);
            return $this->handleError(Messages::getMessage('2027'), "2027");
        }
    }

    /*     * *****************************************************************************
     * * Private Methods
     * ***************************************************************************** */

    /**
     * Get the specified channel.
     * @param  integer $channelId Channel number.
     * @return object                EPG Channel object.
     */
    function getChannel($channelId) {
        $channels = $this->getChannelsFromEPGData();
        if ($channels == 2028 || $channels == 2029) {
            return $channels;
        }
        $attributes = '@attributes';
        $channel = Arrays::find($channels, function($ch) use($channelId, $attributes) {
                    $channelAttributes = $ch->$attributes;
                    if ($channelAttributes->channelId == $channelId) {
                        return true;
                    }
                });
        return $channel;
    }

    /**
     * Get the current show based on the UTC timestamp.
     * @param  collection $shows EPG shows collection
     * @return object          Current show from the specified channel.
     */
    function getCurrentShow($shows) {

        $attributes = '@attributes';
        $currentShow = Arrays::find($shows, function($show) use($attributes) {
                    $showAttributes = $show->$attributes;
                    $duration = $showAttributes->duration;
                    $startTime = Carbon::parse($showAttributes->adjustedstartTime);
                    $endTime = $this->getEndTimeUTCFromDuration($startTime, $duration);
                    $currentTime = Carbon::now();
                    
                    if ($currentTime > $startTime && $currentTime < $endTime) {
                        return true;
                    }
                });
        return $currentShow;
    }

    /**
     * Get custom channel object from the specified EPG channel.
     * @param  object $channel EPG channel object.
     * @return object          Custom channel object.
     */
    function getChannelInfo($channel) {
        $attributes = '@attributes';
        $channelAttributes = $channel->$attributes;
        return array(
            'channelId' => (property_exists($channelAttributes, 'channelId') ? (int) $channelAttributes->channelId : 0),
            'channelNumber' => (property_exists($channelAttributes, 'channelNumber') ? (int) $channelAttributes->channelNumber : 0),
            'description' => (property_exists($channelAttributes, 'description') && is_string($channelAttributes->description) ? $channelAttributes->description : '')
        );
    }

    /**
     * Get custom channel object along with the shows collection.
     * @param  object $channel EPG channel object
     * @return object          Custom channel object with shows array.
     */
    function getChannelInfoWithShow($channel) {
        $attributes = '@attributes';
        $channelAttributes = $channel->$attributes;
        $shows = (property_exists($channel, 'Show') ? $channel->Show : []);
        
        if (Input::has('startTimeUTC')) {
            try {
                $startTimeUTC = Carbon::parse(Input::get('startTimeUTC'));
                $endTimeUTC = (Input::has('endTimeUTC') ? Carbon::parse(Input::get('endTimeUTC')) : Carbon::now()->addDays(30));
                $shows = Arrays::filter($shows, function($show) use($attributes, $startTimeUTC, $endTimeUTC) {
                            $showAttributes = $show->$attributes;
                            $duration = $showAttributes->duration;
                            $startTime = Carbon::parse($showAttributes->startTimeUtc);
                            if ($startTime >= $startTimeUTC && $startTime < $endTimeUTC) {
                                return true;
                            }
                        });
                $shows = array_values($shows);
            } catch (Exception $e) {
                $this->logs->warning('EPGController getChannelInfoWithShow Exception Occuered.');
                // Ignore the exception and proceed to return the entire EPG.
            }
        }
        return array(
            'channelId' => (property_exists($channelAttributes, 'channelId') ? (int) $channelAttributes->channelId : 0),
            'channelNumber' => (property_exists($channelAttributes, 'channelNumber') ? (int) $channelAttributes->channelNumber : 0),
            'description' => (property_exists($channelAttributes, 'description') ? $channelAttributes->description : ''),
            'shows' => array_map(array($this, "getShowInfo"), $shows)
        );
    }

    /**
     * Get Custom show object from the EPG show object.
     * @param  object $show EPG show object
     * @return object       Custom show object
     */
    function getShowInfo($show) {
        $attributes = '@attributes';
        $showAttributes = $show->$attributes;
        $eventId = $showAttributes->eventId;
        $duration = $showAttributes->duration;
        $startTime = Carbon::parse($showAttributes->startTimeUtc);
        $adjustedStartTime = Carbon::parse($showAttributes->adjustedstartTime);
        $adjustedEndTime = Carbon::parse($showAttributes->adjustedEndTime);
        $endTime = $this->getEndTimeUTCFromDuration($startTime, $duration);
        $rating = (is_object($show->Rating) && property_exists($show->Rating, '@attributes') && property_exists($show->Rating->$attributes, 'tv')) ? $show->Rating->$attributes->tv : '';
        return array(
            'startTimeUtc' => $startTime->toIso8601String(),
            'endTimeUtc' => $endTime->toIso8601String(),
            'Title' => (property_exists($show, 'Title') ? $show->Title : ''),
            'Description' => (property_exists($show, 'Description') && is_string($show->Description) ? $show->Description : ''),
            'ShowType' => (property_exists($show, 'ShowType') ? $show->ShowType : ''),
            'Cast' => (property_exists($show, 'Cast') ? $show->Cast : ''),
            'DisplayGenre' => (property_exists($show, 'DisplayGenre') ? $show->DisplayGenre : ''),
            'Rating' => $rating,
            'adjustedstarttime' => $adjustedStartTime->toIso8601String(),
            'adjustedendtime' => $adjustedEndTime->toIso8601String(),
            'eventId' => $eventId,
        );
    }

    /**
     * Read EPG data from the file.
     * @return array EPG channels array.
     */
    function getChannelsFromEPGDataPageVise($page) {
        try {
            $epgChannels = $this->getRedis(Config::get('streamvision.redis_channels_key'));
            if (!$epgChannels) {
                ini_set('memory_limit', '1024M');

                $date_page = $this->getDatePage($page);

                if (!$date_page) {
                    return 2028;
                    $this->error("File not found!", 404);
                }

                $data = array();

                $filePath = realpath(base_path() . "/epgdata/");

                if (file_exists($filePath . "/Schedule-" . $date_page["date"] . "-" . $date_page["page"] . ".json")) {
                    $data = json_decode(file_get_contents($filePath . "/Schedule-" . $date_page["date"] . "-" . $date_page["page"] . ".json"));
                }

                $array = array();

                if (count($data) > 0) {
                    foreach ($data->Channel as $channel_id => $channel) {

                        $channel = (array) $channel;
                        if (isset($channel['@attributes'])) {
                            $array[$channel_id]['@attributes'] = $channel['@attributes'];
                        }

                        foreach ($channel['Show'] as $show) {
                            $array[$channel_id]['Show'][] = (array) $show;
                        }
                    }
                }

                $array = array_values($array);

                if (!empty($array)) {

                    $array = array('Channel' => $array);
                    $epgChannels = json_encode($array);

                    $channels = json_decode($epgChannels, true)['Channel'];

                    $epgChannels = json_encode(array('Channel' => $channels));
                    $this->setRedis(Config::get('streamvision.redis_channels_key'), $epgChannels);
                } else {
                    if (!file_exists($filePath)) {
                        return 2028;
                        $this->error("File not found!", 404);
                    } else {
                        return 2029;
                    }
                }
            }


            $channels = json_decode($epgChannels);
            $channels = $channels->Channel;

            return $channels;
        } catch (Exception $e) {
            $this->error($e, 500);
        }
    }

    /**
     * Read EPG data from the file.
     * @return array EPG channels array.
     */
    function getChannelsFromEPGData() {
        try {
            $epgChannels = $this->getRedis(Config::get('streamvision.redis_channels_key'));
            if (!$epgChannels) {
                ini_set('memory_limit', '1024M');

                $data1 = array();
                $data2 = array();
                $data3 = array();

                $filePath = realpath(base_path() . "/epgdata/");
                $current_date = date("Y-m-d");
                $tomorrow_date = date("Y-m-d", strtotime($current_date . ' +1 day'));
                $date_after_tomorrow_date = date("Y-m-d", strtotime($tomorrow_date . ' +1 day'));

                if (file_exists($filePath . "/Schedule-" . $current_date . ".json")) {
                    $data1 = json_decode(file_get_contents($filePath . "/Schedule-" . $current_date . ".json"));
                }

                if (file_exists($filePath . "/Schedule-" . $tomorrow_date . ".json")) {
                    $data2 = json_decode(file_get_contents($filePath . "/Schedule-" . $tomorrow_date . ".json"));
                }


                if (file_exists($filePath . "/Schedule-" . $date_after_tomorrow_date . ".json")) {
                    $data3 = json_decode(file_get_contents($filePath . "/Schedule-" . $date_after_tomorrow_date . ".json"));
                }

                $array = array();

                if (count($data1) > 0) {
                    foreach ($data1->Channel as $channel_id => $channel) {

                        $channel = (array) $channel;
                        if (isset($channel['@attributes'])) {
                            $array[$channel_id]['@attributes'] = $channel['@attributes'];
                        }

                        foreach ($channel['Show'] as $show) {
                            $array[$channel_id]['Show'][] = (array) $show;
                        }
                    }
                }

                if (count($data2) > 0) {
                    foreach ($data2->Channel as $channel_id => $channel) {

                        $channel = (array) $channel;
                        if (isset($channel['@attributes'])) {
                            $array[$channel_id]['@attributes'] = $channel['@attributes'];
                        }

                        foreach ($channel['Show'] as $show) {
                            $array[$channel_id]['Show'][] = (array) $show;
                        }
                    }
                }

                if (count($data3) > 0) {
                    foreach ($data3->Channel as $channel_id => $channel) {

                        $channel = (array) $channel;
                        if (isset($channel['@attributes'])) {
                            $array[$channel_id]['@attributes'] = $channel['@attributes'];
                        }

                        foreach ($channel['Show'] as $show) {
                            $array[$channel_id]['Show'][] = (array) $show;
                        }
                    }
                }


                $array = array_values($array);
                $array = array('Channel' => $array);

                $epgChannels = json_encode($array);

                if ($epgChannels !== false) {
                    $filePathYesterday = realpath(base_path() . $this->yesterdayEpgFileLocation);
                    $yeserdayChannels = $this->readEpgXmlFile($filePathYesterday);

                    $channels = json_decode($epgChannels, true)['Channel'];

                    if ($yeserdayChannels !== false) {

                        $yChannels = json_decode($yeserdayChannels, true)['Channel'];

                        // add channels and shows from yesterdays data, #5600
                        foreach ($yChannels AS &$yChannel) {

                            $channel = $this->getChannelInArray($channels, $yChannel['@attributes']['channelId']);

                            if ($channel === false) {
                                $channels[] = $yChannel;
                            } else {
                                if (!array_key_exists('Show', $channel)) {
                                    $channel['Show'] = array();
                                }

                                if (array_key_exists('Show', $yChannel) && $yChannel['Show'] != null) {
                                    $added = false;
                                    foreach ($yChannel['Show'] AS &$show) {
                                        if (!$this->isShowInArray($channel['Show'], $show)) {
                                            $channel['Show'][] = $show;
                                        }
                                    }
                                }
                                if (count($channel['Show']) == 0) {
                                    unset($channel['Show']);
                                }
                            }
                        }
                    }

                    $epgChannels = json_encode(array('Channel' => $channels));
                    $this->setRedis(Config::get('streamvision.redis_channels_key'), $epgChannels);
                } else {
                    if (!file_exists($filePath)) {
                        return 2028;
                        $this->error("File not found!", 404);
                    } else {
                        return 2029;
                    }
                }
            }

            $channels = json_decode($epgChannels);
            $channels = $channels->Channel;

            $free_channels = Channel::getAllFreeChannels();

            if ($free_channels) {
                foreach ($free_channels as $key => $value) {
                    $attributes = '@attributes';
                    $channel = new stdClass();
                    $channel->$attributes = json_decode(json_encode([
                        'channelId' => $value->titan_channel_id,
                        'channelNumber' => '',
                        'majorChannelNumber' => '3',
                        'minorChannelNumber' => '1',
                        'callsign' => '',
                        'network' => '',
                        'broadcastType' => 'digital',
                        'description' => 'Free Channel Show',
                    ]));
                    $shows = [];
                    $show = new stdClass();
                    $show->$attributes = json_decode(json_encode([
                        "startTimeUtc" => date("Y-m-d") . "T07:00:00",
                        "endTimeUtc" => date("Y-m-d", strtotime("+1 day")) . "T07:00:00",
                        "adjustedstartTime" => date("Y-m-d") . "T00:00:00",
                        "adjustedEndTime" => date("Y-m-d", strtotime("+1 day")) . "T00:00:00",
                        "duration" => "P0Y0M1DT0H0M0S",
                        "eventId" => "1535751768000",
                        "programId" => "4991964",
                        "parentProgramId" => "2038977",
                        "isHd" => true,
                        "year" => "2016",
                        "originalAirDate" => "2016-06-19",
                        "cc" => true
                    ]));
                    $show->Title = 'Free Channel Show';
                    $show->EpisodeTitle = 'Free Channel Show';
                    $show->Description = 'Free Channel Show';
                    $show->ShowType = 'Series';
                    $show->Cast = [];
                    $show->DisplayGenre = 'True';
                    $show->Category = [];
                    $show->Rating = new stdClass();
                    $show->Rating->$attributes = new stdClass();
                    $show->Rating->$attributes->tv = 'TV-14';
                    $show->Rating->$attributes->other = 'D,L,S';
                    array_push($shows, $show);
                    $channel->Show = $shows;
                    $channels[] = $channel;
                }
            }

            return $channels;
        } catch (Exception $e) {
            $this->error($e, 500);
        }
    }

    /**
     * Reads XML EPG data fro ma file.
     * @param String $filePath
     * @return String|boolean json converted data or false on failure
     */
    public static function readEpgXmlFile($filePath) {
        if (file_exists($filePath)) {
            $file_contents = File::get($filePath);
            try {
                $formatter = Formatter::make($file_contents, Formatter::XML);
                return $formatter->toJson();
            } catch (Exception $e) {
                return false;
            }
//                    $this->setRedis(Config::get('streamvision.redis_channels_key'), $epgChannels);
        } else {
            return false;
        }
    }

    /**
     * Check if a channel is in the specified array.
     * @param mixed $array array gotten from reading EPG xml file, processing to json, getting the Channel key
     * @param string $channelId
     * @return mixed false if not found, the channel array if is found
     */
    private function getChannelInArray($array, $channelId) {
        foreach ((array) $array AS &$channel) {
            if (isset($channel['@attributes']['channelId'])) {
                if ($channel['@attributes']['channelId'] == $channelId) {
                    return $channel;
                }
            }
        }
        return false;
    }

    /**
     * Checks if a show exists in a show array gotten from EPG XML.
     * @param array $array
     * @param array $show
     * @return boolean
     */
    private function isShowInArray(&$array, $show) {
        foreach ($array AS &$arrShow) {
            $attrDiff = array_diff($arrShow['@attributes'], $show['@attributes']);
            if (count($attrDiff) == 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Filters Titan Channels to return only those which have a corresponding SV Channel
     * @param  [Array] $titanChannels List of all Titan Channels
     * @return [Array]                List of filtered Titan Channels which have a corresponding SV Channel
     */
    function getOnlySVChannels($titanChannels) {
        $svChannels = EChannel::where('channel_type', 1)->where('status', 1)->select('title', 'channel_id', 'titan_channel_id', 'short_description', 'channel_logo', 'emerald_subscription_type', 'free_channel', 'channel_number')->orderBy('sort_order', 'ASC')->get();
        $svChannelsInfoToReturn = $svChannels->map(function($svChannel) use ($titanChannels) {
            $titanChannel;
            if (!empty($svChannel->titan_channel_id) && array_key_exists($svChannel->titan_channel_id, $titanChannels)) {
                $titanChannel = $titanChannels[$svChannel->titan_channel_id];
                if (empty($titanChannel['description'])) {
                    $titanChannel['description'] = $svChannel->short_description;
                }
                $titanChannel['svChannelId'] = (int) $svChannel->channel_id;
                $titanChannel['channelLogo'] = asset("assets/data/image/" . $svChannel->channel_logo);
                $titanChannel['title'] = $svChannel->title;
                $titanChannel['subscription_type'] = $svChannel->emerald_subscription_type;
            } else {
                $titanChannel = array();
                $titanChannel['channelId'] = '';
                $titanChannel['channelNumber'] = '';
                $titanChannel['title'] = $svChannel->title;
                $titanChannel['description'] = $svChannel->short_description;
                $titanChannel['svChannelId'] = (int) $svChannel->channel_id;
                $titanChannel['channelLogo'] = asset("assets/data/image/" . $svChannel->channel_logo);
                $titanChannel['subscription_type'] = $svChannel->emerald_subscription_type;
            }
            $titanChannel['channelNumberCMS'] = $svChannel->channel_number;
            return $titanChannel;
        });
        return $svChannelsInfoToReturn;
    }

    /**
     * Filters Titan Channels to return only those which have a corresponding SV Channel without adding any additional info
     * @param  [Array] $titanChannels List of all Titan Channels
     * @return [Array]                List of filtered Titan Channels which have a corresponding SV Channel
     */
    function getOnlySVChannelsWithoutAdditionalInfo($titanChannels) {
        $svChannels = EChannel::whereNotNull('titan_channel_id')->select('title', 'channel_id', 'titan_channel_id', 'channel_logo')->orderBy('sort_order', 'ASC')->get();
        $svChannelsInfoToReturn = $svChannels->filter(function($svChannel) {
                    return !empty($svChannel->titan_channel_id);
                })->map(function($svChannel) use ($titanChannels) {
            $titanChannel = $titanChannels[$svChannel->titan_channel_id];
            return $titanChannel;
        });
        return $svChannelsInfoToReturn;
    }

    /**
     * Get the end time based on the start time and duration.
     * @param  object $startTimeUTC Carbon Start Time in UTC
     * @param  string $duration     EPG duration in format: P0Y0M3DT0H0M0S
     * @return object               Carbon End Time in UTC
     */
    function getEndTimeUTCFromDuration($startTimeUTC, $duration) {
        // Regex to parse number of days.
        preg_match('~M(.*?)DT~', $duration, $output);
        $days = $output[1];

        // Regex to parse number of hours.
        preg_match('~DT(.*?)H~', $duration, $output);
        $hours = $output[1];

        // Regex to parse number of minutes.
        preg_match('~H(.*?)M~', $duration, $output);
        $minutes = $output[1];

        $endTimeUTC = $startTimeUTC->copy()->addDays((int) $days)->addHours((int) $hours)->addMinutes((int) $minutes);
        return $endTimeUTC;
    }

    function setRedis($key, $value) {
        try {
            Redis::set($key, $value);
            Redis::expire($key, Config::get('streamvision.redis_cache_expire_interval')); // 6 hours.
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    function getRedis($key) {
        try {
            return Redis::get($key);
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    function XML2JSON($xml) {

        function normalizeSimpleXML($obj, &$result) {
            $data = $obj;
            if (is_object($data)) {
                $data = get_object_vars($data);
            }
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $res = null;
                    normalizeSimpleXML($value, $res);
                    if (($key == '@attributes') && ($key)) {
                        $result = $res;
                    } else {
                        $result[$key] = $res;
                    }
                }
            } else {
                $result = $data;
            }
        }

        normalizeSimpleXML(simplexml_load_string($xml), $result);
        return json_encode($result);
    }

    function utf8ize($d) {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = $this->utf8ize($v);
            }
        } else if (is_object($d)) {
            foreach ($d as $k => $v) {
                $d->$k = $this->utf8ize($v);
            }
        } else {
            return utf8_encode($d);
        }

        return $d;
    }

}
