<?php

class ClientServicesController extends BaseController {

      private $uid = 0;

      public function __construct() {
            $this->uid = Auth::id();
      }

      /**
       * Show All Customer Services
       * @param string $customerID
       * @return view
       */
      public function customerServices($customerID) {
            $selectServices = false;
            $customer = Customers::getCustomer($customerID);
            if ( Input::get('assign-service') ) {
                  $postData = Input::all();

                  $messages = ['service.required' => 'Service field must not be empty'];
                  $rules = ['service' => 'required'];

                  $validator = Validator::make($postData, $rules, $messages);

                  if ( $validator->fails() ) {  // send back to the page with the input data and errors
                        return Redirect::to('customer/' . $customerID . '/services')->withInput()->withErrors($validator);
                  }
                  else {
                        $accountID = mt_rand();
                        if ( strlen($accountID) > 11 ) {
                              $accountID = substr($accountID, 0, 11);
                        }
                        $selectedService = Services::getService($postData["service"]);
                        $serviceData = [
                              'CustomerID' => $customerID,
                              'ServiceID' => $accountID,
                              'ServiceModule' => $selectedService->InternalRef,
                              'ServiceName' => $selectedService->InternalName,
                              'Login' => uniqid(),
                              'Password' => uniqid(),
                              'Values' => "",
                              'Active' => "1",
                              'CreatedDate' => date('Y-m-d H:i:s'),
                              'UpdatedDate' => date('Y-m-d H:i:s')
                        ];
                        DB::table('customer_services')->insert($serviceData);

                        if ( $selectedService->InternalRef == "DVR" ) {
                              $serviceData['Action'] = "add";
                              $serviceData['ServiceType'] = $selectedService->InternalName;
                              $serviceData['AccountID'] = $accountID;
                              DVR::manageDVRStorage($serviceData);
                        }

                        Session::flash('flash_message', 'Service Added successfully.');
                        Session::flash('flash_type', 'alert-success');

                        return Redirect::to('customer/' . $customerID . '/services');
                  }
            }

            if ( !CustomerServices::checkCustomerServices($customerID, "Roku") ) {
                  $allServices = Services::getallServices("Roku");
            }
            elseif ( !CustomerServices::checkCustomerServices($customerID, "DVR") ) {
                  $allServices = Services::getallServices("DVRBasic");
            }
            elseif ( !CustomerServices::checkCustomerServices($customerID, "DVRWholeHome") ) {
                  $allServices = Services::getallServices("DVRWholeHome");
            }
            else {
                  $allServices = Services::getallServices("Others");
            }

            foreach ( $allServices as $service ) {
                  $selectServices[$service->ID] = $service->Title;
            }

            $data['allServices'] = $allServices;
            $data['selectServices'] = $selectServices;
            $data['customerServices'] = CustomerServices::getCustomerService($customerID);
            return View::make('customer-services/services-list')->with($data);
      }

      public function changeServiceStatus($id) {
            $error = false;
            $service = CustomerServices::getService($id);
            $error = CustomerServices::checkGlobalServiceDependency($service);
            if ( $error ) {
                  echo $error;
            }
            else {
                  $status = "1";
                  if ( $service->Active == "1" ) {
                        $status = "0";
                  }
                  CustomerServices::updateServiceStatus($id, $status);

                  if ( $service->ServiceModule == "DVR" ) {
                        $service = (array) $service;
                        $service['Action'] = "update";
                        $service['ServiceType'] = $service['ServiceName'];
                        $service['AccountID'] = $service['ServiceID'];
                        DVR::manageDVRStorage($service);
                  }
            }
      }

      public function customerServiceRemove($id) {
            $error = false;
            $service = CustomerServices::getService($id);
            $error = CustomerServices::checkGlobalServiceDependency($service);
            if ( $error ) {
                  echo $error;
            }
            else {
                  CustomerServices::deleteService($service->CustomerID, $id);

                  if ( $service->ServiceModule == "DVR" ) {
                        $service = (array) $service;
                        $service['Action'] = "delete";
                        $service['AccountID'] = $service['ServiceID'];
                        DVR::manageDVRStorage($service);
                  }

                  Session::flash('flash_message', 'Service Deleted successfully.');
                  Session::flash('flash_type', 'alert-success');
            }
      }

}
