<?php
/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
      //
});


App::after(function($request, $response) {
      //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
      if ( Auth::guest() ) {
            if ( Request::ajax() ) {
                  return Response::make('Unauthorized', 401);
            }
            else {
                  return Redirect::guest('login');
            }
      }
});


Route::filter('auth.basic', function() {
      return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
      if ( Auth::check() )
            return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
      if ( Session::token() != Input::get('_token') ) {
            throw new Illuminate\Session\TokenMismatchException;
      }
});


App::error(function(Exception $exception, $httpCode) {
      Log::error($exception);

      if ( Request::is('api/emerald/*') ) {
            return Response::xml(['retcode' => -1, 'message' => $exception->getMessage()]);
      }
      else if ( Request::is('api/epg/*') || Request::is('api/customer/*') || Request::is('api/device/*') ) {
            return Response::json(['success' => false, 'data' => null, 'message' => $exception->getMessage()]);
      }
});

/**
 * Authorizes a user based on customerId and password to provide access to DVR featrues.
 */
Route::filter('dvr_customer_auth', function($route) {
      $customerId = $route->getParameter('customerID');
      $password = $route->getParameter('Password');

      // force password to not be null, so hasDvrBasicService checks it correctly
      $password = $password == null ? '' : $password;

      $errorCode;
      if ( !Customers::checkCustomer($customerId) ) {
            $errorCode = '2001';
      }
      else {
            // returns TRUE if there are no errors, or error code otherwise
            $errorCode = Customers::hasDvrBasicService($customerId, $password);
      }

      if ( $errorCode !== true ) {
            return BaseController::getErrorReturnValue(Messages::getMessage($errorCode), $errorCode);
      }
});

Route::filter('required_settings', function($route) {

      $errorCode = NULL;

      if ( !Setting::get('cm_mode') ) {
            $errorCode = '2023';
      }

      if ( Setting::get('cm_mode') != "cms" ) {
            if ( !Setting::get('radius-server-ip') ) {
                  $errorCode = '2018';
            }
      }

      if ( !Setting::get('nimble_mode') ) {
            $errorCode = '2020';
      }
      else {
            if ( Setting::get('nimble_mode') == "live" ) {
                  if ( !Setting::get('nimble_stream') || !Setting::get('nimble_dvr') ) {
                        $errorCode = '2021';
                  }
            }
            if ( Setting::get('nimble_mode') == "backup" ) {
                  if ( !Setting::get('nimble_stream_backup') || !Setting::get('nimble_dvr_backup') ) {
                        $errorCode = '2022';
                  }
            }
      }

      if ( $errorCode ) {
            return BaseController::getErrorReturnValue(Messages::getMessage($errorCode), $errorCode);
      }
});
