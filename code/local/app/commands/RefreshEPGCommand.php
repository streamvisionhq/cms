<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Collection;

class RefreshEPGCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'sv:refreshepg';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Refreshes the EPG by populating the data from the EPG flat file.';

	/**
	 * File location of the EPG file.
	 * @var string
	 */
	// protected $epgFileLocation = '../epgdata/GetSchedules.xml';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$epgFileLocation = $this->option('file');
		$this->info($epgFileLocation);
		try {
			ini_set('memory_limit', '512M');
			if(file_exists($epgFileLocation)) {
				$file_contents = File::get($epgFileLocation);
				$formatter = Formatter::make($file_contents, Formatter::XML);
				$json = $formatter->toJson();
				$channels = new Collection(json_decode($json)->Channel);
				$epgs = EPG::all()->toArray();
				$this->populateEPGHistory($epgs);
				EPG::truncate();
				$attributes = '@attributes';
				foreach($channels as $channel) {
					$channelAttributes = $channel->$attributes;
					$newEPGChannel = new EPG;
					$shows = (property_exists($channel, 'Show') ? new Collection($channel->Show) : new Collection(array()));
					$newEPGChannel->channelId = (property_exists($channelAttributes, 'channelId') ? (int)$channelAttributes->channelId : 0);
					$newEPGChannel->channelNumber = (property_exists($channelAttributes, 'channelNumber') ? (int)$channelAttributes->channelNumber : 0);
					$newEPGChannel->majorChannelNumber = (property_exists($channelAttributes, 'majorChannelNumber') ? (int)$channelAttributes->majorChannelNumber : 0);
					$newEPGChannel->minorChannelNumber = (property_exists($channelAttributes, 'minorChannelNumber') ? (int)$channelAttributes->minorChannelNumber : 0);
					$newEPGChannel->callsign = (property_exists($channelAttributes, 'callsign') ? $channelAttributes->callsign : '');
					$newEPGChannel->network = (property_exists($channelAttributes, 'network') ? $channelAttributes->network : '');
					$newEPGChannel->broadcastType = (property_exists($channelAttributes, 'broadcastType') ? $channelAttributes->broadcastType : '');
					$newEPGChannel->description = (property_exists($channelAttributes, 'description') ? $channelAttributes->description : '');
					$newEPGChannel->epgdata = $shows->toJson();
					$newEPGChannel->save();
					/*$newEPGChannel = array(
							'channelId' => (property_exists($channelAttributes, 'channelId') ? (int)$channelAttributes->channelId : 0),
							'channelNumber' => (property_exists($channelAttributes, 'channelNumber') ? (int)$channelAttributes->channelNumber : 0),
							'majorChannelNumber' => (property_exists($channelAttributes, 'majorChannelNumber') ? (int)$channelAttributes->majorChannelNumber : 0),
							'minorChannelNumber' => (property_exists($channelAttributes, 'minorChannelNumber') ? (int)$channelAttributes->minorChannelNumber : 0),
							'callsign' => (property_exists($channelAttributes, 'callsign') ? $channelAttributes->callsign : ''),
							'network' => (property_exists($channelAttributes, 'network') ? $channelAttributes->network : ''),
							'broadcastType' => (property_exists($channelAttributes, 'broadcastType') ? $channelAttributes->broadcastType : ''),
							'description' => (property_exists($channelAttributes, 'description') ? $channelAttributes->description : ''),
							'epgdata' => $shows->toJson(),
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
						);
					array_push($epgChannels, $newEPGChannel);*/
				}
				// EPG::insert($epgChannels);
				$this->info("EPG data saved successfully");
			} else {
				$this->error("File not found!");
			}
		}
		catch(Exception $e) {
			$this->error($e);
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	/*protected function getArguments()
	{
		return array(
			array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}*/

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('file', null, InputOption::VALUE_OPTIONAL, 'EPG File Path', './epgdata/GetSchedules.xml'),
		);
	}

	protected function populateEPGHistory($epgs)
	{
		foreach ($epgs as $epg) {
			unset($epg['id']);
			EPGHistory::insert(array($epg));
		}
	}

}
