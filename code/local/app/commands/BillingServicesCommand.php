<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Collection;

class BillingServicesCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sv:billingservices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pushes the data to controll every midnight';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire() {
        // your update function here
        CustomerServices::getAllBillableServices();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return [
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return [
        ];
    }

}
