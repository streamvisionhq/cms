<?php

return array( 
        'service_provider_url' => 'http://staging-control.streamvisiontv.com/api/',
	'reg_code_salt' => '2E78BCF3-99AE-4403-939C-EC97312A4082',
	'redis_cache_expire_interval' => 60 * 60 * 1, // 1 hour
	'redis_channels_key' => 'epg:channels:json',
        'services_category' => array(
            'roku' => 'Roku',
            'dvr' => 'DVR'
        ),
        'roku_services' => array(
            'StreamVisionBasic',
            'StreamVisionBronze',
            'StreamVisionSilver',
            'StreamVisionGold',
            'StreamVisionPlatinum'
        ),
        'dvr_services' => array(
            'basic' => 'DVRBasic', // the DVR service needed for the CMS to report that client has CMS enabled
            'wholeHome' => 'DVRWholeHome',
            'additional' => 'DVRAdditional',
        )
);