<?php

abstract class SubscriptionType extends Enum {

    const StreamVisionSelect = 1;
    const StreamVisionHispanic = 2;
    const StreamVisionChoice = 4;
    const StreamVisionChoicePlus = 8;
    const StreamVisionFreeTV = 16;

    public static function hasAccess($userSubscriptionType, $channelSubscriptionType) {
        if (!self::isValidName($userSubscriptionType)) {
            return false;
        }

        if(!$accessRights = self::get_access_rights($userSubscriptionType)){
            throw new Exception("User subscription type was not found");
        }        

        return self::checkRights($accessRights, $channelSubscriptionType);
    }
    
    /**
     * 
     * @param string $userSubscriptionType
     * @return type
     */
    public static function get_access_rights($userSubscriptionType, $subscriptionName=false) {
        
        $accessRights;
        $subscriptions;
        
        switch (strtolower($userSubscriptionType)) {
            case 'streamvisionselect':
                $accessRights = (self::StreamVisionSelect);
                $subscriptions = array('StreamVisionSelect');
                break;

            case 'streamvisionhispanic':
                $accessRights = (self::StreamVisionHispanic);
                $subscriptions = array('StreamVisionHispanic');
                break;

            case 'streamvisionchoice':
                $accessRights = (self::StreamVisionChoice);
                $subscriptions = array('StreamVisionChoice');
                break;
            
            case 'streamvisionchoiceplus':
                $accessRights = (self::StreamVisionChoicePlus);
                $subscriptions = array('StreamVisionChoicePlus');
                break;                       
            
            case 'streamvisionfreetv':
                $accessRights = (self::StreamVisionFreeTV);
                $subscriptions = array('StreamVisionFreeTV');
                break;                       

            default:
                $accessRights = null;
                $subscriptions = null;
                break;
        }
        
        if($subscriptionName === true){
            return $subscriptions;
        }else{
            return $accessRights;
        }        
    }

    private static function checkRights($userRights, $accessRightString) {
        $subscriptionConst = self::getConstants()[$accessRightString];
        return (($userRights & $subscriptionConst) === $subscriptionConst);
    }

}
