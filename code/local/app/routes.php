<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::get('/', function() {
    return Redirect::to('login');
});

Route::get('login', array('uses' => 'CommonController@showLogin'));
Route::post('login', array('uses' => 'CommonController@doLogin'));
Route::get('logout', array('uses' => 'CommonController@doLogout'));
Route::get('forgot', array('uses' => 'CommonController@showForgot'));
Route::post('forgot', array('uses' => 'CommonController@doForgot'));
Route::get('dashboard', array('before' => 'auth', 'uses' => 'HomeController@getIndex'));
Route::get('settings', array('before' => 'auth', 'uses' => 'UserController@showProfile'));
Route::post('settings', array('before' => 'auth', 'uses' => 'UserController@saveProfile'));
Route::get('settings/dvr', array('before' => 'auth', 'uses' => 'DVRController@settings'));
Route::post('settings/channel', array('before' => 'auth', 'uses' => 'ChannelManagerController@settings'));
Route::get('settings/channel', array('before' => 'auth', 'uses' => 'ChannelManagerController@settings'));
Route::post('settings/dvr', array('before' => 'auth', 'uses' => 'DVRController@settings'));
Route::get('settings/radius', array('before' => 'auth', 'uses' => 'SettingsController@radius'));
Route::post('settings/radius', array('before' => 'auth', 'uses' => 'SettingsController@radius'));
Route::get('settings/cms', array('before' => 'auth', 'uses' => 'SettingsController@cms'));
Route::post('settings/cms', array('before' => 'auth', 'uses' => 'SettingsController@cms'));
Route::get('settings/refresh_epg', array('before' => 'auth', 'uses' => 'SettingsController@refresh_epg'));
Route::get('settings/authentication', array('before' => 'auth', 'uses' => 'SettingsController@authentication'));
Route::post('settings/authentication', array('before' => 'auth', 'uses' => 'SettingsController@authentication'));
Route::get('changepassword', array('before' => 'auth', 'uses' => 'UserController@showChangePassword'));
Route::post('changepassword', array('before' => 'auth', 'uses' => 'UserController@savePassword'));

Route::get('messages', array('before' => 'auth', 'uses' => 'MessagesController@manageMessages'));
Route::get('message/edit/{errorCode}', array('before' => 'auth', 'uses' => 'MessagesController@editMessage'));
Route::post('message/edit/{errorCode}', array('before' => 'auth', 'uses' => 'MessagesController@editMessage'));

Route::get('services', array('before' => 'auth', 'uses' => 'ServicesController@manageServices'));
Route::get('customer/{customerID}/services', array('before' => 'auth', 'uses' => 'ClientServicesController@customerServices'));
Route::get('service/add/', array('before' => 'auth', 'uses' => 'ServicesController@addService'));
Route::post('service/add/', array('before' => 'auth', 'uses' => 'ServicesController@addService'));
Route::get('service/edit/{ID}', array('before' => 'auth', 'uses' => 'ServicesController@editService'));
Route::post('service/edit/{ID}', array('before' => 'auth', 'uses' => 'ServicesController@editService'));

Route::get('update', array('before' => 'auth', 'uses' => 'UpdateController@checkUpdate'));

Route::get('customers', array('before' => 'auth', 'uses' => 'ClientController@getIndex'));
Route::get('add-new-customer', array('before' => 'auth', 'uses' => 'ClientController@addNewClient'));
Route::get('edit-customer/{CustomerID}', array('before' => 'auth', 'uses' => 'ClientController@editClient'));
Route::get('roku-devices', array('before' => 'auth', 'uses' => 'RokuDeviceController@getIndex'));


Route::get('billable-services', array('before' => 'auth', 'uses' => 'BillingController@billableServicesData'));

Route::get('customer/service/change-status/{ID}', array('before' => 'auth', 'uses' => 'ClientServicesController@changeServiceStatus'));
Route::get('customer/service/remove/{ID}', array('before' => 'auth', 'uses' => 'ClientServicesController@customerServiceRemove'));

Route::get('channelmanager', array('before' => 'auth', 'uses' => 'ChannelManagerController@showChannel'));

Route::get('channelmanager/list', array('before' => 'auth', 'uses' => 'ChannelManagerController@showChannels'));

Route::get('channelmanager/addlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@addLiveStream'));

Route::post('channelmanager/addlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@doAddLiveStream'));

Route::get('channelmanager/editlivestream}', array('before' => 'auth', 'uses' => 'ChannelManagerController@modifyLiveStream'));

Route::get('channelmanager/editlivestream/{segments}', 'ChannelManagerController@modifyLiveStream')->where('segments', '(.*)');

Route::post('channelmanager/editlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@doModifyLiveStream'));

Route::get('channelmanager/addcategory', array('before' => 'auth', 'uses' => 'ChannelManagerController@addCategory'));

Route::post('channelmanager/addcategory', array('before' => 'auth', 'uses' => 'ChannelManagerController@doAddCategory'));

Route::get('channelmanager/editcategory/{segments}', array('before' => 'auth', 'uses' => 'ChannelManagerController@modifyCategory'))->where('segments', '(.*)');

Route::post('channelmanager/editcategory', array('before' => 'auth', 'uses' => 'ChannelManagerController@doModifyCategory'));

Route::get('channelmanager/sortchannel', array('before' => 'auth', 'uses' => 'ChannelManagerController@sortChannel'));

Route::post('channelmanager/sortchannel', array('before' => 'auth', 'uses' => 'ChannelManagerController@saveChannelOrder'));

Route::get('channelmanager/sortlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@sortLiveStream'));

Route::post('channelmanager/savestreamorder', array('before' => 'auth', 'uses' => 'ChannelManagerController@saveStreamorder'));

Route::get('channelmanager/addvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@addvideoondemand'));

Route::post('channelmanager/addvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@doAddVideoOnDemand'));

Route::get('channelmanager/editvideoondemand/{segments}', 'ChannelManagerController@modifyVideoOnDemand')->where('segments', '(.*)');

Route::post('channelmanager/editvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@doModifyVideoOnDemand'));

Route::get('channelmanager/removechannel/{segments}', 'ChannelManagerController@removeChannel')->where('segments', '(.*)');

Route::get('channelmanager/getvideos/{segments}', 'ChannelManagerController@showCollection')->where('segments', '(.*)');

Route::get('channelmanager/makeactive/{segments}', 'ChannelManagerController@makeActive')->where('segments', '(.*)');

Route::post('channelmanager/savestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@saveLivestream'));

Route::post('channelmanager/saveserverurl', array('before' => 'auth', 'uses' => 'ChannelManagerController@saveServerUrl'));

Route::get('channelmanager/makeinactive/{segments}', 'ChannelManagerController@makeInactive')->where('segments', '(.*)');

Route::get('medialibrary', array('before' => 'auth', 'uses' => 'MediaLibraryController@showMediaLibrary'));

Route::get('medialibrary/addcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@addCollection'));

Route::post('medialibrary/addcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@doAddCollection'));

Route::get('medialibrary/editcollection/{segments}', 'MediaLibraryController@modifyCollection')->where('segments', '(.*)');

Route::post('medialibrary/editcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@doModifyCollection'));

Route::get('medialibrary/makecollectionactive/{segments}', 'MediaLibraryController@makeCollectionActive')->where('segments', '(.*)');

Route::get('medialibrary/makecollectioninactive/{segments}', 'MediaLibraryController@makeCollectionInactive')->where('segments', '(.*)');

Route::get('medialibrary/removecollection/{segments}', 'MediaLibraryController@removeCollection')->where('segments', '(.*)');

Route::get('medialibrary/sortcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@sortCollection'));

Route::post('medialibrary/savecollectionorder', array('before' => 'auth', 'uses' => 'MediaLibraryController@saveCollectionorder'));

Route::get('medialibrary/addvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@addVideo'));

Route::post('medialibrary/addvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@doAddVideo'));

Route::get('medialibrary/addvideos', array('before' => 'auth', 'uses' => 'MediaLibraryController@addVideos'));

Route::post('medialibrary/addvideos', array('before' => 'auth', 'uses' => 'MediaLibraryController@doAddVideos'));

Route::get('medialibrary/editvideo/{segments}', 'MediaLibraryController@editVideo')->where('segments', '(.*)');

Route::post('medialibrary/editvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@doEditVideo'));

Route::get('medialibrary/listvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@getVideo'));

Route::get('medialibrary/processjob/{segments}', 'MediaLibraryController@processTranscodeJob')->where('segments', '(.*)');

Route::get('medialibrary/removevideo/{segments}', 'MediaLibraryController@removeVideo')->where('segments', '(.*)');

Route::get('medialibrary/makemediaactive/{segments}', 'MediaLibraryController@makeMediaActive')->where('segments', '(.*)');

Route::get('medialibrary/makemediainactive/{segments}', 'MediaLibraryController@makeMediaInactive')->where('segments', '(.*)');

Route::get('medialibrary/getvideocollections/{segments}', 'MediaLibraryController@mediaByCollection')->where('segments', '(.*)');

Route::get('medialibrary/video/{segments}', 'MediaLibraryController@showMedia')->where('segments', '(.*)');

Route::get('medialibrary/addmediatocollection/{segments}', 'MediaLibraryController@addMediaToCollection')->where('segments', '(.*)');

Route::get('medialibrary/removemediafromcollection/{segments}', 'MediaLibraryController@removeMediaFromCollection')->where('segments', '(.*)');

Route::get('medialibrary/searchvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@getSearchVideo'));

Route::get('medialibrary/testmedia', array('before' => 'auth', 'uses' => 'MediaLibraryController@testMedia'));

Route::get('transfer/customers', ['uses' => 'TransferController@customerTransfer']);

Route::get('test', ['uses' => 'TestController@test']);
Route::get('process', ['uses' => 'LogController@process']);

Route::group(array('prefix' => 'cron/'), function() {
    Route::get('dvr/deactivate-dvrs', ['uses' => 'DVRController@deactivateDVR']);
});

Route::group(array('before' => 'required_settings', 'prefix' => 'api/'), function() {
    Route::any('user/{segments}', 'ApiClientController@processChannelApi')->where('segments', '(.*)');

    Route::any('channel/{segments}', 'ApiChannelController@processChannelApi')->where('segments', '(.*)');

    Route::any('medialibrary/{segments}', 'ApiCollectionController@processCollectionApi')->where('segments', '(.*)');

    Route::group(array('prefix' => 'epg'), function() {
        Route::get('/', ['uses' => 'EPGController@getFullEPG']);
        Route::get('channels', ['uses' => 'EPGController@getChannels']);
        Route::get('channels/currentShows', ['uses' => 'EPGController@getCurrentShows']);
        Route::get('channels/{channelNumber}/currentShow', ['uses' => 'EPGController@getCurrentChannelShow']);
    });

    Route::group(array('prefix' => 'epgv2'), function() {
        Route::get('/page/{page}', ['uses' => 'EPGController@getPageViseEPG']);
        Route::get('channels', ['uses' => 'EPGController@getChannels_v2']);
    });

    Route::group(array('prefix' => 'emerald'), function() {
        Route::post('update', ['uses' => 'EmeraldController@update']);
    });

    Route::group(array('prefix' => 'customer'), function() {
        Route::get('/{customerID}/{Password}/devices', ['uses' => 'CustomerController@getRegisteredDevices']);
        Route::post('/{deviceID}/authorize', ['uses' => 'CustomerController@authorizeChannel']);
        Route::get('/refreshEPG', ['protected' => false, 'uses' => 'CustomerController@refreshEPG']);
    });

    Route::group(array('prefix' => 'device'), function() {
        Route::get('/{customerID}/{deviceID}/{channelID}/keepalive', ['uses' => 'DeviceController@keepAlive']);
        Route::get('/{customerID}/{Password}/{deviceID}/{Identifier}/account', ['uses' => 'DeviceController@deviceAuthentication']);
        Route::get('/{customerID}/{Password}/{deviceID}/delete', ['uses' => 'DeviceController@deleteDevice']);
    });

    Route::group(array('prefix' => 'dvr'), function() {
        Route::post('/{customerID}/{Password}/record', ['before' => 'dvr_customer_auth', 'uses' => 'DVRController@record']);
        Route::post('/{customerID}/{Password}/delete', ['before' => 'dvr_customer_auth', 'uses' => 'DVRController@delete']);
        Route::post('/{customerID}/{Password}/update', ['before' => 'dvr_customer_auth', 'uses' => 'DVRController@update']);
        Route::post('/{customerID}/{Password}/playlist', ['before' => 'dvr_customer_auth', 'uses' => 'DVRController@playlist']);
        Route::post('/{customerID}/{Password}/storage', ['before' => 'dvr_customer_auth', 'uses' => 'DVRController@storage']);
        Route::post('/getEpg', ['uses' => 'DVRController@getEpg']);
        Route::post('/getEpgv2/page/{page}', ['uses' => 'DVRController@getEpgv2']);
        Route::get('/{customerID}/{deviceID}/{eventID}/authorize', ['uses' => 'DVRController@authorizeRecordedStream']);
        Route::get('refresh', ['uses' => 'DVRController@refreshChannels']);
    });
    
});

Route::filter('auth', function() {
    if (is_null(Auth::User())) {
        return Redirect::to('login');
    }
});
