@extends('layouts.master')

@section('js')
<script src="http://malsup.github.com/jquery.form.js"></script>
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Edit Video</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'medialibrary/editvideo', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmmedia')) }}
                {{ Form::hidden('media_id', $media_id) }}
                {{ Form::hidden('job_id', $job_id) }}
                {{ Form::hidden('poster_id', $poster_id) }}
                {{ Form::hidden('poster1_img', $poster1_img) }}
                {{ Form::hidden('poster2_img', $poster2_img) }}
                {{ Form::hidden('poster3_img', $poster3_img) }}
                {{ Form::hidden('modified_by', Auth::id()) }}
                <div class="form-group">
                    {{ Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('name', $name,  array('placeholder'=>'Please enter Name', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_name"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('media_name', 'Browse Media', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::file('media_name', $attributes = array()) }}
                        <span class="error-display" id="error_mediafile"></span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ HTML::image(getCdnPath().$media_image, '',  array( 'width' => 120, 'height' => 80, 'style' => 'float:left;') ) }}
                    </div>
                </div>
                <div class="form-group progressfrm">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <div id="progress">
                            <div id="bar"></div>
                            <div id="percent">0%</div >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <span style="float:left; text-align:left;">
                            Browse, select, and upload your video file using the button provided below. <br/>NOTICE: You're limited to a maximum upload filesize of 2 GB and video file type must be of mp4.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('add_poster1', 'Add Poster', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <div class="div_posters">
                            {{ HTML::image(getCdnPath().$poster1_img, '',  array( 'width' => 269, 'height' => 152) ) }}
                        </div>

                        <div class="div_posters">
                            {{ HTML::image(getCdnPath().$poster2_img, '',  array( 'width' => 142, 'height' => 202) ) }}
                        </div>

                        <div class="div_posters">
                            {{ HTML::image(getCdnPath().$poster3_img, '',  array( 'width' => 209, 'height' => 209) ) }}
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <div class="div_posters">
                            {{ Form::file('add_poster1', $attributes = array()) }}
                            <span>NOTICE: Please upload file type must be of jpg and 4X3 format.</span>
                            <span class="error-display" id="error_poster1"></span>
                        </div>

                        <div class="div_posters">
                            {{ Form::file('add_poster2', $attributes = array()) }}
                            <span>NOTICE: Please upload file type must be of jpg and portrait format.</span>
                            <span class="error-display" id="error_poster2"></span>
                        </div>

                        <div class="div_posters">
                            {{ Form::file('add_poster3', $attributes = array()) }}
                            <span>NOTICE: Please upload file type must be of jpg and square format.</span>
                            <span class="error-display" id="error_poster3"></span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('meta_tag', 'Meta Tag', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('meta_tag', $meta_tag, array('placeholder'=>'Please enter meta tag', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_metatag"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('meta_description', 'Meta Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('meta_description', $meta_description, array('placeholder'=>'Please enter meta description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_metadesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('descriptions', 'Descriptions', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('descriptions', $descriptions, array('placeholder'=>'Please enter descriptions', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('frame_width', 'Frame Size', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('frame_width', $frame_width,  array('placeholder'=>'W', 'class'=>'form-control w8 ')) }}

                        {{ Form::text('frame_height', $frame_height,  array('placeholder'=>'H', 'class'=>'form-control w8 rtxt')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('file_size', 'File Size', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('file_size', $file_size,  array('placeholder'=>'Please enter file size', 'class'=>'form-control w15')) }}
                        in bytes
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('duration', 'Video Duration', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('duration', $job_duration,  array('placeholder'=>'Please enter duration of video', 'class'=>'form-control w15')) }}
                        in milliseconds
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('contents', 'Content', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('contents', $content, array('placeholder'=>'Please enter content', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('genre', 'Genre', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('genre', $genre, array('placeholder'=>'Please enter genre', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('rating', 'Rating', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <select name="rating" id="rating" class="selectbox w8">
                            @for ($i = 1; $i <= 5; $i += 0.5)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('rights', 'Rights', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('rights', $rights,  array('placeholder'=>'Please enter rights', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('release_date', 'Release Date', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('release_date', $release_date,  array('placeholder'=>'Please enter release date', 'class'=>'form-control', 'id'=>'release_date')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('show_description', 'Show Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('show_description', $show_description, array('placeholder'=>'Please enter show description', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('closed_captions', 'Closed Captions', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('closed_captions', $closed_captions, array('placeholder'=>'Please enter closed captions', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('generic_tags', 'Generic Tags', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('generic_tags', $generic_tags, array('placeholder'=>'Please enter generic tags', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('similar_content', 'Similar Content', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('similar_content', $similar_content, array('placeholder'=>'Please enter similar content', 'class'=>'form-control')) }}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'editmedia')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_videolibrary(1)" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</div>
<style type="text/css">
    .progressfrm { display:none; }
    #progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; display:none; }
    #bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px;  }
    #percent { position:absolute; display:inline-block; top:3px; left:48%; color:#821c34; }
</style>

<script type="text/javascript">


      $("#release_date").datepicker({ dateFormat: 'yy-mm-dd' });
      $(document).ready(function() {

      var options = {
      beforeSend: function()
      {
      $(".error-display").html("");
      $("#error_mediafile").html("");
      window.scrollTo(0, 0);
      showOverlay();
      },
              uploadProgress: function(event, position, total, percentComplete)
              {
              $(".progressfrm").show();
              $("#progress").show();
              $("#bar").width('0%');
              $("#percent").html("0%");
              $("#bar").width(percentComplete + '%');
              $("#percent").html(percentComplete + '%');
              },
              success: function()
              {
              $("#bar").width('100%');
              $("#percent").html('100%');
              $(".progressfrm").hide().delay(2000);
              $("#progress").hide().delay(2000);
              },
              complete: function(response)
              {
              var response = jQuery.parseJSON(response.responseText);
              if (response['validation_failed'] == 1) {
              if (response['errors']['name']) { $("#error_name").html(response['errors']['name']); }

              if (response['errors']['meta_tag']) { $("#error_metatag").html(response['errors']['meta_tag']); }

              if (response['errors']['meta_description']) { $("#error_metadesc").html(response['errors']['meta_description']); }

              if (response['errors']['media_name']) { $("#error_mediafile").html(response['errors']['media_name']); }

              if (response['errors']['poster_1']) { $("#error_poster1").html(response['errors']['poster_1']); }

              if (response['errors']['poster_2']) { $("#error_poster2").html(response['errors']['poster_2']); }

              if (response['errors']['poster_3']) { $("#error_poster3").html(response['errors']['poster_3']); }

              }

              if (response['success'] == 1) {
              $(".error-display").html('');
              $("#frm_success").html(response['suc_msg']);
              $("#frm_success").show();
              $("input[type=submit]").attr('disabled', 'disabled');
              call_videolibrary({{ $curPage }}).delay(3000);
              }
              hideOverlay();
              },
              error: function()
              {
              $("#error_mediafile").html("<font color='red'> ERROR: unable to upload files</font>");
              hideOverlay();
              }

      };
      $("#frmmedia").ajaxForm(options);
      });

</script>
@stop

