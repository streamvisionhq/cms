<li> 
    @if(count($medias) > 0)
    <?php
    $links = $medias->count();
    $cpage = $medias->getCurrentPage();
    $last = ceil($medias->getTotal() / 10);
    $start = ( ( $cpage - $links ) > 0 ) ? $cpage - $links : 1;
    $end = ( ( $cpage + $links ) < $last ) ? $cpage + $links : $last;
    ?>
    <ul class="pagination pagination-lg">
        @for ($i = $start; $i <= $end; $i++)
        @if($i == $cpage)
        <li class="active"><a href="javascript:void(0)">{{$i}}</a></li>
        @else
        <li><a href="javascript:void(0)" onClick="call_showvideocollection('{{$collection_id}}', 'tab-{{$collection_id}}', '{{$collection_status}}', '{{$i}}');">{{$i}}</a></li>
        @endif
        @endfor

        <span>{{$medias->getFrom()}} -  {{$medias->getTo()}} of {{$medias->getTotal()}}</span>
    </ul>
    @endif
    @if($collection_id > 1)
    <div class="lft_img">
        <a data-original-title="Edit" href="{{ URL::to('medialibrary/editcollection/'.$collection_id) }}">{{ HTML::image('assets/images/vedio_edit.png', '', array('title'=>'edit collection')) }}</a>						
        <a data-original-title="Delete" alt="remove collection" href="javascript:void(0);" onClick="call_removecollection('{{$collection_id}}');">{{ HTML::image('assets/images/vedio_delete.png', '', array('title'=>'remove collection')) }}</a>

        @if($collection_status == 1)
        <a data-original-title="Active Channel" alt="make channel inactive" href="javascript:void(0)"  href="javascript:void(0);" onClick="collection_changestatus('{{$collection_id}}', 'inactive');">{{ HTML::image('assets/images/active.png', '', array('title'=>'make channel inactive')) }}</a>
        @else
        <a data-original-title="Inactive Channel" alt="make channel active"  href="javascript:void(0)" onClick="collection_changestatus('{{$collection_id}}', 'active');">{{ HTML::image('assets/images/inactive.png', '', array('title'=>'make channel active')) }}</a>
        @endif
    </div>
    @endif
</li>
@foreach($medias as $media)
<li>
    <span class="vedio_lft">
        <a  href="{{ URL::to('medialibrary/video/'.$media->media_id) }}" target="_blank">
            {{ HTML::image(getCdnPath().$media->image_name, '',  array( 'width' => 95, 'height' => 53) ) }}
        </a></span>
    <span class="vedio_rft">
        <h6>{{ $media->name }}</h6> 
        <p>&nbsp;</p>
        <a href="javascript:void(0);" onClick="remove_mediafromcollection('{{$collection_id}}', '{{$media->media_id}}', '{{$media->id}}')">Remove from collection</a>
    </span>
</li>
@endforeach