@foreach($lvideos as $lvideo)
<li id="vd_{{ $lvideo->media_id }}">
    <span class="vedio_lft">
        <a  href="{{ URL::to('medialibrary/video/'.$lvideo->media_id) }}" target="_blank">
            {{ HTML::image(getCdnPath().$lvideo->media_image, '',  array( 'width' => 95, 'height' => 52) ) }}
        </a>
    </span>
    <span class="vedio_rft">
        <h6>{{ $lvideo->name }}</h6> 
    </span>
</li>
@endforeach	

@if(sizeof($lvideos) > 0)
<p class="load_more" id="load_more"><a href="javascript:void(0);" onClick="call_listsearchmedia('{{$term}}', '{{$page+1}}')">load more</a></p>
@endif