@extends('layouts.master')

@section('content')
<span class="paginate" style="display:none;"><a href="javascript:void(0)"></a></span>
<div class="content_container">
    <div class="panel panel-default">
        <div id="livestream">
            <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/sortcollection') }}'">Sort Collections</button>
            &nbsp;&nbsp;		<button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/addcollection') }}'">Add Collection</button>
            &nbsp;&nbsp;
            <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/listvideo') }}'">Manage Video</button>
        </div>
    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="panel panel-default">	  
        <div class="panel-heading"><h3 class="panel-title">Media Library</h3></div>
        <div class="panel-body doc_c">
            <div id="accordion">

                @foreach($collections as $collection)

                <h3 onClick="call_showvideocollection('{{$collection->collection_id}}', 'tab-{{$collection->collection_id}}', '{{$collection->status}}', '1');">{{$collection->collection_name}}</h3>
                <div>
                    <div class="bottom_slide droppable" id="dp_{{$collection->collection_id}}">
                        <p style="border:dashed 2px #ccc; text-align:center; padding:10px 0px;">Drop here to add to collection</p>
                        <input type="hidden" id="status-{{$collection->collection_id}}" value="{{$collection->status}}" />

                        <ul class="vedio_cnt">

                            <span  id="tab-{{$collection->collection_id}}"></span>
                        </ul>
                    </div>
                </div>
                @endforeach

                <h3  onClick="call_showvideocollection('1', 'tab-1', '1', '1');">Uncategorized</h3>
                <div>
                    <div class="bottom_slide droppable" id="dp_1">
                        <p style="border:dashed 2px #ccc; text-align:center; padding:10px 0px;">Drop here to add to collection</p>
                        <input type="hidden" id="status-1" value="1" />
                        <ul class="vedio_cnt">

                            <span  id="tab-1"></span>
                        </ul>
                    </div>
                </div>
            </div>			      

            <div class="detail_cnt">
                <div class="detail_cnt_top">
                    <h3>Video Keyword Search</h3>

                    <p>Search: Video Title</p>
                    <input type="text" name="search_term" id="search_term" value="" />
                    <input type="hidden" name="search_page" id="search_page" value="1" />
                    <input type="button" value="Search" onClick = "call_searchvideo()">
                </div>

                <div class="detail_cnt_btm" id="list_searchmedia">
                    <label>Videos</label>   
                    <ul class="vedio_cnt_nxt" id="draggable">
                        <p class="loading" id="loading">loading</p>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="dialog-status" title="Media Library" style="display:none;">
    <p></p>
</div>
<style type="text/css">
    #accordion { width:800px; }

    #accordion div { background:#131313; color:#fff;}
</style>
<script type="text/javascript">
      $(function() {
      $('#draggable').find('li').draggable({
      helper: 'clone'
      });
      $(".droppable").droppable({
      drop: function(event, ui) {
      var dragelm = ui.draggable.attr("id");
      var dropelm = $(this).attr('id');
      dgelm = dragelm.split("_");
      dpelm = dropelm.split("_");
      call_mediatocollection(dgelm[1], dpelm[1]);
      }
      });
      var icons = {
      header: "ui-icon-folder-collapsed",
              activeHeader: "ui-icon-folder-open"
      };
      $("#accordion").accordion({ icons: icons });
      });
      var call_searchvideo = function() {
      var sterm = $("#search_term").val();
      var spage = $("#search_page").val();
      call_listsearchmedia(sterm, spage);
      }

      $('#search_term').keypress(function(event){

      var keycode = (event.keyCode ? event.keyCode : event.which);
      if (keycode == '13'){
      var sterm = $("#search_term").val();
      var spage = $("#search_page").val();
      call_listsearchmedia(sterm, spage);
      }
      });
      var call_listsearchmedia = function(x, y) {
      var url = "{{ URL::to('medialibrary/searchvideo') }}" + "?term=" + x + "&page=" + y;
      if (y > 1) {
      $("#load_more").remove();
      $("#list_searchmedia ul").append('<p class="loading" id="loading">loading...</p>');
      } else {
      $("#list_searchmedia ul").html('<p class="load_more" id="load_more">loading...</p>');
      }

      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'html',
              beforeSend: function(){

              },
              success: function(data) {
              if (y > 1) {
              $("#loading").remove();
              $("#list_searchmedia ul").append(data);
              } else {
              $("#list_searchmedia ul").html(data);
              }
              $("#search_term").val('');
              $('#draggable').find('li').draggable({
              helper: 'clone'
              });
              }
      });
      }

      var call_mediatocollection = function(x, y) {
      var url = "{{ URL::to('medialibrary/addmediatocollection') }}/" + y + "/" + x;
      $("#dialog-status p").html("The selected media added to collection.");
      var tab_id = "tab-" + y;
      var status_id = "status-" + y;
      var status = $(status_id).val();
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      //call_medialibrary();
                      call_showvideocollection(y, tab_id, status, '1')
                      }
                      }
              });
              }
              }
      });
      }

      var remove_mediafromcollection = function(x, y, z) {
      var r = confirm("Do you want to remove this video from this collection");
      var url = "{{ URL::to('medialibrary/removemediafromcollection') }}/" + z;
      $("#dialog-status p").html("The selected media was removed from collection.");
      var tab_id = "tab-" + x;
      var status_id = "#status-" + x;
      var status = $(status_id).val();
      if (r == true) {
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_showvideocollection(x, tab_id, status, '1')
                      }
                      }
              });
              }
              }
      });
      }
      }

      var collection_changestatus = function(x, y) {
      if (y == 'active') {
      var url = "{{ URL::to('medialibrary/makecollectionactive') }}/" + x;
      $("#dialog-status p").html("The selected collection was make active.");
      var status_val = 1;
      } else {
      var url = "{{ URL::to('medialibrary/makecollectioninactive') }}/" + x;
      $("#dialog-status p").html("The selected collection was make inactive.");
      var status_val = 2;
      }
      var tab_id = "tab-" + x;
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_showvideocollection(x, tab_id, status_val, '1')
                      }
                      }
              });
              }
              }
      });
      }

      var call_removecollection = function(x) {
      var url = "{{ URL::to('medialibrary/removecollection') }}/" + x;
      var r = confirm("Do you want to remove this collection");
      $("#dialog-status p").html("The selected collection was removed successfully.");
      if (r == true) {
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_medialibrary().delay(3000);
                      }
                      }
              });
              }
              }
      });
      }
      }

      var call_showvideocollection = function(x, y, z, pg) {
      if (x > 0) {
      var url = "{{ URL::to('medialibrary/getvideocollections') }}/" + x + "/" + z + "?page=" + pg;
      } else {
      var url = "{{ URL::to('medialibrary/getvideocollections') }}/1/" + z + "?page=" + pg;
      }

      var attr_id = "#" + y;
      if (parseInt(x) > 0) {
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'html',
              beforeSend: function(){
              $(attr_id).html('<p class="loadmlibrary">loading....</p>');
              },
              success: function(data) {
              $(attr_id).html(data);
              }
      });
      }
      }

      @if ($collect_id > 0)
              call_showvideocollection('{{ $collect_id }}', 'tab-{{ $collect_id }}', '{{ $collect_status }}', '1')
              @ else
              call_showvideocollection('1', 'tab-1', '1', '1')
              @endif

              call_listsearchmedia('', 1)
</script>
@stop