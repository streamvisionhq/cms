@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div id="livestream">
            <button class="btn btn-primary" type="button" >
                Live Stream <span class="badge">Good</span>
            </button>
        </div>
    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Dashboard</h3>
        </div>
        <div class="panel-body detail_cnt">

            <div class="well">
                <h2>Top Ten Watched Channels</h2>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Show Name</th>
                            <th>Monthly Minutes</th>
                            <th>Date Added</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1472</td>
                            <td>Attack of the Clones</td>
                            <td>11,200</td>
                            <td>01/04/2012</td>
                        </tr>
                        <tr>
                            <td>2831</td>
                            <td>Revenge of the Sith</td>
                            <td>9,010</td>
                            <td>01/04/2012</td>
                        </tr>
                        <tr>
                            <td>3832</td>
                            <td>A New Hope</td>
                            <td>8,010</td>
                            <td>01/04/2012</td>
                        </tr>
                        <tr>
                            <td>13832</td>
                            <td>Empire Strikes Back</td>
                            <td>7,010</td>
                            <td>01/04/2012</td>
                        </tr>
                        <tr>
                            <td>836</td>
                            <td>Return of the Jedi</td>
                            <td>6,093</td>
                            <td>01/04/2012</td>
                        </tr>
                        <tr>
                            <td>7463</td>
                            <td>The Force Awakens</td>
                            <td>5,093</td>
                            <td>12/08/2012</td>
                        </tr>
                        <tr>
                            <td>9203</td>
                            <td>The Fellowship of the Ring</td>
                            <td>4,093</td>
                            <td>12/1/2012</td>
                        </tr>
                        <tr>
                            <td>8394</td>
                            <td>The Two Towers</td>
                            <td>3,093</td>
                            <td>09/1/2012</td>
                        </tr>
                        <tr>
                            <td>093</td>
                            <td>Return of the King</td>
                            <td>2,093</td>
                            <td>09/12/2012</td>
                        </tr>
                        <tr>
                            <td>2093</td>
                            <td>The Hobbit</td>
                            <td>1,093</td>
                            <td>09/12/2012</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="well_bottom">

                <!-- Start of row -->
                <div class="row box_cnt">
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Total live channels </h3>            
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">121</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Total VOD channels </h3>           
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">17</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Total Subscribers  </h3>    
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">1,941</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of row -->

                <!-- Start of row -->
                <div class="row box_cnt">
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Total Watching Now </h3>            
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">320</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Daily Minutes Watched</h3>           
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">120,301</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Monthly Minutes Watched</h3>    
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">1,742,030</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of row -->

                <!-- Start of row -->
                <div class="row box_cnt">
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Daily Average of Minutes Watched </h3>            
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">93,472</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Weekly Average of Minutes Watched </h3>           
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">540,283</a> </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">          
                            <div class="caption caption_box">
                                <h3>Monthly Average of Minutes Watched </h3>    
                                <p class="caption_ptg"><a role="button" class="btn btn-primary number_btns" href="javascript:void(0);">210,392,012</a> </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of row -->

            </div>

        </div>
    </div>
</div>
@stop