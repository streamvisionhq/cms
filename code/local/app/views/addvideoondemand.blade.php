@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Channel Manager</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Add Video on Demand Playlist</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'channelmanager/addvideoondemand', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmvideoondemand')) }}
                {{ Form::hidden('channel_type', '2') }}
                {{ Form::hidden('created_by', Auth::id()) }}
                {{ Form::hidden('trailers', '', array('id'=>'trailers')) }}
                {{ Form::hidden('parent_id', $parent_id) }}
                <div class="form-group">
                    {{ Form::label('title', 'Title', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('title', '',  array('placeholder'=>'Please enter title', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_title"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('short_description', 'Short Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('short_description',   '',array('placeholder'=>'Please enter short description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_sdesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('long_description', 'Long Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('long_description',   '',array('placeholder'=>'Please enter long description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_ldesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('channel_logo', 'Channel Logo', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::file('channel_logo', $attributes = array()) }}
                        <span class="error-display" id="error_clogo"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('associate_trailer', 'Add Media', array('class' => 'col-sm-2 control-label')) }}
                    <br/>
                    <table width="800" border="0" class="tabl_vedio">
                        <tr>
                            <td width="400" valign="top">
                                <h3 class="fav">Media Library</h3>
                                <select name="collection" id="collection" onchange="call_showcollection(this.value, 1, 1)">
                                    @foreach($collections as $collection)
                                    <option value="{{$collection->collection_id}}">{{$collection->collection_name}}</option>
                                    @endforeach
                                </select>
                                <div id="drags">
                                    <ul id="draggable" style="height:400px; overflow-y:scroll;">
                                        @foreach($videos as $video)
                                        <li class="ui-state-default">{{ HTML::image(getCdnPath().$video->media_image, '',  array( 'width' => 50) ) }}<h3>{{$video->name}}</h3><span class="sr-{{$video->media_id}}"></span></li>
                                        @endforeach
                                        @if(sizeof($videos) > 0)
                                        <p class="load_more" id="load_more"><a href="javascript:void(0);" onClick="call_showcollection('1', '2', 2)">load more</a></p>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                            <td width="50">
                                {{ HTML::image('assets/images/arr.png', '',  array( 'width' => 20) ) }}
                            </td>
                            <td width="400" valign="top">
                                <h3 class="fav">Your Video on Demand Playlist</h3>
                                <ul  id="sortable" style="height:450px !important; overflow-y:scroll;">
                                </ul>
                            </td>
                    </table>

                    <span class="error-display trail" id="error_trailers"></span>
                </div>		


                <div class="form-group">
                    {{ Form::label('pre_roll', 'Pre Rolls', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <span style="float:left">
                            {{ Form::radio('pre_roll', 1, true); }}
                            On
                            {{ Form::radio('pre_roll', 2); }}
                            Off
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'addvideoondemand')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_channelmanager('25', 'sort_order', 'asc', '1', '2')" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>


</div>
<style type="text/css">
    ul #draggable, #sortable {
        list-style-type: none;
        width: 100%;
    }

    #draggable h3 {
        font: bold 12px Helvetica, Verdana, sans-serif;
    }

    #draggable li img {
        float: left;
        margin: 0 0 0 0;
    }

    #draggable li p {
        font: 200 12px/1.5 Georgia, Times New Roman, serif;
    }

    #draggable li {
        padding: 10px;
        overflow: auto;
        border-bottom:solid 1px #ccc;
    }

    #draggable li:hover {
        background: #eee;
        cursor: pointer;
    }

    #sortable h3 {
        font: bold 12px Helvetica, Verdana, sans-serif;
    }

    #sortable li img {
        float: left;
        margin: 0 15px 0 0;
    }

    #sortable li p {
        font: 200 12px/1.5 Georgia, Times New Roman, serif;
    }

    #sortable li {
        padding: 10px;
        overflow: auto;
        border-bottom:solid 1px #ccc;
    }

    #sortable li:hover {
        background: #eee;
        cursor: pointer;
    }
</style>

<script type="text/javascript">
      $("#draggable > li").draggable({
          connectToSortable: "#sortable",
          cursor: "crosshair",
          helper: function (event) {
              var id = $(this).attr('id');
              var ret = $(this).clone();
              ret.attr('dragId', id);
              return ret;
          }
      });

      $("#sortable").sortable({
          placeholder: "ui-state-highlight",
          start: function (event, ui) {

          },
          stop: function (event, ui) {
              call_newlist();
          }
      });

      var removeelem = function (x) {
          var elm = "#" + x;
          $(elm).parent().remove();
      }

      var call_newlist = function () {
          var i = 1;
          var srt = "sort-";
          var slen = $("#sortable li").length;
          var sortarr = [];
          $(".minibox").remove();
          $("#sortable li").each(function () {
              var spl = $(this).children('span').attr("class").split("-");
              var s = srt + i;
              $(this).append('<a href="javascript:void(0);" class="minibox" >{{ HTML::image('assets / images / delete_list.png', '',  array( 'width' => 20) ) }}</a>');
              $(this).children('a.minibox').attr("id", s);
              $(this).children('a.minibox').attr("onClick", "removeelem('" + s + "')");
              sortarr.push(spl[1]);
              i++;
          });

          var medialist = sortarr.join();
          $("#trailers").val(medialist);
          $("#sortable").effect("highlight", {color: '#303030'}, 3000);
      }

      var call_showcollection = function (x, y, z) {
          var url = "{{ URL::to('channelmanager/getvideos') }}/" + x + "?page=" + y;

          if (z == 1) {
              $("#drags li").remove();
          }

          $("#load_more").remove();
          $("#drags ul").append('<p class="loading" id="loading">loading...</p>');

          $.ajax({
              type: 'GET',
              url: url,
              dataType: 'html',
              beforeSend: function () {

              },
              success: function (data) {
                  $("#loading").remove();

                  if (y > 1) {

                      $("#drags ul").append(data);
                  } else {
                      $("#drags ul").append(data);
                  }

                  $("#draggable > li").draggable({
                      connectToSortable: "#sortable",
                      helper: function (event) {
                          var id = $(this).attr('id');
                          var ret = $(this).clone();
                          ret.attr('dragId', id);
                          return ret;
                      }
                  });
              }
          });
      }

      $(document).ready(function () {
          $("#addvideoondemand").click(function () {

              showOverlay();
              $(".error-display").html("");
              $("#frmvideoondemand").submit(function (event)
              {
                  var postData = $(this).serializeArray();
                  var formURL = $(this).attr("action");
                  var formData = new FormData(this);
                  $.ajax(
                          {
                              url: formURL,
                              type: 'POST',
                              data: formData,
                              mimeType: "multipart/form-data",
                              contentType: false,
                              cache: false,
                              processData: false,
                              success: function (data, textStatus, jqXHR)
                              {
                                  var response = JSON.parse(data);

                                  if (response['validation_failed'] == 1) {
                                      if (response['errors']['title']) {
                                          $("#error_title").html(response['errors']['title']);
                                      }

                                      if (response['errors']['short_description']) {
                                          $("#error_sdesc").html(response['errors']['short_description']);
                                      }

                                      if (response['errors']['long_description']) {
                                          $("#error_ldesc").html(response['errors']['long_description']);
                                      }

                                      if (response['errors']['trailers']) {
                                          $("#error_trailers").html(response['errors']['trailers']);
                                      }

                                      if (response['errors']['channel_logo']) {
                                          $("#error_clogo").html(response['errors']['channel_logo']);
                                      }

                                  }

                                  if (response['success'] == 1) {
                                      $(".error-display").html('');
                                      $("#frm_success").html('Video on demand created successfully.');
                                      $("#frm_success").show();
                                      $("input[type=submit]").attr('disabled', 'disabled');
                                      call_channelmanager('25', 'sort_order', 'asc', '1', '2').delay(3000);
                                  }

                                  hideOverlay();
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $("#frm_error").html('AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '');
                                  $("#frm_error").show();
                              }
                          });
                  event.preventDefault();	//STOP default action
                  event.unbind();
              });
          });
      });
</script>
@stop

