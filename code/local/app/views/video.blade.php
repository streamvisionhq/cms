@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div id="livestream">
            <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/addvideos') }}'">Upload Videos</button>
            &nbsp;&nbsp;
            <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/addvideo') }}'">Upload Video</button>
        </div>
    </div>
    <br clear="all" />
    <br clear="all" />

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>

        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Videos</a></li>
            </ul>
            <div class="well">
                <div><?php echo $videos->links(); ?></div>
                <div class="video-gallery">
                    <ul>
                        @foreach($videos as $video)
                        <li>
                            <div class="video-gallery-thumb">
                                @if($video->job_status_id == 4 )
                                <a  href="{{ URL::to('medialibrary/video/'.$video->media_id) }}" target="_blank">
                                    @else
                                    <a  href="javascript:void(0);">
                                        @endif
                                        {{ HTML::image(getCdnPath().$video->media_image, '',  array( 'width' => 254, 'height' => 165) ) }}
                                    </a>
                                    <div class="video-btm-txt">
                                        <div class="video-btm-txt-first">
                                            <div class="album-txt">{{$video->name}}</div>
                                            <div class="album-dur">
                                                <?php echo ($video->job_duration > 0) ? gmdate("H:i:s", ($video->job_duration * 0.001)) : "00:00:00"; ?>
                                            </div>
                                            <div class="videotxt"></div>
                                        </div>
                                    </div>
                            </div>
                            <!---------btm-------->
                            <div class="clr"></div>
                            <div class="video-status">
                                <div class="video-stat-lft">
                                    <div class="stat-txt">Status :</div>
                                    <div class="stat-compen" style="color:{{$video->job_status_color}};">
                                        {{$video->job_status_desc}}

                                    </div>
                                </div>
                                <div class="video-icon-rgt" >
                                    @if($video->job_status_id == 4 ) 
                                    <a data-original-title="Edit" href="{{ URL::to('medialibrary/editvideo/'.$video->media_id) }}?cur_page={{ $curPage }}">{{ HTML::image('assets/images/edit.png', '', array('title'=>'edit channel')) }}</a>

                                    <a data-original-title="Delete" alt="delete channel" href="javascript:void(0);" onClick="call_removemedia('{{$video->media_id}}', '{{$video->job_id}}');">{{ HTML::image('assets/images/delete.png', '', array('title'=>'remove channel')) }}</a>

                                    @if($video->status == 1)
                                    <a data-original-title="Active Channel" alt="make channel active" href="javascript:void(0)"  href="javascript:void(0);" onClick="media_changestatus('{{$video->media_id}}', 'inactive', '{{ $curPage }}');">{{ HTML::image('assets/images/active.png', '', array('title'=>'make channel active')) }}</a>
                                    @else
                                    <a data-original-title="Inactive Channel" alt="make channel inactive"  href="javascript:void(0)" onClick="media_changestatus('{{$video->media_id}}', 'active', '{{ $curPage }}');">{{ HTML::image('assets/images/inactive.png', '', array('title'=>'make channel inactive')) }}</a>
                                    @endif
                                    @elseif($video->job_status_id == 5)
                                    <a data-original-title="Delete" alt="delete channel" href="javascript:void(0);" onClick="call_removemedia('{{$video->media_id}}', '{{$video->job_id}}');">{{ HTML::image('assets/images/delete.png', '', array('title'=>'remove channel')) }}</a>
                                    @else
                                    <a data-original-title="Edit" href="javascript:void(0);" onClick="call_processjob('{{$video->media_id}}', '{{$video->job_id}}', '{{ $curPage }}');" style="color:#1aa8b4; ">Request Panda</a></div>
                                @endif

                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div><?php echo $videos->links(); ?></div>
            </div>
        </div>
    </div>
</div>
<div id="dialog-status" title="Videos" style="display:none;">
    <p></p>
</div>	
<script type="text/javascript">
      var call_processjob = function(x, y, z) {
      var url = "{{ URL::to('medialibrary/processjob/') }}/" + x + "/" + y;
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              $("#dialog-status p").html(data['suc_data']);
              hideOverlay();
              $("#dialog-status").dialog({

              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_videolibrary(z).delay(3000);
                      }
                      }
              });
              }

              if (data['error']) {
              $("#dialog-status p").html(data['err_data']);
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_videolibrary().delay(3000);
                      }
                      }
              });
              }
              }
      });
      }

      var media_changestatus = function(x, y, z) {
      if (y == 'active') {
      var url = "{{ URL::to('medialibrary/makemediaactive') }}/" + x;
      $("#dialog-status p").html("The selected video was make active.");
      } else {
      var url = "{{ URL::to('medialibrary/makemediainactive') }}/" + x;
      $("#dialog-status p").html("The selected video was make inactive.");
      }

      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_videolibrary(z).delay(3000);
                      }
                      }
              });
              }
              }
      });
      }

      var call_removemedia = function(x, y) {
      var url = "{{ URL::to('medialibrary/removevideo') }}/" + x + "/" + y;
      var r = confirm("Do you want to remove this video");
      $("#dialog-status p").html("The selected video was removed successfully.");
      if (r == true) {
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_videolibrary(1).delay(3000);
                      }
                      }
              });
              }
              }
      });
      }
      }
</script>
@stop

