@extends('layouts.master')

@section('js')
<script src="http://malsup.github.com/jquery.form.js"></script>
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Add Video</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'medialibrary/addvideo', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmmedia')) }}
                {{ Form::hidden('created_by', Auth::id()) }}
                <div class="form-group">
                    {{ Form::label('name', 'Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('name', '',  array('placeholder'=>'Please enter Name', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_name"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('media_name', 'Browse Media', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::file('media_name', $attributes = array()) }}
                        <span class="error-display" id="error_mediafile"></span>
                    </div>
                </div>
                <div class="form-group progressfrm">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <div id="progress">
                            <div id="bar"></div>
                            <div id="percent">0%</div >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('', '&nbsp;', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <span style="float:left; text-align:left;">
                            Browse, select, and upload your video file using the button provided below. <br/>NOTICE: You're limited to a maximum upload filesize of 2 GB and video file type must be of mp4.
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('meta_tag', 'Meta Tag', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('meta_tag',   '',array('placeholder'=>'Please enter meta tag', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_metatag"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('meta_description', 'Meta Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('meta_description',   '',array('placeholder'=>'Please enter meta description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_metadesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'addmedia')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_videolibrary(1)" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

</div>
<style type="text/css">
    .progressfrm { display:none; }
    #progress { position:relative; width:400px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; display:none; }
    #bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px;  }
    #percent { position:absolute; display:inline-block; top:3px; left:48%; color:#821c34; }
</style>

<script type="text/javascript">
      $(document).ready(function () {
          var options = {
              beforeSend: function ()
              {
                  $(".error-display").html("");
                  $("#error_mediafile").html("");
                  window.scrollTo(0, 0);
                  showOverlay();
              },
              uploadProgress: function (event, position, total, percentComplete)
              {
                  $(".progressfrm").show();
                  $("#progress").show();
                  $("#bar").width('0%');
                  $("#percent").html("0%");
                  $("#bar").width(percentComplete + '%');
                  $("#percent").html(percentComplete + '%');
              },
              success: function ()
              {
                  $("#bar").width('100%');
                  $("#percent").html('100%');
                  $(".progressfrm").hide().delay(2000);
                  $("#progress").hide().delay(2000);
              },
              complete: function (response)
              {
                  var response = jQuery.parseJSON(response.responseText);

                  if (response['validation_failed'] == 1) {
                      if (response['errors']['name']) {
                          $("#error_name").html(response['errors']['name']);
                      }

                      if (response['errors']['meta_tag']) {
                          $("#error_metatag").html(response['errors']['meta_tag']);
                      }

                      if (response['errors']['meta_description']) {
                          $("#error_metadesc").html(response['errors']['meta_description']);
                      }

                      if (response['errors']['media_name']) {
                          $("#error_mediafile").html(response['errors']['media_name']);
                      }
                  }

                  if (response['success'] == 1) {
                      $(".error-display").html('');
                      $("#frm_success").html('Transcoded Job created successfully for media file.');
                      $("#frm_success").show();
                      $("input[type=submit]").attr('disabled', 'disabled');
                      call_videolibrary().delay(3000);
                  }
                  hideOverlay();
              },
              error: function ()
              {
                  $("#error_mediafile").html("<font color='red'> ERROR: unable to upload files</font>");
                  hideOverlay();
              }

          };

          $("#frmmedia").ajaxForm(options);
      });

</script>
@stop

