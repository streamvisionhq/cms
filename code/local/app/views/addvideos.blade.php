@extends('layouts.master')

@section('js')
<script src="http://malsup.github.com/jquery.form.js"></script>
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Add Videos</a></li>
            </ul>
            <div class="well">
                <div class="form-group progressfrm">
                    <div class="progress">
                        <div class="bar"></div >
                        <div class="percent">0%</div >
                    </div>
                </div>

                <div id="status"></div>
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'medialibrary/addvideos', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmmedia')) }}
                {{ Form::hidden('created_by', Auth::id()) }}
                <table border="0" width="80%" class="tblvideorow">
                    <tr id="rowId">
                        <td>
                            <div>
                                <label>Name</label>
                                <input type="text" name="name[1]" class="form-control" id="title_1" />
                            </div>

                            <div>
                                <label>Browse Media</label>
                                <input type="file" name="media_name[1]" id="media_1" />
                            </div>
                            <div>
                                <span style="float:left; text-align:left; margin-left:40%;">
                                    Browse, select, and upload your video file using the button provided below. <br>NOTICE: You're limited to a maximum upload filesize of 20MB and video file type must be of mp4.
                                </span>
                            </div>
                            <div id="status_1" class=""></div>
                        </td>
                        <td style="width:76px;"></td>
                    </tr>
                </table>

                <div id="addedRows"></div>
                <table border="0" width="80%" class="tblvideorow">
                    <tr>
                        <td class="add_more">
                            <span onclick="addMoreRows(this.form);">Add More</span>
                        </td>
                    </tr>
                </table>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'addmedia')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_videolibrary(1)" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>

</div>
<style type="text/css">
    .progressfrm { display:none; }
    .progress { position:relative; width: 80%; background-color: #131313; border: 1px solid #ddd; padding: 1px; border-radius: 3px; display:none; }
    .bar { background-color: #B4F5B4; width:0%; height:20px; border-radius: 3px;  }
    .percent { position:absolute; display:inline-block; top:3px; left:48%; color:#821c34; }

    .tblvideorow { width: 80%; margin-bottom: 20px; padding: 2px; }
    .tblvideorow div { width: 100%; padding: 25px 0px;}
    .tblvideorow div label { width: 40%; float: left; color: #b4b4b4 !important; font-family: "open_sansregular"; font-size: 14px; font-weight: 400; text-align: left; }
    .tblvideorow div input { width: 60%; float: left; }
    .add_more { width: 100; padding: 15px 0px;}
    .add_more span { float: right; color: #fff; cursor: pointer; width: 84px; background-color: #337ab7; border-color: #2e6da4; color: #fff; padding: 5px 10px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;border:0px solid #FFFF00; }
    .add_more span:hover { font-weight: 600; }
</style>

<script type="text/javascript">
      var rowCount = 1;
      function addMoreRows(frm) {
          rowCount++;

          var recRow = '<table border="0" width="90%" class="tblvideorow">';
          recRow += '<tr id="rowCount' + rowCount + '"><td>';
          recRow += '<div>';
          recRow += '<label>Name</label>';
          recRow += '<input type="text"  name="name[' + rowCount + ']" class="form-control" id="title_' + rowCount + '" />';
          recRow += '</div>';
          recRow += '<div>';
          recRow += '<label>Browse Media</label>';
          recRow += '<input type="file" name="media_name[' + rowCount + ']" id="media_' + rowCount + '" />';
          recRow += '</div>';
          recRow += '<div><span style="float:left; text-align:left; margin-left:40%;">Browse, select, and upload your video file using the button provided below. <br>NOTICE: You\'re limited to a maximum upload filesize of 20MB and video file type must be of mp4.</span></div>';
          recRow += '<div id="status_' + rowCount + '" class=""></div>';
          recRow += '</td>';
          recRow += '<td>';
          recRow += '<a href="javascript:void(0);" onclick="removeRow(' + rowCount + ');">Delete</a></td></tr>';
          recRow += '</td></tr></table>';


          jQuery('#addedRows').append(recRow);
      }

      function removeRow(removeNum) {
          jQuery('#rowCount' + removeNum).parent().parent().remove();
      }

      var bar = $('.bar');
      var percent = $('.percent');
      var status = $('#status');

      $('form').ajaxForm({
          beforeSend: function () {
              $(".progress").css("display", "block");
              var percentVal = '0%';
              bar.width(percentVal)
              percent.html(percentVal);

              if ($(".error-display").length > 0) {
                  $(".error-display").html("");
                  $(".error-display").removeAttr("class");
              }

              if ($(".suc-display").length > 0) {
                  $(".suc-display").html("");
                  $(".suc-display").removeAttr("class");
              }

              window.scrollTo(0, 0);
              showOverlay();
          },
          uploadProgress: function (event, position, total, percentComplete) {
              $(".progressfrm").show();
              var percentVal = percentComplete + '%';
              bar.width(percentVal)
              percent.html(percentVal);
              //console.log(percentVal, position, total);
          },
          success: function () {
              var percentVal = '100%';
              bar.width(percentVal)
              percent.html(percentVal);
              $(".progressfrm").hide().delay(2000);
          },
          complete: function (xhr) {
              var response = jQuery.parseJSON(xhr.responseText);

              $.each(response['code'], function (key, value) {
                  //console.log( key + ": " + value );
                  //console.log( response['status'][key] );
                  var st = "#status_" + key;
                  var title = "#title_" + key;
                  var file = "#media_" + key;
                  $(st).removeAttr("class");
                  if (response['code'][key] == 'failed') {
                      $(st).attr("class", "error-display");
                  } else {
                      $(st).attr("class", "suc-display");
                      $(title).val('');
                      $(file).val('');
                  }

                  $(st).html(response['status'][key]);
              });

              hideOverlay();
          }
      });
</script>
@stop
