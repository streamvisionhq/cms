@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Channel Manager</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Add Category</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'channelmanager/addcategory', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmcategory')) }}
                {{ Form::hidden('channel_type', '3') }}
                {{ Form::hidden('parent_id', $parent_id) }}
                {{ Form::hidden('created_by', Auth::id()) }}
                <div class="form-group">
                    {{ Form::label('category_name', 'Category Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('category_name', '',  array('placeholder'=>'Please enter category name', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_cname"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('short_description', 'Short Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('short_description',   '',array('placeholder'=>'Please enter Short Description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_cdesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('category_image', 'Category Image', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::file('category_image', $attributes = array()) }}
                        <span class="error-display" id="error_cimg"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'addcategory')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_channelmanager('25', 'sort_order', 'asc', '1', '1')" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
      $(document).ready(function () {
          $("#addcategory").click(function () {

              showOverlay();
              $(".error-display").html("");
              $("#frmcategory").submit(function (event)
              {
                  var postData = $(this).serializeArray();
                  var formURL = $(this).attr("action");
                  var formData = new FormData(this);
                  $.ajax(
                          {
                              url: formURL,
                              type: 'POST',
                              data: formData,
                              mimeType: "multipart/form-data",
                              contentType: false,
                              cache: false,
                              processData: false,
                              success: function (data, textStatus, jqXHR)
                              {
                                  var response = JSON.parse(data);

                                  if (response['validation_failed'] == 1) {
                                      if (response['errors']['category_name']) {
                                          $("#error_cname").html(response['errors']['category_name']);
                                      }

                                      if (response['errors']['category_name']) {
                                          $("#error_cdesc").html(response['errors']['short_description']);
                                      }

                                      if (response['errors']['category_image']) {
                                          $("#error_cimg").html(response['errors']['category_image']);
                                      }
                                  }

                                  if (response['success'] == 1) {
                                      $(".error-display").html('');
                                      $("#frm_success").html('Category created successfully.');
                                      $("#frm_success").show();
                                      $("input[type=submit]").attr('disabled', 'disabled');
                                      call_channelmanager('25', 'sort_order', 'asc', '1', '1').delay(3000);
                                  }

                                  hideOverlay();
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $("#frm_error").html('AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '');
                                  $("#frm_error").show();
                              }
                          });
                  event.preventDefault();	//STOP default action
                  event.unbind();
              });
          });
      });
</script>
@stop

