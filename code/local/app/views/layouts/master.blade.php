<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>STREAMVISION TV Beta</title>
        {{ HTML::style('assets/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/style.css') }}
        {{ HTML::style('https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css') }}
        @yield('css')

        <script type="text/javascript">
            baseURL = "{{ URL::to('/') }}";
        </script>
        {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js')}}
        {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js')}}
        {{HTML::script('assets/js/bootstrap.min.js')}}
        {{HTML::script('assets/js/streamvision.js')}}
        @yield('js')
    </head>
    <body>
        <div class="overlay" id="overlay"></div>
        <div id="dvLoading"></div>
        <div id="container">
            <header>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="wrapp">
                        <div class="navbar-header">
                            <button data-target="#bs-example-navbar-collapse-6" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar">a</span>
                                <span class="icon-bar">b</span>
                                <span class="icon-bar">c</span>
                            </button>
                            <a class="navbar-brand logo head_logo" href="{{ URL::to('/channelmanager') }}">STREAMVISION <span class="tv">TV Beta</span></a>

                        </div>
                        <div id="genmidnav">
                            <ul class="nav nav-pills">
                                <li role="presentation"><a class="head_mnu" href="{{ URL::to('channelmanager') }}">Channel Manager</a></li>                                    
                                <li role="presentation"><a class="head_mnu" href="{{ URL::to('customers') }}">Customers</a></li>
                                @if(Setting::all()["cm_mode"] == "cms")
                                <li role="presentation"><a class="head_mnu" href="{{ URL::to('add-new-customer') }}">Add Customer</a></li>
                                @endif
                                <li role="presentation"><a class="head_mnu" href="{{ URL::to('roku-devices') }}">Devices</a></li>
                                <!-- <li role="presentation"><a class="head_mnu" href="{{ URL::to('dashboard') }}">Dashboard</a></li> -->
                            </ul>
                        </div>
                        <div id="genrgnav">

                            <!-- Small button group -->
                            <div class="btn-group">
                                <button aria-expanded="false" data-toggle="dropdown" class="btn btn-info dropdown-toggle signed_hud" type="button">
                                    @if(Auth::user()->firstname)
                                    Signed in as {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}
                                    @else
                                    Signed in
                                    @endif                                        
                                    <span class="caret"></span>
                                </button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="{{ URL::to('settings') }}">Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('messages') }}">Error Messages</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('services') }}">Services</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('update') }}">Check Update</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('settings/refresh_epg') }}">Update EPG</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ URL::to('logout') }}">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </nav>
            </header>
            <section id="content">
                @yield('content')
            </section>

            <footer id="footer">
                <p>
                    Copyright to &copy; <?php echo date('Y'); ?> StreamVision, LLC – All Rights Reserved.<br>
                    Version: 1.1.16.4
                </p>                
            </footer>
        </div>
    </body>
</html>
<script type="text/javascript">
    function showOverlay() {
        $("#overlay").show();
        $("#dvLoading").show();
    }

    function hideOverlay() {
        $("#overlay").hide();
        $("#dvLoading").hide();
    }

    function call_channelmanager(pp, sb, so, pg, cat) {
        location.href = "{{ URL::to('channelmanager') }}" + "?per_page=" + pp + "&sort_by=" + sb + "&sort_order=" + so + "&channel_type=" + cat + "&page=" + pg;
    }

    function call_medialibrary() {
        location.href = "{{ URL::to('medialibrary') }}";
    }

    function call_videolibrary(x) {
        location.href = "{{ URL::to('medialibrary/listvideo') }}" + "?page=" + x;
    }
</script>
<style type="text/css">
    .ui-dialog-titlebar-close { display:none; }
</style>