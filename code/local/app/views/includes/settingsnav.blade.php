<ul class="nav nav-tabs"  id="tognav">
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings') }}">General</a></li>
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings/authentication') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings/authentication') }}" style="color:#d4d4d4;">Authentication</a></li>
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings/cms') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings/cms') }}">Customer Management System</a></li>
    @if($cm_mode == "emerald")
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings/radius') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings/radius') }}">Radius Authentication</a></li>
    @endif
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings/dvr') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings/dvr') }}">DVR</a></li>
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'settings/channel') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('settings/channel') }}">Streams</a></li>
    <li role="presentation" @if (Route::getCurrentRoute()->getPath()== 'changepassword') {{ 'class="active"' }} {{ 'style="background:#1e1e1e;"' }} @endif><a href="{{ URL::to('changepassword') }}" style="color:#d4d4d4;">Change Password</a></li>
</ul>