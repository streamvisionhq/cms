@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Channel Manager</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Edit Livestream</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'channelmanager/editlivestream', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmlivestream')) }}
                {{ Form::hidden('channel_id', $channel_id) }}
                {{ Form::hidden('channel_type', '1') }}
                {{ Form::hidden('modified_by', Auth::id()) }}
                {{ Form::hidden('hidchannel_logo', $channel_logo) }}
                <div class="form-group">
                    {{ Form::label('free_channel', 'Free Channel', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10 text-left">
                        {{ Form::checkbox('free_channel', '1', ($free_channel == '1') ? true : false) }}
                    </div>
                </div>
                
                <div class="form-group">
                    {{ Form::label('title', 'Title', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('title', $title,  array('placeholder'=>'Please enter title', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_title"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('short_description', 'Short Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('short_description', $short_description, array('placeholder'=>'Please enter short description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_sdesc"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('long_description', 'Long Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::textarea('long_description', $long_description, array('placeholder'=>'Please enter long description', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_ldesc"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    {{ Form::label('channel_number', 'Channel Number', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::number('channel_number', $channel_number,  array('placeholder'=>'Please enter channel number', 'class'=>'form-control', 'min'=>'1', 'step'=>'0.01')) }}
                        <span class="error-display" id="error_title"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('channel_logo', 'Channel Logo', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::file('channel_logo', $attributes = array()) }}
                        <br/>
                        {{ HTML::image('/assets/data/image/'.$channel_logo, 'logo', array( 'width' => 100, 'style' => 'float:left;' )) }}
                        <span class="error-display" id="error_clogo"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('path', 'Channel Path', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('path', $path,  array('placeholder'=>'Please enter channel path', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_lstream"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('titan_channel_id', 'Titan TV Channel Reference', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('titan_channel_id', $titan_channel_id,  array('placeholder'=>'Please enter Titan TV Channel Reference', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_titan_channel_id"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('callsign', 'Call Sign', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('callsign', $callsign,  array('placeholder'=>'Please enter the Call Sign ID', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_callsign"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('stream_name', 'Stream Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('stream_name', $streamname,  array('placeholder'=>'Example: master/KTVK-DT-3.1', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_sname"></span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('service_category', 'Service Category', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::select('service_category', $services, $service_category, array('class'=>'form-control')) }}
                        <span class="error-display" id="error_scat"></span>
                    </div>
                </div>

                <div class="form-group" style="display: none;">
                    {{ Form::label('pre_roll', 'Pre Rolls', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <span style="float:left">
                            @if ($pre_roll == 1)
                            {{ Form::radio('pre_roll', 1, true) }}
                            @else
                            {{ Form::radio('pre_roll', 1) }}
                            @endif
                            On
                            @if ($pre_roll == 2)
                            {{ Form::radio('pre_roll', 2, true) }}
                            @else
                            {{ Form::radio('pre_roll', 2) }}
                            @endif
                            Off
                        </span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'editlivestream')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_channelmanager('25', 'sort_order', 'asc', '1', '1')" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        var fields = ['titan_channel_id', 'callsign', 'stream_name', 'stream_name', 'service_category'];

        if ($("#free_channel").is(":checked")) {
            $.each(fields, function () {
                $($("#" + this)).closest('.form-group').hide();
            });
        }

        $("#free_channel").on("change", function () {
            if ($(this).is(":checked")) {
                $.each(fields, function () {
                    $($("#" + this)).closest('.form-group').hide();
                });
            } else {
                $.each(fields, function () {
                    $($("#" + this)).closest('.form-group').show();
                });
            }
        });

        $("#editlivestream").click(function () {

            showOverlay();
            $(".error-display").html("");
            $("#frmlivestream").submit(function (event)
            {
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                var formData = new FormData(this);
                $.ajax(
                        {
                            url: formURL,
                            type: 'POST',
                            data: formData,
                            mimeType: "multipart/form-data",
                            contentType: false,
                            cache: false,
                            processData: false,
                            success: function (data, textStatus, jqXHR)
                            {
                                var response = JSON.parse(data);

                                if (response['validation_failed'] == 1) {
                                    if (response['errors']['title']) {
                                        $("#error_title").html(response['errors']['title']);
                                    }

                                    if (response['errors']['short_description']) {
                                        $("#error_sdesc").html(response['errors']['short_description']);
                                    }

                                    if (response['errors']['long_description']) {
                                        $("#error_ldesc").html(response['errors']['long_description']);
                                    }

                                    if (response['errors']['livestream_url']) {
                                        $("#error_lstream").html(response['errors']['livestream_url']);
                                    }

                                    if (response['errors']['backupstream_url']) {
                                        $("#error_bstream").html(response['errors']['backupstream_url']);
                                    }

                                    if (response['errors']['server_url']) {
                                        $("#error_surl").html(response['errors']['server_url']);
                                    }

                                    if (response['errors']['channel_logo']) {
                                        $("#error_clogo").html(response['errors']['channel_logo']);
                                    }

                                    if (response['errors']['titan_channel_id']) {
                                        $("#error_titan_channel_id").html(response['errors']['titan_channel_id']);
                                    }

                                    if (response['errors']['callsign']) {
                                        $("#error_callsign").html(response['errors']['callsign']);
                                    }

                                    if (response['errors']['stream_name']) {
                                        $("#error_sname").html(response['errors']['stream_name']);
                                    }

                                    if (response['errors']['service_category']) {
                                        $("#error_scat").html(response['errors']['error_scat']);
                                    }

                                }

                                if (response['success'] == 1) {
                                    $(".error-display").html('');
                                    $("#frm_success").html('Livestream modified successfully.');
                                    $("#frm_success").show();
                                    $("input[type=submit]").attr('disabled', 'disabled');
                                    call_channelmanager('{{ $per_page }}', '{{ $sort_by }}', '{{ $sort_order }}', '{{ $page }}', '1').delay(3000);
                                }

                                hideOverlay();
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                $("#frm_error").html('AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '');
                                $("#frm_error").show();
                            }
                        });
                event.preventDefault();	//STOP default action
//                  event.unbind();
            });
        });
    });
</script>
@stop

