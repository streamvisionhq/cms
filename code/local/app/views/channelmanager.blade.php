﻿@extends('layouts.master')

@section('js')
<script type="text/javascript">
      $ = jQuery.noConflict(true);</script>
{{HTML::script('http://code.jquery.com/jquery-1.3.2.min.js')}}
{{HTML::script('https://code.jquery.com/ui/1.7.2/jquery-ui.min.js')}}
{{HTML::script('assets/js/ui.nestedSortable.js')}}

<script type="text/javascript">
      jq132 = jQuery.noConflict(true);</script>
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div id="livestream">
            <button class="btn btn-primary" onClick="location ='{{ URL::to('channelmanager/sortchannel') }}'" />Sort Channels</button>
            &nbsp;&nbsp;
            <!-- <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('channelmanager/addcategory') }}'">Add Category</button>
            &nbsp;&nbsp; -->
            <!-- <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('medialibrary/addvideos') }}'">Upload Videos</button>
            &nbsp;&nbsp; -->
            <button class="btn btn-primary" type="button" onClick="location ='{{ URL::to('channelmanager/addlivestream') }}'">Add New Live Stream</button>
            &nbsp;&nbsp;
            <!-- <button class="btn btn-primary" type="button"  onClick="location ='{{ URL::to('channelmanager/addvideoondemand') }}'">Add New Video On Demand  Channel</button> -->
        </div>
    </div>




    <br clear="all" />
    <div class="panel panel-default">	  
        <div class="panel-heading"><h3 class="panel-title">Channel Manager</h3></div>

        <div class="panel-body doc_c">
            {{ Form::hidden('p_page', $per_page, $attributes = array('id'=>'p_page')) }}
            {{ Form::hidden('c_page', $curPage, $attributes = array('id'=>'c_page')) }}
            <div class="table_wrap">
                <div class="tr_title">

                    <div class="col colu_0">S.No</div>
                    <div class="col colu_4">Channel ID</div>
                    <div class="col colu_8">Titan Channel reference</div>
                    <div class="col colu_3">Channel Icon</div>
                    <div class="col colu_1">Channel Name</div>
                    <div class="col colu_8">Channel Path</div>
                    <div class="col colu_1">Stream Name</div>
                    <div class="col colu_4">Sort Order</div>
                    <div class="col colu_7">Actions</div>				
                </div>
                <div class="dd" id="nestable">
                    <ul class="table_sec ui-sortable" style="height:auto !important">

                        @if(sizeof($channels) > 0)
                        @foreach($channels as $channel)
                        <?php $j = $channel->s_no - 1; ?>
                        <li id="item_{{$channel->channel_id}}" <?php
                        if ( $channel->channel_type == 1 ) {
                              echo 'class="lsr"';
                        }
                        ?>>
                            <div class="first_comn">
                                <div class="col colu_0">{{ $j }}</div>
                                <div class="col colu_4">{{$channel->channel_id}}</div>
                                <div class="col colu_8">{{$channel->titan_channel_id}}</div>
                                <div class="col colu_3">{{ HTML::image('assets/data/image/'.$channel->channel_logo, '',  array( 'width' => 24, 'height' => 24) ) }}</div>
                                <div class="col colu_1" {{$channel->ch_class}}"> {{$channel->title}}</div>					
                                <div class="col colu_8">{{$channel->path}}</div>
                                <div class="col colu_1">{{$channel->stream_name}}</div>
                                <div class="col colu_4">{{$channel->sort_order}}</div>
                                <div class="col colu_7">

                                    @if($channel->channel_type == 3)
                                    <a data-original-title="Edit" href="{{ URL::to('channelmanager/addcategory') }}?parent_id={{ $channel->channel_id }}">{{ HTML::image('assets/images/category_add.png', '', array('title'=>'Add Sub Category')) }}</a>

                                    <a data-original-title="Edit" href="{{ URL::to('channelmanager/addvideoondemand') }}?parent_id={{ $channel->channel_id }}">{{ HTML::image('assets/images/vod_add.png', '', array('title'=>'Add Sub channel')) }}</a>
                                    @endif

                                    @if($channel->channel_type == 1)
                                    <a data-original-title="Edit" href="{{ URL::to('channelmanager/editlivestream/'.$channel->channel_id) }}?per_page={{ $per_page }}&sort_by={{ $sort_by }}&sort_order={{ $sort_order }}&page={{ $curPage }}">{{ HTML::image('assets/images/edit.png', '', array('title'=>'edit channel')) }}</a>
                                    @elseif($channel->channel_type == 2)
                                    <a data-original-title="Edit" alt="edit channel" href="{{ URL::to('channelmanager/editvideoondemand/'.$channel->channel_id) }}?per_page={{ $per_page }}&sort_by={{ $sort_by }}&sort_order={{ $sort_order }}&page={{ $curPage }}">{{ HTML::image('assets/images/edit.png', '', array('title'=>'edit channel')) }}</a>
                                    @else
                                    <a data-original-title="Edit" alt="edit category" href="{{ URL::to('channelmanager/editcategory/'.$channel->channel_id) }}?per_page={{ $per_page }}&sort_by={{ $sort_by }}&sort_order={{ $sort_order }}&page={{ $curPage }}">{{ HTML::image('assets/images/edit.png', '', array('title'=>'edit category')) }}</a>
                                    @endif

                                    <a data-original-title="Delete" alt="remove channel" href="javascript:void(0);" onClick="call_del('{{$channel->channel_id}}', '{{$channel->channel_type}}');">{{ HTML::image('assets/images/delete.png', '', array('title'=>'remove channel')) }}</a>

                                    @if($channel->status == 1)
                                    <a data-original-title="Active Channel" alt="make channel inactive" href="javascript:void(0)"  href="javascript:void(0);" onClick="call_changestatus('{{$channel->channel_id}}', 'inactive', '{{ $per_page }}', '{{ $sort_by }}', '{{ $sort_order }}', '{{ $curPage }}');">{{ HTML::image('assets/images/active.png', '', array('title'=>'make channel inactive')) }}</a>
                                    @else
                                    <a data-original-title="Inactive Channel" alt="make channel active"  href="javascript:void(0)" onClick="call_changestatus('{{$channel->channel_id}}', 'active', '{{ $per_page }}', '{{ $sort_by }}', '{{ $sort_order }}', '{{ $curPage }}');">{{ HTML::image('assets/images/inactive.png', '', array('title'=>'make channel active')) }}</a>
                                    @endif

                                    @if($channel->channel_type == 1)
                                    <a data-original-title="watch live stream" alt="watch live stream"  href="javascript:void(0)" class="manage_livestream" id="l_{{$channel->channel_id}}">{{ HTML::image('assets/images/watch_live.png', '', array('title'=>'watch live stream')) }}</a>
                                    <span style="display:none;" id="ls_{{$channel->channel_id}}">{{$channel->channel_id}}</span>
                                            {{--*/ $url = parse_url($channel->path) /*--}}
                                            @if(isset($url['scheme']))
                                                    <span style="display:none;" id="lsurl_{{$channel->channel_id}}">{{$channel->path}}</span>
                                            @else
                                                    <span style="display:none;" id="lsurl_{{$channel->channel_id}}">http://{{$ip}}:{{$port}}/{{$channel->path}}</span>
                                            @endif
                                    @endif
                                </div>

                            </div>
                            <?php
                            $top_child = 1;
                            display_channel_childnodes($channel->channel_id, 0, $subchannels, $index, $j, array(), $per_page, $sort_by, $sort_order, $curPage, $top_child);
                            ?>
                        </li>

                        @endforeach
                        @else

                        @endif
                    </ul>
                </div>
                <p>

                </p>
            </div>

            <nav>
                <div class="perpage-select">Show &nbsp;
                    <select class="selct_box" id="select_perpage" style="font-family:'open_sansregular'; font-size:12px;">
                        <option value="" >Select</option>
                        <option value="{{ URL::to('channelmanager?per_page=25&sort_by='.$sort_by.'&sort_order='.$sort_order) }}"  @if ($per_page == 25) {{ 'selected="selected"' }} @endif >25</option>
                        <option value="{{ URL::to('channelmanager?per_page=50&sort_by='.$sort_by.'&sort_order='.$sort_order) }}" @if ($per_page == 50) {{ 'selected="selected"' }} @endif >50</option>
                        <option value="{{ URL::to('channelmanager?per_page=100&sort_by='.$sort_by.'&sort_order='.$sort_order) }}" @if ($per_page == 100) {{ 'selected="selected"' }} @endif >100</option>
                    </select>
                    &nbsp;
                    Entries
                </div>

                <?php echo $channels->appends(array('per_page' => $per_page, 'sort_by' => $sort_by, 'sort_order' => $sort_order))->links(); ?>
            </nav>
        </div>
    </div>
</div>
<div id="dialog-message" title="Channel Manager" style="display:none;">
    <p>The channel removed successfully.</p>
</div>

<div id="dialog-status"  title="Channel Manager" style="display:none;">
    <!-- form start -->
    <div id="dialog-form" class="frm_live_stream"  title="Live Stream">
        <form name="frmmanagestream" id="frmmanagestream" method="post">
            <input type="hidden" name="channel_id" id="channel_id" value="" />

            <fieldset>
                <label for="name">Livestream URL</label>
                <input type="text" name="livestream_url" id="livestream_url" value="" class="text ui-widget-content ui-corner-all" style="border:solid 1px #aaaaaa !important; width:350px;">
                <span class="error-display" id="error_lstream"></span>

                <!-- Allow form submission with keyboard without duplicating the dialog button -->
                <input type="submit"  tabindex="-1" style="position:absolute; top:-1000px">
            </fieldset>
        </form>
    </div>
    <!-- form end -->
    <p></p>
</div>



<p></p>


</div>

<div id="dialog-sortable" title="Channel Manager" style="display:none;">
    <p>The channel sorted successfully.</p>
</div>
<script type="text/javascript">
      /*jq132(document).ready(function(){
       var data = '';
       var perpage = $("#p_page").val();
       var curpage = $("#c_page").val();
       
       
       jq132('#demo2').sortable({
       'cursor':'move',
       'items':'li',
       'placeholder':'placeholder',
       'nested':'ul'
       })
       
       jq132('.lsr').sortable({
       'cursor':'move',
       'items':'> li',
       'placeholder':'placeholder',
       'nested':false
       })
       
       jq132('#serialize').click(function(event){
       //alert(jq132('#demo2').sortable('serialize'));
       showOverlay();
       data = jq132('#demo2').sortable('serialize');
       data = data + '&perpage=' + perpage + '&curpage=' + curpage;
       
       var url = "{{ URL::to('channelmanager/sortchannel') }}";
       
       jq132.ajax({
       type: "POST",
       url: url,
       data: data,
       success: function(response)
       {
       //console.log(response);
       var response = JSON.parse(response)
       //alert(response['success']);
       if (response['success']) {
       hideOverlay();
       jq132( "#dialog-sortable" ).dialog({
       modal: true,
       buttons: {
       Ok: function() {
       jq132( this ).dialog( "close" );
       call_channelmanager('25', 'sort_order', 'asc', '1', '1').delay(3000);
       }
       }
       });
       }
       }
       });
       })
       });*/

      $(function(){
      // bind change event to select
      $('#select_perpage').bind('change', function () {
      var url = $(this).val(); // get selected value
      if (url) { // require a URL
      window.location = url; // redirect
      }
      return false;
      });
      });
      var call_del = function(x, y, z) {
      var url = "{{ URL::to('channelmanager/removechannel') }}/" + x + "/" + y;
      var r = confirm("Do you want to remove this channel");
      if (r == true) {
      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-message").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_channelmanager('25', 'sort_order', 'asc', '1', y).delay(3000);
                      }
                      }
              });
              }
              }
      });
      }
      }

      var call_changestatus = function(x, y, pp, sb, so, pg, c_type) {
      if (y == 'active') {
      var url = "{{ URL::to('channelmanager/makeactive') }}/" + x;
      $("#dialog-status p").html("The selected channel was make active.");
      } else {
      var url = "{{ URL::to('channelmanager/makeinactive') }}/" + x;
      $("#dialog-status p").html("The selected channel was make inactive.");
      }

      $.ajax({
      type: 'GET',
              url: url,
              dataType: 'json',
              beforeSend: function(){
              showOverlay();
              },
              success: function(data) {
              if (data['success']) {
              hideOverlay();
              $("#dialog-status").dialog({
              modal: true,
                      buttons: {
                      Ok: function() {
                      $(this).dialog("close");
                      call_channelmanager(pp, sb, so, pg, c_type).delay(3000);
                      }
                      }
              });
              }
              }
      });
      }

      $(function() {
      var dialog;
      var form;
      $(".manage_livestream").button().on("click", function() {
      var res = $(this).attr("id").split('_');
      var cid = "#ls_" + res[1];
      var curl = "#lsurl_" + res[1];
      $("#channel_id").val($(cid).html());
      $("#livestream_url").val($(curl).html());
      dialog.dialog("open");
      $("#dialog-form").parent().addClass("frm_live_stream");
      });
      dialog = $("#dialog-form").dialog({
      autoOpen: false,
              height: 350,
              width: 550,
              modal: true,
              buttons: {
              Cancel: function() {
              dialog.dialog("close");
              }
              },
              close: function() {
              form[0].reset();
              }
      });
      form = dialog.find("form").on("submit", function(event) {
      event.preventDefault();
      manageStream();
      });
      function manageStream() {
      var channel_id = $("#channel_id").val();
      var livestream_url = $("#livestream_url").val();
      var formURL = "{{ URL::to('channelmanager/savestream') }}";
      $("#error_lstream").html('');
      $.ajax(
      {
      url : formURL,
              type: "POST",
              data : {channel_id : channel_id, livestream_url : livestream_url},
              success:function(data, textStatus, jqXHR)
              {
              if (data['validation_failed'] == 1) {
              if (data['errors']['livestream_url']) { $("#error_lstream").html(data['errors']['livestream_url']); }
              }

              if (data['success'] == 1) {
              dialog.dialog("close");
              call_channelmanager('25', 'sort_order', 'asc', '1', '1').delay(3000);
              }
              },
              error: function(jqXHR, textStatus, errorThrown)
              {

              }
      });
      }
      });</script>
<style type="text/css">
    .ui-dialog-titlebar-close { display:none; }
</style>
<script type="text/javascript">
      $(".tip4").click(function() {
      $.fancybox({
      'padding'		: 0,
              'autoScale'		: false,
              'transitionIn'	: 'none',
              'transitionOut'	: 'none',
              'title'			: this.title,
              'width'			: 680,
              'height'		: 495,
              'href'			: this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
              'type'			: 'swf',
              'swf'			: {
              'wmode'		: 'transparent',
                      'allowfullscreen'	: 'true'
              }
      });
      return false;
      });


</script>
@stop

<?php

function display_channel_childnodes($parent_id, $level, $subchannel, $index, $j, $k, $per_page, $sort_by, $sort_order, $curPage, $top_child) {

      if ( isset($index[$parent_id]) ) {
            $class = ($top_child == 1) ? ' child1' : '';
            $lvl = $level + 1;
            echo '<ul class="table_sec' . $class . '" style="height:auto !important">';
            foreach ( $index[$parent_id] as $id ) {
                  $k[$level] = $subchannel[$id]['s_no'];
                  $imp = implode('.', $k);
                  $sub_prefix = ($subchannel[$id]['channel_type'] == '3') ? str_repeat('Sub ', $lvl) : '';

                  echo '<li  id="item_' . $subchannel[$id]['channel_id'] . '">';
                  //echo '<div class="dd-handle">';
                  echo '<div class="col colu_0">';
                  echo HTML::image('assets/images/status_on.png', '', array('width' => 16, 'height' => 16));
                  echo '</div>';
                  echo '<div class="col colu_1">' . $j . '.' . $imp . '</div>';
                  echo '<div class="col colu_2 ' . $subchannel[$id]['ch_class'] . '">' . $subchannel[$id]['title'] . '</div>';
                  echo '<div class="col colu_3">' . HTML::image('assets/data/image/' . $subchannel[$id]['channel_logo'], '', array('width' => 24, 'height' => 24)) . '</div>';
                  echo '<div class="col colu_4">' . $sub_prefix . $subchannel[$id]['channel_typedesc'] . '</div>';
                  echo '<div class="col colu_5">' . $subchannel[$id]['sort_order'] . '</div>';
                  echo '<div class="col colu_6">';

                  if ( $subchannel[$id]['channel_type'] == 3 ) {
                        echo '<a data-original-title="Edit" href="' . URL::to('channelmanager/addcategory/') . '?parent_id=' . $subchannel[$id]['channel_id'] . '">' . HTML::image('assets/images/category_add.png', '', array('title' => 'Add Sub Category')) . '</a>';

                        echo '<a data-original-title="Edit" href="' . URL::to('channelmanager/addvideoondemand/') . '?parent_id=' . $subchannel[$id]['channel_id'] . '">' . HTML::image('assets/images/vod_add.png', '', array('title' => 'Add Sub channel')) . '</a>';
                  }

                  if ( $subchannel[$id]['channel_type'] == 2 ) {
                        echo '<a data-original-title="Edit" alt="edit channel" href="' . URL::to('channelmanager/editvideoondemand/' . $subchannel[$id]['channel_id']) . '?per_page=' . $per_page . '&sort_by=' . $sort_by . '&sort_order=' . $sort_order . '&page=' . $curPage . '">' . HTML::image('assets/images/edit.png', '', array('title' => 'edit channel')) . '</a>';
                  }
                  else {
                        echo '<a data-original-title="Edit" alt="edit category" href="' . URL::to('channelmanager/editcategory/' . $subchannel[$id]['channel_id']) . '?per_page=' . $per_page . '&sort_by=' . $sort_by . '&sort_order=' . $sort_order . '&page=' . $curPage . '">' . HTML::image('assets/images/edit.png', '', array('title' => 'edit category')) . '</a>';
                  }

                  echo '<a data-original-title="Delete" alt="remove channel" href="javascript:void(0);" onClick="call_del(\'' . $subchannel[$id]['channel_id'] . '\', \'' . $subchannel[$id]['channel_type'] . '\');">' . HTML::image('assets/images/delete.png', '', array('title' => 'remove channel')) . '</a>';

                  if ( $subchannel[$id]['status'] == 1 ) {
                        echo '<a data-original-title="Active Channel" alt="make channel inactive" href="javascript:void(0)"  href="javascript:void(0);" onClick="call_changestatus(\'' . $subchannel[$id]['channel_id'] . '\', \'inactive\', \'' . $per_page . '\', \'' . $sort_by . '\', \'' . $sort_order . '\', \'' . $curPage . '\');">' . HTML::image('assets/images/active.png', '', array('title' => 'make channel inactive')) . '</a>';
                  }
                  else {
                        echo '<a data-original-title="Inactive Channel" alt="make channel active" href="javascript:void(0)"  href="javascript:void(0);" onClick="call_changestatus(\'' . $subchannel[$id]['channel_id'] . '\', \'active\', \'' . $per_page . '\', \'' . $sort_by . '\', \'' . $sort_order . '\', \'' . $curPage . '\');">' . HTML::image('assets/images/inactive.png', '', array('title' => 'make channel active')) . '</a>';
                  }

                  echo '</div>';
                  echo '<div class="col colu_7">' . $subchannel[$id]['channel_created'] . '</div>';
                  //echo '</div>';
                  //echo 'item_'.$subchannel[$id]['channel_id'];
                  $top_child = 0;
                  display_channel_childnodes($id, $level + 1, $subchannel, $index, $j, $k, $per_page, $sort_by, $sort_order, $curPage, $top_child);

                  echo '</li>';
            }
            echo '</ul>';
      }
}
?>
