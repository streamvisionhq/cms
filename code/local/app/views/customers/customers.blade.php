@extends('layouts.master')

@section('content')
<div class="content_container">
    <br clear="all" />
    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Customer Filter</h3>
        </div>
        <div class="well">
            @if ( Session::has('flash_message') )
            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>  
            @endif
            <form class="form-horizontal" method="get">
                <div class="form-group">
                    <label for="customerID" class="col-sm-2 control-label">Customer ID</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="customerID" placeholder="Customer ID" type="text" name="customerID" value="<?php echo Input::get('customerID'); ?>">                
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstName" class="col-sm-2 control-label">Firstname</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="firstName" placeholder="Firstname" type="text" name="firstName" value="<?php echo Input::get('firstName'); ?>">                
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Lastname</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="lastName" placeholder="Lastname" type="text" name="lastName" value="<?php echo Input::get('lastName'); ?>">                
                    </div>
                </div>
                @if($cm_mode == "cms")
                <div class="form-group">
                    <label for="email" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="email" placeholder="Email" type="text" name="email" value="<?php echo Input::get('email'); ?>">                
                    </div>
                </div>
                @endif
                <div class="form-group">
                    <label for="deviceID" class="col-sm-2 control-label">Device ID</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="deviceID" placeholder="Device ID" type="text" name="deviceID" value="<?php echo Input::get('deviceID'); ?>">                
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" name="action" value="filter">
                        <input type="submit" name="customerSearch" value="Search">
                    </div>
                </div>    
            </form>
        </div>

        <div class="panel-heading">
            <h3 class="panel-title">Customers</h3>
        </div>
        <?php
        $columnClass = "colu_6";
        if ( $cm_mode == "cms" ) {
              $columnClass = "colu_10";
        }
        ?>
        <div class="panel-body detail_cnt">
            <div class="table_wrap">
                <div class="tr_title">
                    <div class="col colu_6">Customer ID</div>
                    <div class="col colu_6">Firstname</div>
                    <div class="col colu_6">Lastname</div>
                    <div class="col colu_6">Date</div>
                    <div class="col <?php echo $columnClass; ?>">Actions</div>

                </div>
                @if(sizeof($customers) > 0)
                @foreach($customers as $customer)
                <div class="dd" id="nestable">
                    <ul class="table_sec ui-sortable" style="height:auto !important">
                        <li id="item_1">
                            <div class="first_comn">                                        
                                <div class="col colu_6">{{ $customer->CustomerID }}</div>
                                <div class="col colu_6 ">{{ $customer->FirstName }}</div>
                                <div class="col colu_6">{{ $customer->LastName }}</div>
                                <div class="col colu_6">{{ date('F d, Y', strtotime($customer->Date)) }}</div>
                                <div class="col <?php echo $columnClass; ?>">
                                    <a class="cus-btn" href="{{ URL::to('roku-devices?action=filter&customerID='.$customer->CustomerID) }}">{{ HTML::image('assets/images/customer-devices.jpg', '',  array( 'title' => 'Devices List','height' => 20) ) }}</a>
                                    @if($cm_mode == "cms")
                                    <a class="cus-btn" href="{{ URL::to('customer/'.$customer->CustomerID.'/services') }}" data-original-title="Manage Services" alt="Manage Services">{{ HTML::image('assets/images/customer-services.jpg', '',  array( 'title' => 'Manage Services', 'height' => 20) ) }}</a>
                                    <a class="cus-btn" href="{{ URL::to('edit-customer/'.$customer->CustomerID) }}" data-original-title="Edit Customer" alt="Edit Customer">{{ HTML::image('assets/images/customer-edit.jpg', '',  array( 'title' => 'Edit Customer', 'height' => 20) ) }}</a>
                                    @endif
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endforeach
                <nav>
                    <div class="perpage-select">Show &nbsp;
                        <select class="selct_box" id="select_perpage" style="font-family:'open_sansregular'; font-size:12px;">
                            <option value="" >Select</option>
                            <option value="{{ URL::to('customers?per_page=25') }}"  @if ($perPage == 25) {{ 'selected="selected"' }} @endif >25</option>
                            <option value="{{ URL::to('customers?per_page=50') }}" @if ($perPage == 50) {{ 'selected="selected"' }} @endif >50</option>
                            <option value="{{ URL::to('customers?per_page=100') }}" @if ($perPage == 100) {{ 'selected="selected"' }} @endif >100</option>
                        </select>
                        &nbsp;
                        Entries
                    </div>
                    <?php echo $customers->appends(array('per_page' => $perPage))->links(); ?>
                </nav>                    
                @else
                <p style="text-align: center;">No Customer Found</p>
                @endif                
            </div>




        </div>
    </div>
</div>
@stop