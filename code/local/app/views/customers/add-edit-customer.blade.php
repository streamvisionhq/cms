@extends('layouts.master')

@section('content')
<div class="content_container">
    <br clear="all" />
    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title"><?php echo!empty($customer) ? "Edit Customer" : "Add New Customer"; ?></h3>
        </div>
        <div class="well">
            @if ( Session::has('flash_message') )
            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>  
            @endif
            <form class="form-horizontal" method="get">
                <div class="form-group">
                    <label for="firstName" class="col-sm-2 control-label">Firstname *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="firstName" placeholder="Firstname" type="text" name="firstName" value="<?php
                        if ( !empty(Input::old('firstName')) ) {
                              echo Input::old('firstName');
                        }
                        elseif ( empty(Input::old('firstName')) && !empty($errors->first('firstName')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->FirstName) ) {
                              echo $customer->FirstName;
                        }
                        ?>">                
                        <span class="error-display">{{$errors->first('firstName')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Lastname *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="lastName" placeholder="Lastname" type="text" name="lastName" value="<?php
                        if ( !empty(Input::old('lastName')) ) {
                              echo Input::old('lastName');
                        }
                        elseif ( empty(Input::old('lastName')) && !empty($errors->first('lastName')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->LastName) ) {
                              echo $customer->LastName;
                        }
                        ?>">                
                        <span class="error-display">{{$errors->first('lastName')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstName" class="col-sm-2 control-label">Password *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="password" placeholder="Password" type="text" name="password" value="<?php
                        if ( !empty(Input::old('password')) ) {
                              echo Input::old('password');
                        }
                        elseif ( empty(Input::old('password')) && !empty($errors->first('password')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Password) ) {
                              echo $customer->Password;
                        }
                        ?>">                
                        <span class="error-display">{{$errors->first('password')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Address1</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="address1" placeholder="Address1" type="text" name="address1" value="<?php
                        if ( !empty(Input::old('address1')) ) {
                              echo Input::old('address1');
                        }
                        elseif ( empty(Input::old('address1')) && !empty(Input::old('save-customer')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Address1) ) {
                              echo $customer->Address1;
                        }
                        ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Address2</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="address2" placeholder="Address2" type="text" name="address2" value="<?php
                        if ( !empty(Input::old('address2')) ) {
                              echo Input::old('address2');
                        }
                        elseif ( empty(Input::old('address2')) && !empty(Input::old('save-customer')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Address2) ) {
                              echo $customer->Address2;
                        }
                        ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">City *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="city" placeholder="City" type="text" name="city" value="<?php
                        if ( !empty(Input::old('city')) ) {
                              echo Input::old('city');
                        }
                        elseif ( empty(Input::old('city')) && !empty($errors->first('city')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->City) ) {
                              echo $customer->City;
                        }
                        ?>">
                        <span class="error-display">{{$errors->first('city')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">State *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="state" placeholder="State" type="text" name="state" value="<?php
                        if ( !empty(Input::old('state')) ) {
                              echo Input::old('state');
                        }
                        elseif ( empty(Input::old('state')) && !empty($errors->first('state')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->State) ) {
                              echo $customer->State;
                        }
                        ?>">
                        <span class="error-display">{{$errors->first('state')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Zip *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="zip" placeholder="Zip" type="text" name="zip" value="<?php
                        if ( !empty(Input::old('zip')) ) {
                              echo Input::old('zip');
                        }
                        elseif ( empty(Input::old('zip')) && !empty($errors->first('zip')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Zip) ) {
                              echo $customer->Zip;
                        }
                        ?>">
                        <span class="error-display">{{$errors->first('zip')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="phone" placeholder="Phone" type="text" name="phone" value="<?php
                        if ( !empty(Input::old('phone')) ) {
                              echo Input::old('phone');
                        }
                        elseif ( empty(Input::old('phone')) && !empty(Input::old('save-customer')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Phone) ) {
                              echo $customer->Phone;
                        }
                        ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">Email *</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="email" placeholder="Email" type="text" name="email" value="<?php
                        if ( !empty(Input::old('email')) ) {
                              echo Input::old('email');
                        }
                        elseif ( empty(Input::old('email')) && !empty($errors->first('email')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Email) ) {
                              echo $customer->Email;
                        }
                        ?>">
                        <span class="error-display">{{$errors->first('email')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">{{$custom1}}</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="custom1" placeholder="{{$custom1}}" type="text" name="custom1" value="<?php
                        if ( !empty(Input::old('custom1')) ) {
                              echo Input::old('custom1');
                        }
                        elseif ( empty(Input::old('custom1')) && !empty(Input::old('save-customer')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Custom1) ) {
                              echo $customer->Custom1;
                        }
                        ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastName" class="col-sm-2 control-label">{{$custom2}}</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="custom2" placeholder="{{$custom2}}" type="text" name="custom2" value="<?php
                        if ( !empty(Input::old('custom2')) ) {
                              echo Input::old('custom2');
                        }
                        elseif ( empty(Input::old('custom2')) && !empty(Input::old('save-customer')) ) {
                              echo "";
                        }
                        elseif ( !empty($customer->Custom2) ) {
                              echo $customer->Custom2;
                        }
                        ?>">                
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('current_url', 'Status', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        <?php
                        $selected = false;
                        if ( isset($customer->Status) ) {
                              $selected = $customer->Status;
                        }
                        elseif ( Input::get('status') ) {
                              $selected = Input::get('status');
                        }
                        ?>
                        {{ Form::select('status', ['1' => 'Active', '0' => 'Inactive'], $selected, ["class" => "form-control"]) }}
                        <span class="error-display">{{$errors->first('cm_mode')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" name="action" value="add-customer-form">
                        <input type="submit" name="save-customer" value="Save">
                    </div>
                </div>    
            </form>
        </div>
    </div>
</div>
@stop