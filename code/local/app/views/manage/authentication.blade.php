@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') )
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings/authentication', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}

                <div class="form-group">
                    {{ Form::label('api-key', "API Key", array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('api-key', $api_key, array('placeholder' => "Please enter service api key", 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('api-key')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('serviceProviderID', 'Service Provider ID', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('serviceProviderID', $serviceProviderID, array('placeholder' => 'Please enter service provider id', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('serviceProviderID')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('name' => 'api-key-settings')) }}

                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop