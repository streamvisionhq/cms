@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Update EPG</h3>
        </div>
        <div class="panel-body">            

            <div class="well">

                <?php if ( Input::get('action') == 'doUpdate' ) { ?>
                      @if ($updated == 1)
                      <p><strong>EPG Updated Successfully</strong></p>
                      @else
                      <p><strong>Update of EPG was Unsuccessful</strong></p>
                      @endif               
                <?php }
                else {
                      ?>
                      <p>
                          If EPG is already downloaded it will not affect and download EPG again.
                      </p>

                      <div class="col-sm-offset-2 col-sm-10">
                          <a href="{{ URL::to('settings/refresh_epg?action=doUpdate') }}">Click here if EPG failed to download</a>
                      </div>
                      <?php
                }
                ?>
            </div>


        </div>
    </div>
</div>
@stop