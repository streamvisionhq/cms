@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div id="livestream">
            <button class="btn btn-primary" onClick="location ='{{ URL::to('service/add/') }}'" />Add Service</button>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Manage Services</h3>
        </div>
        <div class="panel-body">		  
            <table class="table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>External Name</th>
                        <th>Internal Name</th>
                        <th>External Reference</th>
                        <th>Internal Reference</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                @if(sizeof($services) > 0)
                @foreach($services as $service)
                <tbody>
                    <tr>
                        <td>{{ $service->Title }}</td>
                        <td>{{ $service->ExternalName }}</td>
                        <td>{{ $service->InternalName }}</td>
                        <td>{{ $service->ExternalRef }}</td>
                        <td>{{ $service->InternalRef }}</td>
                        <td><a href="{{ URL::to('service/edit/'.$service->ID) }}">Edit</a></td>
                    </tr>
                </tbody>
                @endforeach
                @endif
            </table>
        </div>
    </div>
</div>
@stop