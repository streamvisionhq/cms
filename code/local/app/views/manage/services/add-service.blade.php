@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Add Service</h3>
        </div>
        <div class="panel-body">
            <div class="well">
                @if ( Session::has('flash_message') )
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>
                @endif
                {{ Form::open(array('url'=>'service/add/', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('title', 'Service Title', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('title', "", array('placeholder'=>'Service Title', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('title')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('internalName', 'Internal Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('internalName', "", array('placeholder'=>'Internal Name', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('internalName')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('externalName', 'External Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('externalName', "", array('placeholder'=>'External Name', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('externalName')}}</span>
                    </div>
                </div>						

                <div class="form-group">
                    {{ Form::label('internalRef', 'Internal Reference', array('class' => 'col-sm-2 control-label')) }}                    <div class="col-sm-10">
                        {{ Form::text('internalRef', "", array('placeholder'=>'Internal Reference', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('internalRef')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('externalRef', 'External Reference', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('externalRef', "", array('placeholder'=>'External Reference', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('externalRef')}}</span>
                    </div>
                </div>                                

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Save', array('name' => 'add-service')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop