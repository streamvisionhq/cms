@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') )
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings/cms', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    @if(!$cm_mode)
                    <p class="cus-notice">Notice! You will have to stick to the selected system & it cannot be changed after it has been made.</p>
                    @endif
                    {{ Form::label('current_url', 'Select System', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::select('cm_mode', ['' => 'Select System', 'cms' => 'CMS', 'emerald' => 'Emerald'], $cm_mode, $options) }}
                        <span class="error-display">{{$errors->first('cm_mode')}}</span>
                    </div>
                </div>

                <div class="form-group" style="display: none;">
                    {{ Form::label('customer-custom-field-1-label', DEFAULT_CUSTOM_FIELD_LABEL1, array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('customer-custom-field-1-label', $customer_custom_field_1_label, array('placeholder'=>DEFAULT_CUSTOM_FIELD_LABEL1, 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('customer-custom-field-1-label')}}</span>
                    </div>
                </div>

                <div class="form-group" style="display: none;">
                    {{ Form::label('customer-custom-field-2-label', DEFAULT_CUSTOM_FIELD_LABEL2, array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('customer-custom-field-2-label', $customer_custom_field_2_label, array('placeholder'=>DEFAULT_CUSTOM_FIELD_LABEL2, 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('customer-custom-field-2-label')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('name' => 'customer-management-settings')) }}

                    </div>
                </div>
                {{ Form::close() }}

            </div>
        </div>
    </div>
</div>
@stop