@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') ) 
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings/channel', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}

                <div class="form-group">
                    {{ Form::label('nimble_port', 'Nimble Port', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('nimble_port', $nimble_port, array('placeholder'=>'Nimble Port', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('nimble_port')}}</span>
                    </div>
                </div> 

                <div class="form-group">
                    {{ Form::label('current_url', 'Nimble Mode', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::select('nimble_mode', [
                            'live' => 'Live',
                            'backup' => 'Backup'], $nimble_mode, ['class' => 'form-control'] 
                         ) }}
                    </div>
                </div>

                <div id="live_cont">
                    <div class="nimble_heading"><h2>Live Nimble IPs @if ($nimble_mode == "live")<span class="nimble-mode">(Active)</span>@endif</h2></div>
                    <div class="form-group">
                        {{ Form::label('nimble_stream', 'Live Streaming Host', array('class' => 'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('nimble_stream', $nimble_stream, array('placeholder'=>'Streaming Nimble', 'class'=>'form-control')) }}
                            <span class="error-display">{{$errors->first('nimble_stream')}}</span>
                        </div>
                    </div>               

                    <div class="form-group">
                        {{ Form::label('nimble_dvr', 'Re-Streaming Nimble Host', array('class' => 'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('nimble_dvr', $nimble_dvr, array('placeholder'=>'DVR Nimble', 'class'=>'form-control')) }}
                            <span class="error-display">{{$errors->first('nimble_dvr')}}</span>
                        </div>
                    </div> 
                </div>

                <div id="backup_cont">
                    <div class="nimble_heading"><h2 class="nimble_heading">Backup Nimble IPs @if ($nimble_mode == "backup")<span class="nimble-mode">(Active)</span>@endif</h2></div>
                    <div class="form-group">
                        {{ Form::label('nimble_stream_backup', 'Live Streaming Host', array('class' => 'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('nimble_stream_backup', $nimble_stream_backup, array('placeholder'=>'Streaming Nimble Backup', 'class'=>'form-control')) }}
                            <span class="error-display">{{$errors->first('nimble_stream_backup')}}</span>
                        </div>
                    </div>                               

                    <div class="form-group">
                        {{ Form::label('nimble_dvr_backup', 'Re-Streaming Nimble Host', array('class' => 'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('nimble_dvr_backup', $nimble_dvr_backup, array('placeholder'=>'DVR Nimble Backup', 'class'=>'form-control')) }}
                            <span class="error-display">{{$errors->first('nimble_dvr_backup')}}</span>
                        </div>
                    </div>
                </div>                

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Save', array('name' => 'edit-channel-settings')) }}

                    </div>
                </div>
                {{ Form::close() }}



            </div>


        </div>
    </div>
</div>
@stop