@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') )
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings/radius', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('radius-server-ip', 'Radius Server IP', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('radius-server-ip', $radiusServerIP, array('placeholder'=>'Radius Server IP', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('radius-server-ip')}}</span>
                    </div>
                </div>               

                <div class="form-group">
                    {{ Form::label('radius-server-host', 'Radius Server Host', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('radius-server-host', $radiusServerHost, array('placeholder'=>'Radius Server Host', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('radius-server-host')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('radius-shared-secret', 'Radius Shared Secret', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('radius-shared-secret', $radiusSharedSecret, array('placeholder'=>'Radius Shared Secret', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('radius-shared-secret')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('name' => 'edit-radius-settings')) }}

                    </div>
                </div>
                {{ Form::close() }}



            </div>


        </div>
    </div>
</div>
@stop