@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') ) 
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings/dvr', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('dvr-endpoint', 'API Endpoint', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('dvr-endpoint', $endpoint, array('placeholder'=>'DVR API Endpoint', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('dvr-endpoint')}}</span>
                    </div>
                </div>               

                <div class="form-group">
                    {{ Form::label('dvr-grace-period', 'Grace Period (Days)', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('dvr-grace-period', $gracePeriod, array('placeholder'=>'DVR Grace Period (Days)', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('dvr-grace-period')}}</span>
                    </div>
                </div>                                                                                                    

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Save', array('name' => 'edit-dvr-settings')) }}

                    </div>
                </div>
                {{ Form::close() }}



            </div>


        </div>
    </div>
</div>
@stop