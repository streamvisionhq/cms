@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Edit Message</h3>
        </div>
        <div class="panel-body">		  

            <div class="well">
                @if ( Session::has('flash_message') ) 
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'message/edit/'.$message->ErrorCode, 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('errorCode', 'Error Code', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('errorCode', $message->ErrorCode, array('readonly' => 'readonly', 'placeholder'=>'Error Code', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('errorCode')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('message', 'Message', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('message', $message->Message, array('placeholder'=>'Message', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('message')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('description', $message->Description, array('placeholder'=>'Description', 'readonly' =>'readonly', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('description')}}</span>
                    </div>
                </div>						

                <div class="form-group">
                    {{ Form::label('type', 'Type', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::select('type', array('error' => 'Error', 'success' => 'Success'), $message->Type, array('class' => 'col-sm-2 control-label')) }}
                        <span class="error-display">{{$errors->first('type')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('name' => 'edit-message')) }}

                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop