@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Manage Messages</h3>
        </div>
        <div class="panel-body">		  
            <table class="table">
                <thead>
                    <tr>
                        <th>Error Code</th>
                        <th>Message</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                @if(sizeof($messages) > 0)
                @foreach($messages as $message)
                <tbody>
                    <tr>
                        <td>{{ $message->ErrorCode }}</td>
                        <td>{{ $message->Message }}</td>
                        <td>{{ $message->Description }}</td>
                        <td>{{ $message->Type }}</td>
                        <td><a href="{{ URL::to('message/edit/'.$message->ErrorCode) }}">Edit</a></td>
                    </tr>
                </tbody>
                @endforeach
                @endif
            </table>
        </div>
    </div>
</div>
@stop