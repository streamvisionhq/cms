<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Hi, {{ $first_name }} {{ $last_name }}!</h1>
        <p>Your account details modified successfully.</p>
        <p>&nbsp;</p>
        <p>Regards</p>
        <p>Pro Livestream</p>
    </body>
</html>