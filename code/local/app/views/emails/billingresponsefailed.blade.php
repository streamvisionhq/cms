<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <?php
        ?>
        <p><strong>Service Provider:</strong> <?php echo $data->service_provider_name; ?></p>
        <p><strong>Service Provider ID:</strong> <?php echo $data->service_provider_id; ?></p>
        <p><strong>Number of Services Failed:</strong> <?php echo $data->invalidCount; ?></p>
        <p><strong>Reason of Failure:</strong></br> <?php
            foreach ( $data->service_failed_count as $key => $failed_count ) {
                  ?>
                  <?php echo $failed_count; ?> service(s) failed for <?php echo $key; ?></br>
                  <?php
            }
            ?></p>
        <p><strong>Timestamp:</strong> <?php echo date("Y-m-d H:i:s"); ?></p>
        <?php
        ?>
    </body>
</html>