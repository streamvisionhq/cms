<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Hi, {{ $first_name }} {{ $last_name }}!</h1>
        <p>The account has been created successfully.</p>
        <p>Please find below your api keys.</p>
        <p>Application Key : {{ $consumer_key }}</p>
        <p>Application Secret : {{ $consumer_secret }}</p>
        <p>&nbsp;</p>
        <p>Regards</p>
        <p>Pro Livestream</p>
    </body>
</html>