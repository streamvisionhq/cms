<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Hi, {{ $firstname }} {{ $lastname }}!</h1>

        <p>The login credentials are as follows</p>
        <p>URL : <a href="{{ URL::to('/') }}">{{ URL::to('/') }}</a></p>
        <p>Username : {{ $username }}</p>
        <p>Password : {{ $password }}</p>
        <p>&nbsp;</p>
        <p>Regards</p>
        <p>Pro Livestream</p>
    </body>
</html>