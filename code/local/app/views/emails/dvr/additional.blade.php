<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Notification</h2>

        <div>
            Client <b>({{ $customerID }})</b> purchases Additional DVR Service (Account ID: {{ $accountID }}), but client doesn't have Basic DVR Service.
        </div>
    </body>
</html>
