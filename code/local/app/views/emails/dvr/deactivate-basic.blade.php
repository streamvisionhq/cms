<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Notification</h2>

        <div>
            Client's <b>({{ $customerID }})</b> Basic DVR Service (Account ID: {{ $accountID }}) is deactivated, while their Whole Home or Additional DVR Service is active.
        </div>
    </body>
</html>
