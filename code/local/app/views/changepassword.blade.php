@extends('layouts.master')

@section('content')
<div class="content_container">	    	
    <div class="panel panel-default">		  
        <div class="panel-heading"><h3 class="panel-title">Settings</h3></div>
        <div class="panel-body com_bdy">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') ) 
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'changepassword', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('oldpassword', 'Old Password', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::password('oldpassword', array('placeholder'=>'Please enter old password', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('oldpassword')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('password', 'Password', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::password('password', array('placeholder'=>'Please enter password', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('password')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('password_confirmation', 'Password Confirmation', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::password('password_confirmation', array('placeholder'=>'Please enter password confirmation', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('password_confirmation')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array()) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>				
        </div>
    </div>
</div>
@stop