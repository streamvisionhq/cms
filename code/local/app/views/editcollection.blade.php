@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>
        <div class="panel-body com_bdy">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">Edit Collection</a></li>
            </ul>
            <div class="well">
                <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
                <div class="alert alert-success" id="frm_success" style="display:none;"></div>
                {{ Form::open(array('url'=>'medialibrary/editcollection', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'id'=>'frmcollection')) }}
                {{ Form::hidden('collection_id', $collection_id) }}
                {{ Form::hidden('modified_by', Auth::id()) }}

                <div class="form-group">
                    {{ Form::label('collection_name', 'Collection Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('collection_name', $collection_name,  array('placeholder'=>'Please enter collection name', 'class'=>'form-control')) }}
                        <span class="error-display" id="error_collectionname"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array('id'=>'editcollection')) }}
                        <input type="button" name="cancel" id="cancel" onClick="call_medialibrary()" value="Cancel" />
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
      $(document).ready(function () {
          $("#editcollection").click(function () {

              showOverlay();

              $("#frmcollection").submit(function (event)
              {
                  var postData = $(this).serializeArray();
                  var formURL = $(this).attr("action");
                  var formData = new FormData(this);
                  $.ajax(
                          {
                              url: formURL,
                              type: 'POST',
                              data: formData,
                              mimeType: "multipart/form-data",
                              contentType: false,
                              cache: false,
                              processData: false,
                              success: function (data, textStatus, jqXHR)
                              {
                                  var response = JSON.parse(data);

                                  if (response['validation_failed'] == 1) {
                                      if (response['errors']['collection_name']) {
                                          $("#error_collectionname").html(response['errors']['collection_name']);
                                      }
                                  }

                                  if (response['success'] == 1) {
                                      $(".error-display").html('');
                                      $("#frm_success").html('Collection modified successfully.');
                                      $("#frm_success").show();
                                      $("input[type=submit]").attr('disabled', 'disabled');
                                      call_medialibrary().delay(3000);
                                  }

                                  hideOverlay();
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $("#frm_error").html('AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '');
                                  $("#frm_error").show();
                              }
                          });
                  event.preventDefault();	//STOP default action
                  event.unbind();
              });
          });
      });
</script>
@stop

