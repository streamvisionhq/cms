@extends('layouts.master')

@section('content')
<div class="content_container">
    <div class="panel panel-default">

    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="panel panel-default">	  
        <div class="panel-heading"><h3 class="panel-title">Sort Collections</h3></div>

        <div class="panel-body doc_c">
            <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
            <div class="alert alert-success" id="frm_success" style="display:none;"></div>
            {{ Form::open(array('url'=>'medialibrary/savecollectionorder', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal', 'files' => true, 'id'=>'frmsavecollection')) }}
            <ul id="sortables" class="ui-sortables" style="height:auto !important;">
                @foreach($collections as $collection)
                <li class="ui-state-default">
                    <input type="hidden" name="collection[]" value="{{$collection->collection_id}}" />
                    <span>

                        &nbsp;&nbsp;
                        {{$collection->collection_name}}
                    </span>
                </li>
                @endforeach


            </ul>
            <div style="width:90%; margin:0px auto;">

                <input type="submit" name="submit" value="Save Order" id="savecollectionorder" />
                <input type="button" name="cancel" id="cancel" onClick="call_medialibrary()" value="Back to collections" />
            </div>
            </form>	  
        </div>
    </div>
</div>

<script>
      $(function () {
          $("#sortables").sortable({
              placeholder: "ui-sortables-placeholder"
          });
      });

      $(document).ready(function () {
          $("#savecollectionorder").click(function () {

              showOverlay();
              $(".error-display").html("");
              $("#frmsavecollection").submit(function (event)
              {
                  var postData = $(this).serializeArray();
                  var formURL = $(this).attr("action");
                  var formData = new FormData(this);
                  $.ajax(
                          {
                              url: formURL,
                              type: 'POST',
                              data: formData,
                              mimeType: "multipart/form-data",
                              contentType: false,
                              cache: false,
                              processData: false,
                              success: function (data, textStatus, jqXHR)
                              {
                                  var response = JSON.parse(data);

                                  if (response['success'] == 1) {
                                      $(".error-display").html('');
                                      $("#frm_success").html('Collection Order saved successfully.');
                                      $("#frm_success").show();

                                      call_listcollection().delay(3000);
                                  }

                                  hideOverlay();
                              },
                              error: function (jqXHR, textStatus, errorThrown)
                              {
                                  $("#frm_error").html('AJAX Request Failed<br/> textStatus=' + textStatus + ', errorThrown=' + errorThrown + '');
                                  $("#frm_error").show();
                              }
                          });
                  event.preventDefault();	//STOP default action
                  event.unbind();
              });
          });
      });

      var call_listcollection = function () {
          location.reload();
      }
</script>

@stop