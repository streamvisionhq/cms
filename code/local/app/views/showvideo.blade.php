@extends('layouts.master')

@section('css')
{{ HTML::style('assets/videojs/video-js.css') }}
@stop

@section('js')
{{HTML::script('assets/videojs/video.js')}}
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Media Library</h3>
        </div>

        <div class="panel-body">
            <ul class="nav nav-tabs"  id="tognav">
                <li role="presentation" class="active"style="background:#1e1e1e;"><a href="javascript:void(0);">{{ $name }} </a></li>
            </ul>
            <div class="well">
                <!-- add video html -->

                <div class="video-gallery"  align="left">



                    <div class="media_details">
                        <video id="example_video_1" class="video-js vjs-default-skin" controls preload="none" width="900" height="400" poster="{{ URL::to(getCdnPath().$media_image) }}" data-setup="{}">
                            <source src="{{ URL::to(getCdnPath().$media_name) }}" type='video/mp4' />
                            <p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
                        </video>
                        <h3>Details {{ HTML::image('assets/images/ar.png', '',  array( ) ) }}</h3>
                        <table border="0" cellpadding="0" cellspacing="0" class="tblVideodetails">
                            <tr>
                                <td class="vdLft">Meta Tag</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $meta_tag }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Meta Description</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $meta_description }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Descriptions</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $descriptions }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Frame Size</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $frame_width }} X {{ $frame_height }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">File Size</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $file_size }} bytes</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Content</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $content }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Genre</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $genre }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Rating</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $rating }} / 5</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Rights</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $rights }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Release Date</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $release_date }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Show Description</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $show_description }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Closed Captions</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $closed_captions }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Generic Tags</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $generic_tags }}</td>
                            </tr>

                            <tr>
                                <td class="vdLft">Similar Content</td>
                                <td class="vdMdl"> : </td>
                                <td class="vdRgt">{{ $similar_content }}</td>
                            </tr>
                        </table>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<div id="dialog-status" title="Videos" style="display:none;">
    <p></p>
</div>	
@stop

