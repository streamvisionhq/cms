@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">CMS Update</h3>
        </div>
        <div class="panel-body">		  			
            <div class="well">
                <?php if ( Input::get('action') == 'doUpdate' ) { ?>
                      @if ($updated == 1)
                      <p><strong>Updated Successfully</strong></p>
                      @else
                      <p><strong>Update of CMS was Unsuccessful</strong></p>
                      @endif
                <?php }
                else {
                      ?>
                      @if ($update == 0)
                      <p><strong>No update found, CMS up-to-date</strong></p>
                      <p><strong>Current Version:</strong> {{ $currentVersion }}</p>
                      @else
                      <p><strong>Current Version:</strong> {{ $currentVersion }}</p>
                      <p><strong>Update To:</strong> {{ $updateVersion }}</p>

                      <p>
                          While updating your all customer service will be stopped until your CMS updated successfully, Are you sure you want to update your CMS.
                      </p>
                      <div class="col-sm-offset-2 col-sm-10">
                          <a href="{{ URL::to('update?action=doUpdate') }}">Continue to update</a>
                      </div>
                      @endif
<?php } ?>                      
            </div>


        </div>
    </div>
</div>
@stop