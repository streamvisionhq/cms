@foreach($videos as $video)
<li class="ui-state-default ui-draggable ui-draggable-handle">{{ HTML::image(getCdnPath().$video->media_image, '',  array( 'width' => 50) ) }}<h3>{{$video->name}}</h3><span class="sr-{{$video->media_id}}"></span></li>
@endforeach

@if(sizeof($videos) > 0)
<p class="load_more" id="load_more"><a href="javascript:void(0);" onClick="call_showcollection('{{$collection_id}}', '{{$page}}', 2)">load more</a></p>
@endif