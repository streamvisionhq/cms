@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">		  
        <div class="panel-heading">
            <h3 class="panel-title">Settings</h3>
        </div>
        <div class="panel-body">
            @include('includes.settingsnav')

            <div class="well">
                @if ( Session::has('flash_message') ) 
                <div class="alert {{ Session::get('flash_type') }}">
                    <h3>{{ Session::get('flash_message') }}</h3>
                </div>  
                @endif
                {{ Form::open(array('url'=>'settings', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-horizontal')) }}
                <div class="form-group">
                    {{ Form::label('firstname', 'First Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('firstname', $firstname, array('placeholder'=>'Please enter first name', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('firstname')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('lastname', 'Last Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('lastname', $lastname, array('placeholder'=>'Please enter last name', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('lastname')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('email', $email, array('placeholder'=>'Please enter email', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('email')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('username', 'Username', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('username', $username, array('placeholder'=>'Please enter username', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('username')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('rootPath', 'Root Path', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('rootPath', $rootPath, array('placeholder'=>'Please enter Root Path', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('rootPath')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('rootURL', 'Root URL', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('rootURL', $rootURL, array('placeholder'=>'Please enter Root URL', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('rootURL')}}</span>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('cmsName', 'CMS Name', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::text('cmsName', $cmsName, array('placeholder'=>'Please enter CMS Name', 'class'=>'form-control')) }}
                        <span class="error-display">{{$errors->first('cmsName')}}</span>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        {{ Form::submit('Submit', array()) }}

                    </div>
                </div>
                {{ Form::close() }}



            </div>


        </div>
    </div>
</div>
@stop