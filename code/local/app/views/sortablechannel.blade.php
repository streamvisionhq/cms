@extends('layouts.master')

@section('js')
<script type="text/javascript">
      $ = jQuery.noConflict(true);
</script>
{{HTML::script('http://code.jquery.com/jquery-1.3.2.min.js')}}
{{HTML::script('https://code.jquery.com/ui/1.7.2/jquery-ui.min.js')}}
{{HTML::script('assets/js/ui.nestedSortable.js')}}

<script type="text/javascript">
      jq132 = jQuery.noConflict(true);
</script>
@stop

@section('content')
<div class="content_container">
    <div class="panel panel-default">

    </div>
    <br clear="all" />
    <br clear="all" />
    <div class="panel panel-default">	  
        <div class="panel-heading"><h3 class="panel-title">Sort Channel</h3></div>

        <div class="panel-body doc_c">
            <div class="alert alert-danger" id="frm_error" style="display:none;"></div>
            <div class="alert alert-success" id="frm_success" style="display:none;"></div>


            <ul id="demo2" style="height:auto !important;">
                @foreach($channels as $channel)
                <?php
                switch ( $channel->channel_type ) {
                      case 1:
                            $img_src = 'assets/images/live_stream.png';
                            $class_li = 'ls';
                            break;

                      case 2:
                            $img_src = 'assets/images/vod.png';
                            $class_li = 'vod';
                            break;

                      default:
                            $img_src = 'assets/images/category.png';
                            $class_li = 'ct';
                            break;
                }
                ?>
                <li id="item_{{$channel->channel_id}}" class="ui-state-default <?php echo $class_li; ?>">
                    {{ HTML::image($img_src, '', array()) }}
                    {{$channel->title}}					
                    <?php
                    display_channel_childnodes($channel->channel_id, 0, $subchannels, $index);
                    ?>
                </li>
                @endforeach
            </ul>

            <div style="width:90%; margin:20px auto; float:left;">
                <input type="submit" name="serialize" id="serialize" value="Save Channel Order" />
                <input type="button" name="cancel" id="cancel" onClick="call_channelmanager('25', 'sort_order', 'asc', '1', '1')" value="Back to Channel Manager" />
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
      jq132(document).ready(function () {
          var data = '';

          jq132('#demo2').sortable({
              'cursor': 'move',
              'items': 'li',
              'placeholder': 'ui-sortables-placeholder',
              'nested': 'ul',
              'update': function (event, ui) {

                  switch (ui.item.attr("class")) {
                      case 'ui-state-default ls':
                          if (ui.item.parent().attr("class") != 'ui-sortable') {
                              jq132(this).sortable('cancel');
                          }
                          break;

                      case 'ct':
                      case 'ui-state-default ct':
                          if (ui.item.parent().parent().attr("class") == 'ui-state-default ls' || ui.item.parent().parent().attr("class") == 'ui-state-default vod' || ui.item.parent().parent().attr("class") == 'vod') {
                              jq132(this).sortable('cancel');
                          }
                          break;

                      case 'vod':
                      case 'ui-state-default vod':
                          if (ui.item.parent().parent().attr("class") == 'ui-state-default ls' || ui.item.parent().parent().attr("class") == 'vod' || ui.item.parent().parent().attr("class") == 'ui-state-default vod') {
                              jq132(this).sortable('cancel');
                          }
                          break;
                  }
              },
          })

          jq132('#serialize').click(function (event) {
              showOverlay();
              data = jq132('#demo2').sortable('serialize');

              var url = "{{ URL::to('channelmanager/sortchannel') }}";

              jq132.ajax({
                  type: "POST",
                  url: url,
                  data: data,
                  success: function (response)
                  {
                      var response = JSON.parse(response)

                      if (response['success']) {
                          hideOverlay();
                          jq132(".error-display").html('');
                          jq132("#frm_success").html('Channel Order saved successfully.');
                          jq132("#frm_success").show();
                          call_channelmanager('25', 'sort_order', 'asc', '1', '1').delay(3000);
                      }
                  }
              })
          })
      });
</script>
@stop

<?php

function display_channel_childnodes($parent_id, $level, $subchannel, $index) {
      if ( isset($index[$parent_id]) ) {
            $lvl = $level + 1;
            echo '<ul style="height:auto !important" class="cat">';
            foreach ( $index[$parent_id] as $id ) {
                  switch ( $subchannel[$id]['channel_type'] ) {
                        case 1:
                              $img_src = 'assets/images/live_stream.png';
                              $class_li = 'ls';
                              break;

                        case 2:
                              $img_src = 'assets/images/vod.png';
                              $class_li = 'vod';
                              break;

                        default:
                              $img_src = 'assets/images/category.png';
                              $class_li = 'ct';
                              break;
                  }
                  echo '<li id="item_' . $subchannel[$id]['channel_id'] . '" class="' . $class_li . '">';
                  echo HTML::image($img_src, '', array());
                  echo $subchannel[$id]['title'];
                  display_channel_childnodes($id, $level + 1, $subchannel, $index);
                  echo '</li>';
            }
            echo '</ul>';
      }
}
?>