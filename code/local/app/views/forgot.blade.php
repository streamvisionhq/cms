<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>STREAMVISION TV Beta - Forgot Password</title>
        {{ HTML::style('assets/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/login-style.css') }}
    </head>
    <body>
        <section class="container">
            <h1>STREAMVISION <span class="tv">TV Beta</span></h1>
            {{ Form::open(array('url'=>'forgot', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-signin')) }}
            @if ( Session::has('flash_message') )

            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>

            @endif
            <ul>
                <li>
                    {{ Form::label('Forgot Password', 'Forgot Password') }}
                </li>
                <li>
                    {{ Form::text('email', Input::old('email'), array('placeholder'=>'Please enter your email address')) }}
                    <span class="error-display">{{$errors->first('email')}}</span>
                </li>
                <li>
                    {{ Form::submit('Submit!') }}
                </li>
                <li>
                    <a href="{{ URL::to('login') }}">Back to login</a>
                </li>
            </ul>
            {{ Form::close() }}
        </section>
    </body>
</html>