@extends('layouts.master')

@section('content')
<div class="content_container">
    <br clear="all" />
    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Roku Devices Filter</h3>
        </div>
        <div class="well">
            <form class="form-horizontal" method="get">
                <div class="form-group">
                    <label for="deviceID" class="col-sm-2 control-label">Device ID</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="deviceID" placeholder="Device ID" type="text" name="deviceID" value="<?php echo Input::get('deviceID'); ?>">                
                    </div>
                </div>
                <div class="form-group">
                    <label for="customerID" class="col-sm-2 control-label">Customer ID</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="customerID" placeholder="Customer ID" type="text" name="customerID" value="<?php echo Input::get('customerID'); ?>">                
                    </div>
                </div>
                <div class="form-group">
                    <label for="identifier" class="col-sm-2 control-label">Identifier</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="identifier" placeholder="Identifier" type="text" name="identifier" value="<?php echo Input::get('identifier'); ?>">                
                    </div>
                </div>            
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" name="action" value="filter">
                        <input type="submit" name="deviceSearch" value="Search">
                    </div>
                </div>    
            </form>
        </div>        

        <div class="panel-heading">
            <h3 class="panel-title">
                Roku Devices    
            </h3>
        </div>
        <div class="panel-body detail_cnt">
            <div class="table_wrap">
                <div class="tr_title">
                    <div class="col colu_6">Device ID</div>
                    <div class="col colu_6">Customer ID</div>
                    <div class="col colu_6">Identifier</div>
                    <div class="col colu_6">Default Device</div>
                    <div class="col colu_6">Status</div>
                    <div class="col colu_6">Date</div>
                    <div class="col colu_6">Action</div>
                </div>
                @if(sizeof($devices) > 0)
                @foreach($devices as $device)
                <div class="dd device_{{ $device->DeviceID }} customer_{{ $device->CustomerID }}" id="nestable">
                    <ul class="table_sec ui-sortable" style="height:auto !important">
                        <li id="item_1">
                            <div class="first_comn">                                        
                                <div class="col colu_6">{{ $device->DeviceID }}</div>
                                <div class="col colu_6">{{ $device->CustomerID }}</div>
                                <div class="col colu_6">{{ $device->Identifier }}</div> 
                                <div class="col colu_6 default">
                                    @if($device->Default == 1)
                                    Default
                                    @endif
                                </div>
                                <div class="col colu_6 status">
                                    @if($device->Status == 1)
                                    Active
                                    @else
                                    Inactive
                                    @endif
                                </div>
                                <div class="col colu_6">{{ date('F d, Y', strtotime($device->Date)) }}</div>
                                <div class="col colu_6">
                                    <a data-original-title="Delete" alt="Delete Roku Device" href="javascript:void(0);" onclick="deleteRokuDevice('{{ $device->DeviceID }}', '{{ $device->DeviceID }}');">{{ HTML::image('assets/images/delete.png', '',  array( 'title' => 'Delete Device', 'width' => 14, 'height' => 16) ) }}</a>                                    
                                    @if($device->Default == 0)
                                    <a class="set_default_button" data-original-title="Set as Default" alt="Set as default" href="javascript:void(0);" onclick="markDefault('{{ $device->DeviceID }}', '{{ $device->CustomerID }}');">{{ HTML::image('assets/images/set_as_default.png', '',  array( 'title' => 'Set as Default', 'width' => 14, 'height' => 16) ) }}</a>
                                    @if($device->Status == 1)
                                    <a class="status-link" data-original-title="Inactive" alt="Inactive Roku Device" href="javascript:void(0);" onclick="statusRokuDevice('{{ $device->DeviceID }}', 0);">{{ HTML::image('assets/images/active.png', '',  array( 'class' => 'status-icon', 'title' => 'Inactive Device', 'width' => 14, 'height' => 16) ) }}</a>
                                    @else
                                    <a class="status-link" data-original-title="Activate" alt="Activate Roku Device" href="javascript:void(0);" onclick="statusRokuDevice('{{ $device->DeviceID }}', 1);">{{ HTML::image('assets/images/inactive.png', '',  array( 'class' => 'status-icon', 'title' => 'Activate Device', 'width' => 14, 'height' => 16) ) }}</a>
                                    @endif                                    
                                    @endif
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endforeach
                <nav>
                    <div class="perpage-select">Show &nbsp;
                        <select class="selct_box" id="select_perpage" style="font-family:'open_sansregular'; font-size:12px;">
                            <option value="" >Select</option>
                            <option value="{{ URL::to('roku-devices?per_page=25') }}"  @if ($perPage == 25) {{ 'selected="selected"' }} @endif >25</option>
                            <option value="{{ URL::to('roku-devices?per_page=50') }}" @if ($perPage == 50) {{ 'selected="selected"' }} @endif >50</option>
                            <option value="{{ URL::to('roku-devices?per_page=100') }}" @if ($perPage == 100) {{ 'selected="selected"' }} @endif >100</option>
                        </select>
                        &nbsp;
                        Entries
                    </div>
                    <?php echo $devices->appends(array('per_page' => $perPage))->links(); ?>
                </nav>                
                @else
                <p style="text-align: center;">No Device Found</p>
                @endif                
            </div>
        </div>
    </div>
</div>
<div id="dialog-message" title="" style="display:none;">
    <p></p>
</div>
@stop