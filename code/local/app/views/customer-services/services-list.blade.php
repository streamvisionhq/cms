@extends('layouts.master')

@section('content')
<div class="content_container">  		
    <div class="panel panel-default">
        <div class="well">
            @if ( Session::has('flash_message') )
            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>  
            @endif
            <form class="form-horizontal" method="get">
                <div class="form-group">
                    {{ Form::label('Add Service', 'Add Service', array('class' => 'col-sm-2 control-label')) }}
                    <div class="col-sm-10">
                        {{ Form::select('service', [''=>'Select Service'] + $selectServices, '', ["class" => "form-control"]) }}
                        <span class="error-display">{{$errors->first('service')}}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" name="action" value="filter">
                        <input type="submit" name="assign-service" value="Assign">
                    </div>
                </div>    
            </form>
        </div>
        <div class="panel-heading">
            <h3 class="panel-title">Assigned Services</h3>
        </div>
        <div class="panel-body">		  
            <table class="table">
                <thead>
                    <tr>
                        <th>Service ID</th>
                        <th>Service Name</th>
                        <th>Service Module</th>
                        <th>Status</th>
                        <th>Active/Inactive/Remove</th>
                    </tr>
                </thead>
                @if(sizeof($customerServices) > 0)
                @foreach($customerServices as $cservice)
                <tbody>
                    <tr data-id="{{ $cservice->ServiceID }}">
                        <td>{{ $cservice->ServiceID }}</td>
                        <td>{{ $cservice->ServiceName }}</td>
                        <td>{{ $cservice->ServiceModule }}</td>
                        <td>{{ $cservice->Active }}</td>
                        <td>
                            <div class="five">
                                <?php
                                $active = false;
                                if ( $cservice->Active == "0" ) {
                                      $active = " button-active";
                                }
                                ?>
                                <div class="button-wrap<?php echo $active; ?>">
                                    <div class="button-bg">
                                        <div class="button-out"><span class="glyphicon glyphicon-remove"></span></div>
                                        <div class="button-in"><span class="glyphicon glyphicon-ok"></span></div>
                                        <div class="button-switch"></div>
                                    </div>
                                </div>
                            </div>
                            <a href="javascript:void();" class="unassign-service">Remove</a>
                        </td>
                    </tr>
                </tbody>
                @endforeach
                @endif
            </table>
        </div>
    </div>
</div>
@stop