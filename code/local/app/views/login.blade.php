<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>STREAMVISION TV Beta - Admin Login</title>
        {{ HTML::style('assets/css/bootstrap.min.css') }}
        {{ HTML::style('assets/css/login-style.css') }}

        {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js')}}
    </head>
    <body>
        <section class="container">
            <h1>STREAMVISION <span class="tv">TV Beta</span></h1>
            {{ Form::open(array('url'=>'login', 'method'=>'POST', 'accept-charset'=>'UTF-8', 'class'=>'form-signin')) }}
            @if ( Session::has('flash_message') )

            <div class="alert {{ Session::get('flash_type') }}">
                <h3>{{ Session::get('flash_message') }}</h3>
            </div>

            @endif
            <ul>
                <li>
                    {{ Form::label('LOGIN', 'LOGIN') }}
                </li>
                <li>
                    {{ Form::text('username', Input::old('username'), array('onfocus'=>'call_fcplaceholder(\'username\')', 'onblur'=>'call_blplaceholder(\'username\')', 'id' => 'username')) }}
                    <br/>
                    <span class="error-display">{{$errors->first('username')}}</span>
                </li>
                <li>
                    {{ Form::password('password', array('onfocus'=>'call_fcplaceholder(\'password\')', 'onblur'=>'call_blplaceholder(\'password\')', 'id' => 'password')) }}
                    <br/>
                    <span class="error-display">{{$errors->first('password')}}</span>
                </li>
                <li>
                    {{ Form::submit('Login') }}
                </li>
                <li>
                    <a href="{{ URL::to('forgot') }}">Forgot Password</a>
                </li>
            </ul>
            {{ Form::close() }}
        </section>
    </body>
</html>

<script>
      if ($("#username").val() == '') {
          $("#username").val('Username');
      }
      if ($("#password").val() == '') {
          $("#password").val('Password');
      }

      function call_fcplaceholder(x) {
          if (x == 'username') {
              if ($("input[type='text'][name='username']").val() == 'Username') {
                  $("#username").val('');
              }
          }

          if (x == 'password') {
              if ($("input[type='password'][name='password']").val() == 'Password') {
                  $("#password").val('');
              }
          }
      }

      function call_blplaceholder(x) {
          if (x == 'username') {
              if ($("input[type='text'][name='username']").val() == '') {
                  $("#username").val('Username');
              }
          }

          if (x == 'password') {
              if ($("input[type='password'][name='password']").val() == '') {
                  $("#password").val('Password');
              }
          }
      }
</script>