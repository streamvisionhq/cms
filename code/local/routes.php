<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', function()
{  	
	 return Redirect::to('login'); 
});

Route::get('login', array('uses' => 'CommonController@showLogin'));
Route::post('login', array('uses' => 'CommonController@doLogin'));
Route::get('logout', array('uses' => 'CommonController@doLogout'));
Route::get('forgot', array('uses' => 'CommonController@showForgot'));
Route::post('forgot', array('uses' => 'CommonController@doForgot'));
Route::get('dashboard', array('before' => 'auth', 'uses' => 'HomeController@getIndex'));
Route::get('settings', array('before' => 'auth', 'uses' => 'UserController@showProfile'));
Route::post('settings', array('before' => 'auth', 'uses' => 'UserController@saveProfile'));
Route::get('changepassword', array('before' => 'auth', 'uses' => 'UserController@showChangePassword'));
Route::post('changepassword', array('before' => 'auth', 'uses' => 'UserController@savePassword'));

Route::get('channelmanager', array('before' => 'auth', 'uses' => 'ChannelManagerController@showChannel'));

Route::get('channelmanager/addlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@addLiveStream'));

Route::post('channelmanager/addlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@doAddLiveStream'));

Route::get('channelmanager/editlivestream}', array('before' => 'auth', 'uses' => 'ChannelManagerController@modifyLiveStream'));

Route::get('channelmanager/editlivestream/{segments}', 'ChannelManagerController@modifyLiveStream')->where('segments', '(.*)');

Route::post('channelmanager/editlivestream', array('before' => 'auth', 'uses' => 'ChannelManagerController@doModifyLiveStream'));

Route::get('channelmanager/addvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@addvideoondemand'));

Route::post('channelmanager/addvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@doAddVideoOnDemand'));

Route::get('channelmanager/editvideoondemand/{segments}', 'ChannelManagerController@modifyVideoOnDemand')->where('segments', '(.*)');

Route::post('channelmanager/editvideoondemand', array('before' => 'auth', 'uses' => 'ChannelManagerController@doModifyVideoOnDemand'));

Route::get('channelmanager/removechannel/{segments}', 'ChannelManagerController@removeChannel')->where('segments', '(.*)');

Route::get('channelmanager/getvideos/{segments}', 'ChannelManagerController@showCollection')->where('segments', '(.*)');

Route::get('channelmanager/makeactive/{segments}', 'ChannelManagerController@makeActive')->where('segments', '(.*)');

Route::get('channelmanager/makeinactive/{segments}', 'ChannelManagerController@makeInactive')->where('segments', '(.*)');

Route::get('medialibrary', array('before' => 'auth', 'uses' => 'MediaLibraryController@showMediaLibrary'));

Route::get('medialibrary/addcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@addCollection'));

Route::post('medialibrary/addcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@doAddCollection'));

Route::get('medialibrary/editcollection/{segments}', 'MediaLibraryController@modifyCollection')->where('segments', '(.*)');

Route::post('medialibrary/editcollection', array('before' => 'auth', 'uses' => 'MediaLibraryController@doModifyCollection'));

Route::get('medialibrary/makecollectionactive/{segments}', 'MediaLibraryController@makeCollectionActive')->where('segments', '(.*)');

Route::get('medialibrary/makecollectioninactive/{segments}', 'MediaLibraryController@makeCollectionInactive')->where('segments', '(.*)');

Route::get('medialibrary/removecollection/{segments}', 'MediaLibraryController@removeCollection')->where('segments', '(.*)');

Route::get('medialibrary/addvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@addVideo'));

Route::post('medialibrary/addvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@doAddVideo'));

Route::get('medialibrary/editvideo/{segments}', 'MediaLibraryController@editVideo')->where('segments', '(.*)');

Route::post('medialibrary/editvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@doEditVideo'));

Route::get('medialibrary/listvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@getVideo'));

Route::get('medialibrary/processjob/{segments}', 'MediaLibraryController@processTranscodeJob')->where('segments', '(.*)');

Route::get('medialibrary/removevideo/{segments}', 'MediaLibraryController@removeVideo')->where('segments', '(.*)');

Route::get('medialibrary/makemediaactive/{segments}', 'MediaLibraryController@makeMediaActive')->where('segments', '(.*)');

Route::get('medialibrary/makemediainactive/{segments}', 'MediaLibraryController@makeMediaInactive')->where('segments', '(.*)');

Route::get('medialibrary/getvideocollections/{segments}', 'MediaLibraryController@mediaByCollection')->where('segments', '(.*)');

Route::get('medialibrary/video/{segments}', 'MediaLibraryController@showMedia')->where('segments', '(.*)');

Route::get('medialibrary/addmediatocollection/{segments}', 'MediaLibraryController@addMediaToCollection')->where('segments', '(.*)');

Route::get('medialibrary/removemediafromcollection/{segments}', 'MediaLibraryController@removeMediaFromCollection')->where('segments', '(.*)');

Route::get('medialibrary/searchvideo', array('before' => 'auth', 'uses' => 'MediaLibraryController@getSearchVideo'));

Route::group(array('prefix' => 'api/v1/'), function()
{
	Route::any('client/signup', 'ApiClient@signUp');
	Route::any('client/account', 'ApiClient@myAccount');
	Route::any('client/update/{segments}', 'ApiClient@modifyClient')->where('segments', '(.*)');
	Route::any('client/status/update/{segments}', 'ApiClient@modifyClientStatus')->where('segments', '(.*)');
	Route::any('client/regenerateapikey/{segments}', 'ApiClient@regenerateClientApi')->where('segments', '(.*)');
	Route::any('client/destroy/{segments}', 'ApiClient@removeClient')->where('segments', '(.*)');
	
	Route::any('livestreams/list', 'ApiChannel@listLiveStreams');
	Route::any('livestreams/show/{segments}', 'ApiChannel@showLiveStream')->where('segments', '(.*)');
	Route::any('videosondemand/list', 'ApiChannel@listVideosOnDemand');
	Route::any('videosondemand/show/{segments}', 'ApiChannel@showVideosOnDemand')->where('segments', '(.*)');
	Route::any('playlists/list/{segments}', 'ApiChannel@playLists')->where('segments', '(.*)');
	
	Route::any('videos/list', 'ApiCollection@listVideos');
});

Route::group(array('prefix' => 'api/'), function()
{
	Route::any('registerapi', 'ApiProliveController@register');
	
});

Route::filter('auth', function()
{
    if(is_null(Auth::User())) {
		return Redirect::to('login'); 
	} 
});