<?php
/*
 * This script is handling the epg & spliting it into pages
 */

$data = file_get_contents("GetSchedules.xml");
$xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);

$array = array();

$count_channel = 0;

$pages = 2;
$interval = ceil(24 / $pages);
$step = 60 * 60 * $interval;

$range = hoursRange($lower = 0, $upper = 86400, $step);

foreach ( $xml->Channel as $data ) {
    $channel = (array) $data;

    if ( isset($channel['Show']) ) {

        foreach ( (array) $channel['Show'] as $key => $show_data ) {

            $show = (array) $show_data;

            $show_date = date("Y-m-d", strtotime($show['@attributes']['adjustedstartTime']));
            $show_time = date("H:i", strtotime($show['@attributes']['adjustedstartTime']));

            if ( strtotime($show_time) >= strtotime($range[0]) && strtotime($show_time) < strtotime($range[1]) ) {
                $array[$show_date]['1']['Channel'][$channel['@attributes']['channelId']]["@attributes"] = $channel['@attributes'];
                $array[$show_date]['1']['Channel'][$channel['@attributes']['channelId']]["Show"][] = $show;
            }
            elseif ( strtotime($show_time) > strtotime($range[1]) ) {
                $array[$show_date]['2']['Channel'][$channel['@attributes']['channelId']]["@attributes"] = $channel['@attributes'];
                $array[$show_date]['2']['Channel'][$channel['@attributes']['channelId']]["Show"][] = $show;
            }
        }
    }

    $count_channel++;
}

foreach ( $array as $date => $channels ) {
    foreach ( $channels as $interval => $value ) {
        $filename = 'Schedule-' . $date . '-' . $interval . '.json';
        if ( !file_exists($filename) ) {
            $fp = fopen($filename, 'w');
            fwrite($fp, json_encode($value));
            fclose($fp);
            update_last_modified_date($filename);
        }
    }
}

/**
 * Will update the last modified date of given file
 * @param string $epg_file
 */
function update_last_modified_date($epg_file) {

    $file = 'epg_pages_last_modified.txt';

    if ( !file_exists($file) ) {
        $fh = fopen($file, 'w');
    }

    $dates = file_get_contents($file);
    $dates = json_decode($dates, true);

    $content = [];

    if ( !empty($dates) ) {

        $dates[$epg_file] = date("Y-m-d H:i:s");

        $fp = fopen($file, 'w');
        fwrite($fp, json_encode($dates));
        fclose($fp);

        $today = date('Y-m-d');
        $tomorrow = date('Y-m-d', strtotime("+1 day"));
        $after_tomorrow = date('Y-m-d', strtotime("+2 day"));

        foreach ( $dates as $key => $value ) {
            if ( strpos($key, $today) !== false ) {
                $content[$key] = $value;
            }

            if ( strpos($key, $tomorrow) !== false ) {
                $content[$key] = $value;
            }

            if ( strpos($key, $after_tomorrow) !== false ) {
                $content[$key] = $value;
            }
        }
    }
    else {
        $content[$epg_file] = date("Y-m-d H:i:s");
    }

    $fp = fopen($file, 'w');
    fwrite($fp, json_encode($content));
    fclose($fp);
}

/**
 * Will return the ranges of time like 12:00, 01:00 but in form of array
 * @param int $lower
 * @param int $upper
 * @param int $step
 * @param string $format
 * @return array ranges
 */
function hoursRange($lower = 0, $upper = 86400, $step = 3600) {
    $times = array();
    foreach ( range($lower, $upper, $step) as $increment ) {
        $increment = gmdate('H:i', $increment);
        if ( !in_array($increment, $times) ) {
            $times[] = (string) $increment;
        }
    }

    return $times;
}
