<?php
/**
 * Author: Santhosh Reddy
 * Description: Simple script to copy ftp file.
 */
// Log file.
$logfile = "titanftp.log";
$log     = "------------------Starting Script-----------------\r\n";


// Derive file name. Format: 'Default-2015-06-19.gz';
$today = date("Y-m-d");
$file  = 'Default-'.$today.'.gz';
//$file = 'Default-2015-08-13.gz';

$msg = "Filename: $file\r\n";
echo $msg;
$log .= $msg;

// Setup FTP server credentials.
$ftp_server    = 'ftp.data.titantv.com';
$ftp_user_name = 'AireBeam';
$ftp_user_pass = 'mwjOIzF2P';

// Setup basic FTP connection.
$conn_id = ftp_connect($ftp_server);

// Login with username and password
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// store yeserdays date in a file, so we only overwrite YesterdaysSchedules.xml when
// the date changes, as the cron for this can run multiple times a day
$yesterDateFile   = 'yesterdate.txt';
$yesterDateInFile = file_get_contents($yesterDateFile);
$yesterday        = date('Y-m-d', time() - 86400);

mail('zeeshan@codup.io', 'EPG Start', 'EPG Start');

// upload a file
if (ftp_get($conn_id, $file, $file, FTP_ASCII)) {
    $msg = "Successfully downloaded $file\r\n";
    echo $msg;
    $log .= $msg;

    if (file_exists($file)) {
        $out_file_name = 'GetSchedules-Temp.xml';

        echo 'Checking if'.$yesterDateInFile.' == '.$yesterday;
        if ($yesterDateInFile != $yesterday) {
            // #5600: copy yesterdays file, so we can access full daysdata, for users who 
            // request EPG data for the first time in a different timezone
            $yesterdayFile = 'YesterdaySchedules.xml';
            copy($out_file_name, $yesterdayFile);
            file_put_contents($yesterDateFile, $yesterday);
        }

        // Raising this value may increase performance
        $buffer_size = 4096 * 4096; // read 16kb at a time
        // Open our files (in binary mode)
        $gz_file  = gzopen($file, 'rb');
        $out_file = fopen($out_file_name, 'w');

        // Keep repeating until the end of the input file
        while (!gzeof($gz_file)) {
            // Read buffer-size bytes
            // Both fwrite and gzread and binary-safe
            fwrite($out_file, gzread($gz_file, $buffer_size));
        }

        // Files are done, close files
        fclose($out_file);
        gzclose($gz_file);

        rename('GetSchedules.xml', 'GetSchedules-Delete.xml');
        rename('GetSchedules-Temp.xml', 'GetSchedules.xml');
        unlink('GetSchedules-Delete.xml');
    }
} else {
    $headers = "From: StreamVision <notifications@streamvisiontv.com>";
    $message = "There was a problem while downloading EPG from titan.";
    mail('gaf@bluerivernet.com,asim.bawany@gmail.com,zeeshan@codup.io,rob@airebeam.net,brandon@airebeam.net,steve@streamvisiontv.com,jerry@streamvisiontv.com', 'SV Notification: EPG Download Failed', $message, $headers);
    
    $msg     = json_encode(error_get_last());
    $msg .= "\r\nThere was a problem while downloading $file\r\n";
    echo $msg;
    $log .= $msg;
}

$log .= "------------------Ending Script-------------------\r\n\r\n";

file_put_contents($logfile, $log, FILE_APPEND);

// close the connection
ftp_close($conn_id);
?>
