<?php

if (file_exists("epg_flag.txt")) {
    $epg_flag_fp = fopen('epg_flag.txt', 'w');
    fwrite($epg_flag_fp, 0);
    fclose($epg_flag_fp);
}

$headers = "From: StreamVision <notifications@streamvisiontv.com>";
$to = 'gaf@bluerivernet.com,asim.bawany@gmail.com,zeeshan@codup.io,rob@airebeam.net,brandon@airebeam.net,steve@streamvisiontv.com,jerry@streamvisiontv.com,waqar@codup.io,adam@bawanymedia.com,adam@codup.io';
//$source = 'http://data.titantvguide.titantv.com/service.asmx/GetSchedules?registrationKey=A47zs5vhy0KbMWqQQ%2b5CnQ%3d%3d';

$ftp_server    = 'ftps-data.titantv.com';
$ftp_user_name = 'AireBeam';
$ftp_user_pass = 'W0YPW9UMtBoRBpteFkz8';

$destination = 'GetSchedules-Dump.xml';
$destination_original = 'GetSchedules.xml';
$max_attempts = 3; // These are the attempts on the Titans server if data not found, before generating error email to the admin.
$epg_flag = 0;

$dir = dirname(__DIR__);
include($dir . '/vendor/apache/log4php/src/main/php/Logger.php');
$path = $dir . '/app/config/config.xml';

Logger::configure($path);
//Logger::configure('apache\config.xml');
$logger = Logger::getLogger("EPG Cron Job");
$logger->info("EPG Guide Cron job start.");

// store yeserdays date in a file, so we only overwrite YesterdaysSchedules.xml when
// the date changes, as the cron for this can run multiple times a day
$yesterDateFile = 'yesterdate.txt';
$yesterDateInFile = file_get_contents($yesterDateFile);
$yesterday = date('Y-m-d', time() - 86400);

try {
    if ($yesterDateInFile != $yesterday) {
        // #5600: copy yesterdays file, so we can access full days data, for users who 
        // request EPG data for the first time in a different timezone
        $yesterdayFile = 'YesterdaySchedules.xml';
        copy($destination_original, $yesterdayFile);
        
        $yesterday_fp = fopen($yesterDateFile, 'w');
        fwrite($yesterday_fp, $yesterday);
        fclose($yesterday_fp);        
    }

    $current_attempt = 1;
    
    do {
        
        $conn_id = ftp_ssl_connect($ftp_server);
        if(!ftp_login($conn_id, $ftp_user_name, $ftp_user_pass)){
            $logger->error("Login failed, unable to login through ftp username : " . $ftp_user_name);
        }else{
            ftp_pasv($conn_id, true);
        }
        
        if(ftp_get($conn_id, $destination, "New-Channels-".date("Y-m-d").".xml", FTP_ASCII)) {
            ftp_close($conn_id);
            $data = file_get_contents($destination);
            $max_attempts = 0;
            
            if ($data !== false) {
                $xml = simplexml_load_string($data);
                if ($xml !== false) {
                    $xml_array = (array) $xml[0];
                    if ($xml_array['@attributes']['error'] == 'OK') {
                        $epg_flag = 1;
                        break;
                    } else {
                        $logger->error("EPG Guide Cron job has error in XML File on Attempt #" . $current_attempt);
                    }
                } else {
                    $logger->error("EPG Guide Cron job could not be parsed on Attempt #" . $current_attempt);
                }
            }
            
        }else{
            $max_attempts--;
            $logger->error("EPG Guide Cron job could not be downloaded from titan server on Attempt #" . $current_attempt);
        }
        
        $current_attempt++;
        
    } while ($max_attempts > 0);
    
    if ($epg_flag > 0) {
        //$handle = fopen($destination, "w");
        //fwrite($handle, $data);
        //fclose($handle);

        rename('GetSchedules.xml', 'GetSchedules-Delete.xml');
        rename('GetSchedules-Dump.xml', 'GetSchedules.xml');
        unlink('GetSchedules-Delete.xml');

        if (file_exists("epg_flag.txt")) {
            $epg_flag_fp = fopen('epg_flag.txt', 'w');
            fwrite($epg_flag_fp, 1);
            fclose($epg_flag_fp);
        }

        $data = file_get_contents("GetSchedules.xml");
        $xml = simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);

        $array = array();

        $count_channel = 0;
        foreach ($xml->Channel as $data) {
            $channel = (array) $data;

            if (isset($channel['Show'])) {

                foreach ((array) $channel['Show'] as $key => $show_data) {

                    $show = (array) $show_data;
                    $show_date = date("Y-m-d", strtotime($show['@attributes']['adjustedstartTime']));

                    $array[$show_date]['Channel'][$channel['@attributes']['channelId']]["@attributes"] = $channel['@attributes'];

                    $array[$show_date]['Channel'][$channel['@attributes']['channelId']]["Show"][] = $show;
                }
            }

            $count_channel++;
        }

        foreach ($array as $date => $channels) {
            $filename = 'Schedule-' . $date . '.json';
            $fp = fopen($filename, 'w');
            fwrite($fp, json_encode($channels));
            fclose($fp);
        }                
        
        $logger->info("EPG Guide Cron job successfully downloaded.");
        
        // includes the page vise spliting script for epg
        include_once 'epg_pages.php';
        
    } else {
        $subject = "SV Notification: EPG Guide Download Error";
        $txt = "EPG Failed\n\nCMS:" . get_setting("root-url", $path) . "\n\nCMS Name:" . get_setting("cms-name", $path) . "\n\nDate & Time:" . date("Y-m-d H:i:s");

        $logger->error("EPG Guide Cron job download error.");
        if (@mail($to, $subject, $txt, $headers)) {
            $logger->info("EPG Guide Cron job download error email send.");
        } else {
            $logger->error("EPG Guide Cron job download error email not send.");
        }
    }
    
    $logger->info("EPG Guide Cron job ends.");
    return true;
} catch (Exception $e) {
    $logger->info("EPG Guide Cron job exceptional.");
    $subject = "SV Notification:EPG Guide Download Exception";
    $txt = "EPG Failed\n\nCMS URL:" . get_setting("root-url", $path) . "\n\nCMS Name:" . get_setting("cms-name", $path) . "\n\nDate & Time:" . date("Y-m-d H:i:s") . "\n\nException Error: " . $e->getMessage();

    if (@mail($to, $subject, $txt, $headers)) {
        $logger->info("EPG Guide Cron job exceptional email send.");
    } else {
        $logger->error("EPG Guide Cron job exceptional email not send.");
    }
}

function get_setting($key, $path){
    $config_file = fopen($path, "r");
    $xmlString = fread($config_file, filesize($path));
    fclose($config_file);

    $xml = new SimpleXMLElement($xmlString);
    $xml = (array)$xml->appender;
    $xml = (array)$xml['param'];

    //Get Database Name
    $host_array = (array)$xml[0];
    $host_array = explode(";", $host_array['@attributes']['value']);
    $host_array = explode("=", $host_array[1]);
    $database_name = $host_array[1];

    //Get Username
    $username_array = (array)$xml[1];
    $username = $username_array['@attributes']['value'];

    //Get Password
    $password_array = (array)$xml[2];
    $password = $password_array['@attributes']['value'];

    $conn = mysqli_connect("localhost", $username, $password, $database_name);

    $sql = "SELECT `value` FROM settings WHERE `key` = '{$key}'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            return $row["value"];
        }
    }else{
        return false;
    }
}
?>
